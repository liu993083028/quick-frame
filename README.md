quick-frame

#### 项目介绍
快速开发的框架

#### 软件架构
软件架构说明


#### 安装教程

1、gradlew -v 查看当前项目所用的Gradle版本
2、gradlew clean 清除9GAG/app目录下的build文件夹
3、gradlew build 编译项目并生成相应的apk文件
4、gradlew assembleDebug 编译并打Debug包
5、gradlew assembleRelease 编译并打Release的包
6、gradlew installRelease Release 模式打包并安装
7、gradlew uninstallRelease 卸载Release模式包

使用jenkins打包流程
1.登录http://192.168.20.222:7000网页
2.![第一步](./doc/image/jenkins打包第一步.png)

![](./doc/image/第二步.png)

![](./doc/image/第三步.png)

![](./doc/image/下载对应的apk.png)

