
def saveObject(Object object,String path){

    try {
        def desFile = new File(path)
        if(!desFile.exists()){
            desFile.createNewFile();
        }
        desFile.withObjectOutputStream{
            out ->
                out.writeObject(object)
        }
        return  true;
    } catch (Exception e) {
    }
    retrun false;
}

def readObject(String path){
    def obj = null;
    try{
        def file = new File(path)
        if(file == null || !file.exists()){
            return null
        }
        file.withObjectInputStram{
            input ->
                obj = input.readObject()
        }
    }catch (Exception e){

    }
    return obj
}