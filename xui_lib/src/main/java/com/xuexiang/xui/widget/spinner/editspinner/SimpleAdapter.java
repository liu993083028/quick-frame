package com.xuexiang.xui.widget.spinner.editspinner;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.text.Html;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.ColorInt;
import androidx.annotation.DrawableRes;

import com.xuexiang.xui.R;

import java.util.ArrayList;
import java.util.List;

/**
 * SimpleAdapter
 *
 * @author WrBug
 * @since 2017/2/25
 */
public class SimpleAdapter<T> extends BaseEditSpinnerAdapter implements EditSpinnerFilter {

    private Context mContext;
    private String mKeyword;
    private List<T> mSpinnerData;
    private List<T> mCacheData;
    private int[] indexs;
    private int textColor;
    private float textSize;
    private int backgroundSelector;
    private boolean mIsFilterKey;

    public SimpleAdapter(Context context, List<T> spinnerData) {
        this.mContext = context;
        this.mSpinnerData = spinnerData;
        mCacheData = new ArrayList<>(this.mSpinnerData);
//        for(T t :spinnerData){
//            if(t instanceof String){
//                List<String> list = (List<String>)spinnerData;
//                mCacheData = new ArrayList<>(list);
//                break;
//            }
//            mCacheData.add(t);
//        }

        indexs = new int[mSpinnerData.size()];
    }

    @Override
    public EditSpinnerFilter getEditSpinnerFilter() {
        return this;
    }



    @Override
    public List<T> getData() {
        return mSpinnerData;
    }

    public SimpleAdapter<T> setIsFilterKey(boolean isFilterKey) {
        mIsFilterKey = isFilterKey;
        return this;
    }
    @Override
    public int getCount() {
        return mCacheData == null ? 0 : mCacheData.size();
    }

    @Override
    public String getItemString(int position) {
        if(mCacheData.size()>position){
            return mCacheData.get(position).toString();
        }else{
            return  mSpinnerData.get(position).toString();
        }

    }

    public T getItem(int position) {
        if(mCacheData.size()>position){
            return mCacheData.get(position);
        }

        return null;
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final TextView textView;
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.ms_list_item, parent,false);
            textView = (TextView) convertView;
            if(textColor!=0){
                textView.setTextColor(textColor);
            }
            if(textSize!=0){
                textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            }
            if (backgroundSelector != 0) {
                textView.setBackgroundResource(backgroundSelector);
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                Configuration config = mContext.getResources().getConfiguration();
                if (config.getLayoutDirection() == View.LAYOUT_DIRECTION_RTL) {
                    textView.setTextDirection(View.TEXT_DIRECTION_RTL);
                }
            }
            convertView.setTag(textView);

        } else {
            textView = (TextView)convertView.getTag();
        }
        if(mKeyword != null){
            textView.setText(Html.fromHtml(getItemString(position).replaceFirst(mKeyword, "<font color=\"#aa0000\">" + mKeyword + "</font>")));
        }else{
            textView.setText(getItemString(position));
        }

        return textView;
    }

    @Override
    public boolean onFilter(String keyword) {

        if (TextUtils.isEmpty(keyword)) {
            if(mCacheData.size() == mSpinnerData.size()){
                return false;
            }
            mCacheData.clear();
            mKeyword = null;
            mCacheData.addAll(mSpinnerData);

            for (int i = 0; i < indexs.length; i++) {
                indexs[i] = i;
            }
        } else {

            StringBuilder builder = new StringBuilder();
            builder.append("[^\\s]*").append(keyword).append("[^\\s]*");
            int j = 0;
            for (int i = 0; i < mSpinnerData.size(); i++) {
                if (mSpinnerData.get(i).toString().replaceAll("\\s+", "|").matches(builder.toString())) {
                    if(j == 0){
                        mCacheData.clear();
                        if (mIsFilterKey) {
                            mKeyword = keyword;
                        }
                    }
                    j++;
                    indexs[mCacheData.size()] = i;
                    mCacheData.add((mSpinnerData.get(i)));

                }

            }
        }
        notifyDataSetChanged();
        return mCacheData.size() <= 0;
    }

    @Override
    public void showFilterKey(String keyword) {
        this.mKeyword = keyword;
        if (mCacheData.size() != mSpinnerData.size()) {
            mCacheData.clear();
            mCacheData.addAll(mSpinnerData);
        }
        notifyDataSetChanged();

    }


    public SimpleAdapter<T> setTextColor(@ColorInt int textColor) {
        this.textColor = textColor;
        return this;
    }

    public SimpleAdapter<T> setTextSize(float textSize) {
        this.textSize = textSize;
        return this;
    }

    public SimpleAdapter<T> setBackgroundSelector(@DrawableRes int backgroundSelector) {
        this.backgroundSelector = backgroundSelector;
        return this;
    }


}
