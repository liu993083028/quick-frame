package com.chcit.mobile;

import android.os.Bundle;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.textfield.TextInputLayout;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import android.view.Gravity;
import android.widget.Button;
import android.widget.TextView;

import com.chcit.custom.view.dialog.DialogPlus;
import com.chcit.custom.view.dialog.ViewHolder;
import com.chcit.custom.view.edit.SuperInputEditText;
import com.chcit.custom.view.toolbar.CommonToolbar;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TestActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    CommonToolbar toolbar;
    @BindView(R.id.tiet_dispensing_no)
    SuperInputEditText tietNo;
    @BindView(R.id.til_dispensing_no)
    TextInputLayout tilNo;
    @BindView(R.id.tiet_dispensing_realLocator)
    SuperInputEditText tietRealLocator;
    @BindView(R.id.til_dispensing_realLocator)
    TextInputLayout tilRealLocator;
    @BindView(R.id.tiet_dispensing_pointLocator)
    SuperInputEditText tietPointLocator;
    @BindView(R.id.til_dispensing_pointLocator)
    TextInputLayout tilPointLocator;
    @BindView(R.id.app_bar_layout)
    AppBarLayout appBarLayout;
    @BindView(R.id.nsv)
    NestedScrollView nsv;
    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        ButterKnife.bind(this);
        initToobar();
        initDialog();

    }

    private void initToobar() {

       /* toolbar.addLeftIcon(1,R.drawable.ic_return,v->{
            ArmsUtils.snackbarTextWithLong("left on image");
        });*/
        //toolbar.<AppCompatImageView>getViewByTag(1).setImageResource(R.drawable.ic_return);
        toolbar.addBackIcon(R.drawable.ic_return);
        //toolbar.<AppCompatImageView>getLeftMenuView(0).setImageResource(R.drawable.svg_right_icon);
        toolbar.setTitleText(getTitle(),15,getResources().getColor(R.color.white));

    }

    private void initDialog() {
        DialogPlus dialog = DialogPlus.newDialog(this)
                .setContentHolder(new ViewHolder(R.layout.dialog_dispensing))
                //.setExpanded(true)  // This will enable the expand feature, (similar to android L share dialog)
                .setGravity(Gravity.BOTTOM | Gravity.END)
                .setCancelable(false)
                .create();

        Button ok = dialog.findViewById(R.id.dialog_dispensing_ok);
        TextView tv = dialog.findViewById(R.id.tv_dialog_dispensing_detail);
        Button canel = dialog.findViewById(R.id.dialog_dispensing_cancel);
        ok.setOnClickListener(v -> {
            dialog.dismiss();
        });
        canel.setOnClickListener(v -> {
            dialog.dismiss();
        });

        dialog.show();
    }
}
