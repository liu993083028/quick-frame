package com.chcit.mobile.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathDashPathEffect;
import android.graphics.PathMeasure;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import com.xuexiang.xui.utils.DensityUtils;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class Dashboard extends View {
    private static final int ANGLE = 120;
    private static final float RADIUS = DensityUtils.dp2px(150);
    private static final float LENGTH =  DensityUtils.dp2px(100);
    Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
    Path dash = new Path();
    private final PathDashPathEffect effect;
    public Dashboard(Context context,@Nullable AttributeSet attrs){
        super(context,attrs);

    }


    {
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(DensityUtils.dp2px(2));
        dash.addRect(0,0,DensityUtils.dp2px(2),DensityUtils.dp2px(10),Path.Direction.CW);

        Path arc = new Path();
        arc.addArc(getWidth()/2 -RADIUS,getHeight()/2-RADIUS,getWidth()/2+RADIUS,
                getHeight()/2+RADIUS,90+ ANGLE/2,360-ANGLE);
        PathMeasure pathMeasure = new PathMeasure(arc,false);

        effect = new PathDashPathEffect(dash,   (pathMeasure.getLength()-DensityUtils.dp2px(2))/20,0,PathDashPathEffect.Style.ROTATE);

    }
    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
    }



    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawArc(getWidth()/2 -RADIUS,getHeight()/2-RADIUS,getWidth()/2+RADIUS,
                getHeight()/2+RADIUS,90+ ANGLE/2,360-ANGLE,false,paint);
        //画刻度
        paint.setPathEffect(effect);
        //画表盘
        canvas.drawArc(getWidth()/2 -RADIUS,getHeight()/2-RADIUS,getWidth()/2+RADIUS,
                getHeight()/2+RADIUS,90+ ANGLE/2,360-ANGLE,false,paint);
        paint.setPathEffect(null);
        //画指针
        canvas.drawLine(getWidth()/2,
                getHeight()/2,
                (float) Math.cos(Math.toRadians(getAngleFromMark(5))) * LENGTH + (float)getWidth() / 2 ,
                (float) Math.sin(Math.toRadians(getAngleFromMark(5))) * LENGTH + (float)getHeight() / 2 ,
                paint);
    }

    int getAngleFromMark(int mark){
        return (int)(90 + (float)ANGLE/2 + (360 - (float)ANGLE )/20 * mark);
    }
}
