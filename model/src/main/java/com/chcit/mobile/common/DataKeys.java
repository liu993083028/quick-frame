package com.chcit.mobile.common;

public enum DataKeys {
   USERNAME,
   WAREHOUSE_ID,
   WAREHOUSE_LIST,
   HOME_ITEM,
   LOG_LEVEL,
   ASN_AUTO_MODE,
   ATTACHMENT_COUNT // 冷链图片数量
}