package com.chcit.mobile.common;

import android.os.Build;

import com.chcit.mobile.BuildConfig;
import com.chcit.mobile.app.utils.SharedPreUtils;
import com.jess.arms.base.app.ConfigKeys;
import com.jess.arms.base.app.Quick;

public interface HttpMethodContains {
    String REQUEST_QUERY_URL = "mobileHandleAction/request.do";
    String LOGIN_QUERY_URL ="mobileHandleAction/login.do";
    String STORAGE_URL = "storageAction/query.do";
    String MAINTAIN_UEL = "conservationAction/queryConservation.do";//养护计划查询
    String BPARTNER_URL = "baseHandleAction/vendor.do";
    String STATUS_URL = "baseHandleAction/refList.do";
    String MOVEN_URL = BuildConfig.DOWN_URL;
    String MAINTAIN_OK_URL ="conservationAction/fill.do";
    String INVENTORY_PLAN_QUERY ="inventoryPlanAction/query.do?page=process";
    String INVENTORY_PLAN_QUERY_DETAIL ="inventoryPlanAction/queryLine.do";
    String INVENTORY_PLAN_CHCKED = "inventoryPlanAction/inventoryIdentical.do";
    String INVENTORY_PLAN_UPDATE ="inventoryPlanAction/saveInventoryplanLine.do";
    String INVENTORY_PLAN_OK = "inventoryPlanAction/inputInventoryQtyCount.do";
    String INVENTORY_PLAN_PACKAGE_OK = "inventoryPlanAction/inventoryPackage.do";
    String WAREHOUSE_QUERY = "getWarehouses";
    String WAREHOUSE_SET_OK = "setWarehouse";
    String PURCHASE_RECEVIE_QUERY_DETAIL = "queryAsnDetail";//采购入库
    String RECEVIE_BATCH_OK ="checkAsnBatchRecieve";
    String CHECK_RECEIVE_OK = "checkAsnRecieve";//验收
    String REJECT_REASON = "getLookup";//拒绝原因
    String LOCATORS = "getLocators";
    String SHIPMENT_OK = "pickListConfirm";//拣货确认
    String SHIPMENT_QUERY = "queryPickList";
    String SHIPMENT_QUERY_DETAIL = "queryPickListDetail";//获取到货验证单
    String MOVE_STORAGE_QUERY = "queryStorageDetail";//移库查询
    String MOVE_OK ="move";
    String REJECT_OK ="rejectAsnReceive";//拒收入库确认
    String MOVEMENT_PLAN_QUERY_DETAIL = "movementPlanAction/queryDetail.do";//移库指示明细查询
    String MOVEMENT_PLAN_CONFIRM = "movementPlanAction/confirm.do";//非包装移库确认
    String MOVEMENT_PLAN_PACKAGE_CONFIRM = "movementPlanAction/confirm.do";//包装移库确认

    String DOWN_URL = "android/yp-spd.json";
    String IMAGE_URL = "system/images/icon-blue/";//图片路径
    String SHIPMENT_CODE_QUERY = "getOutCode";
    String SHIPMENT_CODE_CREATE = "createOutCode";
    String SHIPMENT_CODE_DELETE = "deleteOutCode";
    String RECEIVE_CODE_QUERY = "getInCode";
    String RECEIVE_CODE_CREATE = "createInCode";
    String RECEIVE_CODE_DELETE = "deleteInCode";
    String DISPENSING_QUERY ="getPrescriptionInfo";
    String DOCUTOR_DISPENSING_QUERY ="getWardApplyInfo";
    String DISPENSING_OK ="checkPrescriptionInfo";
    String DOCUTOR_DISPENSING_OK ="checkWardApplyInfo";
    String RECEIVE_PACKAGE_QUERY = "queryAsnPackageDetail";//查询包装信息
    String SHIPMENT_PACKAGE_QUERY = "queryCanPickPackages";//查询可选包装信息
    String RECEIVE_REJECT_OK = "rejectAsnPackage";
    String REASON_URL ="asnAction/rejectReason.do";
    String SHIPMENT_PACKAGE_CHECKED = "verifyPickListPackage";
    String SHIPMENT_PACKAGE_OK = "pickPackage";
    String SHIPMENT_REJECT_OK = "pickListJobCancel";
    String PACKAGE_UNIT = "packageAction/queryUnitPackQty.do";//包装定数查询
    String PACKAGE_INFO = "packageAction/query.do";
    String USER_URL = "warehouseAction/warehouseUserList.do";
    String USER_CHECK = "userBaseHandleAction/checkPassword.do";
    String USER_CHECK_CODE = "userBaseHandleAction/queryByUserCode.do";//检测员工号码
    String SHIPMENT_PACKAGE_URL = "inoutAction/shipmentPackage.do";
    String PACKAGE_MERGE = "packageAction/merge.do";//并包
    String  PACKAGE_SPLIT="packageAction/splitOdd.do";//拆零
    String PACKAGE_SUB="packageAction/splitUnit.do";//分包

    String LOCATOR_URL = "warehouseAction/locatorList.do";
    String MOVE_PACKAGE = "movementAction/movePackage.do";
    //验收
    String CHECK_PACKAGE ="asnAction/checkPackage.do";
    //拒收
    String REJECT_PACKAGE ="asnAction/rejectPackage.do";
    //开箱验视
    String INSPECTE_PACKAGE ="asnAction/inspectePackage.do";
    //上架
    String PICKUP_PACKAGELINE ="asnAction/putawayPackage.do";
    //上架退回
    String PICKBACK_PACKAGE ="asnAction/returnPackage.do";
    //非包装验收
    String PRECHECK_PACKAGE ="asnAction/checkLine.do";
    //非包装验收
    String CHECK_ASN ="asnAction/checkLine.do";
    //非包装上架
    String PICKUP_ASN ="asnAction/putawayLine.do";

    //包装拣货确认
    String PACKAGEPICKCOMFIRM_SHIPMENT ="pickListAction/pickPackage.do";
    String LOGIN_OUT = "logout";
    String RECEVIE_QUERY = "queryAsn";//到货通知单
    //String GET_REJECT_REASON="getLookup";//参数refId
    //String CHECK_BATCH_OK = "checkAsnBatchRecieve";
    String REJECT_LINE = "rejectAsLine";//按行拒收或者批量拒收
    String VERIFY_RECEIVE = "verifyAsnReceive";//校验
    String QUERY_REJECT_WAREHOUSE = "rejectAsnWarehouse";//查询拒收仓库
    String MOVE = "move";//移库
    //String QUERY_PICKLIST = "queryPickLIST";//查询拣货一览
    String QUIERY_PICKLIST_DETAIL = "queryPickListDetail";//
    String CANCEL_PICKLIST = "pickListJobCancel";//按行取消拣货
    String CANCEL_PICKLIST_ALL = "pickListJobCancelAll";//按单取消拣货
    String CONFIRM_PICKLIST_OK = "pickListConfirm";//拣货确认
    String TASK_URL = "portalAction/queryWaitDoc.do";
    String REJECT_WARE_QUERY="rejectAsnWarehouse";//查询拒收仓库
    String PRECRIPTION_QUERY = "getPrescriptionInfo";//处方发药查询
    String PRECRIPTION_OK ="checkPrescriptionInfo";//处方拣货确认
    String BPARTNER_QUERY ="queryAsnBpartner";
    String RECEIVE_PACKAGE_OK = "checkAsnPackageRecieve";
    String INVENTORY_PACKAGE_PLAN_QUERY = "queryInventoryPlanDetail";
    String QUERY_LOT_URL = "storageAction/queryStorageLot.do";
    String HIGHT_COST_BIND = "borrowAction/createBorrowOrderByPackage.do";
    interface Common{
        String VENDOR_URL = "asnAction/vendor.do";
    }
    interface Home{
        String MENU_QUERY ="getMenu";
        String Task_URL = "portalAction/queryWaitDoc.do";

    }
    interface Receive{
        // asnAction/queryDetail.do?asnType=PO&page=receive
        String QUERY_DETAIL_URL = "asnAction/queryDetail.do";
        String CHECK_ASN_LINE = "asnAction/checkAsnline.do"; // 采购上架验收
        String REJECT_OK = "asnAction/rejectReceivePackage.do";
        String ASN_LINE_TASK= "asnAction/queryDetail.do"; //查询单号信息
        String ASN_QUERY = "asnAction/query.do";
        String ASN_PACKAGE_LIST = "asnAction/queryPackageDetail.do"; //到货通知单包装信息
        String ASN_TASK_LINE_RE="asnAction/reCheckPackage.do";//撤销验收
        String UPLOAD_IMG = "asnAction/saveAsnPicture.do";
        String QUERY_IMAGE_ID = "asnAction/queryAsnPicture.do";

        String VIEW_ASN_PICTURE = "asnAction/viewAsnPicture.do";//预览图片

        String DELETE_ASN_PICTURE = "asnAction/deleteAsnPicture.do"; //删除图片
    }

    interface Shipment{
        // pickListAction/queryPickListJob.do?page=pick
        String QUERY_DETAIL_URL = "pickListAction/queryPickListJob.do?page=pick";
        //非包装拣货确认
        String CONFIRM_LIST ="pickListAction/pickListConfirm.do"; //批量收货

        String PICKCONFIRM_LINE = "pickListAction/pickConfirm.do";


    }

    interface Inventory{
        // pickListAction/queryPickListJob.do?page=pick
        String QUERY_URL = "packageAction/queryPackageCount.do";
        String PACKAGE_HISTORY_INFO = "packageAction/queryLog.do";
    }

    interface Hight{
        String HIGHT_COST_CREATE = "borrowAction/createBorrowOrderByPackage.do";
        String HIGHT_COST_RECEIVE = "borrowAction/brrowUseByPackage.do";
        String HIGHT_COST_REJECT = "borrowAction/borrowReturnByPackage.do";
        String HIGHT_QUERY_BIND = "rFIDAction/queryBind.do";
        String HIGHT_BIND_OK = "rFIDAction/bindPackage.do";
        String HIGHT_UN_BIND = "rFIDAction/unBindPackage.do";
        String HIGHT_COST_QUEERY = "baseHandleAction/invokeEngin.do"; //领出查询
    }

    interface Transship{
        String TRANSSHIP_CONFORM = "asnAction/doASNReg.do";
    }
}
