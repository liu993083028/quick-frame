package com.chcit.mobile.helper.recycler;

import androidx.annotation.Nullable;

import android.util.SparseBooleanArray;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

public abstract class BaseSelectQuickAdapter<T, K extends BaseViewHolder> extends BaseQuickAdapter<T, K> {
    protected  final SparseBooleanArray mSelectedPositions = new SparseBooleanArray();
    protected TextView mTitleBarView;
    protected final HashSet<Integer> selectedItem = new HashSet<>();

    public BaseSelectQuickAdapter(int layoutResId, @Nullable List<T> data) {
        super(layoutResId, data);
        initSelects();
    }

    public BaseSelectQuickAdapter(@Nullable List<T> data) {
        super(data);
        initSelects();
    }

    public BaseSelectQuickAdapter(int layoutResId) {
        super(layoutResId);
        initSelects();
    }

    public void setTitleBarView(TextView titleBarView) {
        mTitleBarView = titleBarView;
    }

    public TextView getTitleBarView() {
        return mTitleBarView;
    }

    /**
     * 用来为Adapter 里的数据item设置标记，默认每个条目为false，选中的话就设置为true
     * @param position
     * @param isChecked
     */
    protected synchronized void setItemChecked(int position, boolean isChecked) {
        mSelectedPositions.put(position, isChecked);
    }

    /**
     * 通过条目位置得到该条目的Boolean值，就可以知道条目有没有选中这时可以在onbindview里设置chekbox的状态了，同时设置chekbox的监听
     * @param position
     * @return
     */
    protected boolean isItemChecked(int position) {
        return mSelectedPositions.get(position);
    }
    //获得选中条目的结果
    public ArrayList<T> getSelectedItem() {
        ArrayList<T> selectList = new ArrayList<>();
        selectList.clear();
        selectedItem.clear();
        for (int i = 0; i < mData.size(); i++) {
            if (isItemChecked(i)) {
                selectedItem.add(i);
                selectList.add(mData.get(i));
            }
        }
        return selectList;
    }

    @Deprecated
    public ArrayList<Integer> getSelectedItemPosition(){
        ArrayList<Integer> list =new  ArrayList<Integer>();
        list.addAll(selectedItem);
        Collections.sort(list);
        return list;
    }
    public void clearSelectItem(){
        selectedItem.clear();
    }
    public void initSelects() {
        mSelectedPositions.clear();
        for (int i = 0; i < mData.size(); i++) {
            setItemChecked(i,false);
        }
    }

   /* @Override
    public void addData(@NonNull T data) {
        if(mTitleBarView!=null){
            mTitleBarView.setText("");
        }
        super.addData(data);
        initSelects();

    }*/

   /* @Override
    public void addData(@NonNull Collection<? extends T> newData) {
        if(mTitleBarView!=null){
            mTitleBarView.setText("");
        }
        super.addData(newData);
        initSelects();
    }
*/
    public void checkAll() {

        for(int i =0;i<mSelectedPositions.size();i++){
            setItemChecked(i,true);

        }
        notifyDataSetChanged();
        if (mTitleBarView != null) {
            String msg = "已选择" + mSelectedPositions.size()+ "/" + mData.size();
            mTitleBarView.setText(msg);
        }
    }

    public void cancelAll() {

        for(int i =0;i<mSelectedPositions.size();i++){
            setItemChecked(i,false);
        }
        if (mTitleBarView != null) {
            String msg = "已选择" + "0/" +mData.size();
            mTitleBarView.setText(msg);
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        int size = super.getItemCount();
        if(mData.size() != mSelectedPositions.size()){
            if( mData.size()>mSelectedPositions.size()){
                for(int i =mSelectedPositions.size();i<mData.size();i++){
                    setItemChecked(i,false);
                }
            }else if(mSelectedPositions.size()>mData.size()){
                initSelects();
            }
            if(mTitleBarView!=null){
                mTitleBarView.setText("总共"+mData.size()+"条记录");
            }
        }

        return size;
    }

    @Override
    public void remove(int position) {
        super.remove(position);
        mSelectedPositions.delete((position));
    }
}
