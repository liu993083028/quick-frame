package com.chcit.mobile.helper.recycler;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

public abstract class BaseItemChangeQuickAdapter<T, K extends BaseViewHolder> extends BaseQuickAdapter<T, K> {
    //先声明一个int成员变量
    protected int thisPosition = -1;
    //再定义一个int类型的返回值方法
    public int getChekedPosition() {
        return thisPosition;
    }
    //其次定义一个方法用来绑定当前参数值的方法
    //此方法是在调用此适配器的地方调用的，此适配器内不会被调用到
    public void setCheckedPosition(int thisPosition) {
        int i = this.thisPosition;
        this.thisPosition = thisPosition;
        if(i>-1&& i!= thisPosition){
            notifyItemChanged(i);
        }
        notifyItemChanged(thisPosition);
    }

    public BaseItemChangeQuickAdapter(int layoutResId, @Nullable List<T> data) {
        super(layoutResId, data);

    }

    public BaseItemChangeQuickAdapter(@Nullable List<T> data) {
        super(data);

    }

    public BaseItemChangeQuickAdapter(int layoutResId) {
        super(layoutResId);

    }

}
