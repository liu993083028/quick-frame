package com.chcit.mobile.helper.recycler;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

public class LinearItemDecoration extends RecyclerView.ItemDecoration {

    private Drawable mDrawable;

    /**
     * 传入上下文与资源ID
     *
     * @param context
     * @param resId
     */
    public LinearItemDecoration(Context context, int resId) {
        //获取 Drawable 对象
        mDrawable = ContextCompat.getDrawable(context, resId);
    }

    /**
     * 基本操作是留出分割线位置
     *
     * @param outRect
     * @param view
     * @param parent
     * @param state
     */
    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State
            state) {
        //获取条目
        int position = parent.getChildAdapterPosition(view);
        //如果不是第一个条目，每个条目上方留出传入资源的高度用来绘制分割线
        if (position != 0) {
            outRect.top = mDrawable.getIntrinsicHeight();
        }
    }

    /**
     * 绘制分割线
     *
     * @param canvas
     * @param parent
     * @param state
     */
    @Override
    public void onDraw(Canvas canvas, RecyclerView parent, RecyclerView.State state) {
        Rect mRect = new Rect();
        //设置矩形左边位置
        mRect.left = parent.getPaddingLeft();
        //设置矩形右边位置
        mRect.right = parent.getWidth() - parent.getPaddingRight();
        int chilCount = parent.getChildCount();
        for (int i = 1; i < chilCount; i++) {
            //每个分割线的底部位置都是上一个条目的头部
            mRect.bottom = parent.getChildAt(i).getTop();
            //每个分割线的头部位置都是底部位置-资源的高度
            mRect.top = mRect.bottom - mDrawable.getIntrinsicHeight();
            //设置 Drawable 绘制的位置
            mDrawable.setBounds(mRect);
            mDrawable.draw(canvas);
        }
    }
}

