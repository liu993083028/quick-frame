package com.chcit.mobile.mvp.transship.di.component;

import dagger.BindsInstance;
import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.chcit.mobile.mvp.transship.di.module.TransshipModule;
import com.chcit.mobile.mvp.transship.contract.TransshipContract;

import com.jess.arms.di.scope.FragmentScope;
import com.chcit.mobile.mvp.transship.fragment.TransshipFragment;


/**
 * ================================================
 * Description:
 * <p>
 * Created by MVPArmsTemplate on 10/25/2019 16:22
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms">Star me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms/wiki">See me</a>
 * <a href="https://github.com/JessYanCoding/MVPArmsTemplate">模版请保持更新</a>
 * ================================================
 */
@FragmentScope
@Component(modules = TransshipModule.class, dependencies = AppComponent.class)
public interface TransshipComponent {
    void inject(TransshipFragment fragment);

    @Component.Builder
    interface Builder {
        @BindsInstance
        TransshipComponent.Builder view(TransshipContract.View view);

        TransshipComponent.Builder appComponent(AppComponent appComponent);

        TransshipComponent build();
    }
}