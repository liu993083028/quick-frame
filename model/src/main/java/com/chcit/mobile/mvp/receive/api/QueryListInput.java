package com.chcit.mobile.mvp.receive.api;

public class QueryListInput {

    /**
     * dateArrivedFrom : 2019-10-14
     * dateArrivedTo : 2019-10-07
     * warehouseId : 1000251
     * warehouseId_text : 草药药库
     * bpartnerId : 1005002
     * bpartnerId_text : J001-南京鹤龄药事服务有限公司
     * deliveryNo : 123213123
     * productName : 1434
     */

    private String dateArrivedFrom;
    private String dateArrivedTo;
    private Integer warehouseId;
    private String warehouseId_text;
    private Long bpartnerId;
    private String bpartnerId_text;
    private String deliveryNo;
    private String productName;
    private String asnNo;
    private String asnId;
    private String page;
    private String asnType;
    private String checkStatus;
    private String packageNo;
    private String isSelf;
    private String checkTimeFrom;
    private String checkTimeTo;
    private String isReject;
    private String isPutaway;

    private QueryListInput(Builder builder) {
        setDateArrivedFrom(builder.dateArrivedFrom);
        setDateArrivedTo(builder.dateArrivedTo);
        setWarehouseId(builder.warehouseId);
        setWarehouseId_text(builder.warehouseId_text);
        setBpartnerId(builder.bpartnerId);
        setBpartnerId_text(builder.bpartnerId_text);
        setDeliveryNo(builder.deliveryNo);
        setProductName(builder.productName);
        setAsnNo(builder.asnNo);
        setAsnId(builder.asnId);
        setPage(builder.page);
        setAsnType(builder.asnType);
        setCheckStatus(builder.checkStatus);
        setPackageNo(builder.packageNo);
        setIsSelf(builder.isSelf);
        setCheckTimeFrom(builder.checkTimeFrom);
        setCheckTimeTo(builder.checkTimeTo);
        setIsReject(builder.isReject);
        setIsPutaway(builder.isPutaway);
    }

    public String getDateArrivedFrom() {
        return dateArrivedFrom;
    }

    public void setDateArrivedFrom(String dateArrivedFrom) {
        this.dateArrivedFrom = dateArrivedFrom;
    }

    public String getDateArrivedTo() {
        return dateArrivedTo;
    }

    public void setDateArrivedTo(String dateArrivedTo) {
        this.dateArrivedTo = dateArrivedTo;
    }

    public Integer getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Integer warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getWarehouseId_text() {
        return warehouseId_text;
    }

    public void setWarehouseId_text(String warehouseId_text) {
        this.warehouseId_text = warehouseId_text;
    }

    public Long getBpartnerId() {
        return bpartnerId;
    }

    public void setBpartnerId(Long bpartnerId) {
        this.bpartnerId = bpartnerId;
    }

    public String getBpartnerId_text() {
        return bpartnerId_text;
    }

    public void setBpartnerId_text(String bpartnerId_text) {
        this.bpartnerId_text = bpartnerId_text;
    }

    public String getDeliveryNo() {
        return deliveryNo;
    }

    public void setDeliveryNo(String deliveryNo) {
        this.deliveryNo = deliveryNo;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getAsnNo() {
        return asnNo;
    }

    public void setAsnNo(String asnNo) {
        this.asnNo = asnNo;
    }

    public String getAsnId() {
        return asnId;
    }

    public void setAsnId(String asnId) {
        this.asnId = asnId;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getAsnType() {
        return asnType;
    }

    public void setAsnType(String asnType) {
        this.asnType = asnType;
    }

    public String getCheckStatus() {
        return checkStatus;
    }

    public void setCheckStatus(String checkStatus) {
        this.checkStatus = checkStatus;
    }

    public String getPackageNo() {
        return packageNo;
    }

    public void setPackageNo(String packageNo) {
        this.packageNo = packageNo;
    }

    public String getIsSelf() {
        return isSelf;
    }

    public void setIsSelf(String isSelf) {
        this.isSelf = isSelf;
    }

    public String getCheckTimeFrom() {
        return checkTimeFrom;
    }

    public void setCheckTimeFrom(String checkTimeFrom) {
        this.checkTimeFrom = checkTimeFrom;
    }

    public String getCheckTimeTo() {
        return checkTimeTo;
    }

    public void setCheckTimeTo(String checkTimeTo) {
        this.checkTimeTo = checkTimeTo;
    }

    public String getIsReject() {
        return isReject;
    }

    public void setIsReject(String isReject) {
        this.isReject = isReject;
    }

    public String getIsPutaway() {
        return isPutaway;
    }

    public void setIsPutaway(String isPutaway) {
        this.isPutaway = isPutaway;
    }


    public static final class Builder {
        private String dateArrivedFrom;
        private String dateArrivedTo;
        private Integer warehouseId;
        private String warehouseId_text;
        private Long bpartnerId;
        private String bpartnerId_text;
        private String deliveryNo;
        private String productName;
        private String asnNo;
        private String asnId;
        private String page;
        private String asnType;
        private String checkStatus;
        private String packageNo;
        private String isSelf;
        private String checkTimeFrom;
        private String checkTimeTo;
        private String isReject;
        private String isPutaway;

        public Builder() {
        }

        public Builder dateArrivedFrom(String val) {
            dateArrivedFrom = val;
            return this;
        }

        public Builder dateArrivedTo(String val) {
            dateArrivedTo = val;
            return this;
        }

        public Builder warehouseId(Integer val) {
            warehouseId = val;
            return this;
        }

        public Builder warehouseId_text(String val) {
            warehouseId_text = val;
            return this;
        }

        public Builder bpartnerId(Long val) {
            bpartnerId = val;
            return this;
        }

        public Builder bpartnerId_text(String val) {
            bpartnerId_text = val;
            return this;
        }

        public Builder deliveryNo(String val) {
            deliveryNo = val;
            return this;
        }

        public Builder productName(String val) {
            productName = val;
            return this;
        }

        public Builder asnNo(String val) {
            asnNo = val;
            return this;
        }

        public Builder asnId(String val) {
            asnId = val;
            return this;
        }

        public Builder page(String val) {
            page = val;
            return this;
        }

        public Builder asnType(String val) {
            asnType = val;
            return this;
        }

        public Builder checkStatus(String val) {
            checkStatus = val;
            return this;
        }

        public Builder packageNo(String val) {
            packageNo = val;
            return this;
        }

        public Builder isSelf(String val) {
            isSelf = val;
            return this;
        }

        public Builder checkTimeFrom(String val) {
            checkTimeFrom = val;
            return this;
        }

        public Builder checkTimeTo(String val) {
            checkTimeTo = val;
            return this;
        }

        public Builder isReject(String val) {
            isReject = val;
            return this;
        }

        public Builder isPutaway(String val) {
            isPutaway = val;
            return this;
        }

        public QueryListInput build() {
            return new QueryListInput(this);
        }
    }
}
