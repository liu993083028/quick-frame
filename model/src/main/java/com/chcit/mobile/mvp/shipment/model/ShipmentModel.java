package com.chcit.mobile.mvp.shipment.model;

import android.app.Application;

import com.alibaba.fastjson.JSONObject;
import com.chcit.mobile.common.HttpMethodContains;
import com.chcit.mobile.app.exception.BaseException;
import com.chcit.mobile.mvp.common.api.service.UserService;
import com.chcit.mobile.mvp.entity.PageBean;
import com.chcit.mobile.mvp.entity.ResultBean;
import com.chcit.mobile.mvp.entity.ShipmentBean;
import com.chcit.mobile.mvp.shipment.api.ShipmentInput;
import com.chcit.mobile.mvp.shipment.api.ShipmentApi;
import com.google.gson.Gson;
import com.jess.arms.integration.IRepositoryManager;
import com.jess.arms.mvp.BaseModel;

import com.jess.arms.di.scope.FragmentScope;

import javax.inject.Inject;

import com.chcit.mobile.mvp.shipment.contract.ShipmentContract;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;


@FragmentScope
public class ShipmentModel extends BaseModel implements ShipmentContract.Model {
    @Inject
    Gson mGson;
    @Inject
    Application mApplication;

    @Inject
    public ShipmentModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mGson = null;
        this.mApplication = null;
    }

    //获取拣货任务明细
    @Override
    public Observable<List<ShipmentBean>> getShipments(JSONObject json) {
        return mRepositoryManager.obtainRetrofitService(UserService.class)
                .requestOnString(HttpMethodContains.SHIPMENT_QUERY_DETAIL,json)
                .map(result -> {
                    JSONObject resultJson = JSONObject.parseObject(result);
                    ResultBean data = resultJson.toJavaObject(ResultBean.class);
                    //ResponseBean<ReceiveBean> data =mGson.<ResponseBean<ReceiveBean>>fromJson(result,ResponseBean.class);
                    if(resultJson.getBoolean("success")){

                        if(data.getData().getInteger("total")==0){

                            throw new BaseException("没有查询到数据！");
                        }
                        return data.getRows(ShipmentBean.class);

                    }else{
                        throw new Exception("请求失败: "+(resultJson.getString("msg").isEmpty()?"未知错误":resultJson.getString("msg")));
                    }

                }).subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<ResultBean> doBatchConfirm(JSONObject json) {
        return mRepositoryManager.obtainRetrofitService(UserService.class)
                .request(HttpMethodContains.SHIPMENT_OK,json)
                .map(jsonObject -> {
                    ResultBean resultBean = mGson.fromJson(jsonObject.toJSONString(),ResultBean.class);
                    if(resultBean.isSuccess()){
                        return resultBean;
                    }else{
                        throw new Exception("请求失败: "+resultBean.getMsg());
                    }
                }).subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<List<ShipmentBean>> queryDetail(PageBean pageBean, ShipmentInput shipmentInput) {

        String requestJSON ="{ " +pageBean.toString() + shipmentInput.toString()+" }";
        JSONObject json = JSONObject.parseObject(requestJSON);
        return mRepositoryManager.obtainRetrofitService(ShipmentApi.class)
                .queryDetail(json)
                .map(result -> {
                    if(result.isSuccess()){
                        if(result.getTotal()==0){
                            throw new BaseException("没有查询到数据！");
                        }
                        return result.getRows();

                    }else{
                        throw new Exception("请求失败: "+(result.getMsg().isEmpty()?"未知错误":result.getMsg()));
                    }

                }).subscribeOn(Schedulers.io());
    }


}