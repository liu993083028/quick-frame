package com.chcit.mobile.mvp.shipment.api;

import com.alibaba.fastjson.annotation.JSONField;

public class ShipmentInput {

    /**
     * pickListId : 1001439
     * orderType : PR
     * page : pick
     * asnType : PO
     * dateArrivedFrom : 2019-07-10
     * warehouseId : 1000214
     * warehouseId_text : 西药库
     */

    @JSONField(name = "pickListId")
    private int pickListId;
    @JSONField(name = "orderType")
    private String orderType;
    @JSONField(name = "page")
    private String page = "receive";
    @JSONField(name = "asnType")
    private String asnType;
    @JSONField(name = "dateArrivedFrom")
    private String dateArrivedFrom;
    @JSONField(name = "warehouseId")
    private int warehouseId;
    @JSONField(name = "warehouseId_text")
    private String warehouseIdText;

    public int getPickListId() {
        return pickListId;
    }

    public void setPickListId(int pickListId) {
        this.pickListId = pickListId;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getAsnType() {
        return asnType;
    }

    public void setAsnType(String asnType) {
        this.asnType = asnType;
    }

    public String getDateArrivedFrom() {
        return dateArrivedFrom;
    }

    public void setDateArrivedFrom(String dateArrivedFrom) {
        this.dateArrivedFrom = dateArrivedFrom;
    }

    public int getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(int warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getWarehouseIdText() {
        return warehouseIdText;
    }

    public void setWarehouseIdText(String warehouseIdText) {
        this.warehouseIdText = warehouseIdText;
    }

    @Override
    public String toString() {

        StringBuilder text = new StringBuilder();
        if(page != null){
            text.append("page:").append(page);
        }
        if(pickListId > 0){
            text.append(",pickListId:").append(pickListId);
        }
        if(orderType != null){
            text.append(",orderType:").append(orderType);
        }
        if(asnType != null){
            text.append(",asnType:").append(asnType);
        }
        if(warehouseId > 0 ){
            text.append(",warehouseId:").append(warehouseId);
        }
        if(dateArrivedFrom != null){
            text.append(",dateArrivedFrom:").append(dateArrivedFrom);
        }
      return text.toString();
    }
}
