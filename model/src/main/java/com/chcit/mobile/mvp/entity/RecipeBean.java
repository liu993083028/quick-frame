package com.chcit.mobile.mvp.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.Objects;

public class RecipeBean implements Parcelable {


    /**
     * presNo : 132
     * patientName : 欧测
     * sex : 2
     * age : 28岁
     * presDate : 2018-05-28 11:00:50
     * bpartnerName : 三分院西药房
     * doctorName : 李书芳
     * presID : 1000179
     * presLineID : 1000233
     * productName : 药品mm
     * productSpec : 1g*20片
     * manufacturer : 江西江中
     * qty : 1
     * unit : 盒
     * locatorValue : 1
     * isOdd : N
     */

    private String presNo;
    private String patientName;
    private String sex;
    private String age;
    private String presDate;
    private String bpartnerName;
    private String doctorName;
    private String presID;
    private String presLineID;
    private String productName;
    private String productSpec;
    private String manufacturer;
    private String qty;
    private String unit;
    private String locatorValue;
    private String isOdd;
    private String productCode;
    private String basketCode;
    /**
     * documentNo : 1000076-0-6-2
     * applydate : 2018-12-21 03:21:35
     * applyID : 1000076
     * productSpecID : 1006299
     */

    @JSONField(name = "documentNo")
    private String documentNo;
    @JSONField(name = "applydate")
    private String applydate;
    @JSONField(name = "applyID")
    private String applyID;
    @JSONField(name = "productSpecID")
    private String productSpecID;

    public String getBasketCode() {
        return basketCode;
    }

    public void setBasketCode(String basketCode) {
        this.basketCode = basketCode;
    }

    public String getPresNo() {
        return presNo;
    }

    public void setPresNo(String presNo) {
        this.presNo = presNo;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getPresDate() {
        return presDate;
    }

    public void setPresDate(String presDate) {
        this.presDate = presDate;
    }

    public String getBpartnerName() {
        return bpartnerName;
    }

    public void setBpartnerName(String bpartnerName) {
        this.bpartnerName = bpartnerName;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getPresID() {
        return presID;
    }

    public void setPresID(String presID) {
        this.presID = presID;
    }

    public String getPresLineID() {
        return presLineID;
    }

    public void setPresLineID(String presLineID) {
        this.presLineID = presLineID;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductSpec() {
        return productSpec;
    }

    public void setProductSpec(String productSpec) {
        this.productSpec = productSpec;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getLocatorValue() {
        return locatorValue;
    }

    public void setLocatorValue(String locatorValue) {
        this.locatorValue = locatorValue;
    }

    public String getIsOdd() {
        return isOdd;
    }

    public void setIsOdd(String isOdd) {
        this.isOdd = isOdd;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.presNo);
        dest.writeString(this.patientName);
        dest.writeString(this.sex);
        dest.writeString(this.age);
        dest.writeString(this.presDate);
        dest.writeString(this.bpartnerName);
        dest.writeString(this.doctorName);
        dest.writeString(this.presID);
        dest.writeString(this.presLineID);
        dest.writeString(this.productName);
        dest.writeString(this.productSpec);
        dest.writeString(this.manufacturer);
        dest.writeString(this.qty);
        dest.writeString(this.unit);
        dest.writeString(this.locatorValue);
        dest.writeString(this.isOdd);
    }

    public RecipeBean() {
    }

    protected RecipeBean(Parcel in) {
        this.presNo = in.readString();
        this.patientName = in.readString();
        this.sex = in.readString();
        this.age = in.readString();
        this.presDate = in.readString();
        this.bpartnerName = in.readString();
        this.doctorName = in.readString();
        this.presID = in.readString();
        this.presLineID = in.readString();
        this.productName = in.readString();
        this.productSpec = in.readString();
        this.manufacturer = in.readString();
        this.qty = in.readString();
        this.unit = in.readString();
        this.locatorValue = in.readString();
        this.isOdd = in.readString();
    }

    public static final Creator<RecipeBean> CREATOR = new Creator<RecipeBean>() {
        @Override
        public RecipeBean createFromParcel(Parcel source) {
            return new RecipeBean(source);
        }

        @Override
        public RecipeBean[] newArray(int size) {
            return new RecipeBean[size];
        }
    };

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RecipeBean that = (RecipeBean) o;
        return Objects.equals(presNo, that.presNo) &&
                Objects.equals(patientName, that.patientName) &&
                Objects.equals(sex, that.sex) &&
                Objects.equals(age, that.age) &&
                Objects.equals(presDate, that.presDate) &&
                Objects.equals(bpartnerName, that.bpartnerName) &&
                Objects.equals(doctorName, that.doctorName) &&
                Objects.equals(presID, that.presID) &&
                Objects.equals(presLineID, that.presLineID) &&
                Objects.equals(productName, that.productName) &&
                Objects.equals(productSpec, that.productSpec) &&
                Objects.equals(manufacturer, that.manufacturer) &&
                Objects.equals(qty, that.qty) &&
                Objects.equals(unit, that.unit) &&
                Objects.equals(locatorValue, that.locatorValue) &&
                Objects.equals(isOdd, that.isOdd);
    }

    @Override
    public int hashCode() {

        return Objects.hash(presNo, patientName, sex, age, presDate, bpartnerName, doctorName, presID, presLineID, productName, productSpec, manufacturer, qty, unit, locatorValue, isOdd);
    }

    public String getDocumentNo() {
        return documentNo;
    }

    public void setDocumentNo(String documentNo) {
        this.documentNo = documentNo;
    }

    public String getApplydate() {
        return applydate;
    }

    public void setApplydate(String applydate) {
        this.applydate = applydate;
    }

    public String getApplyID() {
        return applyID;
    }

    public void setApplyID(String applyID) {
        this.applyID = applyID;
    }

    public String getProductSpecID() {
        return productSpecID;
    }

    public void setProductSpecID(String productSpecID) {
        this.productSpecID = productSpecID;
    }
}
