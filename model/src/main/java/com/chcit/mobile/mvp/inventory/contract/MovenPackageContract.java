package com.chcit.mobile.mvp.inventory.contract;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.chcit.mobile.mvp.entity.BaseData;
import com.chcit.mobile.mvp.entity.Locator;
import com.chcit.mobile.mvp.entity.ResultBean;
import com.jess.arms.mvp.IView;
import com.jess.arms.mvp.IModel;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.Field;

public interface MovenPackageContract {
    //对于经常使用的关于UI的方法可以定义到IView中,如显示隐藏进度条,和显示文字消息
    interface View extends IView {

    }

    //Model层定义接口,外部只需关心Model返回的数据,无需关心内部细节,即是否使用缓存
    interface Model extends IModel {
        Observable<BaseData<Locator>> getLocators(JSONObject json);

        Observable<ResultBean> movePackage(int locatorToId, List<String> packageNo);
    }
}
