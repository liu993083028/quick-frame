package com.chcit.mobile.mvp.entity;

public class BpartnerBean {

    /**
     * bpartnerName : 南京医药药业有限公司
     * bpartnerId : 1004822
     */

    private int id;
    private String name;

    public BpartnerBean() {
    }

    public BpartnerBean(int s1, String s2) {
        this.name = s2;
        this.id =s1;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    @Override
    public String toString() {
        return name ;
    }
}
