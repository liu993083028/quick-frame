package com.chcit.mobile.mvp.receive.Entity;

import java.math.BigDecimal;

public class AsnLineBean {

    /**
     * asnId : 1014568
     * asnLineId : 1054136
     * warehouseId : 1000251
     * warehouseName : 草药药库
     * bpartnerName : 江阴天江药业有限公司
     * asnNo : 1001417
     * taxInvoiceNo : 2321
     * deliveryNo : 1000146
     * dateArrived : 2019-10-23
     * docDate : 2019-10-21
     * checkTime :
     * lot : 23222
     * guaranteeDate : 2022-11-30
     * productionDate :
     * productId : 1039865
     * productName : 九节菖蒲颗粒
     * productSpec : 10克/袋
     * manufacturer : 江阴天江药业有限公司
     * productCode : 105722
     * uomName : 袋
     * qtyArrived : 800
     * qtyPutawayed : 0
     * qtyReceived : 0
     * qtyRejected : 0
     * qtyPutawayLeft : 0
     * qtyReturned : 0
     * qtyCheckLeft : 800
     * qtyChecked : 0
     * priceActual : 4.52
     * orderPrice : 4.52
     * lineAmt : 3616
     * isExported : N
     * asnType : PO
     * asnTypeName : 采购
     * storageStatus : S
     * storageStatusName : 正常
     * lineStatus : N
     * lineStatusName : 待处理
     * isControlledProduct : N
     * lPackageQty : 200
     * certificateNo : 无
     * certificateNo_Cur : 无
     * qtyPackaged : 800
     * qtyUnpackaged : 0
     * unitpackqty : 1
     * locatorName : 存储货位
     * invoiceMethod : 1
     * invoiceMethodName : 货票同行
     * locatorId : 1013845
     * receiptType : 1
     * receiptTypeName : 正常采购
     * isStoragePackage : Y
     * productControlLevel : 0
     * productControlLevelName : 中药饮片
     * isPriceDiff : N
     * isPriceDiffConfirmed : N
     */

    private int asnId;
    private int asnLineId;
    private int warehouseId;
    private String warehouseName;
    private String bpartnerName;
    private String asnNo;
    private String taxInvoiceNo;
    private String deliveryNo;
    private String dateArrived;
    private String docDate;
    private String checkTime;
    private String lot;
    private String guaranteeDate;
    private String productionDate;
    private int productId;
    private String productName;
    private String productSpec;
    private String manufacturer;
    private String productCode;
    private String uomName;
    private BigDecimal qtyArrived  = BigDecimal.ZERO;;
    private BigDecimal qtyPutawayed  = BigDecimal.ZERO;;
    private BigDecimal qtyReceived  = BigDecimal.ZERO;;

    private BigDecimal qtyPutawayLeft  = BigDecimal.ZERO;;
    private BigDecimal qtyReturned  = BigDecimal.ZERO;;
    private BigDecimal qtyCheckLeft = BigDecimal.ZERO;
    private BigDecimal qtyChecked  = BigDecimal.ZERO;;
    private BigDecimal priceActual  = BigDecimal.ZERO;;
    private BigDecimal orderPrice  = BigDecimal.ZERO;;
    private BigDecimal lineAmt  = BigDecimal.ZERO;;
    private String isExported;
    private String asnType;
    private String asnTypeName;
    private String storageStatus;
    private String storageStatusName;
    private String lineStatus;
    private String lineStatusName;
    private String isControlledProduct;
    private BigDecimal lPackageQty;
    private String certificateNo;
    private String certificateNo_Cur;
    private BigDecimal qtyPackaged;
    private BigDecimal qtyUnpackaged;
    private BigDecimal unitpackqty;
    private String locatorName;
    private String invoiceMethod;
    private String invoiceMethodName;
    private String locatorId;
    private String receiptType;
    private String receiptTypeName;
    private String isStoragePackage;
    private String productControlLevel;
    private String productControlLevelName;
    private String isPriceDiff;
    private String isPriceDiffConfirmed;
    private BigDecimal packageCountCheckLeft  = BigDecimal.ZERO; //待验收包数
    private BigDecimal packageCountChecked = BigDecimal.ZERO; //已验收包数
    private BigDecimal packageCountArrived = BigDecimal.ZERO;// 到货包数
    private BigDecimal qtyRejected  = BigDecimal.ZERO;;
    private BigDecimal packageCountRejected = BigDecimal.ZERO ;// 已拒包数

    public AsnLineBean(){

    }

    public AsnLineBean(int productId, String productName, String productCode) {
        this.productId = productId;
        this.productName = productName;
        this.productCode = productCode;
    }

    public int getAsnId() {
        return asnId;
    }

    public void setAsnId(int asnId) {
        this.asnId = asnId;
    }

    public int getAsnLineId() {
        return asnLineId;
    }

    public void setAsnLineId(int asnLineId) {
        this.asnLineId = asnLineId;
    }

    public int getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(int warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public String getBpartnerName() {
        return bpartnerName;
    }

    public void setBpartnerName(String bpartnerName) {
        this.bpartnerName = bpartnerName;
    }

    public String getAsnNo() {
        return asnNo;
    }

    public void setAsnNo(String asnNo) {
        this.asnNo = asnNo;
    }

    public String getTaxInvoiceNo() {
        return taxInvoiceNo;
    }

    public void setTaxInvoiceNo(String taxInvoiceNo) {
        this.taxInvoiceNo = taxInvoiceNo;
    }

    public String getDeliveryNo() {
        return deliveryNo;
    }

    public void setDeliveryNo(String deliveryNo) {
        this.deliveryNo = deliveryNo;
    }

    public String getDateArrived() {
        return dateArrived;
    }

    public void setDateArrived(String dateArrived) {
        this.dateArrived = dateArrived;
    }

    public String getDocDate() {
        return docDate;
    }

    public void setDocDate(String docDate) {
        this.docDate = docDate;
    }

    public String getCheckTime() {
        return checkTime;
    }

    public void setCheckTime(String checkTime) {
        this.checkTime = checkTime;
    }

    public String getLot() {
        return lot;
    }

    public void setLot(String lot) {
        this.lot = lot;
    }

    public String getGuaranteeDate() {
        return guaranteeDate;
    }

    public void setGuaranteeDate(String guaranteeDate) {
        this.guaranteeDate = guaranteeDate;
    }

    public String getProductionDate() {
        return productionDate;
    }

    public void setProductionDate(String productionDate) {
        this.productionDate = productionDate;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductSpec() {
        return productSpec;
    }

    public void setProductSpec(String productSpec) {
        this.productSpec = productSpec;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getUomName() {
        return uomName;
    }

    public void setUomName(String uomName) {
        this.uomName = uomName;
    }

    public BigDecimal getQtyArrived() {
        return qtyArrived;
    }

    public void setQtyArrived(BigDecimal qtyArrived) {
        this.qtyArrived = qtyArrived;
    }

    public BigDecimal getQtyPutawayed() {
        return qtyPutawayed;
    }

    public void setQtyPutawayed(BigDecimal qtyPutawayed) {
        this.qtyPutawayed = qtyPutawayed;
    }

    public BigDecimal getQtyReceived() {
        return qtyReceived;
    }

    public void setQtyReceived(BigDecimal qtyReceived) {
        this.qtyReceived = qtyReceived;
    }

    public BigDecimal getQtyRejected() {
        return qtyRejected;
    }

    public void setQtyRejected(BigDecimal qtyRejected) {
        this.qtyRejected = qtyRejected;
    }

    public BigDecimal getQtyPutawayLeft() {
        return qtyPutawayLeft;
    }

    public void setQtyPutawayLeft(BigDecimal qtyPutawayLeft) {
        this.qtyPutawayLeft = qtyPutawayLeft;
    }

    public BigDecimal getQtyReturned() {
        return qtyReturned;
    }

    public void setQtyReturned(BigDecimal qtyReturned) {
        this.qtyReturned = qtyReturned;
    }

    public BigDecimal getQtyCheckLeft() {
        return qtyCheckLeft;
    }

    public void setQtyCheckLeft(BigDecimal qtyCheckLeft) {
        this.qtyCheckLeft = qtyCheckLeft;
    }

    public BigDecimal getQtyChecked() {
        return qtyChecked;
    }

    public void setQtyChecked(BigDecimal qtyChecked) {
        this.qtyChecked = qtyChecked;
    }

    public BigDecimal getPriceActual() {
        return priceActual;
    }

    public void setPriceActual(BigDecimal priceActual) {
        this.priceActual = priceActual;
    }

    public BigDecimal getOrderPrice() {
        return orderPrice;
    }

    public void setOrderPrice(BigDecimal orderPrice) {
        this.orderPrice = orderPrice;
    }

    public BigDecimal getLineAmt() {
        return lineAmt;
    }

    public void setLineAmt(BigDecimal lineAmt) {
        this.lineAmt = lineAmt;
    }

    public String getIsExported() {
        return isExported;
    }

    public void setIsExported(String isExported) {
        this.isExported = isExported;
    }

    public String getAsnType() {
        return asnType;
    }

    public void setAsnType(String asnType) {
        this.asnType = asnType;
    }

    public String getAsnTypeName() {
        return asnTypeName;
    }

    public void setAsnTypeName(String asnTypeName) {
        this.asnTypeName = asnTypeName;
    }

    public String getStorageStatus() {
        return storageStatus;
    }

    public void setStorageStatus(String storageStatus) {
        this.storageStatus = storageStatus;
    }

    public String getStorageStatusName() {
        return storageStatusName;
    }

    public void setStorageStatusName(String storageStatusName) {
        this.storageStatusName = storageStatusName;
    }

    public String getLineStatus() {
        return lineStatus;
    }

    public void setLineStatus(String lineStatus) {
        this.lineStatus = lineStatus;
    }

    public String getLineStatusName() {
        return lineStatusName;
    }

    public void setLineStatusName(String lineStatusName) {
        this.lineStatusName = lineStatusName;
    }

    public String getIsControlledProduct() {
        return isControlledProduct;
    }

    public void setIsControlledProduct(String isControlledProduct) {
        this.isControlledProduct = isControlledProduct;
    }

    public BigDecimal getlPackageQty() {
        return lPackageQty;
    }

    public void setlPackageQty(BigDecimal lPackageQty) {
        this.lPackageQty = lPackageQty;
    }

    public String getCertificateNo() {
        return certificateNo;
    }

    public void setCertificateNo(String certificateNo) {
        this.certificateNo = certificateNo;
    }

    public String getCertificateNo_Cur() {
        return certificateNo_Cur;
    }

    public void setCertificateNo_Cur(String certificateNo_Cur) {
        this.certificateNo_Cur = certificateNo_Cur;
    }

    public BigDecimal getQtyPackaged() {
        return qtyPackaged;
    }

    public void setQtyPackaged(BigDecimal qtyPackaged) {
        this.qtyPackaged = qtyPackaged;
    }

    public BigDecimal getQtyUnpackaged() {
        return qtyUnpackaged;
    }

    public void setQtyUnpackaged(BigDecimal qtyUnpackaged) {
        this.qtyUnpackaged = qtyUnpackaged;
    }

    public BigDecimal getUnitpackqty() {
        return unitpackqty;
    }

    public void setUnitpackqty(BigDecimal unitpackqty) {
        this.unitpackqty = unitpackqty;
    }

    public String getLocatorName() {
        return locatorName;
    }

    public void setLocatorName(String locatorName) {
        this.locatorName = locatorName;
    }

    public String getInvoiceMethod() {
        return invoiceMethod;
    }

    public void setInvoiceMethod(String invoiceMethod) {
        this.invoiceMethod = invoiceMethod;
    }

    public String getInvoiceMethodName() {
        return invoiceMethodName;
    }

    public void setInvoiceMethodName(String invoiceMethodName) {
        this.invoiceMethodName = invoiceMethodName;
    }

    public String getLocatorId() {
        return locatorId;
    }

    public void setLocatorId(String locatorId) {
        this.locatorId = locatorId;
    }

    public String getReceiptType() {
        return receiptType;
    }

    public void setReceiptType(String receiptType) {
        this.receiptType = receiptType;
    }

    public String getReceiptTypeName() {
        return receiptTypeName;
    }

    public void setReceiptTypeName(String receiptTypeName) {
        this.receiptTypeName = receiptTypeName;
    }

    public String getIsStoragePackage() {
        return isStoragePackage;
    }

    public void setIsStoragePackage(String isStoragePackage) {
        this.isStoragePackage = isStoragePackage;
    }

    public String getProductControlLevel() {
        return productControlLevel;
    }

    public void setProductControlLevel(String productControlLevel) {
        this.productControlLevel = productControlLevel;
    }

    public String getProductControlLevelName() {
        return productControlLevelName;
    }

    public void setProductControlLevelName(String productControlLevelName) {
        this.productControlLevelName = productControlLevelName;
    }

    public String getIsPriceDiff() {
        return isPriceDiff;
    }

    public void setIsPriceDiff(String isPriceDiff) {
        this.isPriceDiff = isPriceDiff;
    }

    public String getIsPriceDiffConfirmed() {
        return isPriceDiffConfirmed;
    }

    public void setIsPriceDiffConfirmed(String isPriceDiffConfirmed) {
        this.isPriceDiffConfirmed = isPriceDiffConfirmed;
    }

    public BigDecimal getPackageCountCheckLeft() {
        return packageCountCheckLeft;
    }

    public void setPackageCountCheckLeft(BigDecimal packageCountCheckLeft) {
        this.packageCountCheckLeft = packageCountCheckLeft;
    }

    public BigDecimal getPackageCountChecked() {
        return packageCountChecked;
    }

    public void setPackageCountChecked(BigDecimal packageCountChecked) {
        this.packageCountChecked = packageCountChecked;
    }

    public BigDecimal getPackageCountArrived() {
        return packageCountArrived;
    }

    public void setPackageCountArrived(BigDecimal packageCountArrived) {
        this.packageCountArrived = packageCountArrived;
    }

    public BigDecimal getPackageCountRejected() {
        return packageCountRejected;
    }

    public void setPackageCountRejected(BigDecimal packageCountRejected) {
        this.packageCountRejected = packageCountRejected;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AsnLineBean)) return false;

        AsnLineBean that = (AsnLineBean) o;

        if (productId != that.productId) return false;
        if (!productName.equals(that.productName)) return false;
        return productCode.equals(that.productCode);
    }

    @Override
    public int hashCode() {
        int result = productId;
        result = 31 * result + productName.hashCode();
        result = 31 * result + productCode.hashCode();
        return result;
    }
}
