package com.chcit.mobile.mvp.inventory.model;

import android.app.Application;

import com.alibaba.fastjson.JSONObject;
import com.chcit.mobile.mvp.entity.BaseData;
import com.chcit.mobile.mvp.entity.PackageUnitBean;
import com.chcit.mobile.mvp.entity.PageBean;
import com.chcit.mobile.mvp.inventory.api.InventoryApi;
import com.chcit.mobile.mvp.inventory.model.api.PackageQueyInput;
import com.google.gson.Gson;
import com.jess.arms.integration.IRepositoryManager;
import com.jess.arms.mvp.BaseModel;

import com.jess.arms.di.scope.FragmentScope;

import javax.inject.Inject;

import com.chcit.mobile.mvp.inventory.contract.PackageQueryContract;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;


/**
 * ================================================
 * Description:
 * <p>
 * Created by MVPArmsTemplate on 07/18/2019 14:30
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms">Star me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms/wiki">See me</a>
 * <a href="https://github.com/JessYanCoding/MVPArmsTemplate">模版请保持更新</a>
 * ================================================
 */
@FragmentScope
public class PackageQueryModel extends BaseModel implements PackageQueryContract.Model {
    @Inject
    Gson mGson;
    @Inject
    Application mApplication;

    @Inject
    public PackageQueryModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mGson = null;
        this.mApplication = null;
    }

    @Override
    public Observable<BaseData<PackageUnitBean>> getDataList(PageBean pageBean, PackageQueyInput packageQueyInput) {
        String pageString = JSONObject.toJSONString(pageBean);
        pageString = pageString.substring(0,pageString.length()-1);
        String packageQueryString = JSONObject.toJSONString(packageQueyInput);
        packageQueryString = packageQueryString.substring(1,packageQueryString.length());
        if(packageQueryString.length()>1){
            pageString+=",";
        }
        JSONObject queryJSON = JSONObject.parseObject(pageString + packageQueryString);

        return mRepositoryManager.obtainRetrofitService(InventoryApi.class)
                .getDataList(queryJSON).subscribeOn(Schedulers.io());
    }
}