package com.chcit.mobile.mvp.inventory.model;

import android.app.Application;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.chcit.mobile.helper.HttpClientHelper;
import com.chcit.mobile.mvp.entity.BaseData;
import com.chcit.mobile.mvp.entity.Locator;
import com.chcit.mobile.mvp.entity.ResultBean;
import com.chcit.mobile.mvp.inventory.model.api.InventoryService;
import com.google.gson.Gson;
import com.jess.arms.integration.IRepositoryManager;
import com.jess.arms.mvp.BaseModel;

import com.jess.arms.di.scope.FragmentScope;

import javax.inject.Inject;

import com.chcit.mobile.mvp.inventory.contract.MovenPackageContract;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

@FragmentScope
public class MovenPackageModel extends BaseModel implements MovenPackageContract.Model {
    @Inject
    Gson mGson;
    @Inject
    Application mApplication;

    @Inject
    public MovenPackageModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mGson = null;
        this.mApplication = null;
    }


    @Override
    public Observable<BaseData<Locator>> getLocators(JSONObject json) {
        return  mRepositoryManager.obtainRetrofitService(InventoryService.class)
                .getLocators(json).subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<ResultBean> movePackage(int locatorToId, List<String> packageNo) {
        return mRepositoryManager.obtainRetrofitService(InventoryService.class)
                .movePackage(HttpClientHelper.getSelectWareHouse().getId(),locatorToId,packageNo).subscribeOn(Schedulers.io());
    }
}