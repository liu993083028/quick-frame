package com.chcit.mobile.mvp.inventory.fragment.Stock;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.chcit.mobile.app.base.ScanToolQueryFragment;
import com.google.android.material.textfield.TextInputEditText;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chcit.custom.view.loader.QuickLoader;
import com.chcit.custom.view.spinner.DropDownMenu;
import com.chcit.mobile.R;
import com.chcit.mobile.helper.HttpClientHelper;
import com.chcit.mobile.helper.recycler.RecycleViewDivider;
import com.chcit.mobile.mvp.common.adapter.GirdDropDownAdapter;
import com.chcit.mobile.mvp.common.model.MenuTypeEnum;
import com.chcit.mobile.mvp.entity.BpartnerBean;
import com.chcit.mobile.mvp.entity.PackageUnitBean;
import com.chcit.mobile.mvp.entity.PageBean;
import com.chcit.mobile.mvp.entity.Statu;
import com.chcit.mobile.mvp.inventory.adapter.PackageAdapter;
import com.chcit.mobile.mvp.inventory.model.api.PackageQueyInput;
import com.jess.arms.di.component.AppComponent;
import com.jess.arms.utils.ArmsUtils;
import com.chcit.mobile.mvp.inventory.di.component.DaggerPackageQueryComponent;
import com.chcit.mobile.mvp.inventory.contract.PackageQueryContract;
import com.chcit.mobile.mvp.inventory.presenter.PackageQueryPresenter;
import com.xuexiang.xui.widget.toast.XToast;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

import static com.jess.arms.utils.Preconditions.checkNotNull;


public class PackageQueryFragment extends ScanToolQueryFragment<PackageQueryPresenter> implements PackageQueryContract.View {

    public static final int REFRESH_CODE =  89898; //刷新数据标志

    @BindView(R.id.pr_inventory_dropDownMenu)
    DropDownMenu mDropDownMenu;
    @BindView(R.id.lv_inventory_list)
    RecyclerView mRecyclerView;
    @BindView(R.id.tv_inventory_describe)
    TextView tvDescribe;
    protected MenuTypeEnum type ;
    private ArrayList<BpartnerBean> bpartners;
    private TextInputEditText tivetPackageNo;
    private TextInputEditText tietLocator;
    private BpartnerBean bpartnerBean;
    private Statu mStatu;
    private String headers[] = {"供应商", "输入商品", "包装类型"};
    private PackageUnitBean packageUnitBean;
    private List<PackageUnitBean> packageUnitBeans = new ArrayList<>();
    protected PackageAdapter mAdapter = new PackageAdapter(packageUnitBeans);;
    private int REQUEST_CODE = this.getClass().hashCode();

    @Inject
    PageBean pageBean;
    public static PackageQueryFragment newInstance() {
        PackageQueryFragment fragment = new PackageQueryFragment();
        return fragment;
    }

    @Override
    public void setupFragmentComponent(@NonNull AppComponent appComponent) {
        DaggerPackageQueryComponent //如找不到该类,请编译一下项目
                .builder()
                .appComponent(appComponent)
                .view(this)
                .build()
                .inject(this);
    }


    @Override
    protected int getLayoutId() {
        return R.layout.fragment_inventory;
    }

    @Override
    public void initData(@Nullable Bundle savedInstanceState) {

        mCommonToolbar.getLeftMenuView(0).setOnClickListener(l -> {
            pop();
            l.setOnClickListener(null);
            if (getActivity() != null) {
                _mActivity.finish();
            }

        });
        initDropDownMenu();
    }


    @Override
    public void setData(@Nullable Object data) {

    }

    @Override
    public void showLoading() {
        QuickLoader.showLoading(mContext);
    }

    @Override
    public void hideLoading() {
        QuickLoader.stopLoading();
    }

    @Override
    public void showMessage(@NonNull String message) {
        checkNotNull(message);
        XToast.info(mContext,message).show();
    }

    @Override
    public void launchActivity(@NonNull Intent intent) {
        checkNotNull(intent);
        ArmsUtils.startActivity(intent);
    }

    @Override
    public void killMyself() {
        pop();
        getActivity().finish();
    }

    private void initDropDownMenu() {
        final List<View> popupViews = new ArrayList<>();

        //供应商选择
        final ListView wareView = new ListView(getContext());
        bpartners = new ArrayList<>();
        bpartners.add(new BpartnerBean(1,"不限"));

        final GirdDropDownAdapter<BpartnerBean> bpartnerAdapter = new GirdDropDownAdapter<BpartnerBean>(getContext(), bpartners);
        wareView.setDividerHeight(0);
        wareView.setAdapter(bpartnerAdapter);
        wareView.setOnItemClickListener((parent, view, position, id) -> {
            bpartnerAdapter.setCheckItem(position);
            bpartnerBean = bpartners.get(position);
            if (position == 0) {
                bpartnerBean = null;
            }
            mDropDownMenu.setTabText(position == 0 ? headers[0] : bpartnerBean.getName());
            mDropDownMenu.closeMenu();
        });

        //商品选择
        final View constellationView = getLayoutInflater().inflate(R.layout.pop_inventory, null);
        tivetPackageNo = constellationView.findViewById(R.id.constellation);//厂家
        tietLocator = constellationView.findViewById(R.id.pop_inventory_locator);
        Button ok = constellationView.findViewById(R.id.ok);
        ok.setOnClickListener(v -> {
            String text = tivetPackageNo.getText().toString();
            mDropDownMenu.setTabText(text.isEmpty() ? headers[1] : text);
            mDropDownMenu.closeMenu();
        });

        //存货状态
        final ListView dateView = new ListView(getContext());

        final List<Statu> status = new ArrayList<>();
        status.add(new Statu("", "不限"));
        final GirdDropDownAdapter<Statu> dateAdapter = new GirdDropDownAdapter<Statu>(getContext(), status);
        dateView.setDividerHeight(0);
        dateView.setAdapter(dateAdapter);
        dateView.setOnItemClickListener((parent, view, position, id) -> {
            dateAdapter.setCheckItem(position);
            mStatu = status.get(position);
            if (position == 0) {
                mStatu = null;
            }
            mDropDownMenu.setTabText(position == 0 ? headers[2] : status.get(position).getName());
            mDropDownMenu.closeMenu();
        });
        popupViews.add(wareView);
        popupViews.add(constellationView);
        popupViews.add(dateView);
        final TextView contentView = new TextView(getContext());
        contentView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        contentView.setText("");
        //contentView.setBackgroundColor(getResources().getColor(R.color.gray));
        contentView.setGravity(Gravity.CENTER);
        contentView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        final List<String> list = Arrays.asList(headers);
        mDropDownMenu.setDropDownMenu(list, popupViews, contentView);

        mPresenter.getBpartners(bpartnerBeans -> {
            bpartners.addAll(bpartnerBeans);
            bpartnerAdapter.notifyDataSetChanged();
        });


        mPresenter.getStatus(1000309,status1 -> {
            status.addAll(status1);
            dateAdapter.notifyDataSetChanged();
        },false);

    }

    private int itemPosition = -1;

    private void initRecyclerView() {

        //ArmsUtils.configRecyclerView(mRecyclerView, mLayoutManager);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.addItemDecoration(new RecycleViewDivider(getContext(), LinearLayoutManager.HORIZONTAL, R.drawable.item_drivide_style));
        mAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                mAdapter.loadMoreComplete();
                getData();
            }
        }, mRecyclerView);
        mAdapter.setOnItemClickListener((adapter, view, position) -> {
            if(position >=0 && adapter.getData().size()>position){
                itemPosition = position;
                mAdapter.setCheckedPosition(position);
                packageUnitBean = (PackageUnitBean) adapter.getData().get(position);
                startForResult(PackageDetailFragment.newInstance(packageUnitBean), REQUEST_CODE);
            }

        });
        mRecyclerView.setAdapter(mAdapter);

    }
    private void getData() {

               /* start:0
                limit:25
                bpartnerID:1019706
                showLot:true
                appSessionID:1541585528858.6*/
        PackageQueyInput packageQueyInput = new PackageQueyInput();
        packageQueyInput.setWarehouseId(String.valueOf(HttpClientHelper.getSelectWareHouse().getId()));
        if (!vetPackageNo.getText().toString().isEmpty()) {
            packageQueyInput.setLocatorId_text(vetPackageNo.getText().toString());
        }
        if (!tivetPackageNo.getText().toString().isEmpty()) {
            packageQueyInput.setProductName(tivetPackageNo.getText().toString());
        }
        if (!tietLocator.getText().toString().isEmpty()) {
            packageQueyInput.setVendorIdText(tietLocator.getText().toString());
        }
        if (mStatu != null) {
            packageQueyInput.setPackageType(mStatu.getId());
        }
        if (bpartnerBean != null && bpartnerBean.getId() > 0) {
            packageQueyInput.setVendorId(String.valueOf(bpartnerBean.getId()));
            // data.put("bpartnerBean", bpartnerBean.getId());
        }
        mPresenter.getData(packageQueyInput, list -> {
           mAdapter.addData(list);
        });
    }
    @Override
    protected void onBarcodeEventSuccess(String barcodeData) {
        vetPackageNo.setText(barcodeData);
        btQuery.performClick();
    }

    @Override
    public void onEnterAnimationEnd(Bundle savedInstanceState) {
        super.onEnterAnimationEnd(savedInstanceState);
        initRecyclerView();

    }

    @OnClick({R.id.bt_common_query})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.bt_common_query:
                showLoading();
                mAdapter.getData().clear();
                mAdapter.notifyDataSetChanged();
                mAdapter.setEnableLoadMore(true);
                pageBean.setStart(0);
                getData();
                break;
        }
    }


    @Override
    public void onFragmentResult(int requestCode, int resultCode, Bundle data) {
        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {
            if(itemPosition >=0&& mAdapter.getData().size()>0){
                if(vetPackageNo.getText().toString().isEmpty()){
                    vetPackageNo.setText(packageUnitBeans.get(itemPosition).getLocatorName());
                }
                getData();
            }
        }else if(requestCode == REFRESH_CODE){
            getData();
        }
    }


    @Override
    public void setEnableLoadMore(boolean isLoadMore) {
        mAdapter.setEnableLoadMore(isLoadMore);
    }

    @Override
    public void showTotal(String describe) {
        tvDescribe.setText(describe);
    }
}
