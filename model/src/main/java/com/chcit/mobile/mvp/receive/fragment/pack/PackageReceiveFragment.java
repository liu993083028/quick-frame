package com.chcit.mobile.mvp.receive.fragment.pack;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.chcit.custom.view.loader.QuickLoader;
import com.chcit.mobile.R;
import com.chcit.mobile.app.base.SupportToolbarFragement;
import com.chcit.mobile.di.component.DaggerScanPackageComponent;
import com.chcit.mobile.mvp.common.contract.ScanPackageContract;
import com.chcit.mobile.mvp.common.model.MenuTypeEnum;
import com.chcit.mobile.mvp.common.presenter.ScanPackagePresenter;
import com.chcit.mobile.mvp.receive.adpter.CheckReceiveFragmentAdapter;
import com.chcit.mobile.widget.SpdIndicator;
import com.jess.arms.base.app.Quick;
import com.jess.arms.di.component.AppComponent;
import butterknife.BindView;



public class PackageReceiveFragment extends SupportToolbarFragement<ScanPackagePresenter> implements ScanPackageContract.View {
    @BindView(R.id.check_receive_tabs)
    SpdIndicator mTab;

    @BindView(R.id.check_receive_fragment_pager)
    ViewPager mViewPager;

    protected MenuTypeEnum type;

    private void initView() {
        String[] contents = new String[2];
        switch (type){
            case PACKAGE_PUTAWAY: //上架
               contents[0] = "上架";
               contents[1] = "已上架";
                break;
            case PACKAGE_CHECK:
                contents[0] = "检品";
                contents[1] = "已检";
                break;
        }
        mTab.setTabTitles(contents);
        CheckReceiveFragmentAdapter checkReceiveFragmentAdapter = new CheckReceiveFragmentAdapter(getChildFragmentManager(),type,
                contents);
        mViewPager.setAdapter(checkReceiveFragmentAdapter);

        mTab.setViewPager(mViewPager,checkReceiveFragmentAdapter );

    }



    @Override
    public void setupFragmentComponent(@NonNull AppComponent appComponent) {
        DaggerScanPackageComponent //如找不到该类,请编译一下项目
                .builder()
                .appComponent(appComponent)
                .view(this)
                .build()
                .inject(this);
    }

    @Override
    public View initView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_check_receive, container, false);
    }

    @Override
    public void initData(@Nullable Bundle savedInstanceState) {
        initView();
    }

    @Override
    public void setData(@Nullable Object data) {

    }

    @Override
    public void showLoading() {
        QuickLoader.showLoading(mContext);
    }

    @Override
    public void hideLoading() {
        QuickLoader.showLoading(Quick.getApplicationContext());
    }

    @Override
    public void showMessage(@NonNull String message) {

    }

    @Override
    public void launchActivity(@NonNull Intent intent) {

    }

    @Override
    public void killMyself() {

    }

}