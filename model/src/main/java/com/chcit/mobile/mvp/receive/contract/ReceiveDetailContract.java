package com.chcit.mobile.mvp.receive.contract;

import com.alibaba.fastjson.JSONObject;
import com.chcit.mobile.mvp.entity.Locator;
import com.chcit.mobile.mvp.entity.Reason;
import com.chcit.mobile.mvp.entity.ResultBean;
import com.jess.arms.mvp.IView;
import com.jess.arms.mvp.IModel;
import java.security.cert.CertPathValidatorException;
import java.util.List;

import io.reactivex.Observable;


public interface ReceiveDetailContract {
    //对于经常使用的关于UI的方法可以定义到IView中,如显示隐藏进度条,和显示文字消息
    interface View extends IView {

    }

    //Model层定义接口,外部只需关心Model返回的数据,无需关心内部细节,即是否使用缓存
    interface Model extends IModel {
      Observable<Locator> getLocatorId(JSONObject json);
      Observable<ResultBean> doConfirm(String method,JSONObject json);
      //非包装验收确认
      Observable<ResultBean> checkAsnSubmit(JSONObject json);
      //非包装上架确认
      Observable<ResultBean> pickupAsnSubmit(JSONObject json);
      //按包装验视
      Observable<ResultBean> preCheckRecieve(JSONObject json);
      Observable<List<Locator>> getLocators(JSONObject json);
      Observable<List<Reason>> getReans(JSONObject json);
    }
}
