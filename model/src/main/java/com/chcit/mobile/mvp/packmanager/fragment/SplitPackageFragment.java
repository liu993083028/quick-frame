package com.chcit.mobile.mvp.packmanager.fragment;

import com.chcit.mobile.mvp.common.model.MenuTypeEnum;

public class SplitPackageFragment extends PackageManagerFragment{

    public SplitPackageFragment(){
        this.type = MenuTypeEnum.PACKAGE_SPLIT;
    }
}
