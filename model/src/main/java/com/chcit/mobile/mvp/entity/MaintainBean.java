package com.chcit.mobile.mvp.entity;

public class MaintainBean {

    /**
     * M_Conservation_ID : 1000817
     * M_Warehouse_ID : 1000216
     * WarehouseName : 江苏省中医院第三方仓库
     * ZoneName : 西药库
     * AD_Org_ID : 1000062
     * OrgName : 南京医药股份有限公司
     * M_Product_ID : 1099190
     * ProductName : 瑞舒伐他汀钙片(可定)
     * MedicineName : 瑞舒伐他汀钙片
     * ProductSpec : 10mg*7片
     * Manufacturer : 阿斯利康制药有限公司
     * UOMName : 盒
     * BaseUOMName : 片
     * ProductCode : 4494_6629
     * PricePO : 51.6
     * PriceList : 46.56
     * M_AttributeSetInstance_ID : 1068177
     * ReceiptDate : 2018-06-22 09:21:42.0
     * ConserveLevel : 0
     * ConservationDays : 90
     * ConservationDaysMin : 7
     * ConservationLeftDays : 30
     * M_Locator_ID : 1015275
     * LocatorName : 01Y02-05-01-01-05
     * Vendor_ID : 1021626
     * VendorName : 阿斯利康（无锡）贸易有限公司
     * Lot : 1801A63
     * GuaranteeDate : 2020-07-31
     * NeerGuaranteeDate : N
     * ConservationDate : 2018-11-09
     * StorageQty : 240
     * ConservationQty : 240
     * ConservationMeasure : 03
     * ConservationMeasureName : 除湿
     * CheckQty : 240
     * QtyScrapped : 0
     * CreatedUser : 仓库库管
     * ProductTypeName : 西药
     */

    private int M_Conservation_ID;
    private int M_Warehouse_ID;
    private String WarehouseName;
    private String ZoneName;
    private int AD_Org_ID;
    private String OrgName;
    private int M_Product_ID;
    private String ProductName;
    private String MedicineName;
    private String ProductSpec;
    private String Manufacturer;
    private String UOMName;
    private String BaseUOMName;
    private String ProductCode;
    private double PricePO;
    private double PriceList;
    private int M_AttributeSetInstance_ID;
    private String ReceiptDate;
    private int ConserveLevel;
    private int ConservationDays;
    private int ConservationDaysMin;
    private int ConservationLeftDays;
    private int M_Locator_ID;
    private String LocatorName;
    private int Vendor_ID;
    private String VendorName;
    private String Lot;
    private String GuaranteeDate;
    private String NeerGuaranteeDate;
    private String ConservationDate;
    private int StorageQty;
    private int ConservationQty;
    private String ConservationMeasure;
    private String ConservationMeasureName;
    private int CheckQty;
    private int QtyScrapped;
    private String CreatedUser;
    private String ProductTypeName;

    public int getM_Conservation_ID() {
        return M_Conservation_ID;
    }

    public void setM_Conservation_ID(int M_Conservation_ID) {
        this.M_Conservation_ID = M_Conservation_ID;
    }

    public int getM_Warehouse_ID() {
        return M_Warehouse_ID;
    }

    public void setM_Warehouse_ID(int M_Warehouse_ID) {
        this.M_Warehouse_ID = M_Warehouse_ID;
    }

    public String getWarehouseName() {
        return WarehouseName;
    }

    public void setWarehouseName(String WarehouseName) {
        this.WarehouseName = WarehouseName;
    }

    public String getZoneName() {
        return ZoneName;
    }

    public void setZoneName(String ZoneName) {
        this.ZoneName = ZoneName;
    }

    public int getAD_Org_ID() {
        return AD_Org_ID;
    }

    public void setAD_Org_ID(int AD_Org_ID) {
        this.AD_Org_ID = AD_Org_ID;
    }

    public String getOrgName() {
        return OrgName;
    }

    public void setOrgName(String OrgName) {
        this.OrgName = OrgName;
    }

    public int getM_Product_ID() {
        return M_Product_ID;
    }

    public void setM_Product_ID(int M_Product_ID) {
        this.M_Product_ID = M_Product_ID;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String ProductName) {
        this.ProductName = ProductName;
    }

    public String getMedicineName() {
        return MedicineName;
    }

    public void setMedicineName(String MedicineName) {
        this.MedicineName = MedicineName;
    }

    public String getProductSpec() {
        return ProductSpec;
    }

    public void setProductSpec(String ProductSpec) {
        this.ProductSpec = ProductSpec;
    }

    public String getManufacturer() {
        return Manufacturer;
    }

    public void setManufacturer(String Manufacturer) {
        this.Manufacturer = Manufacturer;
    }

    public String getUOMName() {
        return UOMName;
    }

    public void setUOMName(String UOMName) {
        this.UOMName = UOMName;
    }

    public String getBaseUOMName() {
        return BaseUOMName;
    }

    public void setBaseUOMName(String BaseUOMName) {
        this.BaseUOMName = BaseUOMName;
    }

    public String getProductCode() {
        return ProductCode;
    }

    public void setProductCode(String ProductCode) {
        this.ProductCode = ProductCode;
    }

    public double getPricePO() {
        return PricePO;
    }

    public void setPricePO(double PricePO) {
        this.PricePO = PricePO;
    }

    public double getPriceList() {
        return PriceList;
    }

    public void setPriceList(double PriceList) {
        this.PriceList = PriceList;
    }

    public int getM_AttributeSetInstance_ID() {
        return M_AttributeSetInstance_ID;
    }

    public void setM_AttributeSetInstance_ID(int M_AttributeSetInstance_ID) {
        this.M_AttributeSetInstance_ID = M_AttributeSetInstance_ID;
    }

    public String getReceiptDate() {
        return ReceiptDate;
    }

    public void setReceiptDate(String ReceiptDate) {
        this.ReceiptDate = ReceiptDate;
    }

    public int getConserveLevel() {
        return ConserveLevel;
    }

    public void setConserveLevel(int ConserveLevel) {
        this.ConserveLevel = ConserveLevel;
    }

    public int getConservationDays() {
        return ConservationDays;
    }

    public void setConservationDays(int ConservationDays) {
        this.ConservationDays = ConservationDays;
    }

    public int getConservationDaysMin() {
        return ConservationDaysMin;
    }

    public void setConservationDaysMin(int ConservationDaysMin) {
        this.ConservationDaysMin = ConservationDaysMin;
    }

    public int getConservationLeftDays() {
        return ConservationLeftDays;
    }

    public void setConservationLeftDays(int ConservationLeftDays) {
        this.ConservationLeftDays = ConservationLeftDays;
    }

    public int getM_Locator_ID() {
        return M_Locator_ID;
    }

    public void setM_Locator_ID(int M_Locator_ID) {
        this.M_Locator_ID = M_Locator_ID;
    }

    public String getLocatorName() {
        return LocatorName;
    }

    public void setLocatorName(String LocatorName) {
        this.LocatorName = LocatorName;
    }

    public int getVendor_ID() {
        return Vendor_ID;
    }

    public void setVendor_ID(int Vendor_ID) {
        this.Vendor_ID = Vendor_ID;
    }

    public String getVendorName() {
        return VendorName;
    }

    public void setVendorName(String VendorName) {
        this.VendorName = VendorName;
    }

    public String getLot() {
        return Lot;
    }

    public void setLot(String Lot) {
        this.Lot = Lot;
    }

    public String getGuaranteeDate() {
        return GuaranteeDate;
    }

    public void setGuaranteeDate(String GuaranteeDate) {
        this.GuaranteeDate = GuaranteeDate;
    }

    public String getNeerGuaranteeDate() {
        return NeerGuaranteeDate;
    }

    public void setNeerGuaranteeDate(String NeerGuaranteeDate) {
        this.NeerGuaranteeDate = NeerGuaranteeDate;
    }

    public String getConservationDate() {
        return ConservationDate;
    }

    public void setConservationDate(String ConservationDate) {
        this.ConservationDate = ConservationDate;
    }

    public int getStorageQty() {
        return StorageQty;
    }

    public void setStorageQty(int StorageQty) {
        this.StorageQty = StorageQty;
    }

    public int getConservationQty() {
        return ConservationQty;
    }

    public void setConservationQty(int ConservationQty) {
        this.ConservationQty = ConservationQty;
    }

    public String getConservationMeasure() {
        return ConservationMeasure;
    }

    public void setConservationMeasure(String ConservationMeasure) {
        this.ConservationMeasure = ConservationMeasure;
    }

    public String getConservationMeasureName() {
        return ConservationMeasureName;
    }

    public void setConservationMeasureName(String ConservationMeasureName) {
        this.ConservationMeasureName = ConservationMeasureName;
    }

    public int getCheckQty() {
        return CheckQty;
    }

    public void setCheckQty(int CheckQty) {
        this.CheckQty = CheckQty;
    }

    public int getQtyScrapped() {
        return QtyScrapped;
    }

    public void setQtyScrapped(int QtyScrapped) {
        this.QtyScrapped = QtyScrapped;
    }

    public String getCreatedUser() {
        return CreatedUser;
    }

    public void setCreatedUser(String CreatedUser) {
        this.CreatedUser = CreatedUser;
    }

    public String getProductTypeName() {
        return ProductTypeName;
    }

    public void setProductTypeName(String ProductTypeName) {
        this.ProductTypeName = ProductTypeName;
    }
}
