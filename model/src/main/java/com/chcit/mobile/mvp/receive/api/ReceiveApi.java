package com.chcit.mobile.mvp.receive.api;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.chcit.mobile.common.HttpMethodContains;
import com.chcit.mobile.mvp.entity.BaseData;
import com.chcit.mobile.mvp.entity.BaseResponse;
import com.chcit.mobile.mvp.entity.ResponseBean;
import com.chcit.mobile.mvp.entity.ResultBean;
import com.chcit.mobile.mvp.entity.ShipmentBean;
import com.chcit.mobile.mvp.receive.Entity.ImageUrl;

import java.util.List;
import java.util.Map;
import java.util.Observer;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.QueryMap;

public interface ReceiveApi {
    @GET(HttpMethodContains.Shipment.QUERY_DETAIL_URL)
    Observable<BaseData<ShipmentBean>> queryDetail(@QueryMap JSONObject query);

    @POST(HttpMethodContains.Receive.CHECK_ASN_LINE)
    @FormUrlEncoded
    Observable<ResultBean> checkAsnLine(@Body ReceiveInput query);

    @POST(HttpMethodContains.Receive.REJECT_OK)
    @FormUrlEncoded
    Observable<ResultBean> rejectReceivePackage(@FieldMap JSONObject param);

    @POST(HttpMethodContains.Receive.ASN_QUERY)
    @FormUrlEncoded
    Observable<BaseResponse> queryListData(@FieldMap JSONObject param);

    @POST(HttpMethodContains.Receive.ASN_LINE_TASK)
    @FormUrlEncoded
    Observable<String> queryAsnTask(@FieldMap JSONObject param);

    @POST(HttpMethodContains.Receive.ASN_PACKAGE_LIST)
    @FormUrlEncoded
    Observable<String> queryAsnPackageList(@FieldMap JSONObject param);

    @POST(HttpMethodContains.Receive.ASN_TASK_LINE_RE)
    @FormUrlEncoded
    Observable<BaseResponse> reCheckPackage(@Field("packageNo")String packageNo, @Field("asnId") String asnId);

    @Multipart
    @POST(HttpMethodContains.Receive.UPLOAD_IMG)
    Observable<ResultBean> uploadImg(@PartMap Map<String, RequestBody> map, @Part MultipartBody.Part[] parts);

    @FormUrlEncoded
    @POST(HttpMethodContains.Receive.QUERY_IMAGE_ID)
    Observable<BaseResponse> queryImageIds(@Field("asnId")String asnId);

    @Multipart
    @POST(HttpMethodContains.Receive.VIEW_ASN_PICTURE)
    Observable<BaseResponse> downImages(@PartMap Map<String, RequestBody> map,@Part MultipartBody.Part[] parts);

    @FormUrlEncoded
    @POST(HttpMethodContains.Receive.DELETE_ASN_PICTURE)
    Observable<BaseResponse> deleteImage(@Field("attachmentId")String id);
}
