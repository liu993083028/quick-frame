package com.chcit.mobile.mvp.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;
import java.util.List;

public class ShipmentBean implements Parcelable {


    /**
     * pickListMaId : 1000279
     * pickListId : 1001064
     * lot : wgj01
     * guaranteeDate : 2018-12-31
     * productId : 1037396
     * productName : 低分子肝素钙1
     * productSpec : aa
     * productCode : 2043
     * manufacturer : 葛兰素史克（天津）有限公司
     * uomName : 支
     * qtyTarget : 0.1
     * qtyPicked : 0
     * qtyLeft : 0.1
     * qtyCancelled : 0
     * fromLocatorId : 1010508
     * fromLocatorValue : 入库理货货位
     */

    private int pickListMaId;
    private int pickListId;
    private String lot;
    private String guaranteeDate;
    private int productId;
    private String productName;
    private String productSpec;
    private String manufacturer;
    private String productCode;
    private String uomName;
    private BigDecimal qtyTarget;
    private BigDecimal qtyPicked;
    private BigDecimal qtyLeft;
    private BigDecimal qtyCancelled;
    private int fromLocatorId;
    private String fromLocatorValue;
    private String OrgName ;
    private String QtyOnHand ;
    private String pickListJobId;
    private List<MonitorCode> supervisionCodes;//监管码
    private BigDecimal unitPackQty;
    private String orderType;
    private String orderTypeName;
    private String isControlledProduct;
    private String bpartnerName = "";

    /**
     * pickListJobId : 1000593
     * pickListNo : 1001314
     * qtyTarget : 1
     * qtyPicked : 0
     * qtyLeft : 1
     * qtyCancelled : 0
     * isStoragePackage : N
     * fromLocatorId : 1013769
     */

    private String pickListNo;
    private String isStoragePackage;

    public BigDecimal getUnitPackQty() {
        return unitPackQty;
    }

    public void setUnitPackQty(BigDecimal unitPackQty) {
        this.unitPackQty = unitPackQty;
    }

    @Override
    public String toString() {
        return "品名：" + productName + ", 待拣数量:" + qtyLeft;
    }

    public List<MonitorCode> getSupervisionCodes() {
        return supervisionCodes;
    }

    public void setSupervisionCodes(List<MonitorCode> supervisionCodes) {
        this.supervisionCodes = supervisionCodes;
    }

    public int getPickListMaId() {
        return pickListMaId;
    }

    public void setPickListMaId(int pickListMaId) {
        this.pickListMaId = pickListMaId;
    }

    public int getPickListId() {
        return pickListId;
    }

    public void setPickListId(int pickListId) {
        this.pickListId = pickListId;
    }

    public String getLot() {
        return lot;
    }

    public void setLot(String lot) {
        this.lot = lot;
    }

    public String getGuaranteeDate() {
        return guaranteeDate;
    }

    public void setGuaranteeDate(String guaranteeDate) {
        this.guaranteeDate = guaranteeDate;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductSpec() {
        return productSpec;
    }

    public void setProductSpec(String productSpec) {
        this.productSpec = productSpec;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getUomName() {
        return uomName;
    }

    public void setUomName(String uomName) {
        this.uomName = uomName;
    }

    public BigDecimal getQtyTarget() {
        return qtyTarget;
    }

    public void setQtyTarget(BigDecimal qtyTarget) {
        this.qtyTarget = qtyTarget;
    }

    public BigDecimal getQtyPicked() {
        return qtyPicked;
    }

    public void setQtyPicked(BigDecimal qtyPicked) {
        this.qtyPicked = qtyPicked;
    }

    public BigDecimal getQtyLeft() {
        return qtyLeft;
    }

    public void setQtyLeft(BigDecimal qtyLeft) {
        this.qtyLeft = qtyLeft;
    }

    public BigDecimal getQtyCancelled() {
        return qtyCancelled;
    }

    public void setQtyCancelled(BigDecimal qtyCancelled) {
        this.qtyCancelled = qtyCancelled;
    }

    public int getFromLocatorId() {
        return fromLocatorId;
    }

    public void setFromLocatorId(int fromLocatorId) {
        this.fromLocatorId = fromLocatorId;
    }

    public String getFromLocatorValue() {
        return fromLocatorValue;
    }

    public void setFromLocatorValue(String fromLocatorValue) {
        this.fromLocatorValue = fromLocatorValue;
    }

    public String getOrgName() {
        return OrgName;
    }

    public void setOrgName(String orgName) {
        OrgName = orgName;
    }

    public String getQtyOnHand() {
        return QtyOnHand;
    }

    public void setQtyOnHand(String qtyOnHand) {
        QtyOnHand = qtyOnHand;
    }

    public String getPickListJobId() {
        return pickListJobId;
    }

    public void setPickListJobId(String pickListJobId) {
        this.pickListJobId = pickListJobId;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getOrderTypeName() {
        return orderTypeName;
    }

    public void setOrderTypeName(String orderTypeName) {
        this.orderTypeName = orderTypeName;
    }

    public String getBpartnerName() {
        return bpartnerName;
    }

    public void setBpartnerName(String bpartnerName) {
        this.bpartnerName = bpartnerName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.pickListMaId);
        dest.writeInt(this.pickListId);
        dest.writeString(this.lot);
        dest.writeString(this.guaranteeDate);
        dest.writeInt(this.productId);
        dest.writeString(this.productName);
        dest.writeString(this.productSpec);
        dest.writeString(this.manufacturer);
        dest.writeString(this.productCode);
        dest.writeString(this.uomName);
        dest.writeSerializable(this.qtyTarget);
        dest.writeSerializable(this.qtyPicked);
        dest.writeSerializable(this.qtyLeft);
        dest.writeSerializable(this.qtyCancelled);
        dest.writeInt(this.fromLocatorId);
        dest.writeString(this.fromLocatorValue);
        dest.writeString(this.bpartnerName);
    }

    public ShipmentBean() {
    }

    protected ShipmentBean(Parcel in) {
        this.pickListMaId = in.readInt();
        this.pickListId = in.readInt();
        this.lot = in.readString();
        this.guaranteeDate = in.readString();
        this.productId = in.readInt();
        this.productName = in.readString();
        this.productSpec = in.readString();
        this.manufacturer = in.readString();
        this.productCode = in.readString();
        this.uomName = in.readString();
        this.qtyTarget = (BigDecimal) in.readSerializable();
        this.qtyPicked = (BigDecimal) in.readSerializable();
        this.qtyLeft = (BigDecimal) in.readSerializable();
        this.qtyCancelled = (BigDecimal) in.readSerializable();
        this.fromLocatorId = in.readInt();
        this.fromLocatorValue = in.readString();
        this.bpartnerName = in.readString();
    }

    public static final Creator<ShipmentBean> CREATOR = new Creator<ShipmentBean>() {
        @Override
        public ShipmentBean createFromParcel(Parcel source) {
            return new ShipmentBean(source);
        }

        @Override
        public ShipmentBean[] newArray(int size) {
            return new ShipmentBean[size];
        }
    };


    public String getPickListNo() {
        return pickListNo;
    }

    public void setPickListNo(String pickListNo) {
        this.pickListNo = pickListNo;
    }


    public String getIsStoragePackage() {
        return isStoragePackage;
    }

    public void setIsStoragePackage(String isStoragePackage) {
        this.isStoragePackage = isStoragePackage;
    }

    public String getIsControlledProduct() {
        return isControlledProduct;
    }

    public void setIsControlledProduct(String isControlledProduct) {
        this.isControlledProduct = isControlledProduct;
    }
}
