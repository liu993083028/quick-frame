package com.chcit.mobile.mvp.shipment.fragment;

import android.view.View;

import com.chcit.mobile.mvp.common.model.MenuTypeEnum;
import com.chcit.mobile.mvp.entity.RecipeBean;

import java.util.List;

public class PrescriptionShipmentFragment extends DispensingFragment {

    public static PrescriptionShipmentFragment newInstance(String preNo, List<RecipeBean> list) {
        PrescriptionShipmentFragment fragment = new PrescriptionShipmentFragment();
        fragment.preNo = preNo;
        fragment.unRecipeBeans = list;
        return fragment;
    }
    public PrescriptionShipmentFragment() {
        this.type = MenuTypeEnum.PRESCRIPTION_SHIPMENT;
    }
}
