package com.chcit.mobile.mvp.inventory.di.component;

import dagger.BindsInstance;
import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.chcit.mobile.mvp.inventory.di.module.MovenPackageModule;
import com.chcit.mobile.mvp.inventory.contract.MovenPackageContract;

import com.jess.arms.di.scope.FragmentScope;
import com.chcit.mobile.mvp.inventory.fragment.move.MovenPackageFragment;


/**
 * ================================================
 * Description:
 * <p>
 * Created by MVPArmsTemplate on 07/05/2019 10:35
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms">Star me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms/wiki">See me</a>
 * <a href="https://github.com/JessYanCoding/MVPArmsTemplate">模版请保持更新</a>
 * ================================================
 */
@FragmentScope
@Component(modules = MovenPackageModule.class, dependencies = AppComponent.class)
public interface MovenPackageComponent {
    void inject(MovenPackageFragment fragment);

    @Component.Builder
    interface Builder {
        @BindsInstance
        MovenPackageComponent.Builder view(MovenPackageContract.View view);

        MovenPackageComponent.Builder appComponent(AppComponent appComponent);

        MovenPackageComponent build();
    }
}