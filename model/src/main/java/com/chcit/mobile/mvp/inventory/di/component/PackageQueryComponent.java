package com.chcit.mobile.mvp.inventory.di.component;

import dagger.BindsInstance;
import dagger.Component;

import com.chcit.mobile.mvp.inventory.fragment.Stock.PackageDetailFragment;
import com.jess.arms.di.component.AppComponent;

import com.chcit.mobile.mvp.inventory.di.module.PackageQueryModule;
import com.chcit.mobile.mvp.inventory.contract.PackageQueryContract;

import com.jess.arms.di.scope.FragmentScope;
import com.chcit.mobile.mvp.inventory.fragment.Stock.PackageQueryFragment;


/**
 * ================================================
 * Description:
 * <p>
 * Created by MVPArmsTemplate on 07/18/2019 14:30
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms">Star me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms/wiki">See me</a>
 * <a href="https://github.com/JessYanCoding/MVPArmsTemplate">模版请保持更新</a>
 * ================================================
 */
@FragmentScope
@Component(modules = PackageQueryModule.class, dependencies = AppComponent.class)
public interface PackageQueryComponent {
    void inject(PackageQueryFragment fragment);
    void inject(PackageDetailFragment fragment);
    @Component.Builder
    interface Builder {
        @BindsInstance
        PackageQueryComponent.Builder view(PackageQueryContract.View view);

        PackageQueryComponent.Builder appComponent(AppComponent appComponent);

        PackageQueryComponent build();
    }
}