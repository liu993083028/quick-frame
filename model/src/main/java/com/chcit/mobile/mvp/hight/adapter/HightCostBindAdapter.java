package com.chcit.mobile.mvp.hight.adapter;

import androidx.annotation.Nullable;

import android.widget.ImageView;

import com.chad.library.adapter.base.BaseViewHolder;

import com.chcit.mobile.R;
import com.chcit.mobile.helper.recycler.BaseItemChangeQuickAdapter;
import com.chcit.mobile.mvp.entity.HightCostBindBean;


import java.util.List;

public class HightCostBindAdapter extends BaseItemChangeQuickAdapter<HightCostBindBean, BaseViewHolder> {

    private OnItemClickListener onImageClickListener;
    public HightCostBindAdapter(@Nullable List<HightCostBindBean> data) {
        super(R.layout.item_purchase_scanview,data);
    }

    @Override
    protected void convert(BaseViewHolder helper, HightCostBindBean item) {
        int i = helper.getAdapterPosition();
        if(thisPosition == i){
            helper.getView(R.id.ll_hight_item).setBackgroundColor(mContext.getResources().getColor(R.color.LavenderBlush));
        }else{
            helper.getView(R.id.ll_hight_item).setBackgroundColor(mContext.getResources().getColor(R.color.white));
        }
        helper.setText(R.id.tv_RFID,item.getValue());
        helper.setText(R.id.tv_pakageNo,item.getPackageNo());
        ImageView imageView = helper.getView(R.id.iv_purchase_scan_delete);
        imageView.setOnClickListener(view ->{
            if(onImageClickListener!=null){
                onImageClickListener.onItemClick(this,view,i);
            }
        });
    }

    public OnItemClickListener getOnImageClickListener() {
        return onImageClickListener;
    }

    public void setOnImageClickListener(OnItemClickListener onClickListener) {
        onImageClickListener = onClickListener;
    }
}
