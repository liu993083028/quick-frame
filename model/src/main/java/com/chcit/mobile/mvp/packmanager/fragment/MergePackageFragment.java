package com.chcit.mobile.mvp.packmanager.fragment;

import com.chcit.mobile.mvp.common.model.MenuTypeEnum;

public class MergePackageFragment extends PackageManagerFragment{

    public MergePackageFragment(){
        this.type = MenuTypeEnum.PACKAGE_MERGE;
    }
}
