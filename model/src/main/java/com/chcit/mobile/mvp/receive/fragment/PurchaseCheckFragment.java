package com.chcit.mobile.mvp.receive.fragment;

import com.chcit.mobile.mvp.common.model.MenuTypeEnum;

/**
 *采购入库页面
 */
public class PurchaseCheckFragment extends OldReceiveFragment {

    public PurchaseCheckFragment(){
        this.type = MenuTypeEnum.PURCHASE_CHECK;
    }
}
