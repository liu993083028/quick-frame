package com.chcit.mobile.mvp.inventory.di.module;

import dagger.Binds;
import dagger.Module;

import com.chcit.mobile.mvp.inventory.contract.MovenPackageContract;
import com.chcit.mobile.mvp.inventory.model.MovenPackageModel;


@Module
public abstract class MovenPackageModule {

    @Binds
    abstract MovenPackageContract.Model bindMovenPackageModel(MovenPackageModel model);
}