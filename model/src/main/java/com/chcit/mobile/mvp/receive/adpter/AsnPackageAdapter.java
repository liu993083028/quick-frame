package com.chcit.mobile.mvp.receive.adpter;

import androidx.annotation.Nullable;
import android.view.Gravity;

import com.chad.library.adapter.base.BaseViewHolder;
import com.chcit.mobile.R;
import com.chcit.mobile.helper.recycler.BaseItemChangeQuickAdapter;
import com.chcit.mobile.mvp.receive.Entity.AsnPackageBean;
import com.xuexiang.xui.widget.textview.badge.Badge;
import com.xuexiang.xui.widget.textview.badge.BadgeView;

import java.util.List;

public class AsnPackageAdapter extends BaseItemChangeQuickAdapter<AsnPackageBean,BaseViewHolder> {

    public AsnPackageAdapter(@Nullable List<AsnPackageBean> data) {
        super(R.layout.item_asn_package,data);
    }

    @Override
    protected void convert(BaseViewHolder helper, AsnPackageBean item) {
        int i = helper.getAdapterPosition();
        if(thisPosition == i){
            helper.getView(R.id.ll_asn_package_item).setBackgroundColor(mContext.getResources().getColor(R.color.LavenderBlush));
        }else if((i+2)%2 == 0){
            helper.getView(R.id.ll_asn_package_item).setBackgroundColor(mContext.getResources().getColor(R.color.gray_f2));
        }else{
            helper.getView(R.id.ll_asn_package_item).setBackgroundColor(mContext.getResources().getColor(R.color.white));
        }
        helper.setText(R.id.tv_asn_package_deliveryNo,item.getDeliveryNo());
        String s = "%s/批号%s/效期:%s/货位:%s";
        helper.setText(R.id.tv_asn_package_desc, String.format(s, item.getProductName(),item.getLot(),item.getGuaranteeDate(),item.getLocatorValue()));
        helper.setText(R.id.tv_asn_package_packageNo,item.getPackageNo());
        Badge badge=  (Badge) helper.getView(R.id.tv_asn_package_packageNo).getTag();;
        if(badge == null){
             badge= new BadgeView(mContext)
                    .bindTarget(helper.getView(R.id.tv_asn_package_packageNo))
                    .setBadgeGravity(Gravity.BOTTOM|Gravity.END);
             helper.getView(R.id.tv_asn_package_packageNo).setTag(badge);
        }

        if(item.getQtyRejected().intValue()>0){
            badge.setBadgeText("不合格");
        }else{
            badge.setBadgeText("合格");
        }

    }
}
