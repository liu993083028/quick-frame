package com.chcit.mobile.mvp.inventory.fragment.Stock;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.chcit.mobile.app.base.ScanToolQueryFragment;
import com.chcit.mobile.mvp.inventory.fragment.move.MovenPackageFragment;
import com.google.android.material.textfield.TextInputEditText;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chcit.custom.view.loader.QuickLoader;
import com.chcit.custom.view.spinner.DropDownMenu;
import com.chcit.mobile.R;
import com.chcit.mobile.helper.HttpClientHelper;
import com.chcit.mobile.helper.recycler.RecycleViewDivider;
import com.chcit.mobile.mvp.common.adapter.GirdDropDownAdapter;
import com.chcit.mobile.mvp.common.model.MenuTypeEnum;
import com.chcit.mobile.mvp.entity.BpartnerBean;
import com.chcit.mobile.mvp.entity.PackageBean;
import com.chcit.mobile.mvp.entity.PackageUnitBean;
import com.chcit.mobile.mvp.entity.PageBean;
import com.chcit.mobile.mvp.entity.Statu;
import com.chcit.mobile.mvp.inventory.adapter.PackageDetailAdapter;
import com.chcit.mobile.mvp.inventory.contract.PackageQueryContract;
import com.chcit.mobile.mvp.inventory.di.component.DaggerPackageQueryComponent;
import com.chcit.mobile.mvp.inventory.model.api.PackageDetailInput;
import com.chcit.mobile.mvp.inventory.presenter.PackageQueryPresenter;
import com.jess.arms.di.component.AppComponent;
import com.jess.arms.utils.ArmsUtils;
import com.xuexiang.xui.widget.toast.XToast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.OnClick;

import static com.jess.arms.utils.Preconditions.checkNotNull;


public class PackageDetailFragment extends ScanToolQueryFragment<PackageQueryPresenter> implements PackageQueryContract.View {

    @BindView(R.id.pr_inventory_dropDownMenu)
    DropDownMenu mDropDownMenu;
    @BindView(R.id.lv_inventory_list)
    RecyclerView mRecyclerView;
    @BindView(R.id.tv_inventory_describe)
    TextView tvDescribe;
    TextInputEditText etLot;
    protected MenuTypeEnum type ;
    private ArrayList<BpartnerBean> bpartners;
    private TextInputEditText tietProduct;
    private TextInputEditText tietVender;
    private BpartnerBean bpartnerBean;
    private Statu mStatu;
    private String headers[] = {"供应商", "包装状态","筛选"};
    private PackageUnitBean packageUnitBean;
    private PackageBean packageBean;
    private List<PackageBean> packageBeans = new ArrayList<>();
    protected PackageDetailAdapter mAdapter = new PackageDetailAdapter(packageBeans);;
    private int REQUEST_CODE = this.getClass().hashCode();
    PageBean pageBean = new PageBean();
    public static PackageDetailFragment newInstance(PackageUnitBean packageUnitBean) {
        PackageDetailFragment fragment = new PackageDetailFragment();
        fragment.packageUnitBean = packageUnitBean;
        return fragment;
    }

    @Override
    public void setupFragmentComponent(@NonNull AppComponent appComponent) {
        DaggerPackageQueryComponent //如找不到该类,请编译一下项目
                .builder()
                .appComponent(appComponent)
                .view(this)
                .build()
                .inject(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_inventory;
    }

    @Override
    public void initData(@Nullable Bundle savedInstanceState) {
        mCommonToolbar.setTitleText("包装查询");
        mCommonToolbar.getLeftMenuView(0).setOnClickListener(l -> {
            pop();
            l.setOnClickListener(null);
            if (getActivity() != null) {
                _mActivity.finish();
            }

        });
        vetPackageNo.setHint("请输入包装号");
        initDropDownMenu();

    }


    @Override
    public void setData(@Nullable Object data) {

    }

    @Override
    public void showLoading() {
        QuickLoader.showLoading(mContext);
    }

    @Override
    public void hideLoading() {
        QuickLoader.stopLoading();
    }

    @Override
    public void showMessage(@NonNull String message) {
        checkNotNull(message);
        XToast.info(mContext,message).show();
    }

    @Override
    public void launchActivity(@NonNull Intent intent) {
        checkNotNull(intent);
        ArmsUtils.startActivity(intent);
    }

    @Override
    public void killMyself() {
        pop();
        getActivity().finish();
    }

    private void initDropDownMenu() {
        final List<View> popupViews = new ArrayList<>();

        //供应商选择
        final ListView wareView = new ListView(getContext());
        bpartners = new ArrayList<>();
        bpartners.add(new BpartnerBean(1,"不限"));

        final GirdDropDownAdapter<BpartnerBean> bpartnerAdapter = new GirdDropDownAdapter<BpartnerBean>(getContext(), bpartners);
        wareView.setDividerHeight(0);
        wareView.setAdapter(bpartnerAdapter);
        wareView.setOnItemClickListener((parent, view, position, id) -> {
            bpartnerAdapter.setCheckItem(position);
            bpartnerBean = bpartners.get(position);
            if (position == 0) {
                bpartnerBean = null;
            }
            mDropDownMenu.setTabText(position == 0 ? headers[0] : bpartnerBean.getName());
            mDropDownMenu.closeMenu();
        });

        //存货状态
        final ListView dateView = new ListView(getContext());

        final List<Statu> status = new ArrayList<>();
        status.add(new Statu("", "不限"));
        final GirdDropDownAdapter<Statu> dateAdapter = new GirdDropDownAdapter<Statu>(getContext(), status);
        dateView.setDividerHeight(0);
        dateView.setAdapter(dateAdapter);
        dateView.setOnItemClickListener((parent, view, position, id) -> {
            dateAdapter.setCheckItem(position);
            mStatu = status.get(position);
            String text = null;
            if (position == 0) {
                mStatu = null;
                text = headers[1];
            }else{
                text = status.get(position).getName();
            }
            mDropDownMenu.setTabText(text);
            mDropDownMenu.closeMenu();
        });

        //筛选
        final View constellationView = getLayoutInflater().inflate(R.layout.pop_inventory, null);
        tietProduct = constellationView.findViewById(R.id.constellation);//编码或者品名
        constellationView.findViewById(R.id.til_inventory_lot).setVisibility(View.VISIBLE);
        etLot =  constellationView.findViewById(R.id.et_inventory_lot);
        tietVender = constellationView.findViewById(R.id.pop_inventory_locator);//厂家
        constellationView.findViewById(R.id.reset).setOnClickListener(v->{
            tietProduct.setText("");
            etLot.setText("");
            tietVender.setText("");
            ((TextView)mDropDownMenu.getTabView(4)).setTextColor(0xff111111);
        });
        Button ok = constellationView.findViewById(R.id.ok);
        ok.setOnClickListener(v -> {
            TextView sencodeTab = ((TextView)mDropDownMenu.getTabView(4));
            StringBuilder text = new StringBuilder();
            if(!tietProduct.getText().toString().isEmpty()){
                text.append(tietProduct.getText());
            }
            if(!tietVender.getText().toString().isEmpty()){
                text.append(tietVender.getText());
            }
            if(Objects.requireNonNull(etLot.getText()).length()>0){
                text.append(etLot.getText());
            }
            if(text.length() > 0 ){
                sencodeTab.setTextColor(R.attr.colorPrimary);
            }else{
                sencodeTab.setTextColor(0xff111111);
            }
            mDropDownMenu.closeMenu();
            btQuery.performClick();

        });
        popupViews.add(wareView);
        popupViews.add(dateView);
        popupViews.add(constellationView);
        final TextView contentView = new TextView(getContext());
        contentView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        contentView.setText("");
        //contentView.setBackgroundColor(getResources().getColor(R.color.gray));
        contentView.setGravity(Gravity.CENTER);
        contentView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        final List<String> list = Arrays.asList(headers);
        mDropDownMenu.setDropDownMenu(list, popupViews, contentView);
        mPresenter.getBpartners(bpartnerBeans -> {
            bpartners.addAll(bpartnerBeans);
            bpartnerAdapter.notifyDataSetChanged();
        });

        mPresenter.getStatus(1000466,status1 -> {
            status.addAll(status1);
            dateAdapter.notifyDataSetChanged();
        },false);
        if(packageUnitBean != null){
            tietProduct.setText(packageUnitBean.getProductCode());
            etLot.setText(packageUnitBean.getLot());
            ((TextView)mDropDownMenu.getTabView(4)).setTextColor(R.attr.colorPrimary);
            btQuery.performClick();
        }
    }

    private int itemPosition = -1;

    private void initRecyclerView() {

        //ArmsUtils.configRecyclerView(mRecyclerView, mLayoutManager);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.addItemDecoration(new RecycleViewDivider(getContext(), LinearLayoutManager.HORIZONTAL, R.drawable.item_drivide_style));
        mAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                mAdapter.loadMoreComplete();
                getData();
            }
        }, mRecyclerView);
        mAdapter.setOnItemClickListener((adapter, view, position) -> {
            if(position >=0 && adapter.getData().size()>position){
                itemPosition = position;
                mAdapter.setCheckedPosition(position);
                packageBean = (PackageBean) adapter.getData().get(position);
                startForResult(MovenPackageFragment.newInstance( packageBean), REQUEST_CODE);
            }

        });
        mAdapter.setEnableLoadMore(false);
        mRecyclerView.setAdapter(mAdapter);

    }
    private void getData() {

               /* start:0
                limit:25
                bpartnerID:1019706
                showLot:true
                appSessionID:1541585528858.6*/
        PackageDetailInput packageQueyInput = new PackageDetailInput();
        packageQueyInput.setPackageNo(vetPackageNo.getText().toString());
        packageQueyInput.setProductName(tietProduct.getText().toString());
        packageQueyInput.setVendor(tietVender.getText().toString());
        packageQueyInput.setWarehouseId(String.valueOf(HttpClientHelper.getSelectWareHouse().getId()));
        packageQueyInput.setLot(etLot.getText().toString());
        if(mStatu != null){
            packageQueyInput.setPackageStatus(mStatu.getId());
        }
        if (bpartnerBean != null && bpartnerBean.getId() > 0) {
            packageQueyInput.setVendorId(String.valueOf(bpartnerBean.getId()));
        }
        mPresenter.getPakcageInfo(pageBean,packageQueyInput, list -> {
            mAdapter.addData(list);
        });
    }
    @Override
    protected void onBarcodeEventSuccess(String barcodeData) {
        vetPackageNo.setText(barcodeData);
        btQuery.performClick();
    }

    @Override
    public void onEnterAnimationEnd(Bundle savedInstanceState) {
        super.onEnterAnimationEnd(savedInstanceState);
        initRecyclerView();

    }

    @OnClick({R.id.bt_common_query})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.bt_common_query:
                pageBean.setLimit(8000);
                mAdapter.getData().clear();
                mAdapter.notifyDataSetChanged();
                pageBean.setLimit(8000);
               // mPresenter.setmStart(0);
                getData();
                break;
        }
    }


    @Override
    public void onFragmentResult(int requestCode, int resultCode, Bundle data) {
        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {
            if(itemPosition >=0&& mAdapter.getData().size()>0){
                if(vetPackageNo.getText().toString().isEmpty()){
                    vetPackageNo.setText(packageBeans.get(itemPosition).getLocatorName());
                }
                // mPresenter.setmStart(0);
                getData();
            }

        }
    }


    @Override
    public void setEnableLoadMore(boolean isLoadMore) {
        mAdapter.setEnableLoadMore(isLoadMore);
    }

    @Override
    public void showTotal(String describe) {
        tvDescribe.setText(describe);
    }
}
