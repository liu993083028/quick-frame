package com.chcit.mobile.mvp.entity;

import com.alibaba.fastjson.annotation.JSONField;

import java.math.BigDecimal;

public class PackageUintBean {

    /**
     * unitPackQty : 0
     * storagePackageCount : 0
     */

    @JSONField(name = "unitPackQty")
    private BigDecimal unitPackQty;
    @JSONField(name = "storagePackageCount")
    private String storagePackageCount;
    private BigDecimal  packageCount;


    public BigDecimal getUnitPackQty() {
        return unitPackQty;
    }

    public void setUnitPackQty(BigDecimal unitPackQty) {
        this.unitPackQty = unitPackQty;
    }

    public String getStoragePackageCount() {
        return storagePackageCount;
    }

    public void setStoragePackageCount(String storagePackageCount) {
        this.storagePackageCount = storagePackageCount;
    }

    public BigDecimal getPackageCount() {
        return packageCount;
    }

    public void setPackageCount(BigDecimal packageCount) {
        this.packageCount = packageCount;
    }
}
