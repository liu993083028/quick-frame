package com.chcit.mobile.mvp.shipment.model;

import android.app.Application;

import com.alibaba.fastjson.JSONObject;
import com.chcit.mobile.mvp.common.api.service.UserService;
import com.chcit.mobile.mvp.entity.RecipeBean;
import com.chcit.mobile.mvp.entity.ResultBean;
import com.chcit.mobile.mvp.shipment.contract.DispensingContract;
import com.google.gson.Gson;
import com.jess.arms.di.scope.FragmentScope;
import com.jess.arms.integration.IRepositoryManager;
import com.jess.arms.mvp.BaseModel;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;


@FragmentScope
public class DispensingModel extends BaseModel implements DispensingContract.Model {
    @Inject
    Gson mGson;
    @Inject
    Application mApplication;

    @Inject
    public DispensingModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mGson = null;
        this.mApplication = null;
    }

    @Override
    public Observable<List<RecipeBean>> getData(String method, JSONObject json) {
        return mRepositoryManager.obtainRetrofitService(UserService.class)
                .request(method,json)
                .map(jsonObject -> {
                    ResultBean resultBean = mGson.fromJson(jsonObject.toJSONString(),ResultBean.class);
                    if(resultBean.isSuccess()){

                        return resultBean.getRows(RecipeBean.class);

                    }else{
                        throw new Exception("请求失败: "+resultBean.getMsg());
                    }

                }).subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<ResultBean> doConfirm(String method, JSONObject json) {
        return mRepositoryManager.obtainRetrofitService(UserService.class)
                .request(method,json)
                .map(jsonObject -> {
                    ResultBean resultBean = mGson.fromJson(jsonObject.toJSONString(),ResultBean.class);
                    if(resultBean.isSuccess()){
                        return resultBean;
                    }else{
                        throw new Exception("请求失败: "+resultBean.getMsg());
                    }

                }).subscribeOn(Schedulers.io());
    }
}