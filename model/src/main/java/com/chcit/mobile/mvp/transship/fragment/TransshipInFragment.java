package com.chcit.mobile.mvp.transship.fragment;

import com.chcit.mobile.mvp.common.model.MenuTypeEnum;

/**
 *转运out
 */
public class TransshipInFragment extends TransshipFragment {

    public TransshipInFragment(){
        this.type = MenuTypeEnum.TRANSSHIP_IN;
    }

}
