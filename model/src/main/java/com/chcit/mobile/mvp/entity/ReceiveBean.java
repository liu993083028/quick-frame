package com.chcit.mobile.mvp.entity;

import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;

public class ReceiveBean {


    /**
     * asnId : 1013729
     * asnLineId : 1050735
     * warehouseId : 1000214
     * warehouseName : 西药库
     * bpartnerName : 南京医药药业有限公司
     * asnNo : 1013729
     * taxInvoiceNo : test
     * deliveryNo : 1000297
     * dateArrived : 2018-08-30
     * docDate : 2018-08-28
     * lot : mazy
     * guaranteeDate : 2023-08-28
     * productionDate : 
     * productId : 1038578
     * productName : 丙戊酸钠缓释片<典泰>
     * isStoragePackage : N
     * productSpec : 1片
     * manufacturer : 江苏恒瑞医药股份有限公司
     * productCode : 3236
     * uomName : 片
     * qtyRejected : 0
     * qtyArrived : 12
     * QtyLeft : 0.4
     * qtyPutawayed : 0
     * qtyReceived : 0
     * qtyPutawayLeft : 12
     * PriceActual : 0.066667
     * OrderPrice : 0.066667
     * LineAmt : 0.8
     * IsExported : N
     * ASNType : PO
     * ASNTypeName : 采购
     * StorageStatus : S
     * StorageStatusName : 正常
     * LineStatus : N
     * LineStatusName : 待处理
     * IsControlledProduct : N
     * LPackageQty : 0
     * oddPackageCount : 0
     * unitPackageCount : 1
     * unitQty : 0
     */

    private int asnId;
    private int asnLineId;
    private int warehouseId;
    private String warehouseName;
    private String bpartnerName;
    private String asnNo;
    private String taxInvoiceNo;
    private String deliveryNo;
    private String dateArrived;
    private String docDate;
    private String lot;
    private String guaranteeDate;
    private String productionDate;
    private int productId;
    private String productName;
    private String isStoragePackage;
    private String productSpec;
    private String manufacturer;
    private String productCode;
    private String uomName;
    private BigDecimal qtyRejected = BigDecimal.ZERO;;
    private BigDecimal QtyLeft = BigDecimal.ZERO;
    private BigDecimal qtyReceived = BigDecimal.ZERO;;
    private BigDecimal PriceActual = BigDecimal.ZERO;;
    private BigDecimal OrderPrice = BigDecimal.ZERO;;
    private BigDecimal LineAmt = BigDecimal.ZERO;;
    private String IsExported;
    private String ASNType;
    private String ASNTypeName;
    private String StorageStatus;
    private String StorageStatusName;
    private String LineStatus;
    private String LineStatusName;
    private String IsControlledProduct;
    private int LPackageQty;
    private String oddPackageCount;
    private String unitPackageCount = "0";
    private String unitQty = "0";
    private  int oddQty;
    private String userName;//第二负责人
    private String rejectReasonName; //第二负责人

    private BigDecimal packageCountArrived = BigDecimal.ZERO;// 到货包数
    private BigDecimal qtyArrived = BigDecimal.ZERO;// 到货数量
    private BigDecimal packageCountCheckLeft  = BigDecimal.ZERO; //待验收包数
    private BigDecimal packageCountChecked = BigDecimal.ZERO; //已验收包数
    private BigDecimal qtyCheckLeft = BigDecimal.ZERO; //待验收数量
    private BigDecimal qtyChecked = BigDecimal.ZERO; //已验收数量
    private BigDecimal packageCountPutawayLeft  = BigDecimal.ZERO; //待上架包数
    private BigDecimal packageCountPutawayed = BigDecimal.ZERO; //已上架包数
    private BigDecimal qtyPutawayLeft = BigDecimal.ZERO; //待上架数量
    private BigDecimal qtyPutawayed = BigDecimal.ZERO; //已上架数量
    private BigDecimal packageCountRejected = BigDecimal.ZERO ;// 已拒包数

    //private BigDecimal  receivePackageNum;


    //private BigDecimal waitReceiveNum;// 待收数量

    //private BigDecimal receivedPackageNum;// 已收包数

    //private BigDecimal rejectedReceivePackageNum;// 拒收包数

    //private BigDecimal receivedNum;// 已收数量

    //private BigDecimal rejectedReceiveNum;// 拒收数量

    /**
     * qtyRejected : 10
     * qtyArrived : 10
     * QtyLeft : 0
     * packageCountRejected : 0
     * packageCountArrived : 0
     * packageCountPutawayLeft : 0
     * packageCountPutawayed : 0
     * qtyPutawayed : 0
     * qtyReceived : 0
     * qtyPutawayLeft : 0
     * rejectReasonName : 挤压变形
     * PriceActual : 4
     * OrderPrice : 0
     * LineAmt : 40
     * CheckerName : 仓库老师
     */

    private String CheckerName;
    private String isNeerGuarantee;
    private String error;

    @Override
    public String toString() {
        return   "品名：" + productName + ", 待收数量:" + qtyArrived;
    }

    public BigDecimal getPackageCountCheckLeft() {
        return packageCountCheckLeft;
    }

    public void setPackageCountCheckLeft(BigDecimal packageCountCheckLeft) {
        this.packageCountCheckLeft = packageCountCheckLeft;
    }

    public BigDecimal getPackageCountChecked() {
        return packageCountChecked;
    }

    public void setPackageCountChecked(BigDecimal packageCountChecked) {
        this.packageCountChecked = packageCountChecked;
    }

    public BigDecimal getQtyCheckLeft() {
        return qtyCheckLeft;
    }

    public void setQtyCheckLeft(BigDecimal qtyCheckLeft) {
        this.qtyCheckLeft = qtyCheckLeft;
    }

    public BigDecimal getQtyChecked() {
        return qtyChecked;
    }

    public void setQtyChecked(BigDecimal qtyChecked) {
        this.qtyChecked = qtyChecked;
    }

    public BigDecimal getPackageCountPutawayLeft() {
        return packageCountPutawayLeft;
    }

    public void setPackageCountPutawayLeft(BigDecimal packageCountPutawayLeft) {
        this.packageCountPutawayLeft = packageCountPutawayLeft;
    }

    public BigDecimal getPackageCountPutawayed() {
        if(packageCountPutawayed == null){
            return BigDecimal.ZERO;
        }
        return packageCountPutawayed;
    }

    public void setPackageCountPutawayed(BigDecimal packageCountPutawayed) {

        this.packageCountPutawayed = packageCountPutawayed;
    }

    public BigDecimal getQtyPutawayLeft() {
        return qtyPutawayLeft;
    }

    public void setQtyPutawayLeft(BigDecimal qtyPutawayLeft) {
        this.qtyPutawayLeft = qtyPutawayLeft;
    }

    public BigDecimal getQtyPutawayed() {
        return qtyPutawayed;
    }

    public void setQtyPutawayed(BigDecimal qtyPutawayed) {
        this.qtyPutawayed = qtyPutawayed;
    }

    public int getAsnId() {
        return asnId;
    }

    public void setAsnId(int asnId) {
        this.asnId = asnId;
    }

    public int getAsnLineId() {
        return asnLineId;
    }

    public void setAsnLineId(int asnLineId) {
        this.asnLineId = asnLineId;
    }

    public int getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(int warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public String getBpartnerName() {
        return bpartnerName;
    }

    public void setBpartnerName(String bpartnerName) {
        this.bpartnerName = bpartnerName;
    }

    public String getAsnNo() {
        return asnNo;
    }

    public void setAsnNo(String asnNo) {
        this.asnNo = asnNo;
    }

    public String getTaxInvoiceNo() {
        return taxInvoiceNo;
    }

    public void setTaxInvoiceNo(String taxInvoiceNo) {
        this.taxInvoiceNo = taxInvoiceNo;
    }

    public String getDeliveryNo() {
        return deliveryNo;
    }

    public void setDeliveryNo(String deliveryNo) {
        this.deliveryNo = deliveryNo;
    }

    public String getDateArrived() {
        return dateArrived;
    }

    public void setDateArrived(String dateArrived) {
        this.dateArrived = dateArrived;
    }

    public String getDocDate() {
        return docDate;
    }

    public void setDocDate(String docDate) {
        this.docDate = docDate;
    }

    public String getLot() {
        return lot;
    }

    public void setLot(String lot) {
        this.lot = lot;
    }

    public String getGuaranteeDate() {
        return guaranteeDate;
    }

    public void setGuaranteeDate(String guaranteeDate) {
        this.guaranteeDate = guaranteeDate;
    }

    public String getProductionDate() {
        return productionDate;
    }

    public void setProductionDate(String productionDate) {
        this.productionDate = productionDate;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getIsStoragePackage() {
        return isStoragePackage;
    }

    public void setIsStoragePackage(String isStoragePackage) {
        this.isStoragePackage = isStoragePackage;
    }

    public String getProductSpec() {
        return productSpec;
    }

    public void setProductSpec(String productSpec) {
        this.productSpec = productSpec;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getUomName() {
        return uomName;
    }

    public void setUomName(String uomName) {
        this.uomName = uomName;
    }

    public BigDecimal getQtyLeft() {
        return QtyLeft;
    }

    public void setQtyLeft(BigDecimal QtyLeft) {
        this.QtyLeft = QtyLeft;
    }


    public BigDecimal getQtyRejected() {
        if(qtyRejected == null){
            qtyRejected = BigDecimal.ZERO;
        }
        return qtyRejected;
    }

    public void setQtyRejected(BigDecimal qtyRejected) {
        this.qtyRejected = qtyRejected;
    }

    public BigDecimal getQtyArrived() {
        return qtyArrived;
    }

    public void setQtyArrived(BigDecimal qtyArrived) {
        this.qtyArrived = qtyArrived;
    }

    public BigDecimal getQtyReceived() {
        return qtyReceived;
    }

    public void setQtyReceived(BigDecimal qtyReceived) {
        this.qtyReceived = qtyReceived;
    }

    public BigDecimal getPriceActual() {
        return PriceActual;
    }

    public void setPriceActual(BigDecimal PriceActual) {
        this.PriceActual = PriceActual;
    }

    public BigDecimal getOrderPrice() {
        return OrderPrice;
    }

    public void setOrderPrice(BigDecimal OrderPrice) {
        this.OrderPrice = OrderPrice;
    }

    public BigDecimal getLineAmt() {
        return LineAmt;
    }

    public void setLineAmt(BigDecimal LineAmt) {
        this.LineAmt = LineAmt;
    }

    public String getIsExported() {
        return IsExported;
    }

    public void setIsExported(String IsExported) {
        this.IsExported = IsExported;
    }

    public String getASNType() {
        return ASNType;
    }

    public void setASNType(String ASNType) {
        this.ASNType = ASNType;
    }

    public String getASNTypeName() {
        return ASNTypeName;
    }

    public void setASNTypeName(String ASNTypeName) {
        this.ASNTypeName = ASNTypeName;
    }

    public String getStorageStatus() {
        return StorageStatus;
    }

    public void setStorageStatus(String StorageStatus) {
        this.StorageStatus = StorageStatus;
    }

    public String getStorageStatusName() {
        return StorageStatusName;
    }

    public void setStorageStatusName(String StorageStatusName) {
        this.StorageStatusName = StorageStatusName;
    }

    public String getLineStatus() {
        return LineStatus;
    }

    public void setLineStatus(String LineStatus) {
        this.LineStatus = LineStatus;
    }

    public String getLineStatusName() {
        return LineStatusName;
    }

    public void setLineStatusName(String LineStatusName) {
        this.LineStatusName = LineStatusName;
    }

    public String getIsControlledProduct() {
        return IsControlledProduct;
    }

    public void setIsControlledProduct(String IsControlledProduct) {
        this.IsControlledProduct = IsControlledProduct;
    }

    public int getLPackageQty() {
        return LPackageQty;
    }

    public void setLPackageQty(int LPackageQty) {
        this.LPackageQty = LPackageQty;
    }

    public String getOddPackageCount() {
        return oddPackageCount;
    }

    public void setOddPackageCount(String oddPackageCount) {
        this.oddPackageCount = oddPackageCount;
    }

    public String getUnitPackageCount() {
        return unitPackageCount;
    }

    public void setUnitPackageCount(String unitPackageCount) {
        this.unitPackageCount = unitPackageCount;
    }

    public String getUnitQty() {
        return unitQty;
    }

    public void setUnitQty(String unitQty) {
        this.unitQty = unitQty;
    }

    public int getOddQty() {
        return oddQty;
    }

    public void setOddQty(int oddQty) {
        this.oddQty = oddQty;
    }

    public BigDecimal getReceivePackageNum() {
        return packageCountArrived;
    }

    public BigDecimal getRejectedReceivePackageNum() {
        return packageCountRejected;
    }

    public void setRejectedReceivePackageNum(BigDecimal rejectedReceivePackageNum) {
        this.packageCountRejected = rejectedReceivePackageNum;
    }

    public BigDecimal getRejectedReceiveNum() {
        return qtyRejected;
    }


    public BigDecimal getPackageCountArrived() {
        return packageCountArrived;
    }

    public void setPackageCountArrived(BigDecimal packageCountArrived) {
        this.packageCountArrived = packageCountArrived;
    }

    public BigDecimal getPackageCountRejected() {
        if(packageCountRejected == null){
            return BigDecimal.ZERO;
        }
        return packageCountRejected;
    }

    public void setPackageCountRejected(BigDecimal packageCountRejected) {
        this.packageCountRejected = packageCountRejected;
    }


    public String getCheckerName() {
        return CheckerName;
    }

    public void setCheckerName(String CheckerName) {
        this.CheckerName = CheckerName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getRejectReasonName() {
        return rejectReasonName;
    }

    public void setRejectReasonName(String rejectReasonName) {
        this.rejectReasonName = rejectReasonName;
    }

    public String getIsNeerGuarantee() {
        return isNeerGuarantee;
    }

    public void setIsNeerGuarantee(String isNeerGuarantee) {
        this.isNeerGuarantee = isNeerGuarantee;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }


}
