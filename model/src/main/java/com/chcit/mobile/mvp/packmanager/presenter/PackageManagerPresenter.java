package com.chcit.mobile.mvp.packmanager.presenter;

import android.app.Application;

import com.alibaba.fastjson.JSONObject;
import com.chcit.mobile.common.SPDErrorHandle;
import com.chcit.mobile.mvp.entity.PackageBean;
import com.chcit.mobile.mvp.entity.PackageUintBean;
import com.chcit.mobile.mvp.packmanager.model.PackageManagerModel;
import com.jess.arms.integration.AppManager;
import com.jess.arms.di.scope.FragmentScope;
import com.jess.arms.mvp.BasePresenter;
import com.jess.arms.http.imageloader.ImageLoader;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import me.jessyan.rxerrorhandler.core.RxErrorHandler;

import javax.inject.Inject;

import com.chcit.mobile.mvp.packmanager.contract.PackageManagerContract;
import com.jess.arms.utils.RxLifecycleUtils;

import java.util.List;


@FragmentScope
public class PackageManagerPresenter extends BasePresenter<PackageManagerContract.Model, PackageManagerContract.View> {
    @Inject
    RxErrorHandler mErrorHandler;
    @Inject
    Application mApplication;
    @Inject
    ImageLoader mImageLoader;
    @Inject
    AppManager mAppManager;
    @Inject
    PackageManagerModel commonModel;
    @Inject
    public PackageManagerPresenter(PackageManagerContract.Model model, PackageManagerContract.View rootView) {
        super(model, rootView);
    }

//并包
    public void packageMergeConfirm(JSONObject json, Action onFinally) {
        commonModel.packageMergeConfirm(json)
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .doOnDispose(() -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                })
                .subscribe(new SPDErrorHandle<String>(mErrorHandler) {
                    @Override
                    public void onNext(String resultBean) {
                        try {
                            onFinally.run();
                        } catch (Exception e) {
                            onError(e);
                        }
                    }
                });

    }

//拆零
    public void packageSplitConfirm(JSONObject json, Action onFinally) {
        commonModel.packageSplitConfirm(json)
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .doOnDispose(() -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                })
                .subscribe(new SPDErrorHandle<String>(mErrorHandler) {
                    @Override
                    public void onNext(String resultBean) {
                        try {
                            onFinally.run();
                        } catch (Exception e) {
                            onError(e);
                        }
                    }
                });

    }


    /**
     * productId:1037396
     * warehouseId:1000214
     */
    public void getPakcageInfo(JSONObject paramReq, Consumer<List<PackageBean>> consumer){
        commonModel.queryPackageInfo(paramReq)
                .map(result -> result.toJavaList(PackageBean.class))
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                })
                .subscribe(new SPDErrorHandle<List<PackageBean>>(mErrorHandler) {
                    @Override
                    public void onNext(List<PackageBean> lists) {
                        try {
                            consumer.accept(lists);
                        } catch (Exception e) {
                            onError(e);
                        }
                    }
                });
    }

    public void getPackageUnit(JSONObject paramReq, Consumer<List<PackageUintBean>> consumer){
        commonModel.queryUnitPackQty(paramReq)
                .map(result -> result.toJavaList(PackageUintBean.class))
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .subscribe(new SPDErrorHandle<List<PackageUintBean>>(mErrorHandler) {
                    @Override
                    public void onNext(List<PackageUintBean> lists) {
                        try {
                            consumer.accept(lists);
                        } catch (Exception e) {
                            onError(e);
                        }
                    }
                });
    }



    //分包
    public void packageSubConfirm(JSONObject json, Action onFinally) {
        commonModel.packageSubConfirm(json)
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .doOnDispose(() -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                })
                .subscribe(new SPDErrorHandle<String>(mErrorHandler) {
                    @Override
                    public void onNext(String resultBean) {
                        try {
                            onFinally.run();
                        } catch (Exception e) {
                            onError(e);
                        }
                    }
                });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mErrorHandler = null;
        this.mAppManager = null;
        this.mImageLoader = null;
        this.mApplication = null;
        this.commonModel = null;
    }
}
