package com.chcit.mobile.mvp.inventory.model.api;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public  class PackageQueyInput {


    /**
     * sort : unitPackQtyText
     * dir : desc
     * productName : 213213213
     * lot : 12312
     * vendor : 123213
     * vendorId : 1004951
     * vendorId_text : 12321-test
     * packageType : C
     * packageType_text : 箱包装
     * warehouseId: 1000214
     * warehouseId_text: 西药库
     * locatorId: 1012532
     * locatorId_text: 01Z01-01-01-01-01
     * vendorId: 1004822
     * vendorId_text: 001-南京医药药业有限公司
     * productName: 2320
     * lot: zhb03
     * unitPackQty: 1
     * packageStatus: S
     * packageStatus_text: 在库
     * recievedDays: 10
     */
    private String warehouseId;
    private String sort;
    private String dir;
    private String productName;
    private String lot;
    private String vendor;
    private String vendorId;
    private String locatorId;
    private String vendorIdText;
    private String packageType;
    private String packageTypeText;
    private String locatorId_text;
    public String getLocatorId() {
        return locatorId;
    }

    public String getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(String warehouseId) {
        this.warehouseId = warehouseId;
    }

    public void setLocatorId(String locatorId) {
        this.locatorId = locatorId;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getDir() {
        return dir;
    }

    public void setDir(String dir) {
        this.dir = dir;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getLot() {
        return lot;
    }

    public void setLot(String lot) {
        this.lot = lot;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getVendorIdText() {
        return vendorIdText;
    }

    public void setVendorIdText(String vendorIdText) {
        this.vendorIdText = vendorIdText;
    }

    public String getPackageType() {
        return packageType;
    }

    public void setPackageType(String packageType) {
        this.packageType = packageType;
    }

    public String getPackageTypeText() {
        return packageTypeText;
    }

    public void setPackageTypeText(String packageTypeText) {
        this.packageTypeText = packageTypeText;
    }

    public String getLocatorId_text() {
        return locatorId_text;
    }

    public void setLocatorId_text(String locatorId_text) {
        this.locatorId_text = locatorId_text;
    }
    /* @Override
    public String toString() {
        //遍历sqspb类 成员为String类型 属性为空的全部替换为“/”
        StringBuilder text = new StringBuilder();
        Field[] fields = getClass().getDeclaredFields();
        try {
            for (int i = 0; i < fields.length; i++) {
                // 获取属性的名字
                String name = fields[i].getName();
                // 将属性的首字符大写，方便构造get，set方法
                name = name.substring(0, 1).toUpperCase() + name.substring(1);
                // 获取属性的类型
                String type = fields[i].getGenericType().toString();
                // 如果type是类类型，则前面包含"class "，后面跟类名
                if (type.equals("class java.lang.String")) {
                    Method m = getClass().getMethod("get" + name);
                    // 调用getter方法获取属性值
                    String value = (String) m.invoke(this);
                    if (value == null || value.equals("")) {
                       continue;
                    }
                    text.append(name).append(":").append(value).append(",");

                }
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        if(text.length() > 0){
            return text.substring(0,text.length()-1);
        }
        return text.toString();
    }
*/
}
