package com.chcit.mobile.mvp.entity;

import java.util.Objects;

public class Locator {

    /**
     * id
     * default
     * locatorId : 1013782
     * name : 666
     * sectionId : 1000097
     * zoneId : 1000181
     * value : 6666
     * x : 0
     * y : 0
     * z : 0
     * h : 0
     * locatorUseType : S
     */

    private int locatorId;
    private int id;
    private String name;
    private String sectionId;
    private String zoneId;
    private String value;
    private String x;
    private String y;
    private String z;
    private String h;
    private String locatorUseType;
    public Locator(){

    }
    public Locator( String name){
        this.name = name;
    }
    public Locator(int locatorId, String name, String value) {
        this.locatorId = locatorId;
        this.name = name;
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Locator locator = (Locator) o;
        return Objects.equals(name, locator.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name);
    }
    @Deprecated
    public int getLocatorId() {
        return locatorId;
    }
    @Deprecated
    public void setLocatorId(int locatorId) {
        this.locatorId = locatorId;
    }

    public void setId(int id) {
        this.id = id;
    }
    public int getId() {
      return this.id;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSectionId() {
        return sectionId;
    }

    public void setSectionId(String sectionId) {
        this.sectionId = sectionId;
    }

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getX() {
        return x;
    }

    public void setX(String x) {
        this.x = x;
    }

    public String getY() {
        return y;
    }

    public void setY(String y) {
        this.y = y;
    }

    public String getZ() {
        return z;
    }

    public void setZ(String z) {
        this.z = z;
    }

    public String getH() {
        return h;
    }

    public void setH(String h) {
        this.h = h;
    }

    public String getLocatorUseType() {
        return locatorUseType;
    }

    public void setLocatorUseType(String locatorUseType) {
        this.locatorUseType = locatorUseType;
    }

    @Override
    public String toString() {
        return  name ;
    }
}
