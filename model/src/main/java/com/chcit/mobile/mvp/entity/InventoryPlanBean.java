package com.chcit.mobile.mvp.entity;

public class InventoryPlanBean {


    /**
     * M_InventoryPlan_ID : 1000050
     * DocumentNo : 1000035
     * DocDate : 2018-11-19 10:57:00
     * AD_Client_ID : 1000002
     * AD_Org_ID : 1000062
     * OrgName : 南京医药股份有限公司
     * DocStatus : DR
     * DocStatusName : 新建
     * InventoryPlanType : 1
     * InventoryPlanTypeName : 批号盘存
     * CompleteTime :
     * CommitTime :
     * M_Warehouse_ID : 1000216
     * WarehouseName : 江苏省中医院第三方仓库
     */

    private int M_InventoryPlan_ID;
    private String DocumentNo;
    private String DocDate;
    private String AD_Client_ID;
    private String AD_Org_ID;
    private String OrgName;
    private String DocStatus;
    private String DocStatusName;
    private String InventoryPlanType;
    private String InventoryPlanTypeName;
    private String CompleteTime;
    private String CommitTime;
    private int M_Warehouse_ID;
    private String WarehouseName;

    public int getM_InventoryPlan_ID() {
        return M_InventoryPlan_ID;
    }

    public void setM_InventoryPlan_ID(int M_InventoryPlan_ID) {
        this.M_InventoryPlan_ID = M_InventoryPlan_ID;
    }

    public String getDocumentNo() {
        return DocumentNo;
    }

    public void setDocumentNo(String DocumentNo) {
        this.DocumentNo = DocumentNo;
    }

    public String getDocDate() {
        return DocDate;
    }

    public void setDocDate(String DocDate) {
        this.DocDate = DocDate;
    }

    public String getAD_Client_ID() {
        return AD_Client_ID;
    }

    public void setAD_Client_ID(String AD_Client_ID) {
        this.AD_Client_ID = AD_Client_ID;
    }

    public String getAD_Org_ID() {
        return AD_Org_ID;
    }

    public void setAD_Org_ID(String AD_Org_ID) {
        this.AD_Org_ID = AD_Org_ID;
    }

    public String getOrgName() {
        return OrgName;
    }

    public void setOrgName(String OrgName) {
        this.OrgName = OrgName;
    }

    public String getDocStatus() {
        return DocStatus;
    }

    public void setDocStatus(String DocStatus) {
        this.DocStatus = DocStatus;
    }

    public String getDocStatusName() {
        return DocStatusName;
    }

    public void setDocStatusName(String DocStatusName) {
        this.DocStatusName = DocStatusName;
    }

    public String getInventoryPlanType() {
        return InventoryPlanType;
    }

    public void setInventoryPlanType(String InventoryPlanType) {
        this.InventoryPlanType = InventoryPlanType;
    }

    public String getInventoryPlanTypeName() {
        return InventoryPlanTypeName;
    }

    public void setInventoryPlanTypeName(String InventoryPlanTypeName) {
        this.InventoryPlanTypeName = InventoryPlanTypeName;
    }

    public String getCompleteTime() {
        return CompleteTime;
    }

    public void setCompleteTime(String CompleteTime) {
        this.CompleteTime = CompleteTime;
    }

    public String getCommitTime() {
        return CommitTime;
    }

    public void setCommitTime(String CommitTime) {
        this.CommitTime = CommitTime;
    }

    public int getM_Warehouse_ID() {
        return M_Warehouse_ID;
    }

    public void setM_Warehouse_ID(int M_Warehouse_ID) {
        this.M_Warehouse_ID = M_Warehouse_ID;
    }

    public String getWarehouseName() {
        return WarehouseName;
    }

    public void setWarehouseName(String WarehouseName) {
        this.WarehouseName = WarehouseName;
    }
}
