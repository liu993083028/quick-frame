package com.chcit.mobile.mvp.inventory.contract;

import android.content.Context;

import com.alibaba.fastjson.JSONObject;
import com.chcit.mobile.mvp.entity.BaseData;
import com.chcit.mobile.mvp.entity.ResultBean;
import com.chcit.mobile.mvp.entity.StorageBean;
import com.chcit.mobile.mvp.inventory.entity.InventoryPackageBean;
import com.jess.arms.mvp.IView;
import com.jess.arms.mvp.IModel;

import io.reactivex.Observable;


public interface InventoryPlanContract {
    //对于经常使用的关于UI的方法可以定义到IView中,如显示隐藏进度条,和显示文字消息
    interface View extends IView {
        /**
         * 显示没有更多数据
         */
        void showNoMoreData();

        Context getActivity();
    }

    //Model层定义接口,外部只需关心Model返回的数据,无需关心内部细节,即是否使用缓存
    interface Model extends IModel {
        Observable<BaseData<InventoryPackageBean>> getData(JSONObject json);

    }
}
