package com.chcit.mobile.mvp.shipment.model;

import android.app.Application;

import com.alibaba.fastjson.JSONObject;
import com.chcit.mobile.common.HttpMethodContains;
import com.chcit.mobile.app.exception.BaseException;
import com.chcit.mobile.app.exception.CodeMsg;
import com.chcit.mobile.mvp.common.api.service.CommonService;
import com.chcit.mobile.mvp.common.api.service.UserService;
import com.chcit.mobile.mvp.entity.Locator;
import com.chcit.mobile.mvp.entity.MonitorCode;
import com.chcit.mobile.mvp.entity.ResultBean;
import com.google.gson.Gson;
import com.jess.arms.integration.IRepositoryManager;
import com.jess.arms.mvp.BaseModel;

import com.jess.arms.di.scope.FragmentScope;

import javax.inject.Inject;

import com.chcit.mobile.mvp.shipment.contract.ShipmentDetailContract;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;


@FragmentScope
public class ShipmentDetailModel extends BaseModel implements ShipmentDetailContract.Model {
    @Inject
    Gson mGson;
    @Inject
    Application mApplication;

    @Inject
    public ShipmentDetailModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mGson = null;
        this.mApplication = null;
    }

    //拣货确认
    @Override
    public Observable<ResultBean> pickComfirm(JSONObject json) {
        return mRepositoryManager.obtainRetrofitService(CommonService.class)
                .pickComfirm(json).subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<ResultBean> doConfirm(JSONObject json) {
        return mRepositoryManager.obtainRetrofitService(UserService.class)
                .request(HttpMethodContains.SHIPMENT_OK,json)
                .map(jsonObject -> {
                    ResultBean resultBean = mGson.fromJson(jsonObject.toJSONString(),ResultBean.class);
                    if(resultBean.isSuccess()){
                        return resultBean;
                    }else{
                        throw new Exception("请求失败: "+resultBean.getMsg());
                    }

                }).subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<ResultBean> doConfirm(String method, JSONObject json) {
        return mRepositoryManager.obtainRetrofitService(UserService.class)
                .request(method,json)
                .map(jsonObject -> {
                    ResultBean resultBean = mGson.fromJson(jsonObject.toJSONString(),ResultBean.class);
                    if(resultBean.isSuccess()){
                        return resultBean;
                    }else{
                        throw new Exception("请求失败: "+resultBean.getMsg());
                    }

                }).subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<List<Locator>> getLocators(JSONObject json) {
        return mRepositoryManager.obtainRetrofitService(UserService.class)
                .request(HttpMethodContains.LOCATORS,json)
                .map(jsonObject -> {
                    ResultBean resultBean = mGson.fromJson(jsonObject.toJSONString(),ResultBean.class);
                    if(resultBean.isSuccess()){
                        return resultBean.getRows(Locator.class);
                    }else{
                        throw new Exception("请求失败: "+resultBean.getMsg());
                    }

                }).subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<List<MonitorCode>> getMonitorCodes(JSONObject json) {
        return mRepositoryManager.obtainRetrofitService(CommonService.class)
                .getMonitorCodes(HttpMethodContains.SHIPMENT_CODE_QUERY,json)
                .map(responseBean -> {
                    if(responseBean.isSuccess()){
                        if(responseBean.getData().getTotal()>0){
                            return responseBean.getData().getRows();
                        }else{
                            throw new BaseException(CodeMsg.NO_DATE);
                        }

                    }else{
                        throw new Exception("请求失败: "+responseBean.getMsg());
                    }

                }).subscribeOn(Schedulers.io());
    }
}