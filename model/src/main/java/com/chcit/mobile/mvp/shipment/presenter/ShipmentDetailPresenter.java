package com.chcit.mobile.mvp.shipment.presenter;

import android.app.Application;

import com.alibaba.fastjson.JSONObject;
import com.chcit.mobile.common.HttpMethodContains;
import com.chcit.mobile.common.SPDErrorHandle;
import com.chcit.mobile.mvp.common.contract.CommonContract;
import com.chcit.mobile.mvp.entity.Locator;
import com.chcit.mobile.mvp.entity.MonitorCode;
import com.chcit.mobile.mvp.entity.ResultBean;
import com.jess.arms.integration.AppManager;
import com.jess.arms.di.scope.FragmentScope;
import com.jess.arms.mvp.BasePresenter;
import com.jess.arms.http.imageloader.ImageLoader;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import me.jessyan.rxerrorhandler.core.RxErrorHandler;

import javax.inject.Inject;

import com.chcit.mobile.mvp.shipment.contract.ShipmentDetailContract;
import com.jess.arms.utils.RxLifecycleUtils;

import java.util.List;


@FragmentScope
public class ShipmentDetailPresenter extends BasePresenter<ShipmentDetailContract.Model, ShipmentDetailContract.View> {
    @Inject
    RxErrorHandler mErrorHandler;
    @Inject
    Application mApplication;
    @Inject
    ImageLoader mImageLoader;
    @Inject
    AppManager mAppManager;
   @Inject
    CommonContract.Model commModel;
    public boolean isError = false;

    @Inject
    public ShipmentDetailPresenter(ShipmentDetailContract.Model model, ShipmentDetailContract.View rootView) {
        super(model, rootView);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mErrorHandler = null;
        this.mAppManager = null;
        this.mImageLoader = null;
        this.mApplication = null;
    }

    public void getLocators(JSONObject json, Consumer<List<Locator>> onNext) {

        mModel.getLocators(json)
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                }).subscribe(new SPDErrorHandle<List<Locator>>(mErrorHandler) {

            @Override
            public void onNext(List<Locator> list) {
                try {
                    onNext.accept(list);
                } catch (Exception e) {
                    onError(e);
                }
            }
        });
    }
    public void getMonitorCodes(int id, Consumer<List<MonitorCode>> onNext) {
        JSONObject data = new JSONObject();
        data.put("pickListMaId",id);
        commModel.getMonitorCodes(HttpMethodContains.SHIPMENT_CODE_QUERY,data)
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                }).subscribe(new SPDErrorHandle<List<MonitorCode>>(mErrorHandler) {

            @Override
            public void onNext(List<MonitorCode> list) {
                try {
                    onNext.accept(list);
                } catch (Exception e) {
                    onError(e);
                }
            }
        });
    }

    /**
     * 拣货确认
     *  {asnId:"1212",asnLineId:12123,locatorId:"1212",packageNo:["2323","232323dfdf"]}
     */
    public void pickComfirm(JSONObject paramReq, Action action) {
        mModel.pickComfirm(paramReq)
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                }).subscribe(new SPDErrorHandle<ResultBean>(mErrorHandler) {
            @Override
            public void onNext(ResultBean resultBean) {
                try {
                    if(resultBean.isSuccess()){
                        action.run();
                    }else{
                        String msg = resultBean.getMsg();
                        if(msg.isEmpty()){
                            msg = "未知错误！";
                        }
                        mRootView.showMessage(msg);
                    }

                } catch (Exception e) {
                    onError(e);
                }
            }

            @Override
            public void onError(Throwable t) {
                isError = true;
                super.onError(t);
            }
        });
    }

    public void doConfirm(JSONObject json, Action onFinally) {
        mModel.doConfirm(json)
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .doOnDispose(() -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                }).subscribe(new SPDErrorHandle<ResultBean>(mErrorHandler) {

            @Override
            public void onNext(ResultBean resultBean) {
                try {
                    onFinally.run();
                } catch (Exception e) {
                    onError(e);
                }
            }
        });

    }
    public void doConfirm(String method,JSONObject json, Action onFinally) {
        commModel.doConfirm(method,json)
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .doOnDispose(() -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                }).subscribe(new SPDErrorHandle<ResultBean>(mErrorHandler) {

            @Override
            public void onNext(ResultBean resultBean) {
                try {
                    onFinally.run();
                } catch (Exception e) {
                    onError(e);
                }
            }
            @Override
            public void onError(Throwable t) {
                isError = true;
                super.onError(t);
            }
        });

    }
}
