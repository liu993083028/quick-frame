package com.chcit.mobile.mvp.inventory.presenter;

import android.app.Application;
import com.alibaba.fastjson.JSONObject;
import com.chcit.mobile.app.exception.BaseException;
import com.chcit.mobile.common.SPDErrorHandle;
import com.chcit.mobile.mvp.common.contract.CommonContract;
import com.chcit.mobile.mvp.common.model.CommonModel;
import com.chcit.mobile.mvp.entity.BpartnerBean;
import com.chcit.mobile.mvp.entity.InventoryPlanBean;
import com.chcit.mobile.mvp.inventory.adapter.InventoryPlanQueryAdapter;
import com.chcit.mobile.mvp.inventory.entity.InventoryBean;
import com.jess.arms.integration.AppManager;
import com.jess.arms.di.scope.FragmentScope;
import com.jess.arms.mvp.BasePresenter;
import com.jess.arms.http.imageloader.ImageLoader;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import me.jessyan.rxerrorhandler.core.RxErrorHandler;

import javax.inject.Inject;
import javax.inject.Named;

import com.chcit.mobile.mvp.inventory.contract.InventoryPlanQueryContract;
import com.jess.arms.utils.RxLifecycleUtils;

import java.util.List;


@FragmentScope
public class InventoryPlanQueryPresenter extends BasePresenter<InventoryPlanQueryContract.Model, InventoryPlanQueryContract.View> {
    @Inject
    RxErrorHandler mErrorHandler;
    @Inject
    Application mApplication;
    @Inject
    ImageLoader mImageLoader;
    @Inject
    AppManager mAppManager;
    @Inject
    CommonContract.Model commModel;
    @Inject
    InventoryPlanQueryAdapter mAdapter;

    @Inject
    public InventoryPlanQueryPresenter(InventoryPlanQueryContract.Model model, InventoryPlanQueryContract.View rootView) {
        super(model, rootView);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mErrorHandler = null;
        this.mAppManager = null;
        this.mImageLoader = null;
        this.mApplication = null;
        this.commModel = null;
        this.mAdapter = null;

    }

    public void getData(JSONObject json, Consumer<List<InventoryBean>> onNext){
        mModel.getData(json)
                .map(result -> {
                    if(result.getTotal()>0){
                        return result.getRows();
                    }else{
                        throw  new BaseException("没有查询到盘点数据！");
                    }

                })
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                })
                .subscribe(new SPDErrorHandle<List<InventoryBean>>(mErrorHandler){

                    @Override
                    public void onNext(List<InventoryBean> list) {
                        try {
                            onNext.accept(list);
                        } catch (Exception e) {
                            onError(e);
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        super.onError(t);
                        mAdapter.notifyDataSetChanged();
                    }
                });
    }

    public void getBpartners( Consumer<List<BpartnerBean>> consumer){
        commModel.getBparenerBeans()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .subscribe(new SPDErrorHandle<List<BpartnerBean>>(mErrorHandler){

                    @Override
                    public void onNext(List<BpartnerBean> bpartnerBeans) {
                        try {
                            consumer.accept(bpartnerBeans);
                        } catch (Exception e) {
                            onError(e);
                        }
                    }
                });
    }
}
