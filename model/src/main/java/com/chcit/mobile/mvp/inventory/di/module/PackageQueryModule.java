package com.chcit.mobile.mvp.inventory.di.module;

import com.chcit.mobile.mvp.entity.PageBean;
import com.jess.arms.di.scope.FragmentScope;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

import com.chcit.mobile.mvp.inventory.contract.PackageQueryContract;
import com.chcit.mobile.mvp.inventory.model.PackageQueryModel;

@Module
public abstract class PackageQueryModule {

    @Binds
    abstract PackageQueryContract.Model bindPackageQueryModel(PackageQueryModel model);

    @FragmentScope
    @Provides
    static PageBean providePageBean(){
        return new PageBean();
    }
}