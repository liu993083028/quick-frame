package com.chcit.mobile.mvp.common.presenter;

import android.app.Application;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.chcit.mobile.common.SPDErrorHandle;
import com.chcit.mobile.mvp.common.contract.CommonContract;
import com.chcit.mobile.mvp.common.model.CommonModel;
import com.chcit.mobile.mvp.entity.RecipeBean;
import com.chcit.mobile.mvp.entity.ResultBean;
import com.chcit.mobile.mvp.hight.contract.HightCostContract;
import com.chcit.mobile.mvp.shipment.model.DispensingModel;
import com.jess.arms.di.scope.ActivityScope;
import com.jess.arms.di.scope.FragmentScope;
import com.jess.arms.integration.AppManager;
import com.jess.arms.mvp.BasePresenter;
import com.jess.arms.http.imageloader.ImageLoader;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import me.jessyan.rxerrorhandler.core.RxErrorHandler;

import javax.inject.Inject;

import com.chcit.mobile.mvp.common.contract.TaskContract;
import com.jess.arms.utils.RxLifecycleUtils;

import java.util.List;


@ActivityScope
public class TaskPresenter extends BasePresenter<TaskContract.Model, TaskContract.View> {
    @Inject
    RxErrorHandler mErrorHandler;
    @Inject
    Application mApplication;
    @Inject
    ImageLoader mImageLoader;
    @Inject
    AppManager mAppManager;
    @Inject
    CommonModel commonModel;

    @Inject
    public TaskPresenter(TaskContract.Model model, TaskContract.View rootView) {
        super(model, rootView);
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mErrorHandler = null;
        this.mAppManager = null;
        this.mImageLoader = null;
        this.mApplication = null;
    }



    //获取处方拣货 或者 医嘱拣货的数据
//    public void getPreOrDoctorDataList(String method,JSONObject json, Consumer<List<RecipeBean>> onNext){
//        dispensingModel.getData(method,json)
//                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
//                .observeOn(AndroidSchedulers.mainThread())
//                .doOnSubscribe(disposable -> {
//                    mRootView.showLoading();
//                })
//                .doFinally(() -> {
//                    mRootView.hideLoading();
//                })
//                .subscribe(new SPDErrorHandle<List<RecipeBean>>(mErrorHandler){
//
//                    @Override
//                    public void onNext(List<RecipeBean> list) {
//                        try {
//                            onNext.accept(list);
//                        } catch (Exception e) {
//                            onError(e);
//                        }
//                    }
//                });
//    }
    public void doConfirm(String method, JSONObject json, Action onFinally) {
        commonModel.doConfirm(method, json)
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .doOnDispose(() -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                }).subscribe(new SPDErrorHandle<ResultBean>(mErrorHandler) {

            @Override
            public void onNext(ResultBean resultBean) {
                try {
                    onFinally.run();
                } catch (Exception e) {
                    onError(e);
                }
            }
        });

    }

}
