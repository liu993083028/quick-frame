package com.chcit.mobile.mvp.common.contract;

import com.alibaba.fastjson.JSONObject;
import com.chcit.custom.view.loader.QuickLoader;
import com.chcit.mobile.mvp.entity.BaseResponse;
import com.chcit.mobile.mvp.entity.ResultBean;
import com.chcit.mobile.mvp.receive.Entity.AsnLineBean;
import com.chcit.mobile.mvp.receive.Entity.AsnPackageBean;
import com.chcit.mobile.mvp.receive.api.QueryListInput;
import com.jess.arms.base.app.Quick;
import com.jess.arms.mvp.IView;
import com.jess.arms.mvp.IModel;

import java.util.List;

import io.reactivex.Observable;


public interface ScanPackageContract {
    //对于经常使用的关于UI的方法可以定义到IView中,如显示隐藏进度条,和显示文字消息
    interface View extends IView {

    }

    //Model层定义接口,外部只需关心Model返回的数据,无需关心内部细节,即是否使用缓存
    interface Model extends IModel {
        Observable<String> queryAsnPackageDetail(JSONObject json);
        //1处调用
        Observable<JSONObject> request(String method,JSONObject json);
        //3处调用
        Observable<ResultBean> requestBySubmit(String method, JSONObject json);
        //2处调用
        Observable<String> requestOnString(String method,JSONObject json);
        //按包装验视
        Observable<ResultBean> inspectePackageSubmit(JSONObject json);
        //按包装验收
        Observable<ResultBean> packageReceiveSubmit(JSONObject json);
        //按包装上架
        Observable<ResultBean> pickupPackageSubmit(JSONObject json);
        //按包裝拒收
        Observable<ResultBean> requestRejectBySubmit(JSONObject json);
        //按包裝退回
        Observable<ResultBean> pickbackPackageSubmit(JSONObject json);
        //按包裝拣货确认
        Observable<ResultBean> packagePickComfirm(JSONObject json);
        //按包裝拒收入库确认
        Observable<ResultBean> packageRejctComfirm(JSONObject json);
        //查询到货通知行
        Observable<List<AsnLineBean>> queryAsnLineTask(QueryListInput queryListInput);
        //查询到货通知行
        Observable<List<AsnPackageBean>> queryAsnPackageList(QueryListInput queryListInput);
        //撤销验收行
        Observable<Boolean> reCheckTaskLine(List<String> packagNos,String asnId);

    }
}
