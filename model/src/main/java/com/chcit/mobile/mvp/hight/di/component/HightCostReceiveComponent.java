package com.chcit.mobile.mvp.hight.di.component;

import dagger.BindsInstance;
import dagger.Component;

import com.chcit.mobile.mvp.hight.contract.HightCostContract;
import com.jess.arms.di.component.AppComponent;

import com.chcit.mobile.mvp.hight.di.module.HightCostReceiveModule;

import com.jess.arms.di.scope.FragmentScope;
import com.chcit.mobile.mvp.hight.fragment.HightCostReceiveFragment;


/**
 * ================================================
 * Description:
 * <p>
 * Created by MVPArmsTemplate on 06/17/2019 08:49
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms">Star me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms/wiki">See me</a>
 * <a href="https://github.com/JessYanCoding/MVPArmsTemplate">模版请保持更新</a>
 * ================================================
 */
@FragmentScope
@Component(modules = HightCostReceiveModule.class, dependencies = AppComponent.class)
public interface HightCostReceiveComponent {
    void inject(HightCostReceiveFragment fragment);

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder view(HightCostContract.View view);

        Builder appComponent(AppComponent appComponent);

        HightCostReceiveComponent build();
    }
}