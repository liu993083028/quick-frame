package com.chcit.mobile.mvp.entity;

import com.alibaba.fastjson.JSONObject;

import java.io.Serializable;
import java.util.List;

public class BaseResponseBean<T> implements Serializable {

    /**
     * success : true
     * msg : 成功
     * data : {"total":7,"rows":[{"id":1000214,"name":"西药库","parentId":0},{"id":1000043,"name":"中草药房","parentId":1000214},{"id":1000201,"name":"门诊药房","parentId":1000214},{"id":1000200,"name":"住院药房","parentId":1000214},{"id":1000219,"name":"103科室A","parentId":0},{"id":1000220,"name":"药剂科","parentId":0},{"id":1000235,"name":"宗的仓库","parentId":1000214}]}
     */

    private boolean success;
    private String msg;
    private BaseData<T> data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public BaseData<T> getData() {
        return data;
    }

    public void setData(BaseData<T> data) {
        this.data = data;
    }


}
