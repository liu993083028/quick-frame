package com.chcit.mobile.mvp.inventory.contract;

import com.alibaba.fastjson.JSONObject;
import com.chcit.mobile.common.HttpMethodContains;
import com.chcit.mobile.mvp.entity.BaseData;
import com.chcit.mobile.mvp.entity.PackageUnitBean;
import com.chcit.mobile.mvp.entity.PageBean;
import com.chcit.mobile.mvp.entity.ShipmentBean;
import com.chcit.mobile.mvp.inventory.model.api.PackageQueyInput;
import com.jess.arms.mvp.IView;
import com.jess.arms.mvp.IModel;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;


public interface PackageQueryContract {
    //对于经常使用的关于UI的方法可以定义到IView中,如显示隐藏进度条,和显示文字消息
    interface View extends IView {
       void setEnableLoadMore(boolean isLoadMore);

        /**
         * 显示描述信息
         * @param describe 信息
         */
       void showTotal(String describe);
    }

    //Model层定义接口,外部只需关心Model返回的数据,无需关心内部细节,即是否使用缓存
    interface Model extends IModel {
        @GET(HttpMethodContains.Shipment.QUERY_DETAIL_URL)
        Observable<BaseData<PackageUnitBean>> getDataList(PageBean pageBean, PackageQueyInput packageQueyInput);
    }

}
