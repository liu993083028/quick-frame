package com.chcit.mobile.mvp.common.presenter;

import android.Manifest;
import android.app.Application;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.fragment.app.Fragment;
import androidx.core.app.ComponentActivity;

import com.jess.arms.integration.AppManager;
import com.jess.arms.di.scope.ActivityScope;
import com.jess.arms.mvp.BasePresenter;
import com.jess.arms.http.imageloader.ImageLoader;

import me.jessyan.rxerrorhandler.core.RxErrorHandler;

import javax.inject.Inject;
import com.chcit.mobile.mvp.common.contract.LoginContract;
import com.jess.arms.utils.ArmsUtils;
import com.jess.arms.utils.PermissionUtil;

import java.util.List;


@ActivityScope
public class LoginPresenter extends BasePresenter<LoginContract.Model, LoginContract.View> {
    @Inject
    RxErrorHandler mErrorHandler;
    @Inject
    Application mApplication;
    @Inject
    ImageLoader mImageLoader;
    @Inject
    AppManager mAppManager;

    @Inject
    public LoginPresenter(LoginContract.Model model, LoginContract.View rootView) {
        super(model, rootView);
    }
    /**
     * 使用 2017 Google IO 发布的 Architecture Components 中的 Lifecycles 的新特性 (此特性已被加入 Support library)
     * 使 {@code Presenter} 可以与 {@link ComponentActivity} 和 {@link Fragment} 的部分生命周期绑定
     */
   @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    void onCreate() {
        requestUsers(true);//打开 App 时自动加载列表
    }

    public void requestUsers(final boolean pullToRefresh) {
        //这个请求事件我写在点击事件里面，
        //点击button之后RxPermissions会为我们申请运行时权限
      /* Disposable disposable = mRootView.getRxPermissions().request(
               Manifest.permission.WRITE_EXTERNAL_STORAGE,
               Manifest.permission.READ_EXTERNAL_STORAGE,
               Manifest.permission.ACCESS_COARSE_LOCATION
       ).subscribe( new Consumer<Boolean>() {
            @Override
            public void accept(Boolean granted) throws Exception {
                if (!granted) {
                    mRootView.toLogin();
                }else{
                    ArmsUtils.exitApp();
                }
                //不管是否获取全部权限，进入主页面

            }
        });
*/


        //请求外部存储权限用于适配android6.0的权限管理机制
       PermissionUtil.requestPermission(new PermissionUtil.RequestPermission() {
            @Override
            public void onRequestPermissionSuccess() {
                //request permission success, do something.
                mRootView.toLogin();
            }

            @Override
            public void onRequestPermissionFailure(List<String> permissions) {
                mRootView.showMessage("Request permissions failure，即将退出系统！");
                ArmsUtils.exitApp();

            }

            @Override
            public void onRequestPermissionFailureWithAskNeverAgain(List<String> permissions) {
                mRootView.showMessage("Need to go to the settings");

            }
        }, mRootView.getRxPermissions(), mErrorHandler,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mErrorHandler = null;
        this.mAppManager = null;
        this.mImageLoader = null;
        this.mApplication = null;
    }



}
