package com.chcit.mobile.mvp.hight.di.module;

import dagger.Binds;
import dagger.Module;

import com.chcit.mobile.mvp.hight.contract.HightCostContract;
import com.chcit.mobile.mvp.hight.model.HightCostModel;


@Module
public abstract class HightCostModule {

    @Binds
    abstract HightCostContract.Model bindHightCostModel(HightCostModel model);
}