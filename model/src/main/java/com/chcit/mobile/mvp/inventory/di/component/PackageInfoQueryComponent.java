package com.chcit.mobile.mvp.inventory.di.component;

import dagger.BindsInstance;
import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.chcit.mobile.mvp.inventory.di.module.PackageInfoQueryModule;
import com.chcit.mobile.mvp.inventory.contract.PackageInfoQueryContract;

import com.jess.arms.di.scope.FragmentScope;
import com.chcit.mobile.mvp.inventory.fragment.Stock.PackageInfoQueryFragment;


/**
 * ================================================
 * Description:
 * <p>
 * Created by MVPArmsTemplate on 09/18/2019 16:50
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms">Star me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms/wiki">See me</a>
 * <a href="https://github.com/JessYanCoding/MVPArmsTemplate">模版请保持更新</a>
 * ================================================
 */
@FragmentScope
@Component(modules = PackageInfoQueryModule.class, dependencies = AppComponent.class)
public interface PackageInfoQueryComponent {
    void inject(PackageInfoQueryFragment fragment);

    @Component.Builder
    interface Builder {
        @BindsInstance
        PackageInfoQueryComponent.Builder view(PackageInfoQueryContract.View view);

        PackageInfoQueryComponent.Builder appComponent(AppComponent appComponent);

        PackageInfoQueryComponent build();
    }
}