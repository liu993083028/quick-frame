package com.chcit.mobile.mvp.common.presenter;

import android.app.Application;

import com.alibaba.fastjson.JSONObject;
import com.chcit.mobile.common.SPDErrorHandle;
import com.chcit.mobile.mvp.common.contract.CommonContract;
import com.chcit.mobile.mvp.common.model.CommonModel;
import com.chcit.mobile.mvp.entity.ResultBean;
import com.chcit.mobile.mvp.entity.Statu;
import com.jess.arms.integration.AppManager;
import com.jess.arms.di.scope.FragmentScope;
import com.jess.arms.mvp.BasePresenter;
import com.jess.arms.http.imageloader.ImageLoader;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import me.jessyan.rxerrorhandler.core.RxErrorHandler;
import me.jessyan.rxerrorhandler.handler.ErrorHandleSubscriber;

import javax.inject.Inject;
import javax.inject.Named;

import com.chcit.mobile.mvp.common.contract.MaintainDetailContract;
import com.jess.arms.utils.RxLifecycleUtils;

import java.util.List;


@FragmentScope
public class MaintainDetailPresenter extends BasePresenter<MaintainDetailContract.Model, MaintainDetailContract.View> {
    @Inject
    RxErrorHandler mErrorHandler;
    @Inject
    Application mApplication;
    @Inject
    ImageLoader mImageLoader;
    @Inject
    AppManager mAppManager;


    @Inject
    CommonContract.Model commModel;
    @Inject
    public MaintainDetailPresenter(MaintainDetailContract.Model model, MaintainDetailContract.View rootView) {
        super(model, rootView);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mErrorHandler = null;
        this.mAppManager = null;
        this.mImageLoader = null;
        this.mApplication = null;
    }

    public void querySpinnerData( int id,Consumer<List<Statu>> consumer){
        commModel.refList(id,false)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .subscribe(new SPDErrorHandle<List<Statu>>(mErrorHandler){

                    @Override
                    public void onNext(List<Statu> bpartnerBeans) {
                        try {
                            bpartnerBeans.add(new Statu("",""));
                            consumer.accept(bpartnerBeans);
                        } catch (Exception e) {
                            onError(e);
                        }
                    }
                });
    }

    public void doConfirm(JSONObject json,Action action){
        mModel.doConfirm(json)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                }).subscribe(new SPDErrorHandle<ResultBean>(mErrorHandler) {

            @Override
            public void onNext(ResultBean resultBean) {

                try {
                    if(resultBean.isSuccess()){
                        action.run();
                    }else{
                        mRootView.showMessage(resultBean.getMsg());
                    }

                } catch (Exception e) {
                    onError(e);
                }
            }
        });
    }
}
