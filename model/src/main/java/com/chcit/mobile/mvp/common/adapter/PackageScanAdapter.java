package com.chcit.mobile.mvp.common.adapter;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chcit.mobile.R;
import com.chcit.mobile.mvp.entity.PackageBean;

import java.util.List;

public class PackageScanAdapter extends BaseQuickAdapter<PackageBean,BaseViewHolder> {

    //先声明一个int成员变量
    private int thisPosition = -1;
    //再定义一个int类型的返回值方法
    public int getthisPosition() {
        return thisPosition;
    }
    //其次定义一个方法用来绑定当前参数值的方法
    //此方法是在调用此适配器的地方调用的，此适配器内不会被调用到
    public void setThisPosition(int thisPosition) {
        this.thisPosition = thisPosition;
    }

    private OnClickListener onClickListener;//删除按钮

    public PackageScanAdapter(@Nullable List<PackageBean> data) {
        super(R.layout.item_package_scan_list,data);
    }

    @Override
    protected void convert(BaseViewHolder helper, PackageBean item) {
        int i = helper.getAdapterPosition();
        if(thisPosition == i){
            helper.getView(R.id.ll_package_scan_item).setBackgroundColor(mContext.getResources().getColor(R.color.Moccasin));
        }else if((i+2)%2 == 0){
            helper.getView(R.id.ll_package_scan_item).setBackgroundColor(mContext.getResources().getColor(R.color.gray_f2));
        }else{
            helper.getView(R.id.ll_package_scan_item).setBackgroundColor(mContext.getResources().getColor(R.color.white));
        }

        helper.setText(R.id.tv_package_scan_no,String.valueOf(i+1));
        helper.setText(R.id.tv_package_scan_packageNo,item.getPackageNo());
        if(item.getUnitPackQty()!= null ){
            helper.setText(R.id.tv_package_scan_unitQty,item.getUnitPackQty().toString());
        }
        AppCompatImageView ivDelete = helper.<AppCompatImageView>getView(R.id.image_scan_delete);
        if(onClickListener!= null){
            ivDelete.setOnClickListener(l->{
                onClickListener.onClick(item,helper.getAdapterPosition());
            });
        }else{
            ivDelete.setVisibility(View.GONE);
        }


    }

    public void setOnclickListener(OnClickListener onClickListener){
        this.onClickListener = onClickListener;
    }
    public interface OnClickListener {
        /**
         * Called when a view has been clicked.
         *
         * @param v The view that was clicked.
         */
        void onClick(PackageBean v, int position);
    }
}
