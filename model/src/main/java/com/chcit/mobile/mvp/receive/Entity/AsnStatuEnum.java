package com.chcit.mobile.mvp.receive.Entity;

import android.util.SparseArray;
public enum AsnStatuEnum {
    SHANG_JIA(1,"采购上架"),
    YAN_SHOU(2,"采购验收"),
    REJECT(3,"拒收")
    ;
    private static final SparseArray<String> menus = new SparseArray<>();
    static {
        for (AsnStatuEnum refer : AsnStatuEnum.values()) {
            menus.put(refer.getId(), refer.getDesc());
        }
    }
    private int id;
    private String desc;

    AsnStatuEnum(int i, String desc) {
        this.id = i;
        this.desc = desc;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDesc(){
        return desc;
    }

    public void setDesc(String name) {
        this.desc = name;
    }

    public static String getAsnTypeName(int i){
        return menus.get(i);
    }
}
