package com.chcit.mobile.mvp.inventory.di.module;

import dagger.Binds;
import dagger.Module;

import com.chcit.mobile.mvp.inventory.contract.MovementPlanContract;
import com.chcit.mobile.mvp.inventory.model.MovementPlanModel;


/**
 * ================================================
 * Description:
 * <p>
 * Created by MVPArmsTemplate on 09/27/2019 08:45
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms">Star me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms/wiki">See me</a>
 * <a href="https://github.com/JessYanCoding/MVPArmsTemplate">模版请保持更新</a>
 * ================================================
 */
@Module
public abstract class MovementPlanModule {

    @Binds
    abstract MovementPlanContract.Model bindMovementPlanModel(MovementPlanModel model);

}