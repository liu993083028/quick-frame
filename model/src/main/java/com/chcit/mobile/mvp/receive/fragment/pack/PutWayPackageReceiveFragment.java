package com.chcit.mobile.mvp.receive.fragment.pack;

import com.chcit.mobile.mvp.common.model.MenuTypeEnum;

public class PutWayPackageReceiveFragment extends PackageReceiveFragment{

    public PutWayPackageReceiveFragment() {
        this.type = MenuTypeEnum.PACKAGE_PUTAWAY;
    }
}
