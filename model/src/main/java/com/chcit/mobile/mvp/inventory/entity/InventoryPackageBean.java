package com.chcit.mobile.mvp.inventory.entity;

import com.alibaba.fastjson.annotation.JSONField;

import java.math.BigDecimal;

public class InventoryPackageBean {

    /**
     * inventoryPlanLineId : 1012103
     * inventoryPlanId : 1000121
     * productId : 1037396
     * productCode : 2043
     * productName : 低分子肝素钙1
     * productSpec : 0.4ml:4100iu AXa/支
     * manufacturer : 葛兰素史克（天津）有限公司
     * uomId : 1000266
     * uomName : ml
     * baseUomQty : 1
     * lot : wgj01
     * guaranteeDate : 2018-12-31
     * productionDate :
     * qtyBook : 90
     * qtyCount : 0
     * qtyDiff : -90
     * diffAmt : -990
     * packageQtyBook : 0
     * packageQtyCount : 0
     * packageQtyDiff : 0
     * price : 11
     * lineAmt : 990
     * storageStatusName : 正常
     * locatorValue : 入库理货货位
     * locatorName : 入库理货货位
     * unitPackQty : 0
     */

    @JSONField(name = "inventoryPlanLineId")
    private int inventoryPlanLineId;
    @JSONField(name = "inventoryPlanId")
    private int inventoryPlanId;
    @JSONField(name = "productId")
    private int productId;
    @JSONField(name = "productCode")
    private String productCode;
    @JSONField(name = "productName")
    private String productName;
    @JSONField(name = "productSpec")
    private String productSpec;
    @JSONField(name = "manufacturer")
    private String manufacturer;
    @JSONField(name = "uomId")
    private int uomId;
    @JSONField(name = "uomName")
    private String uomName;
    @JSONField(name = "baseUomQty")
    private String baseUomQty;
    @JSONField(name = "lot")
    private String lot;
    @JSONField(name = "guaranteeDate")
    private String guaranteeDate;
    @JSONField(name = "productionDate")
    private String productionDate;
    @JSONField(name = "qtyBook")
    private BigDecimal qtyBook = BigDecimal.ZERO;
    @JSONField(name = "qtyCount")
    private BigDecimal qtyCount = BigDecimal.ZERO;;
    @JSONField(name = "qtyDiff")
    private BigDecimal qtyDiff = BigDecimal.ZERO;;
    @JSONField(name = "diffAmt")
    private BigDecimal diffAmt = BigDecimal.ZERO;;;
    @JSONField(name = "packageQtyBook")
    private BigDecimal packageQtyBook = BigDecimal.ZERO;;
    @JSONField(name = "packageQtyCount")
    private BigDecimal packageQtyCount = BigDecimal.ZERO;;
    @JSONField(name = "packageQtyDiff")
    private BigDecimal packageQtyDiff;
    @JSONField(name = "price")
    private BigDecimal price;
    
    @JSONField(name = "lineAmt")
    private BigDecimal lineAmt;

    @JSONField(name = "storageStatusName")
    private String storageStatusName;

    @JSONField(name = "locatorValue")
    private String locatorValue;

    @JSONField(name = "locatorName")
    private String locatorName;

    @JSONField(name = "unitPackQty")
    private int unitPackQty;

    @JSONField(name = "isStoragePackage")
    private String isStoragePackage;

    public int getInventoryPlanLineId() {
        return inventoryPlanLineId;
    }

    public void setInventoryPlanLineId(int inventoryPlanLineId) {
        this.inventoryPlanLineId = inventoryPlanLineId;
    }

    public int getInventoryPlanId() {
        return inventoryPlanId;
    }

    public void setInventoryPlanId(int inventoryPlanId) {
        this.inventoryPlanId = inventoryPlanId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductSpec() {
        return productSpec;
    }

    public void setProductSpec(String productSpec) {
        this.productSpec = productSpec;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public int getUomId() {
        return uomId;
    }

    public void setUomId(int uomId) {
        this.uomId = uomId;
    }

    public String getUomName() {
        return uomName;
    }

    public void setUomName(String uomName) {
        this.uomName = uomName;
    }

    public String getBaseUomQty() {
        return baseUomQty;
    }

    public void setBaseUomQty(String baseUomQty) {
        this.baseUomQty = baseUomQty;
    }

    public String getLot() {
        return lot;
    }

    public void setLot(String lot) {
        this.lot = lot;
    }

    public String getGuaranteeDate() {
        return guaranteeDate;
    }

    public void setGuaranteeDate(String guaranteeDate) {
        this.guaranteeDate = guaranteeDate;
    }

    public String getProductionDate() {
        return productionDate;
    }

    public void setProductionDate(String productionDate) {
        this.productionDate = productionDate;
    }

    public BigDecimal getQtyBook() {
        return qtyBook;
    }

    public void setQtyBook(BigDecimal qtyBook) {
        this.qtyBook = qtyBook;
    }

    public BigDecimal getQtyCount() {
        return qtyCount;
    }

    public void setQtyCount(BigDecimal qtyCount) {
        this.qtyCount = qtyCount;
    }

    public BigDecimal getQtyDiff() {
        return qtyDiff;
    }

    public void setQtyDiff(BigDecimal qtyDiff) {
        this.qtyDiff = qtyDiff;
    }

    public BigDecimal getDiffAmt() {
        return diffAmt;
    }

    public void setDiffAmt(BigDecimal diffAmt) {
        this.diffAmt = diffAmt;
    }

    public BigDecimal getPackageQtyBook() {
        return packageQtyBook;
    }

    public void setPackageQtyBook(BigDecimal packageQtyBook) {
        this.packageQtyBook = packageQtyBook;
    }

    public BigDecimal getPackageQtyCount() {
        return packageQtyCount;
    }

    public void setPackageQtyCount(BigDecimal packageQtyCount) {
        this.packageQtyCount = packageQtyCount;
    }

    public BigDecimal getPackageQtyDiff() {
        return packageQtyDiff;
    }

    public void setPackageQtyDiff(BigDecimal packageQtyDiff) {
        this.packageQtyDiff = packageQtyDiff;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getLineAmt() {
        return lineAmt;
    }

    public void setLineAmt(BigDecimal lineAmt) {
        this.lineAmt = lineAmt;
    }

    public String getStorageStatusName() {
        return storageStatusName;
    }

    public void setStorageStatusName(String storageStatusName) {
        this.storageStatusName = storageStatusName;
    }

    public String getLocatorValue() {
        return locatorValue;
    }

    public void setLocatorValue(String locatorValue) {
        this.locatorValue = locatorValue;
    }

    public String getLocatorName() {
        return locatorName;
    }

    public void setLocatorName(String locatorName) {
        this.locatorName = locatorName;
    }

    public int getUnitPackQty() {
        return unitPackQty;
    }

    public void setUnitPackQty(int unitPackQty) {
        this.unitPackQty = unitPackQty;
    }

    public String getIsStoragePackage() {
        return isStoragePackage;
    }

    public void setIsStoragePackage(String isStoragePackage) {
        isStoragePackage = isStoragePackage;
    }
}
