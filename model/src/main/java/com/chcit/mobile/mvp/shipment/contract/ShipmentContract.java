package com.chcit.mobile.mvp.shipment.contract;

import android.content.Context;

import com.alibaba.fastjson.JSONObject;
import com.chcit.mobile.mvp.entity.PageBean;
import com.chcit.mobile.mvp.entity.ResultBean;
import com.chcit.mobile.mvp.entity.ShipmentBean;
import com.chcit.mobile.mvp.shipment.api.ShipmentInput;
import com.jess.arms.mvp.IView;
import com.jess.arms.mvp.IModel;

import java.util.List;

import io.reactivex.Observable;

public interface ShipmentContract {
    //对于经常使用的关于UI的方法可以定义到IView中,如显示隐藏进度条,和显示文字消息
    interface View extends IView {
        /**
         * 更新查询到的数据
         */
        void updateContentList(List<ShipmentBean> shipmentBeans);

        /**
         * 获取页面的控件
         * @param i
         * @return
         */
        <T extends android.view.View> T getViewByTag(int i);

        Context getContext();
    }

    //Model层定义接口,外部只需关心Model返回的数据,无需关心内部细节,即是否使用缓存
    interface Model extends IModel {
        //获取拣货任务明细
        Observable<List<ShipmentBean>> getShipments(JSONObject json);
        //批量收货
        Observable<ResultBean> doBatchConfirm(JSONObject json);

        Observable<List<ShipmentBean>> queryDetail(PageBean pageBean, ShipmentInput shipmentInput );


    }
}
