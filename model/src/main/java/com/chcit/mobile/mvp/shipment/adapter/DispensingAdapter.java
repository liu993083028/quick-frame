package com.chcit.mobile.mvp.shipment.adapter;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseViewHolder;
import com.chcit.mobile.R;
import com.chcit.mobile.helper.recycler.BaseItemChangeQuickAdapter;
import com.chcit.mobile.mvp.entity.RecipeBean;

import java.util.List;

public class DispensingAdapter extends BaseItemChangeQuickAdapter<RecipeBean,BaseViewHolder> {

    public DispensingAdapter(@Nullable List<RecipeBean> data) {
        super(R.layout.item_despensing_list,data);
    }

    @Override
    protected void convert(BaseViewHolder helper, RecipeBean item) {
        int i = helper.getAdapterPosition();
        if(thisPosition == i){
            helper.getView(R.id.ll_dispensing_item).setBackgroundColor(mContext.getResources().getColor(R.color.LavenderBlush));
        }else if((i+2)%2 == 0){
            helper.getView(R.id.ll_dispensing_item).setBackgroundColor(mContext.getResources().getColor(R.color.gray_f2));
        }else{
            helper.getView(R.id.ll_dispensing_item).setBackgroundColor(mContext.getResources().getColor(R.color.white));
        }
        helper.setText(R.id.tv_dispensing_productName,item.getProductName());
        helper.setText(R.id.tv_dispensing_productSpec,item.getProductSpec());
        helper.setText(R.id.tv_dispensing_locator,item.getLocatorValue());
        helper.setText(R.id.tv_dispensing_qty,item.getQty()+item.getUnit());
    }
}
