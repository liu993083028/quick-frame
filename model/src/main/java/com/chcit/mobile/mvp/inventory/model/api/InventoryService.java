/*
 * Copyright 2017 JessYan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.chcit.mobile.mvp.inventory.model.api;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.chcit.mobile.common.HttpMethodContains;
import com.chcit.mobile.mvp.entity.BaseData;
import com.chcit.mobile.mvp.entity.Locator;
import com.chcit.mobile.mvp.entity.ResultBean;
import com.chcit.mobile.mvp.inventory.entity.InventoryBean;
import com.chcit.mobile.mvp.inventory.entity.InventoryPackageBean;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;
import static com.chcit.mobile.common.HttpMethodContains.INVENTORY_PLAN_CHCKED;
import static com.chcit.mobile.common.HttpMethodContains.INVENTORY_PLAN_OK;
import static com.chcit.mobile.common.HttpMethodContains.INVENTORY_PLAN_PACKAGE_OK;
import static com.chcit.mobile.common.HttpMethodContains.INVENTORY_PLAN_QUERY;
import static com.chcit.mobile.common.HttpMethodContains.INVENTORY_PLAN_QUERY_DETAIL;

/**
 * ================================================
 * 存放通用的一些 API
 * <p>
 * Created by JessYan on 08/05/2016 12:05
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * ================================================
 */
public interface InventoryService {


    @GET(INVENTORY_PLAN_QUERY)
    Observable<BaseData<InventoryBean>> getData(@QueryMap JSONObject json);

    @GET(INVENTORY_PLAN_QUERY_DETAIL)
    Observable<BaseData<InventoryPackageBean>> getDataDetail(@QueryMap JSONObject json);

    @GET(INVENTORY_PLAN_CHCKED)
    Observable<ResultBean> doCheked( @QueryMap JSONObject json);

    @GET(INVENTORY_PLAN_OK)
    Observable<ResultBean> doConfirm(@QueryMap JSONObject json);

    @FormUrlEncoded
    @POST(INVENTORY_PLAN_PACKAGE_OK)
    Observable<ResultBean> doConfirmPackage(@FieldMap JSONObject json);

    @POST(HttpMethodContains.LOCATOR_URL)
    @FormUrlEncoded
    Observable<BaseData<Locator>> getLocators(@FieldMap JSONObject json);

//    warehouseId: 1000214
//    locatorToId: 1013386
//    packageNo: ["(00)090000004000002007","(01)012312312300013547"]
    @POST(HttpMethodContains.MOVE_PACKAGE)
    @FormUrlEncoded
    Observable<ResultBean> movePackage(@Field("warehouseId") int warehouseId,@Field("locatorToId")int locatorToId,@Field("packageNo") List<String> packageNo);
}
