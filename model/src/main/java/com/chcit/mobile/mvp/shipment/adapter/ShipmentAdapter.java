package com.chcit.mobile.mvp.shipment.adapter;

import androidx.annotation.Nullable;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import com.chad.library.adapter.base.BaseViewHolder;
import com.chcit.mobile.R;
import com.chcit.mobile.helper.recycler.BaseSelectQuickAdapter;
import com.chcit.mobile.mvp.entity.ShipmentBean;

import java.util.List;

public class ShipmentAdapter extends BaseSelectQuickAdapter<ShipmentBean,BaseViewHolder> {

    private View.OnClickListener mOnClickListener;
    public ShipmentAdapter(@Nullable List<ShipmentBean> data) {
        super(R.layout.item_shipment_data,data);


    }

    public void setOnClickListener(View.OnClickListener onClickListener){
        mOnClickListener = onClickListener;
    }
    @Override
    protected void convert(final BaseViewHolder helper, final ShipmentBean item) {

        CheckBox checkBox = helper.getView(R.id.box_shipment_select);
        LinearLayout l1 = helper.getView(R.id.ll_first);
        if("N".equals(item.getIsStoragePackage())){
            checkBox.setVisibility(View.VISIBLE);
            int position= helper.getAdapterPosition();
            checkBox.setChecked(mSelectedPositions.get(position));
            checkBox.setOnClickListener(v -> {
                int i= helper.getAdapterPosition();
                if (isItemChecked(i)) {
                    setItemChecked(i, false);
                } else {
                    setItemChecked(i, true);
                }

                if (mTitleBarView != null) {
                    String msg = "已选择" + getSelectedItem().size() + "/" + getItemCount();
                    mTitleBarView.setText(msg);
                }
                notifyItemChanged(i);
            });
            l1.setOnClickListener(v->{
                checkBox.performClick();
            });
        }else {
            checkBox.setVisibility(View.INVISIBLE);
        }
        helper.setText(R.id.tv_shipment_GuaranteeDate,item.getGuaranteeDate());
        helper.setText(R.id.tv_shipment_Lot,item.getLot());
        helper.setText(R.id.tv_shipment_Manufacturer,item.getManufacturer());
        helper.setText(R.id.tv_shipment_pname,item.getProductName());
        helper.setText(R.id.tv_shipment_productSpec,item.getProductSpec());
        helper.setText(R.id.tv_shipment_QtyOnHand,item.getQtyLeft().intValue()+"");
        helper.setText(R.id.tv_shipment_productCode,item.getProductCode());
        helper.setText(R.id.tv_shipment_type,item.getOrderTypeName());
        helper.setText(R.id.tv_shipment_pickListNo,item.getPickListNo());
        helper.setText(R.id.tv_shipment_bpatName,item.getBpartnerName() );

    }



}
