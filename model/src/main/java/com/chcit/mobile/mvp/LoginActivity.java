package com.chcit.mobile.mvp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.LinearLayoutCompat;
import android.view.View;
import android.widget.TextView;
import com.chcit.custom.view.button.SwitchButton;
import com.chcit.mobile.BuildConfig;
import com.chcit.mobile.R;
import com.chcit.mobile.app.base.BaseSupportActivity;
import com.chcit.mobile.app.utils.AppUtils;
import com.chcit.mobile.app.utils.SharedPreUtils;
import com.chcit.mobile.di.component.DaggerLoginComponent;
import com.chcit.mobile.di.module.LoginModule;
import com.chcit.mobile.mvp.common.contract.LoginContract;
import com.chcit.mobile.mvp.common.presenter.LoginPresenter;
import com.chcit.mobile.mvp.common.ui.fragment.LoginFragment;
import com.chcit.mobile.mvp.common.ui.fragment.ServerFragment;
import com.jess.arms.base.app.ConfigKeys;
import com.jess.arms.di.component.AppComponent;
import com.jess.arms.utils.ArmsUtils;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.xuexiang.xui.widget.toast.XToast;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import me.yokeyword.fragmentation.ISupportFragment;
import me.yokeyword.fragmentation.SupportFragment;

import static com.jess.arms.utils.Preconditions.checkNotNull;

public class LoginActivity extends BaseSupportActivity<LoginPresenter> implements LoginContract.View {

    @BindView(R.id.version_name)
    TextView versionName;
    @BindView(R.id.switch_button)
    SwitchButton switchButton;
    @BindView(R.id.linear_layout_testmodel)
    LinearLayoutCompat linearLayoutTestmodel;
    ISupportFragment[] fragments ;
    private Disposable timeObservable;

    @Override
    public void setupActivityComponent(@NonNull AppComponent appComponent) {
        DaggerLoginComponent //如找不到该类,请编译一下项目
                .builder()
                .appComponent(appComponent)
                .loginModule(new LoginModule(this))
                .build()
                .inject(this);
    }

    @Override
    public int initView(@Nullable Bundle savedInstanceState) {
        return R.layout.activity_login; //如果你不需要框架帮你设置 setContentView(id) 需要自行设置,请返回 0
    }

    @Override
    public void initData(@Nullable Bundle savedInstanceState) {
        fragments =new  SupportFragment[]{LoginFragment.newInstance(),ServerFragment.newInstance()};
        if(BuildConfig.DEBUG){
            switchButton.setChecked(SharedPreUtils.getInstance().getBoolean(ConfigKeys.TEST_MODE.name(),false));
            switchButton.setOnCheckedChangeListener((v, b) -> {
                if (b) {
                    SharedPreUtils.getInstance().putBoolean(ConfigKeys.TEST_MODE.name(), true);
                } else {
                    SharedPreUtils.getInstance().putBoolean(ConfigKeys.TEST_MODE.name(), false);
                }
                 timeObservable = Observable.interval(2, TimeUnit.SECONDS)
                        .subscribe(aLong -> AppUtils.relaunchApp());


            });
        }else{
            linearLayoutTestmodel.setVisibility(View.INVISIBLE);
            if(switchButton.isChecked()){
                SharedPreUtils.getInstance().putBoolean(ConfigKeys.TEST_MODE.name(), false);
            }

        }
      initData();

    }
    private void initData(){
        versionName.setText("版本号-"+AppUtils.getAppVersionName());
        if (findFragment(LoginFragment.class) == null) {

            //loadRootFragment(R.id.login_page, LoginFragment.newInstance());  // 加载根Fragment
            if(SharedPreUtils.getInstance().getBoolean("FIRST",true)){

//                loadMultipleRootFragment(R.id.login_page,1, fragments);
                loadRootFragment(R.id.login_page,ServerFragment.newInstance());
            }else{

                loadRootFragment(R.id.login_page,LoginFragment.newInstance());
            }

        }
    }


    @Override
    public RxPermissions getRxPermissions() {
        return new RxPermissions(LoginActivity.this);
    }

    @Override
    public void toLogin() {

    }

    @Override
    public void showMessage(@NonNull String message) {
        checkNotNull(message);
         XToast.info(this,message).show();
    }

    public Context getActivity() {
        return this;
    }

    @Override
    public void launchActivity(@NonNull Intent intent) {
        checkNotNull(intent);
        ArmsUtils.startActivity(intent);
    }

    @Override
    public void killMyself() {
        fragments = null;
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        fragments = null;
        if(timeObservable!=null){
            timeObservable.dispose();
        }
    }
}
