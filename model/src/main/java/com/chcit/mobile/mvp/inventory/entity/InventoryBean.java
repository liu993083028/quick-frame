package com.chcit.mobile.mvp.inventory.entity;

public class InventoryBean {

    /**
     * inventoryPlanId : 1000121
     * inventoryPlanNo : 1000121
     * docDate : 2018-12-18 21:51
     * docStatusName : 待审核
     * inventoryPlanTypeName : 计划盘存
     * created : 2018-12-18 21:51
     * createdByName : 仓库老师
     * commitTime : 2018-12-18 21:51
     * commitUserName : 仓库老师
     * completeTime :
     * warehouseId : 1000214
     * warehouseName : 西药库
     * packageLineCount : 0
     */

    private int inventoryPlanId;
    private String inventoryPlanNo;
    private String docDate;
    private String docStatusName;
    private String inventoryPlanTypeName;
    private String created;
    private String createdByName;
    private String commitTime;
    private String commitUserName;
    private String completeTime;
    private int warehouseId;
    private String warehouseName;
    private int packageLineCount;

    public int getInventoryPlanId() {
        return inventoryPlanId;
    }

    public void setInventoryPlanId(int inventoryPlanId) {
        this.inventoryPlanId = inventoryPlanId;
    }

    public String getInventoryPlanNo() {
        return inventoryPlanNo;
    }

    public void setInventoryPlanNo(String inventoryPlanNo) {
        this.inventoryPlanNo = inventoryPlanNo;
    }

    public String getDocDate() {
        return docDate;
    }

    public void setDocDate(String docDate) {
        this.docDate = docDate;
    }

    public String getDocStatusName() {
        return docStatusName;
    }

    public void setDocStatusName(String docStatusName) {
        this.docStatusName = docStatusName;
    }

    public String getInventoryPlanTypeName() {
        return inventoryPlanTypeName;
    }

    public void setInventoryPlanTypeName(String inventoryPlanTypeName) {
        this.inventoryPlanTypeName = inventoryPlanTypeName;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getCreatedByName() {
        return createdByName;
    }

    public void setCreatedByName(String createdByName) {
        this.createdByName = createdByName;
    }

    public String getCommitTime() {
        return commitTime;
    }

    public void setCommitTime(String commitTime) {
        this.commitTime = commitTime;
    }

    public String getCommitUserName() {
        return commitUserName;
    }

    public void setCommitUserName(String commitUserName) {
        this.commitUserName = commitUserName;
    }

    public String getCompleteTime() {
        return completeTime;
    }

    public void setCompleteTime(String completeTime) {
        this.completeTime = completeTime;
    }

    public int getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(int warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public int getPackageLineCount() {
        return packageLineCount;
    }

    public void setPackageLineCount(int packageLineCount) {
        this.packageLineCount = packageLineCount;
    }
}
