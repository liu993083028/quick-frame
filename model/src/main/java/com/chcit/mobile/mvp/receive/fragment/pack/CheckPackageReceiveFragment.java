package com.chcit.mobile.mvp.receive.fragment.pack;

import com.chcit.mobile.mvp.common.model.MenuTypeEnum;

public class CheckPackageReceiveFragment extends PackageReceiveFragment {

    public CheckPackageReceiveFragment() {
        this.type = MenuTypeEnum.PACKAGE_CHECK;
    }
}
