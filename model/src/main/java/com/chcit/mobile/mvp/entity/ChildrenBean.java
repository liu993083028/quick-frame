package com.chcit.mobile.mvp.entity;

public  class ChildrenBean {
    /**
     * id : spd.mobile.po_receive
     * text : 采购入库
     * leaf : true
     * icon : icon_default.png
     * url : PurchaseReceive
     * canRead : true
     * canWrite : true
     * canExport : true
     * canReport : true
     */

    private String id;
    private String text;
    private boolean leaf;
    private String icon;
    private String url;
    private boolean canRead;
    private boolean canWrite;
    private boolean canExport;
    private boolean canReport;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isLeaf() {
        return leaf;
    }

    public void setLeaf(boolean leaf) {
        this.leaf = leaf;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isCanRead() {
        return canRead;
    }

    public void setCanRead(boolean canRead) {
        this.canRead = canRead;
    }

    public boolean isCanWrite() {
        return canWrite;
    }

    public void setCanWrite(boolean canWrite) {
        this.canWrite = canWrite;
    }

    public boolean isCanExport() {
        return canExport;
    }

    public void setCanExport(boolean canExport) {
        this.canExport = canExport;
    }

    public boolean isCanReport() {
        return canReport;
    }

    public void setCanReport(boolean canReport) {
        this.canReport = canReport;
    }
}
