package com.chcit.mobile.mvp.entity;

import java.util.List;

public class MenuBean {

    /**
     * id : spd.mobile
     * text : 手持菜单
     * leaf : false
     * icon : icon_default.png
     * children : [{"id":"spd.mobile.po_receive","text":"采购入库","leaf":true,"icon":"icon_default.png","url":"PurchaseReceive","canRead":true,"canWrite":true,"canExport":true,"canReport":true},{"id":"spd.mobile.allotReceive","text":"院内入库","leaf":true,"icon":"icon_default.png","url":"AllotReceive","canRead":true,"canWrite":true,"canExport":true,"canReport":true},{"id":"spd.mobile.allotRejectReceive","text":"拒收入库","leaf":true,"icon":"icon_default.png","url":"AllotRejectReceive","canRead":true,"canWrite":true,"canExport":true,"canReport":true},{"id":"spd.mobile.w1.wms.prShipment","text":"采退出库","leaf":true,"icon":"icon_default.png","url":"pr_Shipment","canRead":true,"canWrite":true,"canExport":true,"canReport":true},{"id":"spd.mobile.w1.wms.mvShipment","text":"院内出库","leaf":true,"icon":"icon_default.png","url":"mv_Shipment","canRead":true,"canWrite":true,"canExport":true,"canReport":true},{"id":"spd.mobile.w1.wms. movementShipment","text":"移库","leaf":true,"icon":"icon_default.png","url":"movementShipment","canRead":true,"canWrite":true,"canExport":true,"canReport":true}]
     */

    private String id;
    private String text;
    private boolean leaf;
    private String icon;
    private List<ChildrenBean> children;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isLeaf() {
        return leaf;
    }

    public void setLeaf(boolean leaf) {
        this.leaf = leaf;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public List<ChildrenBean> getChildren() {
        return children;
    }

    public void setChildren(List<ChildrenBean> children) {
        this.children = children;
    }


}
