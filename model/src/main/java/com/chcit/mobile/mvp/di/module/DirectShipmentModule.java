package com.chcit.mobile.mvp.di.module;

import dagger.Binds;
import dagger.Module;

import com.chcit.mobile.mvp.shipment.contract.DirectShipmentContract;


/**
 * ================================================
 * Description:
 * <p>
 * Created by MVPArmsTemplate on 03/15/2019 15:05
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms">Star me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms/wiki">See me</a>
 * <a href="https://github.com/JessYanCoding/MVPArmsTemplate">模版请保持更新</a>
 * ================================================
 */
@Module
public abstract class DirectShipmentModule {

    @Binds
    abstract DirectShipmentContract.Model bindDirectShipmentModel(DirectShipmentModel model);
}