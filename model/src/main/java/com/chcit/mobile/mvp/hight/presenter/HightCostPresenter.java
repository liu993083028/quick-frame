package com.chcit.mobile.mvp.hight.presenter;

import android.app.Application;
import android.view.View;
import android.widget.AdapterView;

import com.xuexiang.xui.widget.dialog.materialdialog.MaterialDialog;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.xuexiang.xui.widget.spinner.editspinner.EditSpinner;
import com.chcit.mobile.R;
import com.chcit.mobile.common.SPDErrorHandle;
import com.chcit.mobile.helper.HttpClientHelper;
import com.chcit.mobile.mvp.common.api.cache.CommonCache;
import com.chcit.mobile.mvp.common.api.service.CommonService;
import com.chcit.mobile.mvp.common.model.CommonModel;
import com.chcit.mobile.mvp.di.module.DirectShipmentModel;
import com.chcit.mobile.mvp.entity.HightRegisPackageBean;
import com.chcit.mobile.mvp.entity.Locator;
import com.chcit.mobile.mvp.entity.PackageBean;
import com.chcit.mobile.mvp.entity.PageBean;
import com.chcit.mobile.mvp.entity.Statu;
import com.jess.arms.di.component.AppComponent;
import com.jess.arms.integration.AppManager;
import com.jess.arms.di.scope.FragmentScope;
import com.jess.arms.mvp.BasePresenter;
import com.jess.arms.http.imageloader.ImageLoader;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import me.jessyan.rxerrorhandler.core.RxErrorHandler;

import javax.inject.Inject;

import com.chcit.mobile.mvp.hight.contract.HightCostContract;
import com.jess.arms.utils.RxLifecycleUtils;

import java.util.List;


public class HightCostPresenter extends BasePresenter<HightCostContract.Model, HightCostContract.View> {
    @Inject
    RxErrorHandler mErrorHandler;
    @Inject
    Application mApplication;
    @Inject
    ImageLoader mImageLoader;
    @Inject
    AppManager mAppManager;
    @Inject
    DirectShipmentModel directShipmentModel;
    @Inject
    CommonModel mCommonModel;
    @Inject
    public HightCostPresenter(HightCostContract.Model model, HightCostContract.View rootView) {
        super(model, rootView);
    }
    public void createBorrowOrderByPackage(JSONObject json, Consumer<JSONArray> onNext) {

        mModel.createBorrowOrderByPackage(json)
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                }).subscribe(new SPDErrorHandle<JSONArray>(mErrorHandler) {

            @Override
            public void onNext(JSONArray list) {
                try {
                    onNext.accept(list);
                } catch (Exception e) {
                    onError(e);
                }
            }
        });
    }
    public void borrowUserByPackage(JSONObject json, Consumer<JSONArray> onNext) {

        mModel.borrowUserByPackage(json)
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                }).subscribe(new SPDErrorHandle<JSONArray>(mErrorHandler) {

            @Override
            public void onNext(JSONArray list) {
                try {
                    onNext.accept(list);
                } catch (Exception e) {
                    onError(e);
                }
            }
        });
    }
    public void borrowBackByPackage(JSONObject json, Consumer<JSONArray> onNext) {

        mModel.borrowBackByPackage(json)
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                }).subscribe(new SPDErrorHandle<JSONArray>(mErrorHandler) {

            @Override
            public void onNext(JSONArray list) {
                try {
                    onNext.accept(list);
                } catch (Exception e) {
                    onError(e);
                }
            }
        });
    }

    public void getPakcageInfo(JSONObject paramReq, Consumer<List<PackageBean>> consumer){
        directShipmentModel.queryPackageInfo(paramReq)
                .map(result -> result.toJavaList(PackageBean.class))
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                })
                .subscribe(new SPDErrorHandle<List<PackageBean>>(mErrorHandler) {
                    @Override
                    public void onNext(List<PackageBean> lists) {
                        try {
                            consumer.accept(lists);
                        } catch (Exception e) {
                            onError(e);
                        }
                    }
                });
    }

    public void getRegistedPakcageInfo( String dateForm, String dateTo, Consumer<List<HightRegisPackageBean>> consumer){
        PageBean page = new PageBean();
        page.setLimit(1000);
        page.setStart(0);
        page.setPageSize(6000);
        mModel.queryHightCost(page,dateForm,dateTo)
                .map(result -> result.toJavaList(HightRegisPackageBean.class))
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                })
                .subscribe(new SPDErrorHandle<List<HightRegisPackageBean>>(mErrorHandler) {
                    @Override
                    public void onNext(List<HightRegisPackageBean> lists) {
                        try {
                            consumer.accept(lists);
                        } catch (Exception e) {
                            onError(e);
                        }
                    }
                });
    }
    public  void queryUsers(  Consumer<List<Statu>> consumer) {

        mCommonModel.queryUsers("Y","Y",2,HttpClientHelper.getSelectWareHouse().getId())

                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SPDErrorHandle<List<Statu>>(mErrorHandler) {
                    @Override
                    public void onNext(List<Statu> result) {
                        try {
                            consumer.accept(result);
                        } catch (Exception e) {
                            onError(e);
                        }
                    }
                });
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mErrorHandler = null;
        this.mAppManager = null;
        this.mImageLoader = null;
        this.mApplication = null;
    }
}
