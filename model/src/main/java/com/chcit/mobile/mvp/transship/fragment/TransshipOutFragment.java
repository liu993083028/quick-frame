package com.chcit.mobile.mvp.transship.fragment;

import com.chcit.mobile.mvp.common.model.MenuTypeEnum;

/**
 *转运out
 */
public class TransshipOutFragment extends TransshipFragment {

    public TransshipOutFragment(){
        this.type = MenuTypeEnum.TRANSSHIP_OUT;
    }

}
