package com.chcit.mobile.mvp.hight.di.component;

import dagger.BindsInstance;
import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.chcit.mobile.mvp.hight.di.module.HightCostBindModule;
import com.chcit.mobile.mvp.hight.contract.HightCostBindContract;

import com.jess.arms.di.scope.FragmentScope;
import com.chcit.mobile.mvp.hight.fragment.HightCostBindFragment;


@FragmentScope
@Component(modules = HightCostBindModule.class, dependencies = AppComponent.class)
public interface HightCostBindComponent {
    void inject(HightCostBindFragment fragment);

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder view(HightCostBindContract.View view);

        Builder appComponent(AppComponent appComponent);

        HightCostBindComponent build();
    }
}