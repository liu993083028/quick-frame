package com.chcit.mobile.mvp.inventory.adapter;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chcit.mobile.R;
import com.chcit.mobile.mvp.inventory.entity.InventoryPackageBean;

import java.util.List;

public class InventoryPackagePlanAdapter extends BaseQuickAdapter<InventoryPackageBean, BaseViewHolder>  {

    private OnClickListener onClickListener;//帐实相符
    public InventoryPackagePlanAdapter(@Nullable List<InventoryPackageBean> data) {
        super(R.layout.item_inventory_package, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, InventoryPackageBean item) {
        helper.setText(R.id.tv_inventory_package_productName, item.getProductName());
        helper.setText(R.id.tv_inventory_package_productCode, item.getProductCode());
        helper.setText(R.id.tv_inventory_package_productSpec, item.getProductSpec());
        helper.setText(R.id.tv_inventory_package_manufacturer, item.getManufacturer());
        helper.setText(R.id.tv_inventory_package_locatorvalue, item.getLocatorValue());
        String s = "%s包/%s%s";
        if("Y".equals(item.getIsStoragePackage())){
            helper.setText(R.id.tv_inventory_package_qtyBookValue, String.format(s,item.getPackageQtyBook().toString(),item.getQtyBook().toString(),item.getUomName()));
            helper.setText(R.id.tv_inventory_package_qtyRealValue, String.format(s,item.getPackageQtyCount().toString(),item.getQtyCount().toString(),item.getUomName()));
        }else{
            helper.setText(R.id.bt_inventory_package_scan,"复查");
            helper.setText(R.id.tv_inventory_package_unitPackQtyName,"实存数量");
            helper.setText(R.id.tv_inventory_package_qtyBookValue,item.getQtyBook().toString());
            helper.setText(R.id.tv_inventory_package_qtyRealValue,item.getQtyCount().toString());
        }
        int position = helper.getAdapterPosition();
        helper.getView(R.id.bt_inventory_package_ok).setOnClickListener(l->{
                if(onClickListener != null){
                    onClickListener.onClick(item,position);
                }
        });
        helper.getView(R.id.bt_inventory_package_scan).setOnClickListener(l->{
            if(getOnItemClickListener()!= null){
                getOnItemClickListener().onItemClick(this,l,position);
            }

        });
       /* helper.getView(R.id.bt_inventory_package_scan).setOnClickListener(l->{

        });*/;

    }

    public void setOnclickListener(OnClickListener onClickListener){
        this.onClickListener = onClickListener;
    }

    public interface OnClickListener {
        /**
         * Called when a view has been clicked.
         *
         * @param v The view that was clicked.
         */
        void onClick(InventoryPackageBean v,int position);
    }
}
