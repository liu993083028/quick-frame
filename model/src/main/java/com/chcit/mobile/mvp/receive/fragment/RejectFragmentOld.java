package com.chcit.mobile.mvp.receive.fragment;

import com.chcit.mobile.mvp.common.model.MenuTypeEnum;

/**
 * 拒收入库页面
 */
public class RejectFragmentOld extends OldReceiveFragment {

    public RejectFragmentOld() {
        this.type = MenuTypeEnum.REJECT;
    }
}
