package com.chcit.mobile.mvp.entity;

import java.util.Objects;

public class HightCostBindBean {
    String packageNo;
    Long rfid;
    /**
     * value : 346gegrwb
     * isUsed : Y
     * warehouseId : 1000214
     * warehouseName : 中药库
     * productId : 1037383
     * productName : 丙泊酚注射液
     * productSpec : 20ml:0.2g/支
     * manufacturer : CordenPharmaS.P.A
     * productCode : 2030
     * uomName : 支
     */
    private String value;
    private String isUsed;
    private int warehouseId;
    private String warehouseName;
    private int productId;
    private String productName;
    private String productSpec;
    private String manufacturer;
    private String productCode;
    private String uomName;

    public HightCostBindBean() {

    }

    public HightCostBindBean(String s) {
        packageNo = s;
    }

    public String getPackageNo() {
        return packageNo;
    }

    public void setPackageNo(String packageNo) {
        this.packageNo = packageNo;
    }

    public Long getRFID() {
        return rfid;
    }

    public void setRFID(Long RFID) {
        this.rfid = RFID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HightCostBindBean that = (HightCostBindBean) o;
        return Objects.equals(packageNo, that.packageNo);
    }

    @Override
    public int hashCode() {

        return Objects.hash(packageNo);
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getIsUsed() {
        return isUsed;
    }

    public void setIsUsed(String isUsed) {
        this.isUsed = isUsed;
    }

    public int getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(int warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductSpec() {
        return productSpec;
    }

    public void setProductSpec(String productSpec) {
        this.productSpec = productSpec;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getUomName() {
        return uomName;
    }

    public void setUomName(String uomName) {
        this.uomName = uomName;
    }

    @Override
    public String toString() {
        return
                "包装号：" + packageNo  +'\n' +
                " RFID: " + value + '\n' +
//                ", isUsed='" + isUsed + '\'' +
//                ", 仓库：" + warehouseName + '\n' +
                "品名：" + productName + '\n' +
                "规格：" + productSpec + '\n' +
                "厂家：" + manufacturer + '\n' +
                "编码：" + productCode + '\n' +
                "单位：" + uomName
                ;
    }
}
