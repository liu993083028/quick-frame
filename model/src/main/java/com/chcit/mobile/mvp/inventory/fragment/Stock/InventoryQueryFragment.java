package com.chcit.mobile.mvp.inventory.fragment.Stock;


import com.chcit.mobile.mvp.common.model.MenuTypeEnum;
import com.chcit.mobile.mvp.inventory.fragment.move.InventoryFragment;

public class InventoryQueryFragment extends InventoryFragment {

    public InventoryQueryFragment(){
        super();
        this.type = MenuTypeEnum.INVENTORY_QUERY;
    }
}
