package com.chcit.mobile.mvp.inventory.model;

import android.app.Application;

import com.alibaba.fastjson.JSONObject;
import com.chcit.mobile.mvp.entity.BaseData;
import com.chcit.mobile.mvp.inventory.entity.InventoryPackageBean;
import com.chcit.mobile.mvp.inventory.model.api.InventoryService;
import com.google.gson.Gson;
import com.jess.arms.integration.IRepositoryManager;
import com.jess.arms.mvp.BaseModel;

import com.jess.arms.di.scope.FragmentScope;

import javax.inject.Inject;

import com.chcit.mobile.mvp.inventory.contract.InventoryPlanContract;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;


@FragmentScope
public class InventoryPlanModel extends BaseModel implements InventoryPlanContract.Model {
    @Inject
    Gson mGson;
    @Inject
    Application mApplication;

    @Inject
    public InventoryPlanModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mGson = null;
        this.mApplication = null;
    }

    @Override
    public Observable<BaseData<InventoryPackageBean>> getData(JSONObject json) {
        return mRepositoryManager.obtainRetrofitService(InventoryService.class)
                .getDataDetail(json).subscribeOn(Schedulers.io());
    }


}