package com.chcit.mobile.mvp.common.model;

import android.app.Application;

import com.chcit.mobile.mvp.common.api.service.UserService;
import com.google.gson.Gson;
import com.jess.arms.integration.IRepositoryManager;
import com.jess.arms.mvp.BaseModel;

import com.jess.arms.di.scope.FragmentScope;

import javax.inject.Inject;

import com.chcit.mobile.mvp.common.contract.ServerContract;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;


@FragmentScope
public class ServerModel extends BaseModel implements ServerContract.Model {
    @Inject
    Gson mGson;
    @Inject
    Application mApplication;

    @Inject
    public ServerModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mGson = null;
        this.mApplication = null;
    }


    @Override
    public Observable<String> test(String url) {

        return mRepositoryManager.obtainRetrofitService(UserService.class).test(url).subscribeOn(Schedulers.io());
    }
}