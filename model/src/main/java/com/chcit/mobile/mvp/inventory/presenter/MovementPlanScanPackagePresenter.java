package com.chcit.mobile.mvp.inventory.presenter;

import android.app.Application;

import com.alibaba.fastjson.JSONObject;
import com.chcit.mobile.app.exception.BaseException;
import com.chcit.mobile.common.HttpMethodContains;
import com.chcit.mobile.common.SPDErrorHandle;
import com.chcit.mobile.mvp.entity.PackageBean;
import com.chcit.mobile.mvp.entity.ResultBean;
import com.chcit.mobile.mvp.inventory.model.MovementPlanScanPackageModel;
import com.jess.arms.integration.AppManager;
import com.jess.arms.di.scope.FragmentScope;
import com.jess.arms.mvp.BasePresenter;
import com.jess.arms.http.imageloader.ImageLoader;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import me.jessyan.rxerrorhandler.core.RxErrorHandler;

import javax.inject.Inject;

import com.chcit.mobile.mvp.inventory.contract.MovementPlanScanPackageContract;
import com.jess.arms.utils.RxLifecycleUtils;

import java.util.ArrayList;
import java.util.List;

@FragmentScope
public class MovementPlanScanPackagePresenter extends BasePresenter<MovementPlanScanPackageContract.Model, MovementPlanScanPackageContract.View> {
    @Inject
    RxErrorHandler mErrorHandler;
    @Inject
    Application mApplication;
    @Inject
    ImageLoader mImageLoader;
    @Inject
    AppManager mAppManager;
    @Inject
    MovementPlanScanPackageModel cModel;

    public boolean isError = false; //是否发生过错误，如果最后提交发生错误。返回上一个页面会刷新数据！

    @Inject
    public MovementPlanScanPackagePresenter(MovementPlanScanPackageContract.Model model, MovementPlanScanPackageContract.View rootView) {
        super(model, rootView);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mErrorHandler = null;
        this.mAppManager = null;
        this.mImageLoader = null;
        this.mApplication = null;
    }

    /**
     * 按包装移库
     * @param paramReq
     * @param action
     */
    public void movementPackageSubmit(JSONObject paramReq, Action action) {
        mModel.movementPackageSubmit(paramReq)
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                }).subscribe(new SPDErrorHandle<ResultBean>(mErrorHandler) {
            @Override
            public void onNext(ResultBean resultBean) {
                try {
                    if(resultBean.isSuccess()){
                        action.run();
                    }else{
                        String msg = resultBean.getMsg();
                        if(msg.isEmpty()){
                            msg = "未知错误！";
                        }
                        throw new BaseException(msg);
                    }

                } catch (Exception e) {
                    onError(e);
                }
            }

            @Override
            public void onError(Throwable t) {
                isError = true;
                super.onError(t);
            }
        });
    }

    /**
     *
     * @param paramReq
     * @param consumer
     */
    public void getPakcageInfo(JSONObject paramReq, Consumer<List<PackageBean>> consumer){
        cModel.queryPackageInfo(paramReq)
                .map(result -> result.toJavaList(PackageBean.class))
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                })
                .subscribe(new SPDErrorHandle<List<PackageBean>>(mErrorHandler) {
                    @Override
                    public void onNext(List<PackageBean> lists) {
                        try {
                            consumer.accept(lists);
                        } catch (Exception e) {
                            onError(e);
                        }
                    }
                });
    }

    /**
     * 校验移库包装
     */
    public void verifyPickListPackage(JSONObject paramReq, Consumer<List<PackageBean>> consumer) {
        getPakcageInfo(HttpMethodContains.SHIPMENT_PACKAGE_CHECKED,paramReq,consumer);
    }

    public void getPakcageInfo(String method,JSONObject paramReq, Consumer<List<PackageBean>> consumer){
//        mModel.requestOnString(method,paramReq)
//                .map(result -> {
//                    JSONObject jsonObject = JSONObject.parseObject(result);
//                    if(jsonObject.getBoolean("success")){
//                        JSONObject data = jsonObject.getJSONObject("data");
//                        if(data.getInteger("total")>0){
//                            return data.getJSONArray("rows").toJavaList(PackageBean.class);
//                        }else{
//                            throw new BaseException("没有查询到包装信息");
//                        }
//                    }else {
//                        String msg =jsonObject.getString("msg");
//                        if(msg!=null&&!msg.isEmpty()){
//                            throw new Exception(msg);
//                        }else{
//                            throw new Exception("未知错误！");
//                        }
//                    }
//                })
//                .observeOn(AndroidSchedulers.mainThread())
//                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
//                .doOnSubscribe(disposable -> {
//                    mRootView.showLoading();
//                })
//                .doFinally(() -> {
//                    mRootView.hideLoading();
//                })
//                .subscribe(new SPDErrorHandle<List<PackageBean>>(mErrorHandler) {
//                    @Override
//                    public void onNext(List<PackageBean> lists) {
//                        try {
//                            consumer.accept(lists);
//                        } catch (Exception e) {
//                            onError(e);
//                        }
//                    }
//                });
    }
}
