package com.chcit.mobile.mvp.hight.di.component;

import dagger.BindsInstance;
import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.chcit.mobile.mvp.hight.di.module.HightCostModule;
import com.chcit.mobile.mvp.hight.contract.HightCostContract;

import com.jess.arms.di.scope.FragmentScope;
import com.chcit.mobile.mvp.hight.fragment.HightCostFragment;


/**
 * ================================================
 * Description:
 * <p>
 * Created by MVPArmsTemplate on 06/17/2019 10:04
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms">Star me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms/wiki">See me</a>
 * <a href="https://github.com/JessYanCoding/MVPArmsTemplate">模版请保持更新</a>
 * ================================================
 */
@FragmentScope
@Component(modules = HightCostModule.class, dependencies = AppComponent.class)
public interface HightCostComponent {
    void inject(HightCostFragment fragment);

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder view(HightCostContract.View view);

        Builder appComponent(AppComponent appComponent);

        HightCostComponent build();
    }
}