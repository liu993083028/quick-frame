package com.chcit.mobile.mvp.packmanager.adapter;

import android.annotation.SuppressLint;
import androidx.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.TextView;
import android.widget.Toast;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chcit.mobile.R;
import com.chcit.mobile.helper.recycler.BaseItemChangeQuickAdapter;
import com.chcit.mobile.mvp.entity.PackageUintBean;
import java.math.BigDecimal;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PackageSubAdapter extends BaseItemChangeQuickAdapter<PackageUintBean,BaseViewHolder> {
    private BigDecimal count = BigDecimal.ZERO;
    public PackageSubAdapter(@Nullable List<PackageUintBean> data) {
        super(R.layout.item_packgesplit_list,data);
    }
    private OnCountNumber onCountNumber;

    public void setOnCountNumber(OnCountNumber onCountNumber) {
        this.onCountNumber = onCountNumber;
    }

    @Override
    protected void convert(BaseViewHolder helper, PackageUintBean item) {
        int i = helper.getAdapterPosition();
        if(thisPosition == i){
            helper.getView(R.id.ll_dispensing_item).setBackgroundColor(mContext.getResources().getColor(R.color.LavenderBlush));
        }else if((i+2)%2 == 0){
            helper.getView(R.id.ll_dispensing_item).setBackgroundColor(mContext.getResources().getColor(R.color.gray_f2));
        }else{
            helper.getView(R.id.ll_dispensing_item).setBackgroundColor(mContext.getResources().getColor(R.color.white));
        }
        helper.setText(R.id.tv_unitPackQty,item.getUnitPackQty()+"");
        helper.setText(R.id.tv_baoshu,"0");
        helper.setText(R.id.tv_shuliang,"0");
        helper.setText(R.id.tv_storagePackageCount,item.getStoragePackageCount());
        item.setPackageCount(BigDecimal.ZERO);
        helper.<TextView>getView(R.id.tv_baoshu).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int c, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @SuppressLint("SetTextI18n")
            @Override
            public void afterTextChanged(Editable s) {
                String text = s.toString();
                Pattern p = Pattern.compile("[0-9]*");
                Matcher m = p.matcher(text);
                if(!m.matches() ){
                    Toast.makeText(mContext,"输入的不是数字："+text, Toast.LENGTH_SHORT).show();
                    return;
                }

                if(!text.isEmpty()){
                     BigDecimal changeNumber =  (new BigDecimal(text)).multiply(item.getUnitPackQty());
                     BigDecimal beforeNumben=  new BigDecimal(helper.<TextView>getView(R.id.tv_shuliang).getText().toString());
                    count = beforeNumben.subtract(changeNumber);
                    if(onCountNumber!= null){
                        onCountNumber.onListenerWithText(count);
                    }

                    helper.<TextView>getView(R.id.tv_shuliang).setText((new BigDecimal(text)).multiply(item.getUnitPackQty()).toString());
                    item.setPackageCount(new BigDecimal(text));
                }
            }
        });

    }

    public interface OnCountNumber{
        void onListenerWithText(BigDecimal b);
    }
}
