package com.chcit.mobile.mvp.transship.model;

import android.app.Application;

import com.alibaba.fastjson.JSONObject;
import com.chcit.mobile.app.exception.BaseException;
import com.chcit.mobile.mvp.common.api.service.UserService;
import com.chcit.mobile.mvp.entity.ResultBean;
import com.chcit.mobile.mvp.entity.TransshipBean;
import com.chcit.mobile.mvp.transship.api.TransshipApi;
import com.google.gson.Gson;
import com.jess.arms.integration.IRepositoryManager;
import com.jess.arms.mvp.BaseModel;

import com.jess.arms.di.scope.FragmentScope;

import javax.inject.Inject;

import com.chcit.mobile.mvp.transship.contract.TransshipContract;
import com.jess.arms.utils.ArmsUtils;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

@FragmentScope
public class TransshipModel extends BaseModel implements TransshipContract.Model {
    @Inject
    Gson mGson;
    @Inject
    Application mApplication;

    @Inject
    public TransshipModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mGson = null;
        this.mApplication = null;
    }

    @Override
    public Observable<List<TransshipBean>> getReceives(JSONObject json) {
        return mRepositoryManager.obtainRetrofitService(TransshipApi.class)
                .queryReceives(json)
                .map(result -> {
                    if(result.isSuccess()){
                        if(result.total==0){
                              throw new BaseException("没有查询到数据！");
                        }
                        return  result.getRows();
                    } else{
                        throw new BaseException(result.getMsg());
                    }
                }).subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<ResultBean> doConfirm(JSONObject json) {
        return mRepositoryManager.obtainRetrofitService(TransshipApi.class)
                .transshipConfirm(json).subscribeOn(Schedulers.io());
    }
}