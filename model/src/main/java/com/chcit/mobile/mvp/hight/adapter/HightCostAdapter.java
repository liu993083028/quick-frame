package com.chcit.mobile.mvp.hight.adapter;

import androidx.annotation.Nullable;
import android.widget.ImageView;

import com.chad.library.adapter.base.BaseItemDraggableAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chcit.mobile.R;
import com.chcit.mobile.mvp.entity.PackageBean;


import java.util.List;

public class HightCostAdapter extends BaseItemDraggableAdapter<PackageBean, BaseViewHolder> {

    public HightCostAdapter(@Nullable List<PackageBean> data) {
        super(R.layout.item_hight_cost_scan,data);
    }

    @Override
    protected void convert(BaseViewHolder helper, PackageBean item) {
        helper.setText(R.id.tv_hrs_id,String.valueOf(helper.getAdapterPosition()+1));
        helper.setText(R.id.tv_hrc_packageNo,item.getPackageNo()+"  "+item.getProductName()+"  "
                +item.getProductSpec()+"  "+item.getQty()+item.getUomName()+"  "+item.getGuaranteeDate());
        ImageView imageView = helper.getView(R.id.iv_hrc_delete);
        imageView.setOnClickListener(view ->{
            int position = helper.getAdapterPosition();
            mOnItemSwipeListener.onItemSwiped(null,position);
            remove(position);
        });
    }
}
