package com.chcit.mobile.mvp.transship.adapter;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseViewHolder;
import com.chcit.mobile.R;
import com.chcit.mobile.helper.recycler.BaseSelectQuickAdapter;
import com.chcit.mobile.mvp.entity.TransshipBean;

import java.util.List;

public class TransshipAdapter extends BaseSelectQuickAdapter<TransshipBean,BaseViewHolder> {

    public TransshipAdapter(@Nullable List<TransshipBean> data) {
        super(R.layout.item_transship_data,data);

    }

    @Override
    protected void convert(final BaseViewHolder helper, final TransshipBean item) {

        helper.setText(R.id.tv_transship_no,item.getAsnNo());
        helper.setText(R.id.tv_transship_date,item.getDateArrived());
        helper.setText(R.id.tv_transship_warehouse_fr,item.getBpartnerName());
        helper.setText(R.id.tv_transship_warehouse_to,item.getWarehouseName());
    }
}
