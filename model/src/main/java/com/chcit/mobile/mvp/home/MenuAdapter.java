package com.chcit.mobile.mvp.home;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chcit.mobile.R;
import com.chcit.mobile.mvp.entity.HomeItem;
import com.xuexiang.xui.widget.textview.badge.BadgeView;

import java.util.List;


/**
 * https://github.com/CymChad/BaseRecyclerViewAdapterHelper
 */
public class MenuAdapter extends BaseQuickAdapter<String, BaseViewHolder> {


    private String logName = "关闭";
    public MenuAdapter( List data) {
        super(R.layout.item_menu, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, String item) {
        helper.setText(R.id.tv_menu_name,item);
        if(helper.getAdapterPosition()==0){
            TextView logView = helper.getView(R.id.tv_log_level);
            logView.setVisibility(View.VISIBLE);
            logView.setText(logName);
        }
    }

    public void setLogName(String logName){
        this.logName = logName;
    }
}
