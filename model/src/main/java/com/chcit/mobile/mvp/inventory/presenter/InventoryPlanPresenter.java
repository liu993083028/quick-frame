package com.chcit.mobile.mvp.inventory.presenter;

import android.app.Application;

import com.alibaba.fastjson.JSONObject;
import com.chcit.mobile.common.SPDErrorHandle;
import com.chcit.mobile.mvp.common.contract.CommonContract;
import com.chcit.mobile.mvp.common.model.CommonModel;
import com.chcit.mobile.mvp.entity.BaseData;
import com.chcit.mobile.mvp.entity.StorageBean;
import com.jess.arms.integration.AppManager;
import com.jess.arms.di.scope.FragmentScope;
import com.jess.arms.mvp.BasePresenter;
import com.jess.arms.http.imageloader.ImageLoader;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import me.jessyan.rxerrorhandler.core.RxErrorHandler;

import javax.inject.Inject;
import javax.inject.Named;

import com.chcit.mobile.mvp.inventory.contract.InventoryPlanContract;
import com.jess.arms.utils.RxLifecycleUtils;

import java.util.List;

@FragmentScope
public class InventoryPlanPresenter extends BasePresenter<InventoryPlanContract.Model, InventoryPlanContract.View> {
    @Inject
    RxErrorHandler mErrorHandler;
    @Inject
    Application mApplication;
    @Inject
    ImageLoader mImageLoader;
    @Inject
    AppManager mAppManager;



    private boolean isLoading;
    private int mStart;
    private int mCount = 30;
    @Inject
    public InventoryPlanPresenter(InventoryPlanContract.Model model, InventoryPlanContract.View rootView) {
        super(model, rootView);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mErrorHandler = null;
        this.mAppManager = null;
        this.mImageLoader = null;
        this.mApplication = null;
    }

    public void getData(JSONObject json, Consumer<List<StorageBean>> onNext){
     /*   if (!isLoading) {
            isLoading = true;
            json.put("start", mStart);
            json.put("limit", mCount);
            mModel.getData(json)
                    .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe(disposable -> {
                        mRootView.showLoading();
                    })
                    .subscribe(new SPDErrorHandle<BaseData<StorageBean>>(mErrorHandler){

                        @Override
                        public void onNext(BaseData<StorageBean> response) {
                            isLoading = false;
                            try {
                               if(response.getTotal() == 0){
                                   mRootView.showMessage("没有查到盘点数据！");
                               }else if(response.getTotal()>mStart){
                                    mStart += mCount;
                                    onNext.accept(response.getRows());
                                }else{
                                   mRootView.showNoMoreData();
                               }

                            } catch (Exception e) {
                                onError(e);
                            }
                        }

                        @Override
                        public void onError(Throwable t) {
                            isLoading = false;
                            mRootView.hideLoading();
                            super.onError(t);
                        }

                        @Override
                        public void onComplete() {
                            mRootView.hideLoading();
                        }
                    });
        }*/

    }

    public void setStart(int mStart) {
        this.mStart = mStart;
    }
}
