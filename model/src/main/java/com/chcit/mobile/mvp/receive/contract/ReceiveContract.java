package com.chcit.mobile.mvp.receive.contract;

import android.content.Context;
import android.view.View;

import com.alibaba.fastjson.JSONObject;
import com.chcit.mobile.mvp.entity.BaseResponse;
import com.chcit.mobile.mvp.entity.PageBean;
import com.chcit.mobile.mvp.entity.ReceiveBean;
import com.chcit.mobile.mvp.entity.ResponseBean;
import com.chcit.mobile.mvp.entity.ResultBean;
import com.chcit.mobile.mvp.receive.Entity.AsnResultBean;
import com.chcit.mobile.mvp.receive.Entity.ImageUrl;
import com.chcit.mobile.mvp.receive.api.QueryListInput;
import com.jess.arms.mvp.IView;
import com.jess.arms.mvp.IModel;

import java.util.ArrayList;
import java.util.List;
import io.reactivex.Observable;

public interface ReceiveContract {
    //对于经常使用的关于UI的方法可以定义到IView中,如显示隐藏进度条,和显示文字消息
    interface View extends IView {
        /**
         * 更新查询到的数据
         * @param receiveBeans
         */
       <E> void updateContentList(List<E> receiveBeans);

        /**
         * 获取页面的控件
         * @param i
         * @return
         */
        <T extends android.view.View> T getViewByTag(int i);

        Context getActivity();
    }

    //Model层定义接口,外部只需关心Model返回的数据,无需关心内部细节,即是否使用缓存
    interface Model extends IModel {
        /**
         * 获取采购入库信息
         * @param method
         * @param json
         * @return
         */
        Observable<List<ReceiveBean>> getReceives(String method,JSONObject json);

        /**
         * 批量收货
         * {recieve:[{asnLineId:[1111,2323,343434]}]}
         * @param json
         * @return
         */
        Observable<ResultBean> doBatchConfirm(JSONObject json);

        /**
         * 获取单号信息
         * @return
         */
        Observable<List<AsnResultBean>> queryDataList(PageBean pageBean, QueryListInput queryListInput);

        Observable<ImageUrl> uploadImage(String asnId, List<String> media);

        Observable<List<ImageUrl>> queryImageIds(String asnId);

        Observable<BaseResponse> deleteImage(String iamgeId);
    }
}
