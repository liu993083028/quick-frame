package com.chcit.mobile.mvp.entity;

public class HightRegisPackageBean {

    /**
     * dateOrdered : 2019-10-18 12:22
     * productCode : 040013
     * productFullName : 鸡内金
     * productSpec : 炮 1000g/kg
     * manufacturer : 江苏
     * uomName : kg
     * qtyDelivered : 1
     * qtyReceived : 0
     * qtyRejected : 1
     * packageNo : (01)012312312300023544
     * warehouseName : 草药药库
     * applyUserName : gl
     * confirmUserName : 仓库老师
     * documentNo : 1009409
     */

    private String dateOrdered;
    private String productCode;
    private String productFullName;
    private String productSpec;
    private String manufacturer;
    private String uomName;
    private String qtyDelivered;
    private String qtyReceived;
    private String qtyRejected;
    private String packageNo;
    private String warehouseName;
    private String applyUserName;
    private String confirmUserName;
    private String documentNo;

    public String getDateOrdered() {
        return dateOrdered;
    }

    public void setDateOrdered(String dateOrdered) {
        this.dateOrdered = dateOrdered;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductFullName() {
        return productFullName;
    }

    public void setProductFullName(String productFullName) {
        this.productFullName = productFullName;
    }

    public String getProductSpec() {
        return productSpec;
    }

    public void setProductSpec(String productSpec) {
        this.productSpec = productSpec;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getUomName() {
        return uomName;
    }

    public void setUomName(String uomName) {
        this.uomName = uomName;
    }

    public String getQtyDelivered() {
        return qtyDelivered;
    }

    public void setQtyDelivered(String qtyDelivered) {
        this.qtyDelivered = qtyDelivered;
    }

    public String getQtyReceived() {
        return qtyReceived;
    }

    public void setQtyReceived(String qtyReceived) {
        this.qtyReceived = qtyReceived;
    }

    public String getQtyRejected() {
        return qtyRejected;
    }

    public void setQtyRejected(String qtyRejected) {
        this.qtyRejected = qtyRejected;
    }

    public String getPackageNo() {
        return packageNo;
    }

    public void setPackageNo(String packageNo) {
        this.packageNo = packageNo;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public String getApplyUserName() {
        return applyUserName;
    }

    public void setApplyUserName(String applyUserName) {
        this.applyUserName = applyUserName;
    }

    public String getConfirmUserName() {
        return confirmUserName;
    }

    public void setConfirmUserName(String confirmUserName) {
        this.confirmUserName = confirmUserName;
    }

    public String getDocumentNo() {
        return documentNo;
    }

    public void setDocumentNo(String documentNo) {
        this.documentNo = documentNo;
    }
}
