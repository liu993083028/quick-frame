package com.chcit.mobile.mvp.receive.Entity;

import java.math.BigDecimal;

public class AsnResultBean {

    /**
     * asnId : 1014569
     * bpartnerId : 1005006
     * vendorId : 1005006
     * warehouseId : 1000251
     * warehouseName : 草药药库
     * bpartnerName : 江阴天江药业有限公司
     * vendorName : 江阴天江药业有限公司
     * asnNo : 1001418
     * dateArrived : 2019-10-23
     * docDate : 2019-10-21
     * docStatusName : 已确认
     * totalAmt : 1168
     * asnType : PO
     * asnTypeName : 采购
     * receiptType : 1
     * receiptTypeName : 正常采购
     * docStatus : CO
     * created : 2019-10-21 09:32:22
     * createdByName : System
     * isExported : N
     * siteDocNo : 1001418
     * packageCount : 2
     * priceDiffCount : 0
     */

    private int asnId;
    private int bpartnerId;
    private int vendorId;
    private int warehouseId;
    private String warehouseName;
    private String bpartnerName;
    private String vendorName;
    private String asnNo;
    private String dateArrived;
    private String docDate;
    private String docStatusName;
    private BigDecimal totalAmt;
    private String asnType;
    private String asnTypeName;
    private String receiptType;
    private String receiptTypeName;
    private String docStatus;
    private String created;
    private String createdByName;
    private String isExported;
    private String siteDocNo;
    private String packageCount;
    private String priceDiffCount;

    public int getAsnId() {
        return asnId;
    }

    public void setAsnId(int asnId) {
        this.asnId = asnId;
    }

    public int getBpartnerId() {
        return bpartnerId;
    }

    public void setBpartnerId(int bpartnerId) {
        this.bpartnerId = bpartnerId;
    }

    public int getVendorId() {
        return vendorId;
    }

    public void setVendorId(int vendorId) {
        this.vendorId = vendorId;
    }

    public int getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(int warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public String getBpartnerName() {
        return bpartnerName;
    }

    public void setBpartnerName(String bpartnerName) {
        this.bpartnerName = bpartnerName;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getAsnNo() {
        return asnNo;
    }

    public void setAsnNo(String asnNo) {
        this.asnNo = asnNo;
    }

    public String getDateArrived() {
        return dateArrived;
    }

    public void setDateArrived(String dateArrived) {
        this.dateArrived = dateArrived;
    }

    public String getDocDate() {
        return docDate;
    }

    public void setDocDate(String docDate) {
        this.docDate = docDate;
    }

    public String getDocStatusName() {
        return docStatusName;
    }

    public void setDocStatusName(String docStatusName) {
        this.docStatusName = docStatusName;
    }

    public BigDecimal getTotalAmt() {
        return totalAmt;
    }

    public void setTotalAmt(BigDecimal totalAmt) {
        this.totalAmt = totalAmt;
    }

    public String getAsnType() {
        return asnType;
    }

    public void setAsnType(String asnType) {
        this.asnType = asnType;
    }

    public String getAsnTypeName() {
        return asnTypeName;
    }

    public void setAsnTypeName(String asnTypeName) {
        this.asnTypeName = asnTypeName;
    }

    public String getReceiptType() {
        return receiptType;
    }

    public void setReceiptType(String receiptType) {
        this.receiptType = receiptType;
    }

    public String getReceiptTypeName() {
        return receiptTypeName;
    }

    public void setReceiptTypeName(String receiptTypeName) {
        this.receiptTypeName = receiptTypeName;
    }

    public String getDocStatus() {
        return docStatus;
    }

    public void setDocStatus(String docStatus) {
        this.docStatus = docStatus;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getCreatedByName() {
        return createdByName;
    }

    public void setCreatedByName(String createdByName) {
        this.createdByName = createdByName;
    }

    public String getIsExported() {
        return isExported;
    }

    public void setIsExported(String isExported) {
        this.isExported = isExported;
    }

    public String getSiteDocNo() {
        return siteDocNo;
    }

    public void setSiteDocNo(String siteDocNo) {
        this.siteDocNo = siteDocNo;
    }

    public String getPackageCount() {
        return packageCount;
    }

    public void setPackageCount(String packageCount) {
        this.packageCount = packageCount;
    }

    public String getPriceDiffCount() {
        return priceDiffCount;
    }

    public void setPriceDiffCount(String priceDiffCount) {
        this.priceDiffCount = priceDiffCount;
    }
}
