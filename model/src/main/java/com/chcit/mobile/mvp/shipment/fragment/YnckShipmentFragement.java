package com.chcit.mobile.mvp.shipment.fragment;

import com.chcit.mobile.mvp.common.model.MenuTypeEnum;

/**
 * 院内出库页面
 */
public class YnckShipmentFragement extends ShipmentFragment{

    public YnckShipmentFragement() {
        this.type = MenuTypeEnum.YNCK_SHIPMENT;
    }
}
