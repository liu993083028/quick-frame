package com.chcit.mobile.mvp.packmanager.adapter;

import androidx.annotation.Nullable;
import android.view.View;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chcit.mobile.R;
import com.chcit.mobile.helper.recycler.BaseItemChangeQuickAdapter;
import com.chcit.mobile.mvp.entity.PackageBean;

import java.util.List;

public class PackageMergeAdapter extends BaseItemChangeQuickAdapter<PackageBean,BaseViewHolder> {

    private View.OnClickListener mOnClickListener;
    public PackageMergeAdapter(@Nullable List<PackageBean> data) {
        super(R.layout.item_package_scan_list,data);


    }

    public void setOnClickListener(View.OnClickListener onClickListener){
        mOnClickListener = onClickListener;
    }
    @Override
    protected void convert(final BaseViewHolder helper, final PackageBean item) {
        int i = helper.getAdapterPosition();
        if(thisPosition == i){
            helper.getView(R.id.ll_package_scan_item).setBackgroundColor(mContext.getResources().getColor(R.color.LavenderBlush));
        }else if((i+2)%2 == 0){
            helper.getView(R.id.ll_package_scan_item).setBackgroundColor(mContext.getResources().getColor(R.color.gray_f2));
        }else{
            helper.getView(R.id.ll_package_scan_item).setBackgroundColor(mContext.getResources().getColor(R.color.white));
        }

        helper.setText(R.id.tv_package_scan_no,String.valueOf(i+1));
        helper.setText(R.id.tv_package_scan_packageNo,item.getPackageNo());
        helper.setText(R.id.tv_package_scan_unitQty,item.getQty()+"");
    }



}
