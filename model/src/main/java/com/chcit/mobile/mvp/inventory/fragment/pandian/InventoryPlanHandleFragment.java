package com.chcit.mobile.mvp.inventory.fragment.pandian;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSONObject;
import com.chcit.custom.view.loader.LoaderStyle;
import com.chcit.custom.view.loader.QuickLoader;
import com.chcit.mobile.R;
import com.chcit.mobile.app.base.SupportToolbarFragement;
import com.chcit.mobile.di.component.DaggerInventoryPlanHandleComponent;
import com.chcit.mobile.di.module.CommonModule;
import com.chcit.mobile.di.module.InventoryPlanHandleModule;
import com.chcit.mobile.mvp.entity.Statu;
import com.chcit.mobile.mvp.inventory.contract.InventoryPlanHandleContract;
import com.chcit.mobile.mvp.inventory.entity.InventoryPackageBean;
import com.chcit.mobile.mvp.inventory.presenter.InventoryPlanHandlePresenter;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.jess.arms.di.component.AppComponent;
import com.jess.arms.utils.ArmsUtils;
import com.xuexiang.xui.widget.toast.XToast;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.OnClick;

import static com.jess.arms.utils.Preconditions.checkNotNull;


public class InventoryPlanHandleFragment extends SupportToolbarFragement<InventoryPlanHandlePresenter> implements InventoryPlanHandleContract.View {

    @BindView(R.id.tv_inventory_handle_index)
    TextView tvIndex;
    @BindView(R.id.tv_inventory_handle_productName)
    TextView tvProductName;
    @BindView(R.id.tv_inventory_handle_productSpec)
    TextView tvProductSpec;
    @BindView(R.id.tv_inventory_handle_DocumentNo)
    TextView tvDocumentNo;
    @BindView(R.id.tv_inventory_handle_productCode)
    TextView tvProductCode;
    @BindView(R.id.tv_inventory_handle_uom)
    TextView tvUom;
    @BindView(R.id.tv_inventory_handle_lot)
    TextView tvLot;
    @BindView(R.id.tv_inventory_handle_guaranteeDate)
    TextView tvGuaranteeDate;
    @BindView(R.id.tv_inventory_handle_locator)
    TextView tvLocator;
    @BindView(R.id.et_inventory_handle_description)
    AppCompatEditText etDescription;
    @BindView(R.id.tv_inventory_handle_manufacturer)
    TextView tvManufacturer;
    @BindView(R.id.tv_inventory_handle_storageStatus)
    TextView tvStorageStatus;
    @BindView(R.id.tv_inventory_handle_vendor)
    TextView tvVendor;
    @BindView(R.id.tv_inventory_handle_qtyDiff)
    TextView tvQtyDiff;
    @BindView(R.id.tv_inventory_handle_qtyBook)
    TextView tvQtyBooK;
    @BindView(R.id.et_inventory_handle_QtyCount)
    AppCompatEditText etQtyCount;
    @BindView(R.id.bt_inventory_handle_cancel)
    Button btCancel;
    @BindView(R.id.bt_inventory_handle_ok)
    Button btOk;
    @BindView(R.id.buttom_bar_group)
    LinearLayout buttomBarGroup;
    @BindView(R.id.tv_inventory_handle_inventoryReason)
    MaterialSpinner reasonSpinner;

    private InventoryPackageBean storageBean = null;
    private String reasonId = null;
    public static InventoryPlanHandleFragment newInstance(InventoryPackageBean storageBean) {
        InventoryPlanHandleFragment fragment = new InventoryPlanHandleFragment();
        fragment.storageBean = storageBean;
        return fragment;
    }

    @Override
    public void setupFragmentComponent(@NonNull AppComponent appComponent) {
        DaggerInventoryPlanHandleComponent //如找不到该类,请编译一下项目
                .builder()
                .appComponent(appComponent)
                .inventoryPlanHandleModule(new InventoryPlanHandleModule(this))
                .commonModule(new CommonModule())
                .build()
                .inject(this);
    }

    @Override
    public View initView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_inventory_plan_handle, container, false);
    }

    @Override
    public void initData(@Nullable Bundle savedInstanceState) {
        mCommonToolbar.setTitleText("盘点处理");
        mCommonToolbar.getLeftMenuView(0).setOnClickListener(l -> {
            pop();
        });
        initShowData();
    }


    @Override
    public void setData(@Nullable Object data) {

    }

    @Override
    public void showLoading() {
        QuickLoader.showLoading(mContext,LoaderStyle.SemiCircleSpinIndicator);
    }

    @Override
    public void hideLoading() {
        QuickLoader.stopLoading();
    }

    @Override
    public void showMessage(@NonNull String message) {
        checkNotNull(message);
         XToast.info(mContext,message).show();
    }

    @Override
    public void launchActivity(@NonNull Intent intent) {
        checkNotNull(intent);
        ArmsUtils.startActivity(intent);
    }

    @Override
    public void killMyself() {
       pop();
    }


    @OnClick({R.id.bt_inventory_handle_cancel, R.id.bt_inventory_handle_ok})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.bt_inventory_handle_cancel:
                pop();
                break;
            case R.id.bt_inventory_handle_ok:
             /*
             "inventoryPlanLineId:1012103
            inventoryReason:03
            description:NAN
            qtyCount:80
         */


                JSONObject data = new JSONObject();
                data.put("inventoryPlanLineId", storageBean.getInventoryPlanLineId());
                if(reasonId != null){
                    data.put("inventoryReason",reasonId);
                }
                String description = Objects.requireNonNull(etDescription.getText()).toString();
                if(!description.isEmpty()){
                    data.put("description",description);
                }
                int qtyCount =Integer.valueOf( Objects.requireNonNull(etQtyCount.getText()).toString());
                data.put("qtyCount", qtyCount);
                mPresenter.doConfirm(data, () -> {
                    showMessage("操作成功！");
                    setFragmentResult(RESULT_OK,null);
                    pop();
                });
                break;
        }
    }

    private void initShowData() {

        tvIndex.setText(storageBean.getProductName());
        tvDocumentNo.setText(String.valueOf(storageBean.getInventoryPlanLineId()));
        tvProductName.setText(storageBean.getProductName());
        tvProductSpec.setText(storageBean.getProductSpec());
        tvManufacturer.setText(storageBean.getManufacturer());
        tvProductCode.setText(storageBean.getProductCode());
        tvUom.setText(storageBean.getUomName());
        tvLot.setText(storageBean.getLot());
        tvGuaranteeDate.setText(storageBean.getGuaranteeDate());
        tvVendor.setText(storageBean.getManufacturer());
        tvLocator.setText(storageBean.getLocatorValue());
        tvStorageStatus.setText(storageBean.getStorageStatusName());
        tvQtyDiff.setText(String.valueOf(storageBean.getQtyDiff()));
        tvQtyBooK.setText(String.valueOf(storageBean.getQtyBook()));
        etQtyCount.setText(String.valueOf(storageBean.getQtyCount()));//实盘数量
        /* etInvoiceNo.setText(storageBean.getTaxInvoiceNo());*/

    }

    @Override
    public void onEnterAnimationEnd(Bundle savedInstanceState) {
        super.onEnterAnimationEnd(savedInstanceState);
        initEvent();
        initSpinner();
    }

    private void initEvent() {
        etQtyCount.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s != null){
                    BigDecimal number;
                    if(s.toString().isEmpty()){
                        number = BigDecimal.ZERO.subtract(storageBean.getQtyBook());
                    }else{
                        number = new BigDecimal(s.toString()).subtract(storageBean.getQtyBook());
                    }
                    tvQtyDiff.setText(String.valueOf(number));
                }
            }
        });

    }
    private void initSpinner(){
        reasonSpinner.setOnClickListener(new View.OnClickListener() {
            int i = 0;
            @Override
            public void onClick(View v) {
                if(i==0){
                    mPresenter.getReasons(1000109,list ->{
                        i++;
                        Statu statu = new Statu("","请选择报损原因");
                        List<Statu> status = new ArrayList<>();
                        status.add(statu);
                        status.addAll(list);
                        reasonSpinner.setItems(status);
                        reasonSpinner.setOnItemSelectedListener((view, position, id, item) -> {
                            if(position >-1&&position<list.size()){
                                if(position == 0){
                                    reasonId = null;
                                    reasonSpinner.setText("");
                                }else{
                                    reasonId = list.get(position).getId();
                                }

                            }
                        });
                    });
                }
            }
        });
    }
}
