package com.chcit.mobile.mvp.inventory.presenter;

import android.app.Application;

import com.alibaba.fastjson.JSONObject;
import com.chcit.mobile.common.SPDErrorHandle;
import com.chcit.mobile.mvp.entity.PackageBean;
import com.chcit.mobile.mvp.inventory.entity.PackageHistoryBean;
import com.chcit.mobile.mvp.inventory.model.PackageInfoQueryModel;
import com.jess.arms.integration.AppManager;
import com.jess.arms.di.scope.FragmentScope;
import com.jess.arms.mvp.BasePresenter;
import com.jess.arms.http.imageloader.ImageLoader;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import me.jessyan.rxerrorhandler.core.RxErrorHandler;

import javax.inject.Inject;

import com.chcit.mobile.mvp.inventory.contract.PackageInfoQueryContract;
import com.jess.arms.utils.RxLifecycleUtils;

import java.util.List;


/**
 * ================================================
 * Description:
 * <p>
 * Created by MVPArmsTemplate on 09/18/2019 16:50
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms">Star me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms/wiki">See me</a>
 * <a href="https://github.com/JessYanCoding/MVPArmsTemplate">模版请保持更新</a>
 * ================================================
 */
@FragmentScope
public class PackageInfoQueryPresenter extends BasePresenter<PackageInfoQueryContract.Model, PackageInfoQueryContract.View> {
    @Inject
    RxErrorHandler mErrorHandler;
    @Inject
    Application mApplication;
    @Inject
    ImageLoader mImageLoader;
    @Inject
    AppManager mAppManager;
    @Inject
    PackageInfoQueryModel cModel;

    @Inject
    public PackageInfoQueryPresenter(PackageInfoQueryContract.Model model, PackageInfoQueryContract.View rootView) {
        super(model, rootView);
    }

    /**
     * productId:1037396
     * warehouseId:1000214
     */
    public void getPakcageBaseInfo(JSONObject paramReq, Consumer<List<PackageBean>> consumer){
        cModel.queryPackageInfo(paramReq)
                .map(result -> result.toJavaList(PackageBean.class))
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                })
                .subscribe(new SPDErrorHandle<List<PackageBean>>(mErrorHandler) {
                    @Override
                    public void onNext(List<PackageBean> lists) {
                        try {
                            consumer.accept(lists);
                        } catch (Exception e) {
                            onError(e);
                        }
                    }
                });
    }

    /**
     *
     * @param paramReq
     * @param consumer
     */
    public void getPakcageHistoryInfo(JSONObject paramReq, Consumer<List<PackageHistoryBean>> consumer){
        cModel.queryPackageHistoryInfo(paramReq)
                .map(result -> result.toJavaList(PackageHistoryBean.class))
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                })
                .subscribe(new SPDErrorHandle<List<PackageHistoryBean>>(mErrorHandler) {
                    @Override
                    public void onNext(List<PackageHistoryBean> lists) {
                        try {
                            consumer.accept(lists);
                        } catch (Exception e) {
                            onError(e);
                        }
                    }
                });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mErrorHandler = null;
        this.mAppManager = null;
        this.mImageLoader = null;
        this.mApplication = null;
    }
}
