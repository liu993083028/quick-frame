package com.chcit.mobile.mvp.entity;

public class StorageBean  {


    /**
     * M_Warehouse_ID : 1000216
     * WarehouseName : 江苏省中医院第三方仓库
     * ZoneName : 西药库
     * AD_Org_ID : 1000062
     * OrgName : 南京医药股份有限公司
     * M_Product_ID : 1099341
     * ProductName : 盐酸表柔比星注射剂(艾达生)
     * MedicineName : 盐酸表柔比星注射剂
     * ProductSpec : 10mg
     * Manufacturer : 浙江海正药业股份有限公司
     * UOMName : 瓶
     * ProductCode : 2386_1076
     * PricePO : 92.56
     * PriceList : 99.04
     * AmountPricePO : 22677.2
     * AmountPriceList : 24264.8
     * ReceiptDate : 2018-06-29 09:00:22.0
     * ConserveLevel : 0
     * ConservationDays : 90
     * ConservationDaysMin : 7
     * ConservationLeftDays : 30
     * M_AttributeSetInstance_ID : 1068508
     * M_Locator_ID : 1014915
     * LocatorName : 01Y01-01-02-03-04
     * Vendor_ID : 1023698
     * VendorName : 海正辉瑞制药有限公司
     * Lot : 18008111
     * GuaranteeDate : 2020-02-29
     * NeerGuaranteeDate : N
     * UOMQty : 245
     * UOMBaseQty : 0
     * UOMQtyAvailable : 245
     * UOMBaseQtyAvailable : 0
     * QtyOnHand : 245
     * QtyReserved : 0
     * QtyAllocated : 0
     * QtyAvailable : 245
     * StorageStatus : S
     * StorageStatusName : 正常
     * QtyBaseOnHand : 245
     * BaseUOMName : 瓶
     * QtyFeedingOnHand : 0
     * QtyFeedingPrice : 0
     * ProductTypeName : 西药
     *
     */

    private int M_Warehouse_ID;
    private String WarehouseName;
    private String ZoneName;
    private int AD_Org_ID;
    private String OrgName;
    private int M_Product_ID;
    private String ProductName;
    private String MedicineName;
    private String ProductSpec;
    private String Manufacturer;
    private String UOMName;
    private String ProductCode;
    private double PricePO;
    private double PriceList;
    private double AmountPricePO;
    private double AmountPriceList;
    private String ReceiptDate;
    private int ConserveLevel;
    private int ConservationDays;
    private int ConservationDaysMin;
    private int ConservationLeftDays;
    private int M_AttributeSetInstance_ID;
    private int M_Locator_ID;
    private String LocatorName;
    private String LocatorValue;
    private int Vendor_ID;
    private String VendorName;
    private String Lot;
    private String GuaranteeDate;
    private String NeerGuaranteeDate;
    private int UOMQty;
    private int UOMBaseQty;
    private int UOMQtyAvailable;
    private int UOMBaseQtyAvailable;
    private int QtyOnHand;
    private int QtyReserved;
    private int QtyAllocated;
    private int QtyAvailable;
    private String StorageStatus;
    private String StorageStatusName;
    private int QtyBaseOnHand;
    private String BaseUOMName;
    private String QtyFeedingOnHand;
    private String QtyFeedingPrice;
    private String ProductTypeName;
    private int QtyCount;
    private int QtyBook;
    private int BaseQtyBook;
    private int BaseQtyCount;
    private int BaseUOMQty;
    private String M_InventoryPlanLine_ID;
    private String  M_InventoryPlan_ID;
    private String DocumentNo;
    private String M_Attributesetinstance_ID;
    public int getM_Warehouse_ID() {
        return M_Warehouse_ID;
    }

    public void setM_Warehouse_ID(int M_Warehouse_ID) {
        this.M_Warehouse_ID = M_Warehouse_ID;
    }

    public String getWarehouseName() {
        return WarehouseName;
    }

    public void setWarehouseName(String WarehouseName) {
        this.WarehouseName = WarehouseName;
    }

    public String getZoneName() {
        return ZoneName;
    }

    public void setZoneName(String ZoneName) {
        this.ZoneName = ZoneName;
    }

    public int getAD_Org_ID() {
        return AD_Org_ID;
    }

    public void setAD_Org_ID(int AD_Org_ID) {
        this.AD_Org_ID = AD_Org_ID;
    }

    public String getOrgName() {
        return OrgName;
    }

    public void setOrgName(String OrgName) {
        this.OrgName = OrgName;
    }

    public int getM_Product_ID() {
        return M_Product_ID;
    }

    public void setM_Product_ID(int M_Product_ID) {
        this.M_Product_ID = M_Product_ID;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String ProductName) {
        this.ProductName = ProductName;
    }

    public String getMedicineName() {
        return MedicineName;
    }

    public void setMedicineName(String MedicineName) {
        this.MedicineName = MedicineName;
    }

    public String getProductSpec() {
        return ProductSpec;
    }

    public void setProductSpec(String ProductSpec) {
        this.ProductSpec = ProductSpec;
    }

    public String getManufacturer() {
        return Manufacturer;
    }

    public void setManufacturer(String Manufacturer) {
        this.Manufacturer = Manufacturer;
    }

    public String getUOMName() {
        return UOMName;
    }

    public void setUOMName(String UOMName) {
        this.UOMName = UOMName;
    }

    public String getProductCode() {
        return ProductCode;
    }

    public void setProductCode(String ProductCode) {
        this.ProductCode = ProductCode;
    }

    public double getPricePO() {
        return PricePO;
    }

    public void setPricePO(double PricePO) {
        this.PricePO = PricePO;
    }

    public double getPriceList() {
        return PriceList;
    }

    public void setPriceList(double PriceList) {
        this.PriceList = PriceList;
    }

    public double getAmountPricePO() {
        return AmountPricePO;
    }

    public void setAmountPricePO(double AmountPricePO) {
        this.AmountPricePO = AmountPricePO;
    }

    public double getAmountPriceList() {
        return AmountPriceList;
    }

    public void setAmountPriceList(double AmountPriceList) {
        this.AmountPriceList = AmountPriceList;
    }

    public String getReceiptDate() {
        return ReceiptDate;
    }

    public void setReceiptDate(String ReceiptDate) {
        this.ReceiptDate = ReceiptDate;
    }

    public int getConserveLevel() {
        return ConserveLevel;
    }

    public void setConserveLevel(int ConserveLevel) {
        this.ConserveLevel = ConserveLevel;
    }

    public int getConservationDays() {
        return ConservationDays;
    }

    public void setConservationDays(int ConservationDays) {
        this.ConservationDays = ConservationDays;
    }

    public int getConservationDaysMin() {
        return ConservationDaysMin;
    }

    public void setConservationDaysMin(int ConservationDaysMin) {
        this.ConservationDaysMin = ConservationDaysMin;
    }

    public int getConservationLeftDays() {
        return ConservationLeftDays;
    }

    public void setConservationLeftDays(int ConservationLeftDays) {
        this.ConservationLeftDays = ConservationLeftDays;
    }

    public int getM_AttributeSetInstance_ID() {
        return M_AttributeSetInstance_ID;
    }

    public void setM_AttributeSetInstance_ID(int M_AttributeSetInstance_ID) {
        this.M_AttributeSetInstance_ID = M_AttributeSetInstance_ID;
    }

    public int getM_Locator_ID() {
        return M_Locator_ID;
    }

    public void setM_Locator_ID(int M_Locator_ID) {
        this.M_Locator_ID = M_Locator_ID;
    }

    public String getLocatorName() {
        return LocatorName;
    }

    public void setLocatorName(String LocatorName) {
        this.LocatorName = LocatorName;
    }

    public int getVendor_ID() {
        return Vendor_ID;
    }

    public void setVendor_ID(int Vendor_ID) {
        this.Vendor_ID = Vendor_ID;
    }

    public String getVendorName() {
        return VendorName;
    }

    public void setVendorName(String VendorName) {
        this.VendorName = VendorName;
    }

    public String getLot() {
        return Lot;
    }

    public void setLot(String Lot) {
        this.Lot = Lot;
    }

    public String getGuaranteeDate() {
        return GuaranteeDate;
    }

    public void setGuaranteeDate(String GuaranteeDate) {
        this.GuaranteeDate = GuaranteeDate;
    }

    public String getNeerGuaranteeDate() {
        return NeerGuaranteeDate;
    }

    public void setNeerGuaranteeDate(String NeerGuaranteeDate) {
        this.NeerGuaranteeDate = NeerGuaranteeDate;
    }

    public int getUOMQty() {
        return UOMQty;
    }

    public void setUOMQty(int UOMQty) {
        this.UOMQty = UOMQty;
    }

    public int getUOMBaseQty() {
        return UOMBaseQty;
    }

    public void setUOMBaseQty(int UOMBaseQty) {
        this.UOMBaseQty = UOMBaseQty;
    }

    public int getUOMQtyAvailable() {
        return UOMQtyAvailable;
    }

    public void setUOMQtyAvailable(int UOMQtyAvailable) {
        this.UOMQtyAvailable = UOMQtyAvailable;
    }

    public int getUOMBaseQtyAvailable() {
        return UOMBaseQtyAvailable;
    }

    public void setUOMBaseQtyAvailable(int UOMBaseQtyAvailable) {
        this.UOMBaseQtyAvailable = UOMBaseQtyAvailable;
    }

    public int getQtyOnHand() {
        return QtyOnHand;
    }

    public void setQtyOnHand(int QtyOnHand) {
        this.QtyOnHand = QtyOnHand;
    }

    public int getQtyReserved() {
        return QtyReserved;
    }

    public void setQtyReserved(int QtyReserved) {
        this.QtyReserved = QtyReserved;
    }

    public int getQtyAllocated() {
        return QtyAllocated;
    }

    public void setQtyAllocated(int QtyAllocated) {
        this.QtyAllocated = QtyAllocated;
    }

    public int getQtyAvailable() {
        return QtyAvailable;
    }

    public void setQtyAvailable(int QtyAvailable) {
        this.QtyAvailable = QtyAvailable;
    }

    public String getStorageStatus() {
        return StorageStatus;
    }

    public void setStorageStatus(String StorageStatus) {
        this.StorageStatus = StorageStatus;
    }

    public String getStorageStatusName() {
        return StorageStatusName;
    }

    public void setStorageStatusName(String StorageStatusName) {
        this.StorageStatusName = StorageStatusName;
    }

    public int getQtyBaseOnHand() {
        return QtyBaseOnHand;
    }

    public void setQtyBaseOnHand(int QtyBaseOnHand) {
        this.QtyBaseOnHand = QtyBaseOnHand;
    }

    public String getBaseUOMName() {
        return BaseUOMName;
    }

    public void setBaseUOMName(String BaseUOMName) {
        this.BaseUOMName = BaseUOMName;
    }

    public String getQtyFeedingOnHand() {
        return QtyFeedingOnHand;
    }

    public void setQtyFeedingOnHand(String QtyFeedingOnHand) {
        this.QtyFeedingOnHand = QtyFeedingOnHand;
    }

    public String getQtyFeedingPrice() {
        return QtyFeedingPrice;
    }

    public void setQtyFeedingPrice(String QtyFeedingPrice) {
        this.QtyFeedingPrice = QtyFeedingPrice;
    }

    public String getProductTypeName() {
        return ProductTypeName;
    }

    public void setProductTypeName(String ProductTypeName) {
        this.ProductTypeName = ProductTypeName;
    }

    public int getQtyCount() {
        return QtyCount;
    }

    public void setQtyCount(int qtyCount) {
        QtyCount = qtyCount;
    }

    public int getQtyBook() {
        return QtyBook;
    }

    public void setQtyBook(int qtyBook) {
        QtyBook = qtyBook;
    }

    public String getLocatorValue() {
        return LocatorValue;
    }

    public void setLocatorValue(String locatorValue) {
        LocatorValue = locatorValue;
    }

    public int getBaseQtyBook() {
        return BaseQtyBook;
    }

    public void setBaseQtyBook(int baseQtyBook) {
        BaseQtyBook = baseQtyBook;
    }

    public int getBaseQtyCount() {
        return BaseQtyCount;
    }

    public void setBaseQtyCount(int baseQtyCount) {
        BaseQtyCount = baseQtyCount;
    }

    public int getBaseUOMQty() {
        return BaseUOMQty;
    }

    public void setBaseUOMQty(int baseUOMQty) {
        BaseUOMQty = baseUOMQty;
    }

    public String getM_InventoryPlanLine_ID() {
        return M_InventoryPlanLine_ID;
    }

    public void setM_InventoryPlanLine_ID(String m_InventoryPlanLine_ID) {
        M_InventoryPlanLine_ID = m_InventoryPlanLine_ID;
    }

    public String getM_InventoryPlan_ID() {
        return M_InventoryPlan_ID;
    }

    public void setM_InventoryPlan_ID(String m_InventoryPlan_ID) {
        M_InventoryPlan_ID = m_InventoryPlan_ID;
    }

    public String getDocumentNo() {
        return DocumentNo;
    }

    public void setDocumentNo(String documentNo) {
        DocumentNo = documentNo;
    }

    public String getM_Attributesetinstance_ID() {
        return M_Attributesetinstance_ID;
    }

    public void setM_Attributesetinstance_ID(String m_Attributesetinstance_ID) {
        M_Attributesetinstance_ID = m_Attributesetinstance_ID;
    }
}
