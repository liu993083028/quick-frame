package com.chcit.mobile.mvp.hight.model;

import android.app.Application;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.chcit.mobile.app.exception.BaseException;
import com.chcit.mobile.mvp.entity.BaseResponse;
import com.chcit.mobile.mvp.entity.HightCostBindBean;
import com.chcit.mobile.mvp.entity.ResultBean;
import com.chcit.mobile.mvp.hight.api.HightApi;
import com.google.gson.Gson;
import com.jess.arms.integration.IRepositoryManager;
import com.jess.arms.mvp.BaseModel;

import com.jess.arms.di.scope.FragmentScope;

import javax.inject.Inject;

import com.chcit.mobile.mvp.hight.contract.HightCostBindContract;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;


@FragmentScope
public class HightCostBindModel extends BaseModel implements HightCostBindContract.Model {
    @Inject
    Gson mGson;
    @Inject
    Application mApplication;

    @Inject
    public HightCostBindModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mGson = null;
        this.mApplication = null;
    }


    @Override
    public Observable<List<HightCostBindBean>> queryBind(JSONObject json) {
        return mRepositoryManager.obtainRetrofitService(HightApi.class)
                .queryBind(json)
                .map(result -> {
                    if(result.isSuccess()){
                        if(result.getRows().size()>0){
                            return  result.getRows().toJavaList(HightCostBindBean.class);
                        }else{
                            return new ArrayList<HightCostBindBean>();
                        }
                    }else{
                        throw new BaseException(result.getMsg());
                    }
                }).subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<BaseResponse> bindPackage(String rfVal, String packageNo) {
        return mRepositoryManager.obtainRetrofitService(HightApi.class)
                .bindPackage(rfVal,packageNo)
                .subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<Boolean> unBindPackage(String pakcageId) {
        return mRepositoryManager.obtainRetrofitService(HightApi.class)
                .unBindPackage(pakcageId)
                .map(result -> {
                    if(result.isSuccess()){
                        return  true;
                    }else{
                        throw new BaseException(result.getMsg());
                    }
                }).subscribeOn(Schedulers.io());
    }
}