package com.chcit.mobile.mvp.packmanager.model;

import android.app.Application;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.chcit.mobile.app.exception.BaseException;
import com.chcit.mobile.mvp.common.api.service.CommonService;
import com.google.gson.Gson;
import com.jess.arms.integration.IRepositoryManager;
import com.jess.arms.mvp.BaseModel;

import com.jess.arms.di.scope.FragmentScope;

import javax.inject.Inject;

import com.chcit.mobile.mvp.packmanager.contract.PackageManagerContract;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;


@FragmentScope
public class PackageManagerModel extends BaseModel implements PackageManagerContract.Model {
    @Inject
    Gson mGson;
    @Inject
    Application mApplication;

    @Inject
    public PackageManagerModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mGson = null;
        this.mApplication = null;
    }


    @Override
    public Observable<String> packageMergeConfirm(JSONObject json) {
        return mRepositoryManager.obtainRetrofitService(CommonService.class)
                .packageMergeConfirm(json)
                .map(result -> {
                    JSONObject jsonObject = JSONObject.parseObject(result);
                    if (jsonObject.getBoolean("success")) {
                        String data = jsonObject.getString("data");//新包装ID
                        return data;
                    } else {
                        String msg = jsonObject.getString("msg");
                        if (msg != null && !msg.isEmpty()) {
                            throw new Exception(msg);
                        } else {
                            throw new Exception("未知错误！");
                        }
                    }

                }).subscribeOn(Schedulers.io());
    }


    @Override
    public Observable<String> packageSplitConfirm(JSONObject json) {
        return mRepositoryManager.obtainRetrofitService(CommonService.class)
                .packageSplitConfirm(json)
                .map(result -> {
                    JSONObject jsonObject = JSONObject.parseObject(result);
                    if (jsonObject.getBoolean("success")) {
                        String data = jsonObject.getString("data");//新包装ID
                        return data;
                    } else {
                        String msg = jsonObject.getString("msg");
                        if (msg != null && !msg.isEmpty()) {
                            throw new Exception(msg);
                        } else {
                            throw new Exception("未知错误！");
                        }
                    }

                }).subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<JSONArray> queryPackageInfo(JSONObject json) {
        return mRepositoryManager.obtainRetrofitService(CommonService.class)
                .queryPackageInfo(json)
                .map(result -> {
                    JSONObject jsonObject = JSONObject.parseObject(result);
                    if (jsonObject.getBoolean("success")) {
                        if (jsonObject.getInteger("total") > 0) {
                            return jsonObject.getJSONArray("rows");
                        } else {
                            throw new Exception("包装不存在或已出库！");
                        }

                    } else {
                        String msg = jsonObject.getString("msg");
                        if (msg != null && !msg.isEmpty()) {
                            throw new Exception(msg);
                        } else {
                            throw new Exception("未知错误！");
                        }
                    }
                }).subscribeOn(Schedulers.io());
    }



        /* productId:1037383
     warehouseId:1000214*/
    @Override
    public Observable<JSONArray> queryUnitPackQty(JSONObject json) {
        return mRepositoryManager.obtainRetrofitService(CommonService.class)
                .queryPackageUnit(json)
                .map(result -> {
                    JSONObject jsonObject = JSONObject.parseObject(result);
                    if (jsonObject.getBoolean("success")) {
                        if (jsonObject.getInteger("total") > 0) {
                            return jsonObject.getJSONArray("rows");
                        } else {
                            throw new BaseException("没有查询到包装定数信息！");
                        }

                    } else {
                        String msg = jsonObject.getString("msg");
                        if (msg != null && !msg.isEmpty()) {
                            throw new Exception(msg);
                        } else {
                            throw new Exception("未知错误！");
                        }
                    }
                }).subscribeOn(Schedulers.io());
    }




    @Override
    public Observable<String> packageSubConfirm(JSONObject json) {
        return mRepositoryManager.obtainRetrofitService(CommonService.class)
                .packageSubConfirm(json)
                .map(result -> {
                    JSONObject jsonObject = JSONObject.parseObject(result);
                    if (jsonObject.getBoolean("success")) {
                        String data = jsonObject.getString("data");//新包装ID
                        return data;
                    } else {
                        String msg = jsonObject.getString("msg");
                        if (msg != null && !msg.isEmpty()) {
                            throw new Exception(msg);
                        } else {
                            throw new Exception("未知错误！");
                        }
                    }

                }).subscribeOn(Schedulers.io());
    }

}