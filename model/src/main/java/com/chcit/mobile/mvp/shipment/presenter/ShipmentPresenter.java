package com.chcit.mobile.mvp.shipment.presenter;

import android.app.Application;
import com.alibaba.fastjson.JSONObject;
import com.chcit.mobile.common.SPDErrorHandle;
import com.chcit.mobile.mvp.di.module.DirectShipmentModel;
import com.chcit.mobile.mvp.entity.BaseData;
import com.chcit.mobile.mvp.entity.ShipmentBean;
import com.chcit.mobile.mvp.entity.ResultBean;
import com.chcit.mobile.mvp.shipment.adapter.ShipmentAdapter;
import com.chcit.mobile.mvp.shipment.model.ShipmentDetailModel;
import com.jess.arms.integration.AppManager;
import com.jess.arms.di.scope.FragmentScope;
import com.jess.arms.mvp.BasePresenter;
import com.jess.arms.http.imageloader.ImageLoader;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import me.jessyan.rxerrorhandler.core.RxErrorHandler;
import javax.inject.Inject;
import com.chcit.mobile.mvp.shipment.contract.ShipmentContract;
import com.jess.arms.utils.RxLifecycleUtils;
import java.util.List;

@FragmentScope
public class ShipmentPresenter extends BasePresenter<ShipmentContract.Model, ShipmentContract.View> {
    @Inject
    RxErrorHandler mErrorHandler;
    @Inject
    Application mApplication;
    @Inject
    ImageLoader mImageLoader;
    @Inject
    AppManager mAppManager;
    @Inject
    ShipmentAdapter mAdapter;
    @Inject
    ShipmentDetailModel shipmentDetailModel;
    @Inject
    public ShipmentPresenter(ShipmentContract.Model model, ShipmentContract.View rootView) {
        super(model, rootView);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mErrorHandler = null;
        this.mAppManager = null;
        this.mImageLoader = null;
        this.mApplication = null;
    }

    //获取拣货任务明细
    public void getData(JSONObject json) {
        mModel.getShipments(json)
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                })
                .subscribe(new SPDErrorHandle<List<ShipmentBean>>(mErrorHandler) {
                    @Override
                    public void onNext(List<ShipmentBean> lists) {
                        mRootView.updateContentList(lists);
                    }
                });
    }

    public void doBatchConfirm(JSONObject json,Action action) {
        shipmentDetailModel.pickComfirm(json)
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                }).subscribe(new SPDErrorHandle<ResultBean>(mErrorHandler) {
            @Override
            public void onNext(ResultBean resultBean) {
                try {
                    action.run();
                } catch (Exception e) {
                    onError(e);
                }
            }
        });
    }
}
