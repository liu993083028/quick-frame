package com.chcit.mobile.mvp.common.model;

import android.app.Application;

import com.alibaba.fastjson.JSONObject;
import com.chcit.mobile.mvp.common.api.service.UserService;
import com.chcit.mobile.mvp.entity.ResultBean;
import com.google.gson.Gson;
import com.jess.arms.integration.IRepositoryManager;
import com.jess.arms.mvp.BaseModel;

import com.jess.arms.di.scope.FragmentScope;

import javax.inject.Inject;

import com.chcit.mobile.mvp.common.contract.MaintainDetailContract;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;


@FragmentScope
public class MaintainDetailModel extends BaseModel implements MaintainDetailContract.Model {
    @Inject
    Gson mGson;
    @Inject
    Application mApplication;

    @Inject
    public MaintainDetailModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mGson = null;
        this.mApplication = null;
    }

    @Override
    public Observable<ResultBean> doConfirm(JSONObject json) {
        return mRepositoryManager
                .obtainRetrofitService(UserService.class)
                .doConfirmMaintainResult(json).subscribeOn(Schedulers.io());
    }
}