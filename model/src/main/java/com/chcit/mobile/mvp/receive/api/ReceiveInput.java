package com.chcit.mobile.mvp.receive.api;

public class ReceiveInput {

    /**
     * asnLineId : 1051158
     * qtyPreCheck : 40
     * qtyReject : 0
     * locatorId : 1010510
     * rejectReason : 01
     * isControlledProduct : N
     */

    private String asnLineId;
    private String qtyPreCheck;
    private String qtyReject;
    private String locatorId;
    private String rejectReason;
    private String isControlledProduct;

    public String getAsnLineId() {
        return asnLineId;
    }

    public void setAsnLineId(String asnLineId) {
        this.asnLineId = asnLineId;
    }

    public String getQtyPreCheck() {
        return qtyPreCheck;
    }

    public void setQtyPreCheck(String qtyPreCheck) {
        this.qtyPreCheck = qtyPreCheck;
    }

    public String getQtyReject() {
        return qtyReject;
    }

    public void setQtyReject(String qtyReject) {
        this.qtyReject = qtyReject;
    }

    public String getLocatorId() {
        return locatorId;
    }

    public void setLocatorId(String locatorId) {
        this.locatorId = locatorId;
    }

    public String getRejectReason() {
        return rejectReason;
    }

    public void setRejectReason(String rejectReason) {
        this.rejectReason = rejectReason;
    }

    public String getIsControlledProduct() {
        return isControlledProduct;
    }

    public void setIsControlledProduct(String isControlledProduct) {
        this.isControlledProduct = isControlledProduct;
    }
}
