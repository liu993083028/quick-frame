package com.chcit.mobile.mvp;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.KeyEvent;
import com.chcit.mobile.app.base.BaseSupoortFragment;
import com.chcit.mobile.app.base.ScanActivity;
import com.chcit.mobile.common.DataKeys;
import com.chcit.mobile.mvp.entity.HomeItem;
import com.jess.arms.di.component.AppComponent;
import com.jess.arms.utils.ArmsUtils;
import com.chcit.mobile.di.component.DaggerTaskComponent;
import com.chcit.mobile.mvp.common.contract.TaskContract;
import com.chcit.mobile.mvp.common.presenter.TaskPresenter;
import com.chcit.mobile.R;
import com.xuexiang.xui.widget.toast.XToast;

import me.yokeyword.fragmentation.SupportFragment;

import static com.jess.arms.utils.Preconditions.checkNotNull;


public class TaskActivity extends ScanActivity<TaskPresenter> implements TaskContract.View {
    SupportFragment baseFragment = null;
    @Override
    public void setupActivityComponent(@NonNull AppComponent appComponent) {
        DaggerTaskComponent //如找不到该类,请编译一下项目
                .builder()
                .appComponent(appComponent)
                .view(this)
                .build()
                .inject(this);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {//点击的是返回键
            if (event.getAction() == KeyEvent.ACTION_DOWN && event.getRepeatCount() == 0) {//按键的按下事件
                if(this.mCommonToolbar!=null){
                    this.mCommonToolbar.getLeftMenuView(0).performClick();
                }
            } else if (event.getAction() == KeyEvent.ACTION_UP && event.getRepeatCount() == 0) {//按键的抬起事件

            }
            return true;
        }
        return super.dispatchKeyEvent(event);
    }



    @Override
    public int initView(@Nullable Bundle savedInstanceState) {

        return R.layout.activity_task; //如果你不需要框架帮你设置 setContentView(id) 需要自行设置,请返回 0
    }

    @Override
    public void initData(@Nullable Bundle savedInstanceState) {

        try {
            HomeItem homeItem = getIntent().getParcelableExtra(DataKeys.HOME_ITEM.name());
            baseFragment = (SupportFragment) homeItem.getActivity().newInstance();
        } catch (Exception e) {
            showMessage(e.getMessage());
            finish();
            return;
        }
        loadRootFragment(R.id.fl_container, baseFragment);

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showMessage(@NonNull String message) {
        checkNotNull(message);
        XToast.info(this,message).show();
    }

    @Override
    public void launchActivity(@NonNull Intent intent) {
        checkNotNull(intent);
        ArmsUtils.startActivity(intent);
    }

    @Override
    public void killMyself() {
        finish();
        baseFragment = null;

    }
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Message data = new Message();
        data.what = 0;
        data.obj = intent;
        if(baseFragment instanceof BaseSupoortFragment){
            ((BaseSupoortFragment)baseFragment).setData(data);
        }

    }
    @Override
    public void finish() {
        setResult(RESULT_OK);
        super.finish();

    }
}
