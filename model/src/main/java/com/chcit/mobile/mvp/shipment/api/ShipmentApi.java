package com.chcit.mobile.mvp.shipment.api;

import android.service.media.MediaBrowserService;

import com.alibaba.fastjson.JSONObject;
import com.chcit.mobile.common.HttpMethodContains;
import com.chcit.mobile.mvp.entity.BaseData;
import com.chcit.mobile.mvp.entity.ResultBean;
import com.chcit.mobile.mvp.entity.ShipmentBean;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

import static com.chcit.mobile.common.HttpMethodContains.LOGIN_QUERY_URL;

public interface ShipmentApi {

    @GET(HttpMethodContains.Shipment.QUERY_DETAIL_URL)
    Observable<BaseData<ShipmentBean>> queryDetail(@QueryMap JSONObject query);
}
