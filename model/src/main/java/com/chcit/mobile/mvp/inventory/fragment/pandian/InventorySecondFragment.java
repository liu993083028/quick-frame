package com.chcit.mobile.mvp.inventory.fragment.pandian;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chcit.custom.view.loader.QuickLoader;
import com.chcit.mobile.R;
import com.chcit.mobile.app.base.ScanToolQueryFragment;
import com.chcit.mobile.di.component.DaggerInventoryPackagePlanComponent;
import com.chcit.mobile.di.module.CommonModule;
import com.chcit.mobile.di.module.InventoryPackagePlanModule;
import com.chcit.mobile.helper.recycler.RecycleViewDivider;
import com.chcit.mobile.mvp.inventory.adapter.InventoryPackagePlanAdapter;
import com.chcit.mobile.mvp.inventory.contract.InventoryPlanContract;
import com.chcit.mobile.mvp.inventory.entity.InventoryBean;
import com.chcit.mobile.mvp.inventory.entity.InventoryPackageBean;
import com.chcit.mobile.mvp.inventory.presenter.InventoryPlanPackagePresenter;
import com.jess.arms.di.component.AppComponent;
import com.xuexiang.xui.widget.toast.XToast;

import javax.inject.Inject;
import butterknife.BindView;
import butterknife.OnClick;

import static com.jess.arms.utils.Preconditions.checkNotNull;

//盘点包装处理页面 第二级
public class InventorySecondFragment extends ScanToolQueryFragment<InventoryPlanPackagePresenter> implements InventoryPlanContract.View {

    private static final int REQUEST_CODE = -4;
    @BindView(R.id.lv_ipp_list)
    RecyclerView mRecyclerView;
    @BindView(R.id.rb_ipp_unDone)
    RadioButton rbUnDone;
    @BindView(R.id.rb_ipp_done)
    RadioButton rbDone;
    @BindView(R.id.rg_ipp_groups)
    RadioGroup rgGroup;
    @Inject
    InventoryPackagePlanAdapter mAdapter;

    private int itemPosition = -1;
    private InventoryPackageBean inventoryPackageBean;
    private InventoryBean mInventoryBean;

    public static InventorySecondFragment newInstance(InventoryBean inventoryBean) {
        InventorySecondFragment fragment = new InventorySecondFragment();
        fragment.mInventoryBean = inventoryBean;
        return fragment;
    }

    @Override
    protected void onBarcodeEventSuccess(String barcodeData) {
        vetPackageNo.setText(barcodeData);
        btQuery.performClick();
    }

    @Override
    public void setupFragmentComponent(@NonNull AppComponent appComponent) {
        DaggerInventoryPackagePlanComponent //如找不到该类,请编译一下项目
                .builder()
                .appComponent(appComponent)
                .inventoryPackagePlanModule(new InventoryPackagePlanModule(this))
                .commonModule(new CommonModule())
                .build()
                .inject(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_inventory_package_plan;
    }

    @Override
    public void initData(@Nullable Bundle bundle) {
        vetPackageNo.setHint("请扫码货位");
        initRecyclerView();
        getData();
    }

    @Override
    public void setData(@Nullable Object o) {

    }

    @Override
    public void showMessage(@NonNull String message) {
        checkNotNull(message);
        XToast.info(mContext,message).show();
    }

    @Override
    public void showNoMoreData() {
        mAdapter.setEnableLoadMore(false);
    }

    @Override
    public void showLoading() {
        QuickLoader.showLoading(mContext);
    }

    @Override
    public void hideLoading() {
        QuickLoader.stopLoading();
    }

    private void initRecyclerView() {
        //ArmsUtils.configRecyclerView(mRecyclerView, mLayoutManager);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.addItemDecoration(new RecycleViewDivider(getContext(), LinearLayoutManager.HORIZONTAL, R.drawable.item_drivide));
        mAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                mAdapter.loadMoreComplete();
                getData();
            }
        }, mRecyclerView);
        mAdapter.setOnItemClickListener((adapter, view, position) -> {
           if(position>=0 && mAdapter.getData().size()>position){
               itemPosition = position;
               toDeal(itemPosition);
           }

        });
        mAdapter.setOnclickListener((item,position)->{
            if(position>-1&&mAdapter.getData().size() > position&&!mAdapter.isLoading()){
                JSONObject param = new JSONObject();
                JSONArray planLines = new JSONArray();
                planLines.add(item.getInventoryPlanLineId());
                param.put("inventoryPlanLineId",planLines);
                assert mPresenter != null;
                mPresenter.doChecked(param,()->{
                    mAdapter.remove(position);
                });
            }

        });

        mRecyclerView.setAdapter(mAdapter);


    }

    @Override
    public void onEnterAnimationEnd(Bundle savedInstanceState) {
        super.onEnterAnimationEnd(savedInstanceState);
        rgGroup.setOnCheckedChangeListener((group,id)->{
           btQuery.performClick();
        });
    }

    private void getData() {
        JSONObject data = new JSONObject();
               /*pageNum:1
                pageSize:25
                limit:25
                start:0
                inventoryPlanId:1000123*/

        if(rbDone.isChecked()){
            data.put("processed","Y");
        }else if(rbUnDone.isChecked()){
            data.put("processed","N");
        }
        data.put("inventoryPlanId",mInventoryBean.getInventoryPlanId());//
        // data.put("hasDiff","Y");
        if (!vetPackageNo.getText().toString().isEmpty()) {//货位码
            data.put("locatorValue", vetPackageNo.getText().toString());
        }
             /*   if (!tivetPackageNo.getText().toString().isEmpty()) {
                    data.put("productName", tivetPackageNo.getText().toString());
                }
                if (!tietLocator.getText().toString().isEmpty()) {
                    data.put("manufacturer", tietLocator.getText().toString());
                }
                if (mStatu != null) {
                    data.put("storageStatus", mStatu.getId());
                }
                if (bpartnerBean != null) {
                    data.put("orgID", bpartnerBean.getId());
                }*/
        mPresenter.getData(data, list -> {

            mAdapter.addData(list);
        });

    }

    private void toDeal(int position){
        inventoryPackageBean = mAdapter.getData().get(position);
        if("Y".equals(inventoryPackageBean.getIsStoragePackage())){
            startForResult(InventoryPackagePlanHandleFragment.newInstance(inventoryPackageBean), REQUEST_CODE);
        }else{
            startForResult(InventoryPlanHandleFragment.newInstance(inventoryPackageBean), REQUEST_CODE);
        }

    }

    @Override
    public void onFragmentResult(int requestCode, int resultCode, Bundle data) {
        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {
            if(mAdapter.getData().size()>itemPosition) {
                mAdapter.remove(itemPosition);
            }
        }
    }

    @OnClick({R.id.bt_common_query})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.bt_common_query:
                mAdapter.getData().clear();
                mPresenter.setStart(0);
                getData();
                break;
        }
    }
}
