package com.chcit.mobile.mvp.common.model;


import com.chcit.mobile.R;
import com.chcit.mobile.mvp.hight.fragment.HightCostBindFragment;
import com.chcit.mobile.mvp.hight.fragment.HightCostCreateFragement;
import com.chcit.mobile.mvp.hight.fragment.HightCostReceiveFragment;
import com.chcit.mobile.mvp.hight.fragment.HightCostRejectFragment;
import com.chcit.mobile.mvp.inventory.fragment.pandian.InventoryFirstFragment;
import com.chcit.mobile.mvp.inventory.fragment.Stock.InventoryQueryFragment;
import com.chcit.mobile.mvp.inventory.fragment.move.MovementPlanFragment;
import com.chcit.mobile.mvp.inventory.fragment.move.MovenFragment;
import com.chcit.mobile.mvp.inventory.fragment.move.MovenPackageFragment;
import com.chcit.mobile.mvp.inventory.fragment.Stock.PackageDetailFragment;
import com.chcit.mobile.mvp.inventory.fragment.Stock.PackageInfoQueryFragment;
import com.chcit.mobile.mvp.inventory.fragment.Stock.PackageQueryFragment;
import com.chcit.mobile.mvp.packmanager.fragment.MergePackageFragment;
import com.chcit.mobile.mvp.packmanager.fragment.SplitPackageFragment;
import com.chcit.mobile.mvp.packmanager.fragment.SubPackageFragment;
import com.chcit.mobile.mvp.receive.fragment.pack.CheckPackageReceiveFragment;
import com.chcit.mobile.mvp.receive.fragment.PurchaseCheckFragment;
import com.chcit.mobile.mvp.receive.fragment.PurchaseOldReceiveFragment;
import com.chcit.mobile.mvp.receive.fragment.RejectFragmentOld;
import com.chcit.mobile.mvp.receive.fragment.YnrkOldReceiveFragemnt;
import com.chcit.mobile.mvp.receive.fragment.YnrkCheckFragemnt;
import com.chcit.mobile.mvp.receive.fragment.pack.PutWayPackageReceiveFragment;
import com.chcit.mobile.mvp.shipment.fragment.PurchaseShipmentFragemnt;
import com.chcit.mobile.mvp.shipment.fragment.ScanCardFragment;
import com.chcit.mobile.mvp.shipment.fragment.YnckShipmentFragement;
import com.chcit.mobile.mvp.transship.fragment.TransshipInFragment;
import com.chcit.mobile.mvp.transship.fragment.TransshipOutFragment;


import java.util.HashMap;
import java.util.Map;

public enum MenuTypeEnum {
    /**
     if ("PurchaseReceive".equals(ItemClass)) {
     delegate = PurchaseOldReceiveFragment.class;
     imageResource = R.drawable.svg_pr_receive;
     } else if ("AllotReceive".equals(ItemClass)) {
     delegate = YnrkOldReceiveFragemnt.class;
     imageResource = R.drawable.svg_ynrk;
     } else if ("AllotRejectReceive".equals(ItemClass)) {
     delegate = RejectFragmentOld.class;
     imageResource = R.drawable.svg_reject_receive;
     } else if ("pr_Shipment".equals(ItemClass)) {
     delegate = PurchaseShipmentFragemnt.class;
     imageResource = R.drawable.svg_pr_shipment;
     } else if ("mv_Shipment".equals(ItemClass)) {
     delegate = YnckShipmentFragement.class;
     imageResource = R.drawable.svg_ynck;
     } else if ("movementShipment".equals(ItemClass)) {
     delegate = MovenFragment.class;
     imageResource = R.drawable.svg_moven;
     }else if ("prescriptionOut".equals(ItemClass)) {
     delegate = DispensingFragment.class;
     imageResource = R.drawable.svg_dispensing;
     }else if("inventoryPlan".equals(ItemClass)){
     delegate = InventoryFirstFragment.class;
     imageResource = R.drawable.svg_menu;
     }else if("wardApplyOut".equals(ItemClass)){
     delegate = DoctorAdviceShipmentFragment.class;
     //  imageResource = R.d;
     }else if("subpackage".equals(ItemClass)){
     delegate = SubPackageFragment.class;
     //  imageResource = R.drawable.svg_menu;
     }else if("mergePackage".equals(ItemClass)){
     delegate = MergePackageFragment.class;
     //  imageResource = R.drawable.svg_menu;
     }else if("splitPackage".equals(ItemClass)){
     delegate = SplitPackageFragment.class;
     //delegate = SplitPackageFragment.class;
     // imageResource = R.drawable.svg_menu;
     }else if("PackageShipment".equals(ItemClass)){
     delegate = ScanCardFragment.class;
     } else if("spd.mobile.w3.highCost.create".equals(childrenBean.getId())){
     delegate = ScanCardFragment.class;
     }else if("spd.mobile.w3.highCost.receive".equals(childrenBean.getId())){
     delegate = HightCostReceiveFragment.class;
     }else if("spd.mobile.w3.highCost.reject".equals(childrenBean.getId())){
     delegate = HightCostRejectFragment.class;
     }else if("spd.mobile.w2.wms.RFIDBind".equals(childrenBean.getId())){
     delegate = HightCostBindFragment.class;
     }else if("PackageMovement".equals(ItemClass)) {
     delegate = MovenPackageFragment.class;
     }else if("StockQuery".equals(ItemClass)) {
     delegate = InventoryFragment.class;
     }
     */
    PURCHASE_CHECK(0,"PurchaseCheck","PO", "采购验收", R.drawable.svg_pr_receive, PurchaseCheckFragment.class),
    PACKAGE_CHECK(1,"packageCheck","PO", "验收(包装)", R.drawable.svg_pr_receive, CheckPackageReceiveFragment.class),
    PURCHASE_RECEIVE(2,"PurchaseReceive", "PO","采购上架", R.drawable.svg_menu, PurchaseOldReceiveFragment.class),
    YN_CHECK(3,"AllotCheck", "WO,WR,MO","院内验收",R.drawable.svg_ynrk, YnrkCheckFragemnt.class),
    YN_RECEIVE(4,"AllotReceive", "WO,WR,MO","院内上架",R.drawable.svg_menu, YnrkOldReceiveFragemnt.class),
    REJECT(5,"AllotRejectReceive", "WO,WR,MO","拒收入库",R.drawable.svg_reject_receive, RejectFragmentOld.class),
    PR(6,"pr_Shipment", "PR","采购出库",R.drawable.svg_pr_shipment, PurchaseShipmentFragemnt.class),
    YNCK_SHIPMENT(7,"mv_Shipment", "WO,SR,WR,MO,SO","院内出库",R.drawable.svg_ynck, YnckShipmentFragement.class),
    INVENTORY_MOVEN(8,"movementShipment","", "移库",R.drawable.svg_moven,  MovenFragment.class),
    PRESCRIPTION_SHIPMENT(9,"prescriptionOut","", "处方发药",R.drawable.svg_rx,  ScanCardFragment.class),
    DOCUTOR_ADVICE_SHIPMNET(10,"wardApplyOut", "","医嘱发药",R.drawable.svg_rx,  ScanCardFragment.class),
    MOVEN_PACKAGE(11,"PackageMovement","", "包装移库",R.drawable.svg_package,  MovenPackageFragment.class),
    PACKAGE_SPLIT(12,"splitPackage", "","定数拆零",R.drawable.svg_menu,  SplitPackageFragment.class),
    PACKAGE_MERGE(13,"mergePackage", "","并包",R.drawable.svg_menu,  MergePackageFragment.class),
    PACKAGE_SUB(14,"subpackage", "","拆包",R.drawable.svg_menu,  SubPackageFragment.class),
    INVENTORY_PLAN(15,"inventoryPlan","", "盘点",R.drawable.svg_menu,  InventoryFirstFragment.class),
    SC(16,"PackageShipment", "","发货登记",R.drawable.svg_menu,  ScanCardFragment.class),
    INVENTORY_QUERY(17,"StockQuery", "","库存查询",R.drawable.svg_query, InventoryQueryFragment.class),
    HCB(18,"spd.mobile.w2.wms.RFIDBind", "","RFID绑定",R.drawable.svg_menu, HightCostBindFragment.class),//HightCostBindFragment .class
    HRJ(19,"spd.mobile.w3.highCost.receive","", "扫码消耗",R.drawable.svg_menu, HightCostReceiveFragment.class),
    HIGHT_REJECT(20,"spd.mobile.w3.highCost.reject","", "扫码退回",R.drawable.svg_menu, HightCostRejectFragment.class),
    HIGHT_CREAT(21,"spd.mobile.w3.highCost.create", "","扫码登记",R.drawable.svg_menu , HightCostCreateFragement.class),
    PACKAGE_COUNT_QUERY(22,"packageCountQuery", "","包装库存查询",R.drawable.svg_query ,PackageQueryFragment.class),
    PACKAGE_QUERY(23,"packageQuery", "","包装明细查询",R.drawable.svg_query , PackageDetailFragment.class),
    MOVEMENT_PALM_SHIPMENT(24,"movementPlanShipment","", "移库指示作业", R.drawable.svg_menu, MovementPlanFragment.class),
    PURCHASE_QUERY(25,"packageQuery","", "包装历史查询", R.drawable.svg_query, PackageInfoQueryFragment.class),
    TRANSSHIP_IN(26,"transshipIn","", "转运入", R.drawable.svg_query, TransshipInFragment.class),
    TRANSSHIP_OUT(27,"transshipOut","", "转运出", R.drawable.svg_query, TransshipOutFragment.class),
    PACKAGE_PUTAWAY(28,"packagePutaway","PO", "上架(包装)", R.drawable.svg_query, PutWayPackageReceiveFragment.class);



    private String code;
    private String desc;
    private String type;
    private Class<?> clazz;
    private int imageResource;
    private int index;
    private static final Map<String, MenuTypeEnum> menus = new HashMap<String,MenuTypeEnum>();
    static {
        for (MenuTypeEnum refer : MenuTypeEnum.values()) {
            menus.put(refer.getCode(), refer);
        }
    }

     MenuTypeEnum(int index,String code,String type, String desc, int imageResource,Class<?> clazz) {
        this.code = code;
        this.desc = desc;
        this.type = type;
        this.imageResource = imageResource;
        this.clazz = clazz;
        this.index = index;

    }

    public static Map<String, MenuTypeEnum> getAllClazz() {
        return menus;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public static MenuTypeEnum getDescByCode(int code) {
        return menus.get(code);
    }
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public String getDesc() {
        return desc;
    }
    public void setDesc(String desc) {
        this.desc = desc;
    }
    public Class<?> getClazz() {
        return clazz;
    }
    public void setClazz(Class<?> clazz) {
        this.clazz = clazz;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getImageResource() {
        return imageResource;
    }

    public void setImageResource(int imageResource) {
        this.imageResource = imageResource;
    }
}
