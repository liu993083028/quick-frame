package com.chcit.mobile.mvp.inventory.adapter;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseItemDraggableAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chcit.mobile.R;
import com.chcit.mobile.mvp.entity.PackageBean;


import java.util.List;

public class MvPackageAdapter extends BaseItemDraggableAdapter<PackageBean,BaseViewHolder> {

    public MvPackageAdapter(@Nullable List<PackageBean> data) {
        super(R.layout.item_mv_package_list,data);
    }

    //先声明一个int成员变量
    protected int thisPosition = -1;
    //再定义一个int类型的返回值方法
    public int getChekedPosition() {
        return thisPosition;
    }
    //其次定义一个方法用来绑定当前参数值的方法
    //此方法是在调用此适配器的地方调用的，此适配器内不会被调用到
    public void setCheckedPosition(int thisPosition) {
        int i = this.thisPosition;
        this.thisPosition = thisPosition;
        if(i>-1&& i!= thisPosition){
            notifyItemChanged(i);
        }
        notifyItemChanged(thisPosition);
    }

    @Override
    protected void convert(BaseViewHolder helper, PackageBean item) {
        int i = helper.getAdapterPosition();
        if(thisPosition == i){
            helper.getView(R.id.ll_mv_package_item).setBackgroundColor(mContext.getResources().getColor(R.color.LavenderBlush));
        }else if((i+2)%2 == 0){
            helper.getView(R.id.ll_mv_package_item).setBackgroundColor(mContext.getResources().getColor(R.color.gray_f2));
        }else{
            helper.getView(R.id.ll_mv_package_item).setBackgroundColor(mContext.getResources().getColor(R.color.white));
        }
        helper.setText(R.id.tv_mv_package_no,item.getPackageNo());
        String content = item.getProductName()+" "+item.getProductSpec()+"/共"+item.getQty()+item.getUomName()+" /原货位:"+item.getLocatorName();
        helper.setText(R.id.tv_mv_package_content,content);

    }
}
