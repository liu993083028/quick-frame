package com.chcit.mobile.mvp.receive.presenter;

import android.app.Application;

import com.alibaba.fastjson.JSONObject;


import javax.inject.Inject;

import com.chcit.mobile.app.exception.BaseException;
import com.chcit.mobile.common.HttpMethodContains;
import com.chcit.mobile.common.SPDErrorHandle;
import com.chcit.mobile.mvp.common.contract.CommonContract;
import com.chcit.mobile.mvp.entity.Locator;
import com.chcit.mobile.mvp.entity.MonitorCode;
import com.chcit.mobile.mvp.entity.Reason;
import com.chcit.mobile.mvp.entity.ResultBean;
import com.chcit.mobile.mvp.receive.contract.ReceiveDetailContract;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.jess.arms.di.scope.FragmentScope;
import com.jess.arms.http.imageloader.ImageLoader;
import com.jess.arms.integration.AppManager;
import com.jess.arms.mvp.BasePresenter;
import com.jess.arms.utils.RxLifecycleUtils;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import me.jessyan.rxerrorhandler.core.RxErrorHandler;


@FragmentScope
public class ReceiveDetailPresenter extends BasePresenter<ReceiveDetailContract.Model, ReceiveDetailContract.View> {
    @Inject
    RxErrorHandler mErrorHandler;
    @Inject
    Application mApplication;
    @Inject
    ImageLoader mImageLoader;
    @Inject
    AppManager mAppManager;
    @Inject
    List<Reason> reasons;
   @Inject
    CommonContract.Model commModel;
    public boolean isError = false;

    @Inject
    public ReceiveDetailPresenter(ReceiveDetailContract.Model model, ReceiveDetailContract.View rootView) {
        super(model, rootView);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mErrorHandler = null;
        this.mAppManager = null;
        this.mImageLoader = null;
        this.mApplication = null;
    }

  public void getLocatorId(String locatorValue) {
        JSONObject data = new JSONObject();
        data.put("refId", locatorValue);
        mModel.getLocatorId(data)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                }).subscribe(new SPDErrorHandle<Locator>(mErrorHandler) {

            @Override
            public void onNext(Locator list) {

            }
        });
    }

    public void getReasons(MaterialSpinner materialSpinner) {
        JSONObject data = new JSONObject();
        data.put("refId", "1000068");
        mModel.getReans(data)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                }).subscribe(new SPDErrorHandle<List<Reason>>(mErrorHandler) {

            @Override
            public void onNext(List<Reason> list) {
                reasons.addAll(list);
                materialSpinner.setItems(reasons);
            }
        });
    }

   /* public void doConfirm(JSONObject json,Action onFinally) {
        mModel.doConfirm(HttpMethodContains.CHECK_RECEIVE_OK,json)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                }).subscribe(new SPDErrorHandle<ResultBean>(mErrorHandler) {

            @Override
            public void onNext(ResultBean resultBean) {
                try {
                    onFinally.run();
                } catch (Exception e) {
                    onError(e);
                }
            }
            @Override
            public void onError(Throwable t) {
                isError = true;
                super.onError(t);
            }
        });

    }*/

    public void getLocators(JSONObject json, Consumer<List<Locator>> onNext) {

        mModel.getLocators(json)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                }).subscribe(new SPDErrorHandle<List<Locator>>(mErrorHandler) {

            @Override
            public void onNext(List<Locator> list) {
                try {
                    onNext.accept(list);
                } catch (Exception e) {
                    onError(e);
                }
            }
        });
    }

    public void getMonitorCodes(int id, Consumer<List<MonitorCode>> onNext) {
        JSONObject data = new JSONObject();
        data.put("asnLineId",id);
        commModel.getMonitorCodes(HttpMethodContains.RECEIVE_CODE_QUERY,data)
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                }).subscribe(new SPDErrorHandle<List<MonitorCode>>(mErrorHandler) {

            @Override
            public void onNext(List<MonitorCode> list) {
                try {
                    onNext.accept(list);
                } catch (Exception e) {
                    onError(e);
                }
            }
        });
    }
    public void doConfirm(String method,JSONObject json, Action onFinally) {
        commModel.doConfirm(method,json)
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .doOnDispose(() -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                }).subscribe(new SPDErrorHandle<ResultBean>(mErrorHandler) {

            @Override
            public void onNext(ResultBean resultBean) {
                try {
                    onFinally.run();
                } catch (Exception e) {
                    onError(e);
                }
            }
        });
        }

    /**
     *  非包装验收确认
     * @param paramReq
     * @param action
     */
    public void checkAsnRecieve(JSONObject paramReq, Action action) {
        mModel.checkAsnSubmit(paramReq)
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                }).subscribe(new SPDErrorHandle<ResultBean>(mErrorHandler) {
            @Override
            public void onNext(ResultBean resultBean) {
                try {
                    if(resultBean.isSuccess()){
                        action.run();
                    }else{
                        String msg = resultBean.getMsg();
                        if(msg.isEmpty()){
                            msg = "未知错误！";
                        }
                        throw new BaseException(msg);
                    }

                } catch (Exception e) {
                    onError(e);
                }
            }

            @Override
            public void onError(Throwable t) {
                isError = true;
                super.onError(t);
            }
        });
    }

    /**
     * 非包装上架确认
     * @param paramReq
     * @param action
     */
    public void pickupAsnRecieve(JSONObject paramReq, Action action) {
        mModel.pickupAsnSubmit(paramReq)
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                }).subscribe(new SPDErrorHandle<ResultBean>(mErrorHandler) {
            @Override
            public void onNext(ResultBean resultBean) {
                try {
                    if(resultBean.isSuccess()){
                        action.run();
                    }else{
                        String msg = resultBean.getMsg();
                        if(msg.isEmpty()){
                            msg = "未知错误！";
                        }
                        throw new BaseException(msg);
                    }

                } catch (Exception e) {
                    onError(e);
                }
            }

            @Override
            public void onError(Throwable t) {
                isError = true;
                super.onError(t);
            }
        });
    }

    /**
     * 按包装验视
     *  {asnId:"1212",asnLineId:12123,locatorId:"1212",packageNo:["2323","232323dfdf"]}
     */
    public void preCheckRecieve(JSONObject paramReq, Action action) {
        mModel.preCheckRecieve(paramReq)
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                }).subscribe(new SPDErrorHandle<ResultBean>(mErrorHandler) {
            @Override
            public void onNext(ResultBean resultBean) {
                try {
                    if (resultBean.isSuccess()) {
                        action.run();
                    } else {
                        String msg = resultBean.getMsg();
                        if (msg.isEmpty()) {
                            msg = "未知错误！";
                        }
                        mRootView.showMessage(msg);
                    }

                } catch (Exception e) {
                    onError(e);
                }
            }

            @Override
            public void onError(Throwable t) {
                isError = true;
                super.onError(t);
            }
        });
    }
}
