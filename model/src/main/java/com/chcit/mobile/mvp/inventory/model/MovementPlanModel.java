package com.chcit.mobile.mvp.inventory.model;

import android.app.Application;

import com.alibaba.fastjson.JSONObject;
import com.chcit.mobile.app.exception.BaseException;
import com.chcit.mobile.mvp.common.api.service.UserService;
import com.chcit.mobile.mvp.entity.MovementPlanBean;
import com.chcit.mobile.mvp.entity.ResultBean;
import com.chcit.mobile.mvp.inventory.api.InventoryApi;
import com.google.gson.Gson;
import com.jess.arms.integration.IRepositoryManager;
import com.jess.arms.mvp.BaseModel;

import com.jess.arms.di.scope.FragmentScope;

import javax.inject.Inject;

import com.chcit.mobile.mvp.inventory.contract.MovementPlanContract;
import com.jess.arms.utils.ArmsUtils;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

@FragmentScope
public class MovementPlanModel extends BaseModel implements MovementPlanContract.Model {
    @Inject
    Gson mGson;
    @Inject
    Application mApplication;

    @Inject
    public MovementPlanModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mGson = null;
        this.mApplication = null;
    }

    @Override
    public Observable<List<MovementPlanBean>> getMovementPlans(JSONObject json) {
        return mRepositoryManager.obtainRetrofitService(InventoryApi.class)
                .queryMovementPlanDetail(json)
                .map(result -> {
                    if(result.isSuccess()){
                        if(result.total==0){
                              throw new BaseException("没有查询到数据！");
                        }
                        return  result.getRows();
                    } else{
                        throw new BaseException(result.getMsg());
                    }
                }).subscribeOn(Schedulers.io());
    }
}