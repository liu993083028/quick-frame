package com.chcit.mobile.mvp.receive.Entity;

import android.graphics.Rect;
import android.os.Parcel;

import androidx.annotation.Nullable;

import com.chcit.mobile.common.HttpMethodContains;
import com.xuexiang.xui.widget.imageview.preview.enitity.IPreviewInfo;

import me.jessyan.retrofiturlmanager.RetrofitUrlManager;

public class ImageUrl  implements IPreviewInfo   {

    /**
     * attachmentId : 1000032
     * attachmentUrl : ftp:///GLYY/AD_Attachment/1000032.jpeg
     */

    private int attachmentId;
    private String attachmentUrl;
    private Rect bounds;

    public ImageUrl(){

    }
    public ImageUrl(int attachmentId, String attachmentUrl) {
        this.attachmentId = attachmentId;
        this.attachmentUrl = attachmentUrl;
    }

    public int getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(int attachmentId) {
        this.attachmentId = attachmentId;
    }

    public String getattachmentUrl() {
        return attachmentUrl;
    }

    public void setattachmentUrl(String attachmentUrl) {
        this.attachmentUrl = attachmentUrl;
    }

    @Override
    public String getUrl() {
        return  RetrofitUrlManager.getInstance().getBaseUrl().toString()+ HttpMethodContains.Receive.VIEW_ASN_PICTURE+"?index=0&attachmentId="+attachmentId;
    }

    @Override
    public Rect getBounds() {
        return bounds;
    }

    @Nullable
    @Override
    public String getVideoUrl() {
        return null;
    }

    public void setBounds(Rect bounds) {
        this.bounds = bounds;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.attachmentId);
        dest.writeString(this.attachmentUrl);
        dest.writeParcelable(this.bounds, flags);
    }

    protected ImageUrl(Parcel in) {
        this.attachmentId = in.readInt();
        this.attachmentUrl = in.readString();
        this.bounds = in.readParcelable(Rect.class.getClassLoader());
    }

    public static final Creator<ImageUrl> CREATOR = new Creator<ImageUrl>() {
        @Override
        public ImageUrl createFromParcel(Parcel source) {
            return new ImageUrl(source);
        }

        @Override
        public ImageUrl[] newArray(int size) {
            return new ImageUrl[size];
        }
    };
}
