package com.chcit.mobile.mvp.packmanager.fragment;

import com.chcit.mobile.mvp.common.model.MenuTypeEnum;

public class SubPackageFragment extends PackageManagerFragment{

    public SubPackageFragment(){
        this.type = MenuTypeEnum.PACKAGE_SUB;
    }
}
