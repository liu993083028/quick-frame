package com.chcit.mobile.mvp.di.module;

import android.app.Application;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.chcit.mobile.app.exception.BaseException;
import com.chcit.mobile.app.exception.CodeMsg;
import com.chcit.mobile.mvp.common.api.service.CommonService;
import com.google.gson.Gson;
import com.jess.arms.integration.IRepositoryManager;
import com.jess.arms.mvp.BaseModel;

import com.jess.arms.di.scope.FragmentScope;

import javax.inject.Inject;

import com.chcit.mobile.mvp.shipment.contract.DirectShipmentContract;

import org.apache.commons.lang3.StringUtils;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

@FragmentScope
public class DirectShipmentModel extends BaseModel implements DirectShipmentContract.Model {
    @Inject
    Gson mGson;
    @Inject
    Application mApplication;

    @Inject
    public DirectShipmentModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mGson = null;
        this.mApplication = null;
    }

    @Override
    public Observable<JSONArray> queryPackageInfo(JSONObject json) {
        return mRepositoryManager.obtainRetrofitService(CommonService.class)
                .queryPackageInfo(json)
                .map(result -> {
                    JSONObject jsonObject = JSONObject.parseObject(result);
                    if (jsonObject.getBoolean("success")) {
                        if (jsonObject.getInteger("total") > 0) {
                            return jsonObject.getJSONArray("rows");
                        } else {
                            throw new Exception("包装不存在或已出库！");
                        }

                    } else {
                        String msg = jsonObject.getString("msg");
                        if (msg != null && !msg.isEmpty()) {
                            throw new Exception(msg);
                        } else {
                            throw new Exception("未知错误！");
                        }
                    }
                }).subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<String> shipmentPackageConfirm(JSONObject json) {
        return mRepositoryManager.obtainRetrofitService(CommonService.class)
                .shipmentPackageConfirm(json)
                .map(result -> {
                    JSONObject jsonObject = JSONObject.parseObject(result);
                    if (jsonObject.getBoolean("success")) {
                        return "登记成功";
                    } else {
                        String msg = jsonObject.getString("msg");
                        if (msg != null && !msg.isEmpty()) {
                            throw new Exception(msg);
                        } else {
                            throw new Exception("未知错误！");
                        }
                    }

                }).subscribeOn(Schedulers.io());
    }



}