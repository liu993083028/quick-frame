package com.chcit.mobile.mvp.inventory.adapter;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseViewHolder;
import com.chcit.mobile.R;
import com.chcit.mobile.helper.recycler.BaseItemChangeQuickAdapter;
import com.chcit.mobile.mvp.common.model.MenuTypeEnum;
import com.chcit.mobile.mvp.entity.InventoryBean;

import java.util.List;

public class InventoryAdapter extends BaseItemChangeQuickAdapter<InventoryBean,BaseViewHolder> {

    private MenuTypeEnum type;

    public InventoryAdapter(@Nullable List<InventoryBean> data) {
        super(R.layout.item_inventory_data,data);
    }

    public MenuTypeEnum getType() {
        return type;
    }

    public void setType(MenuTypeEnum type) {
        this.type = type;
    }

    @Override
    protected void convert(BaseViewHolder helper, InventoryBean item) {
        helper.setText(R.id.tv_inventory_pname,item.getProductName());
        helper.setText(R.id.tv_inventory_Lot,item.getLot());
        helper.setText(R.id.tv_inventory_Manufacturer,item.getManufacturer());
        helper.setText(R.id.tv_inventory_GuaranteeDate,item.getGuaranteeDate());
        helper.setText(R.id.tv_inventory_StorageStatusName,item.getStorageStatusName());
        helper.setText(R.id.tv_inventory_QtyOnHand,item.getQtyOnHand()+"");
        helper.setText(R.id.tv_inventory_productSpec,item.getProductSpec());
        helper.setText(R.id.tv_inventory_locator,item.getLocatorName());
        helper.setText(R.id.tv_inventory_VendorName,item.getVendorName());
        int position =  helper.getAdapterPosition();
        if (type == MenuTypeEnum.INVENTORY_QUERY) {
            helper.setText(R.id.tv_inventory_deal, "查看详情");
        }
        if(thisPosition==position){
            helper.setBackgroundColor(R.id.ll_list_item, mContext.getResources().getColor(R.color.MistyRose));
        }else if((position+1)%2 ==0){
            helper.setBackgroundColor(R.id.ll_list_item, mContext.getResources().getColor(R.color.bg_gray));
        }else{
            helper.setBackgroundColor(R.id.ll_list_item, mContext.getResources().getColor(R.color.White));
        }
    }
}
