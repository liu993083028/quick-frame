package com.chcit.mobile.mvp.inventory.presenter;

import android.app.Application;
import android.content.Context;

import com.alibaba.fastjson.JSONObject;
import com.chcit.custom.view.util.DialogUtil;
import com.chcit.mobile.common.SPDErrorHandle;
import com.chcit.mobile.mvp.common.contract.CommonContract;
import com.chcit.mobile.mvp.entity.ResultBean;
import com.chcit.mobile.mvp.entity.Statu;
import com.chcit.mobile.mvp.entity.PackageBean;
import com.jess.arms.integration.AppManager;
import com.jess.arms.di.scope.FragmentScope;
import com.jess.arms.mvp.BasePresenter;
import com.jess.arms.http.imageloader.ImageLoader;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import me.jessyan.rxerrorhandler.core.RxErrorHandler;
import javax.inject.Inject;
import com.chcit.mobile.mvp.inventory.contract.InventoryPlanHandleContract;
import com.jess.arms.utils.RxLifecycleUtils;

import java.util.List;


@FragmentScope
public class InventoryPlanHandlePresenter extends BasePresenter<InventoryPlanHandleContract.Model, InventoryPlanHandleContract.View> {
    @Inject
    RxErrorHandler mErrorHandler;
    @Inject
    Application mApplication;
    @Inject
    ImageLoader mImageLoader;
    @Inject
    AppManager mAppManager;
    @Inject
    CommonContract.Model commonModel;
    @Inject
    public InventoryPlanHandlePresenter(InventoryPlanHandleContract.Model model, InventoryPlanHandleContract.View rootView) {
        super(model, rootView);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mErrorHandler = null;
        this.mAppManager = null;
        this.mImageLoader = null;
        this.mApplication = null;
        this.commonModel = null;
    }

    public void doConfirm(JSONObject json, Action action){
        mModel.doConfirm(json)
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                }).subscribe(new SPDErrorHandle<ResultBean>(mErrorHandler) {

            @Override
            public void onNext(ResultBean resultBean) {

                try {
                    if(resultBean.isSuccess()){
                        action.run();
                    }else{
                        mRootView.showMessage(resultBean.getMsg());
                    }

                } catch (Exception e) {
                    onError(e);
                }
            }
        });
    }
    public void doConfirmPackage(JSONObject json, Action action){
        mModel.doConfirmPackage(json)
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                }).subscribe(new SPDErrorHandle<ResultBean>(mErrorHandler) {

            @Override
            public void onNext(ResultBean resultBean) {

                try {
                    if(resultBean.isSuccess()){
                        action.run();
                    }else{
                        mRootView.showMessage(resultBean.getMsg());
                    }

                } catch (Exception e) {
                    onError(e);
                }
            }
        });
    }
    public void getReasons(int productId, Consumer<List<Statu>> consumer){
        commonModel.refList(productId,false)
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                })
                .subscribe(new SPDErrorHandle<List<Statu>>(mErrorHandler){

                    @Override
                    public void onNext(List<Statu> bpartnerBeans) {
                        try {
                            consumer.accept(bpartnerBeans);
                        } catch (Exception e) {
                            onError(e);
                        }
                    }
                });
    }

    /**
     * productId:1037396
     * warehouseId:1000214
     */
    public void getPakcageInfo(JSONObject paramReq, Consumer<List<PackageBean>> consumer){
       commonModel.queryPackageInfo(paramReq)
                .map(result -> {
                      /*  List<PackageBean> packageBeans = new ArrayList<>();
                        for(int i=0;i<result.size();i++){

                            PackageBean packageBean = result.getJSONObject(0).toJavaObject(PackageBean.class);
                            packageBean.setUnitPackQty(paramReq.getBigDecimal("unitPackQty"));
                            packageBeans.add(packageBean);
                        }*/

                     return result.toJavaList(PackageBean.class);
                })
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
//                .doOnSubscribe(disposable -> {
//                    mRootView.showLoading();
//                })
//                .doFinally(() -> {
//                    mRootView.hideLoading();
//                })
                .subscribe(new SPDErrorHandle<List<PackageBean>>(mErrorHandler) {
                    @Override
                    public void onNext(List<PackageBean> lists) {
                        try {
                            consumer.accept(lists);
                        } catch (Exception e) {
                            onError(e);
                        }
                    }
                });
    }

    public void doChecked(JSONObject json, Action action, Context context){
        commonModel.doChecked(json)
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                }).subscribe(new SPDErrorHandle<ResultBean>(mErrorHandler) {

            @Override
            public void onNext(ResultBean resultBean) {

                try {
                    if(resultBean.isSuccess()){
                        action.run();
                    }else{
                       DialogUtil.showErrorDialog(context,resultBean.getMsg());
                    }

                } catch (Exception e) {
                    onError(e);
                }
            }
        });
    }


}
