package com.chcit.mobile.mvp.entity;

import java.util.Objects;

public class GoodsAllocation {

    /**
     * bpartnerName : 南京医药药业有限公司
     * bpartnerId : 1004822
     */

    private int id;
    private String name;

    public GoodsAllocation() {
    }

    public GoodsAllocation(int s1, String s2) {
        this.id = s1;
        this.name = s2;
    }

    public GoodsAllocation(String barcodeData) {
        this.name = barcodeData;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GoodsAllocation that = (GoodsAllocation) o;
        return Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return name ;
    }
}
