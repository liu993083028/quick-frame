package com.chcit.mobile.mvp.hight.adapter;

import androidx.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;

import com.chad.library.adapter.base.BaseItemDraggableAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chcit.mobile.R;
import com.chcit.mobile.mvp.entity.HightRegisPackageBean;

import java.util.List;

public class HightCostRegisPackageAdapter extends BaseItemDraggableAdapter<HightRegisPackageBean, BaseViewHolder> {

    public HightCostRegisPackageAdapter(@Nullable List<HightRegisPackageBean> data) {
        super(R.layout.item_hight_cost_scan,data);
    }

    @Override
    protected void convert(BaseViewHolder helper, HightRegisPackageBean item) {
        helper.setText(R.id.tv_hrs_id,String.valueOf(helper.getAdapterPosition()+1));
        helper.setText(R.id.tv_hrc_packageNo,item.getPackageNo()+"  "+item.getProductFullName()+"  "
                +item.getProductSpec()+"  领出数量："+item.getQtyDelivered()+item.getUomName()+"  退回数量："+item.getQtyRejected()+item.getUomName()+"  消耗数量："+item.getQtyReceived()+item.getUomName());
        ImageView imageView = helper.getView(R.id.iv_hrc_delete);
        imageView.setVisibility(View.GONE);
    }
}
