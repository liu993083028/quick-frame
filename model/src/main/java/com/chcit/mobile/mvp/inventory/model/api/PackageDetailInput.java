package com.chcit.mobile.mvp.inventory.model.api;

public class PackageDetailInput {

    /**
     * packageNo : (01)012312312300012423
     * warehouseId : 1000214
     * warehouseId_text : 西药库
     * locatorId : 1012532
     * locatorId_text : 01Z01-01-01-01-01
     * vendor : 南京医药药业有限公司
     * vendorId : 1004822
     * vendorId_text : 001-南京医药药业有限公司
     * productName : 2320
     * lot : zhb03
     * unitPackQty : 1
     * packageStatus : S
     * packageStatus_text : 在库
     * dateFrom : 2018-07-19
     * recievedDays : 10
     */

    private String packageNo;
    private String warehouseId;
    private String warehouseId_text;
    private String locatorId;
    private String locatorId_text;
    private String vendor;
    private String vendorId;
    private String vendorId_text;
    private String productName;
    private String lot;
    private String unitPackQty;
    private String packageStatus;
    private String packageStatus_text;
    private String dateFrom;
    private String recievedDays;

    public String getPackageNo() {
        return packageNo;
    }

    public void setPackageNo(String packageNo) {
        this.packageNo = packageNo;
    }

    public String getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(String warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getWarehouseId_text() {
        return warehouseId_text;
    }

    public void setWarehouseId_text(String warehouseId_text) {
        this.warehouseId_text = warehouseId_text;
    }

    public String getLocatorId() {
        return locatorId;
    }

    public void setLocatorId(String locatorId) {
        this.locatorId = locatorId;
    }

    public String getLocatorId_text() {
        return locatorId_text;
    }

    public void setLocatorId_text(String locatorId_text) {
        if(!locatorId_text.isEmpty()) {
            this.locatorId_text = locatorId_text;
        }
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getVendorId_text() {
        return vendorId_text;
    }

    public void setVendorId_text(String vendorId_text) {
        this.vendorId_text = vendorId_text;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getLot() {
        return lot;
    }

    public void setLot(String lot) {
        this.lot = lot;
    }

    public String getUnitPackQty() {
        return unitPackQty;
    }

    public void setUnitPackQty(String unitPackQty) {
        this.unitPackQty = unitPackQty;
    }

    public String getPackageStatus() {
        return packageStatus;
    }

    public void setPackageStatus(String packageStatus) {
        this.packageStatus = packageStatus;
    }

    public String getPackageStatus_text() {
        return packageStatus_text;
    }

    public void setPackageStatus_text(String packageStatus_text) {
        this.packageStatus_text = packageStatus_text;
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    public String getRecievedDays() {
        return recievedDays;
    }

    public void setRecievedDays(String recievedDays) {
        this.recievedDays = recievedDays;
    }
}
