package com.chcit.mobile.mvp.inventory.di.component;

import dagger.BindsInstance;
import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.chcit.mobile.mvp.inventory.di.module.MovementPlanDetailModule;
import com.chcit.mobile.mvp.inventory.contract.MovementPlanDetailContract;

import com.jess.arms.di.scope.FragmentScope;
import com.chcit.mobile.mvp.inventory.fragment.move.MovementPlanDetailFragment;


/**
 * ================================================
 * Description:
 * <p>
 * Created by MVPArmsTemplate on 10/11/2019 15:35
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms">Star me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms/wiki">See me</a>
 * <a href="https://github.com/JessYanCoding/MVPArmsTemplate">模版请保持更新</a>
 * ================================================
 */
@FragmentScope
@Component(modules = MovementPlanDetailModule.class, dependencies = AppComponent.class)
public interface MovementPlanDetailComponent {
    void inject(MovementPlanDetailFragment fragment);

    @Component.Builder
    interface Builder {
        @BindsInstance
        MovementPlanDetailComponent.Builder view(MovementPlanDetailContract.View view);

        MovementPlanDetailComponent.Builder appComponent(AppComponent appComponent);

        MovementPlanDetailComponent build();
    }
}