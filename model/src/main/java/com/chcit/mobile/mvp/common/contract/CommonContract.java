package com.chcit.mobile.mvp.common.contract;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.chcit.mobile.mvp.entity.BpartnerBean;
import com.chcit.mobile.mvp.entity.MonitorCode;
import com.chcit.mobile.mvp.entity.ResultBean;
import com.chcit.mobile.mvp.entity.Statu;
import com.jess.arms.mvp.IModel;

import java.util.List;

import io.reactivex.Observable;

public interface CommonContract {

    //Model层定义接口,外部只需关心Model返回的数据,无需关心内部细节,如是否使用缓存
     interface Model extends IModel {
        //获取供应商信息
        Observable<List<BpartnerBean>> getBparenerBeans();

        //获取字典
        Observable<List<Statu>> refList(int id, boolean update);

        Observable<List<MonitorCode>> getMonitorCodes(String method, JSONObject json);

        Observable<ResultBean> doConfirm(String method, JSONObject json);

        //获取拒收原因
        Observable<List<Statu>> queryReasons();

        Observable<JSONObject> requestBySubmit(String method, JSONObject json);

        Observable<JSONObject> requestOnString(String method, JSONObject json);

        Observable<JSONArray> queryUnitPackQty(JSONObject json);

        //获取包装信息
        Observable<JSONArray> queryPackageInfo(JSONObject json);

        //
        Observable<ResultBean> doChecked(JSONObject json);

        //检测员工号码
        Observable<JSONArray> checkUserCode(JSONObject json);

        //获取有采购任务单的供应商信息
        //获取供应商信息
        Observable<List<BpartnerBean>> getBparenerBeans(String wareHoseId);
    }
}
