/*
 * Copyright 2017 JessYan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.chcit.mobile.mvp.common.api.service;

import java.util.List;
import io.reactivex.Observable;
import com.alibaba.fastjson.JSONObject;
import com.chcit.mobile.common.HttpMethodContains;
import com.chcit.mobile.mvp.entity.BaseData;
import com.chcit.mobile.mvp.entity.BaseResponse;
import com.chcit.mobile.mvp.entity.MaintainBean;
import com.chcit.mobile.mvp.entity.MovementPlanBean;
import com.chcit.mobile.mvp.entity.ResultBean;
import com.chcit.mobile.mvp.entity.UserBean;
import com.chcit.mobile.mvp.inventory.entity.InventoryBean;
import com.chcit.mobile.mvp.inventory.entity.InventoryPackageBean;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Retrofit;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

import static com.chcit.mobile.common.HttpMethodContains.*;

/**
 * ================================================
 * 展示 {@link Retrofit#create(Class)} 中需要传入的 ApiService 的使用方式
 * 存放关于用户的一些 API
 * <p>
 * Created by JessYan on 08/05/2016 12:05
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * ================================================
 */
public interface UserService {
    String HEADER_API_VERSION = "Accept: application/vnd.github.v3+json";
    @GET
    public Observable<String> update(@Url String url);

    @POST
    @Streaming
    public Observable<ResponseBody> down(@Url String url);

    @Headers({HEADER_API_VERSION})
    @GET("/users")
    Observable<List<UserBean>> getUsers(@Query("since") int lastIdQueried, @Query("per_page") int perPage);

    @FormUrlEncoded
    @POST(LOGIN_QUERY_URL)
    Observable<JSONObject> login(@Field("userName") String username,@Field("password") String password);

    @POST(LOGIN_QUERY_URL)
    Observable<String> loginJson(@Body RequestBody requestBody);

    @GET
    Observable<String> test(@Url String url);

    @GET(REQUEST_QUERY_URL)
    Observable<JSONObject> request(@Query("method")String method,@Query("data")JSONObject json);

    @GET(REQUEST_QUERY_URL)
    Observable<String> requestOnString(@Query("method")String method,@Query("data")JSONObject json);

    @GET(REQUEST_QUERY_URL)
    Observable<JSONObject> requestBySubmit(@Query("method")String method,@Query("data")JSONObject json);


    @GET(REQUEST_QUERY_URL)
    Observable<JSONObject> request(@Query("method")String method);


    @FormUrlEncoded
    @POST("asnAction/receiveLine.do")
    Observable<JSONObject> receiveLine(@FieldMap JSONObject json);

    @FormUrlEncoded
    @POST(STORAGE_URL)
    Observable<JSONObject> queryStorage(@FieldMap JSONObject json);

    @GET(BPARTNER_URL)
    Observable<JSONObject> queryBpartners();

    @FormUrlEncoded
    @POST(MOVEN_URL)
    Observable<ResultBean> moven(@FieldMap JSONObject json);

    @FormUrlEncoded
    @POST(MAINTAIN_OK_URL)
    Observable<ResultBean> doConfirmMaintainResult(@FieldMap JSONObject json);

    @FormUrlEncoded
    @POST(MAINTAIN_UEL)
    Observable<BaseData<MaintainBean>> getMaintaions(@FieldMap JSONObject json);

    @FormUrlEncoded
    @POST(INVENTORY_PLAN_QUERY)
    Observable<BaseData<InventoryBean>> getData(@FieldMap JSONObject json);

    @FormUrlEncoded
    @POST(INVENTORY_PLAN_QUERY_DETAIL)
    Observable<BaseData<InventoryPackageBean>> queryInventoryPlanDetail(@FieldMap JSONObject json);

    @FormUrlEncoded
    @POST(INVENTORY_PLAN_UPDATE)
    Observable<ResultBean> doConfirmInventoryPlanResult(@FieldMap JSONObject json);

    //userBaseHandleAction/query.do
    @FormUrlEncoded
    @POST(USER_CHECK_CODE)
    Observable<String> chckUserCode(@FieldMap JSONObject json);

    //userBaseHandleAction/query.do
    @FormUrlEncoded
    @POST(PACKAGE_INFO)
    Observable<BaseResponse> PakcageInfo(@FieldMap JSONObject json);

    @GET(TASK_URL)
    Observable<BaseResponse> queryWaitDoc();

    @GET(REQUEST_QUERY_URL)
    Observable<String> queryMovementPlans(@Query("method")String method,@Query("data")JSONObject json);


}
