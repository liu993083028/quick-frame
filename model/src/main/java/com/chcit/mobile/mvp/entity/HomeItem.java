package com.chcit.mobile.mvp.entity;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * https://github.com/CymChad/BaseRecyclerViewAdapterHelper
 */
public class HomeItem implements Parcelable {
    private String title;
    private Class<?> activity;
    private int imageResource;
    private String url;
    private String id;
    private int taskNumber;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getImageResource() {
        return imageResource;
    }

    public void setImageResource(int imageResource) {
        this.imageResource = imageResource;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Class<?> getActivity() {
        return activity;
    }

    public void setActivity(Class<?> activity) {
        this.activity = activity;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getTaskNumber() {
        return taskNumber;
    }

    public void setTaskNumber(int taskNumber) {
        this.taskNumber = taskNumber;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeSerializable(this.activity);
        dest.writeInt(this.imageResource);
        dest.writeString(this.url);
        dest.writeString(this.id);
    }

    public HomeItem() {
    }

    protected HomeItem(Parcel in) {
        this.title = in.readString();
        this.activity = (Class<?>) in.readSerializable();
        this.imageResource = in.readInt();
        this.url = in.readString();
        this.id = in.readString();
    }

    public static final Creator<HomeItem> CREATOR = new Creator<HomeItem>() {
        @Override
        public HomeItem createFromParcel(Parcel source) {
            return new HomeItem(source);
        }

        @Override
        public HomeItem[] newArray(int size) {
            return new HomeItem[size];
        }
    };

}