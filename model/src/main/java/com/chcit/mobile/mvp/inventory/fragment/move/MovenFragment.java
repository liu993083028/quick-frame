package com.chcit.mobile.mvp.inventory.fragment.move;


import com.chcit.mobile.mvp.common.model.MenuTypeEnum;

public class MovenFragment extends InventoryFragment {

    public MovenFragment(){
        super();
        this.type = MenuTypeEnum.INVENTORY_MOVEN;
    }
}
