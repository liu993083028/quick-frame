package com.chcit.mobile.mvp.receive.adpter;

import androidx.annotation.Nullable;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import com.chad.library.adapter.base.BaseViewHolder;
import com.chcit.mobile.BuildConfig;
import com.chcit.mobile.R;
import com.chcit.mobile.helper.recycler.BaseSelectQuickAdapter;
import com.chcit.mobile.mvp.common.model.MenuTypeEnum;
import com.chcit.mobile.mvp.entity.ReceiveBean;


import org.apache.commons.lang3.StringUtils;

import java.util.List;

public class ReceiveAdapter extends BaseSelectQuickAdapter<ReceiveBean,BaseViewHolder> {

    private View.OnClickListener mOnClickListener;
    private MenuTypeEnum type;
    public ReceiveAdapter(@Nullable List<ReceiveBean> data) {
        super(R.layout.item_receive_data,data);

    }

    public void setOnClickListener(View.OnClickListener onClickListener){
        mOnClickListener = onClickListener;
    }
    @Override
    protected void convert(final BaseViewHolder helper, final ReceiveBean item) {

        CheckBox checkBox = helper.getView(R.id.box_receive_select);
        LinearLayout l1 = helper.getView(R.id.ll_first);
        if("N".equals(item.getIsStoragePackage())){
            checkBox.setVisibility(View.VISIBLE);
            int position= helper.getAdapterPosition();
            checkBox.setChecked(mSelectedPositions.get(position));
            checkBox.setOnClickListener(v -> {
                int i= helper.getAdapterPosition();
                if (isItemChecked(i)) {
                    setItemChecked(i, false);
                } else {
                    setItemChecked(i, true);
                }

                if (mTitleBarView != null) {
                    String msg = "已选择" + getSelectedItem().size() + "/" + getItemCount();
                    mTitleBarView.setText(msg);
                }
                notifyItemChanged(i);
            });
            l1.setOnClickListener(v->{
                checkBox.performClick();
            });
        }else {
            checkBox.setOnClickListener(null);
            l1.setOnClickListener(null);
            checkBox.setVisibility(View.INVISIBLE);
        }

       /* final TextView textView = helper.getView(R.id.tv_receive_deal);
        textView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(mOnClickListener != null){
                    v.setTag(item);
                    mOnClickListener.onClick(v);
                }
            }
        });*/
        helper.setText(R.id.tv_receive_GuaranteeDate,item.getGuaranteeDate());
        helper.setText(R.id.tv_receive_Lot,item.getLot());
        helper.setText(R.id.tv_receive_Manufacturer,item.getManufacturer());
        helper.setText(R.id.tv_receive_pname,item.getProductName());
        helper.setText(R.id.tv_receive_asnTypeName,item.getASNTypeName());
        helper.setText(R.id.tv_receive_productCode,item.getDeliveryNo());
        helper.setText(R.id.tv_receive_VendorName,item.getBpartnerName());
        helper.setText(R.id.tv_receive_QtyOnHand,item.getQtyArrived().toString());
        helper.setText(R.id.tv_receive_productSpec,item.getProductSpec());
        LinearLayout llTaxInvoiceNo = helper.getView(R.id.ll_receive_taxInvoiceNo);
        if(StringUtils.isEmpty(item.getTaxInvoiceNo())){
            llTaxInvoiceNo.setVisibility(View.GONE);
        }else{
            helper.setText(R.id.tv_receive_taxInvoiceNo,item.getTaxInvoiceNo());
        }
        if(type!=null&&type.equals(MenuTypeEnum.REJECT)){
            helper.setText(R.id.tv_receive_vendor_key,"拒收仓库: ");
            helper.setText(R.id.tv_receive_VendorName,item.getWarehouseName());
        }

        if(MenuTypeEnum.PURCHASE_CHECK.getType().equals(item.getASNType())){
            helper.setText(R.id.tv_receive_odd,item.getOddQty()+item.getUomName());
            helper.setText(R.id.tv_receive_unit,item.getUnitPackageCount()+"包/"+item.getUnitQty()+item.getUomName());

        }else{
            helper.getView(R.id.ll_receive_odd).setVisibility(View.GONE);
            helper.getView(R.id.ll_receive_unit).setVisibility(View.GONE);
        }
        if("BZ".equals(BuildConfig.TYPE)){
            if(type!=null&&type.equals(MenuTypeEnum.PURCHASE_RECEIVE)){
                helper.setText(R.id.tv_receive_label_QtyOnHand,"待上架数量:");
                helper.setText(R.id.tv_receive_QtyOnHand,item.getQtyPutawayLeft().toString()+item.getUomName());
                helper.getView(R.id.ll_receive_odd).setVisibility(View.GONE);
                helper.getView(R.id.ll_receive_unit).setVisibility(View.GONE);
            }
        }


    }
    @Override
    public void checkAll() {
        int count = 0;
        for(int i =0;i<mSelectedPositions.size();i++){
            if("N".equals(mData.get(i).getIsStoragePackage())){
                mSelectedPositions.put(i,true);
                count++;
            }
        }
        notifyDataSetChanged();
        if (mTitleBarView != null) {
            String msg = "已选择" + count+ "/" + mData.size();
            mTitleBarView.setText(msg);
        }
    }

    public MenuTypeEnum getType() {
        return type;
    }

    public void setType(MenuTypeEnum type) {
        this.type = type;
    }
}
