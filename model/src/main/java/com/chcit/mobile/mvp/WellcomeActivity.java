package com.chcit.mobile.mvp;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.jess.arms.base.BaseActivity;
import com.jess.arms.di.component.AppComponent;
import com.jess.arms.utils.ArmsUtils;

import com.chcit.mobile.di.component.DaggerWellcomeComponent;
import com.chcit.mobile.di.module.WellcomeModule;
import com.chcit.mobile.mvp.common.contract.WellcomeContract;
import com.chcit.mobile.mvp.common.presenter.WellcomePresenter;

import com.chcit.mobile.R;
import com.xuexiang.xui.widget.toast.XToast;


import static com.jess.arms.utils.Preconditions.checkNotNull;


public  class WellcomeActivity extends BaseActivity<WellcomePresenter> implements WellcomeContract.View {

    @Override
    public void setupActivityComponent(@NonNull AppComponent appComponent) {
        DaggerWellcomeComponent //如找不到该类,请编译一下项目
                .builder()
                .appComponent(appComponent)
                .wellcomeModule(new WellcomeModule(this))
                .build()
                .inject(this);
    }

    @Override
    public int initView(@Nullable Bundle savedInstanceState) {
        return R.layout.activity_wellcome; //如果你不需要框架帮你设置 setContentView(id) 需要自行设置,请返回 0
    }

    @Override
    public void initData(@Nullable Bundle savedInstanceState) {

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showMessage(@NonNull String message) {
        checkNotNull(message);
        XToast.info(this,message).show();
    }

    @Override
    public void launchActivity(@NonNull Intent intent) {
        checkNotNull(intent);
        ArmsUtils.startActivity(intent);
    }

    @Override
    public void killMyself() {
        finish();
    }


}
