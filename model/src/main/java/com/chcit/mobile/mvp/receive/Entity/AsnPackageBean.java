package com.chcit.mobile.mvp.receive.Entity;

import java.math.BigDecimal;
import java.util.Objects;

public class AsnPackageBean {

    /**
     * packageNo : (00)090000018000005636
     * lot : 92322
     * guaranteeDate : 2021-10-21
     * qtyDelivered : 1
     * qtyReceived : 0
     * qtyRejected : 0
     * productId : 1040741
     * productName : 丹参
     * productSpec : 净 1000g/kg
     * manufacturer : 山东
     * uomName : kg
     * productCode : 040225
     * qty : 1
     * qtyText : 1kg
     * vendorId : 1005002
     * vendorName : 南京鹤龄药事服务有限公司
     * isControlledProduct : N
     * checkTime :
     */
    public AsnPackageBean (){

    }
    public AsnPackageBean (String packageNo){
        this.packageNo = packageNo;
    }
    private String packageNo ="";
    private String lot ="";
    private String guaranteeDate ="";
    private BigDecimal qtyDelivered = BigDecimal.ZERO;;
    private BigDecimal qtyReceived = BigDecimal.ZERO;
    private BigDecimal qtyRejected = BigDecimal.ZERO;
    private BigDecimal qtyPutawayed = BigDecimal.ZERO;
    private BigDecimal qtyChecked = BigDecimal.ZERO;
    private int productId;
    private String productName ="";
    private String productSpec ="";
    private String manufacturer="";
    private String uomName="";
    private String productCode="";
    private BigDecimal qty;
    private String qtyText="";
    private int vendorId;
    private String vendorName ="";
    private String isControlledProduct;
    private String checkTime;
    private String asnId;
    private String deliveryNo;
    private String isNeedInspecte = "N";
    private String isColdStorage="N"; //是否是冷链
    private int attachmentCount; //附件数量
    private String isInspecte = "N"; //是否开箱验视过 Y 已验视 N 未验
    private String asnStatus;
    private String locatorValue;
    public String getPackageNo() {
        return packageNo;
    }

    public void setPackageNo(String packageNo) {
        this.packageNo = packageNo;
    }

    public String getLot() {
        return lot;
    }

    public void setLot(String lot) {
        this.lot = lot;
    }

    public String getGuaranteeDate() {
        return guaranteeDate;
    }

    public void setGuaranteeDate(String guaranteeDate) {
        this.guaranteeDate = guaranteeDate;
    }

    public BigDecimal getQtyDelivered() {
        return qtyDelivered;
    }

    public void setQtyDelivered(BigDecimal qtyDelivered) {
        this.qtyDelivered = qtyDelivered;
    }

    public BigDecimal getQtyReceived() {
        return qtyReceived;
    }

    public void setQtyReceived(BigDecimal qtyReceived) {
        this.qtyReceived = qtyReceived;
    }

    public BigDecimal getQtyRejected() {
        return qtyRejected;
    }

    public void setQtyRejected(BigDecimal qtyRejected) {
        this.qtyRejected = qtyRejected;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductSpec() {
        return productSpec;
    }

    public void setProductSpec(String productSpec) {
        this.productSpec = productSpec;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getUomName() {
        return uomName;
    }

    public void setUomName(String uomName) {
        this.uomName = uomName;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public BigDecimal getQty() {
        return qty;
    }

    public BigDecimal getQtyPutawayed() {
        return qtyPutawayed;
    }

    public void setQtyPutawayed(BigDecimal qtyPutawayed) {
        this.qtyPutawayed = qtyPutawayed;
    }

    public void setQty(BigDecimal qty) {
        this.qty = qty;
    }

    public String getQtyText() {
        return qtyText;
    }

    public void setQtyText(String qtyText) {
        this.qtyText = qtyText;
    }

    public int getVendorId() {
        return vendorId;
    }

    public void setVendorId(int vendorId) {
        this.vendorId = vendorId;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getIsControlledProduct() {
        return isControlledProduct;
    }

    public void setIsControlledProduct(String isControlledProduct) {
        this.isControlledProduct = isControlledProduct;
    }

    public String getCheckTime() {
        return checkTime;
    }

    public String getAsnId() {
        return asnId;
    }

    public void setAsnId(String asnId) {
        this.asnId = asnId;
    }

    public String getDeliveryNo() {
        return deliveryNo;
    }

    public void setDeliveryNo(String deliveryNo) {
        this.deliveryNo = deliveryNo;
    }

    public void setCheckTime(String checkTime) {
        this.checkTime = checkTime;
    }

    public String getIsNeedInspecte() {
        return isNeedInspecte;
    }

    public void setIsNeedInspecte(String isNeedInspecte) {
        this.isNeedInspecte = isNeedInspecte;
    }

    public String getIsColdStorage() {
        return isColdStorage;
    }

    public void setIsColdStorage(String isColdStorage) {
        this.isColdStorage = isColdStorage;
    }

    public int getAttachmentCount() {
        return attachmentCount;
    }

    public String getIsInspecte() {
        return isInspecte;
    }

    public void setIsInspecte(String isInspecte) {
        this.isInspecte = isInspecte;
    }

    public void setAttachmentCount(int attachmentCount) {
        this.attachmentCount = attachmentCount;
    }

    public String getAsnStatus() {
        return asnStatus;
    }

    public void setAsnStatus(String asnStatus) {
        this.asnStatus = asnStatus;
    }

    public String getLocatorValue() {
        return locatorValue;
    }

    public void setLocatorValue(String locatorValue) {
        this.locatorValue = locatorValue;
    }

    public BigDecimal getQtyChecked() {
        return qtyChecked;
    }

    public void setQtyChecked(BigDecimal qtyChecked) {
        this.qtyChecked = qtyChecked;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AsnPackageBean)) return false;
        AsnPackageBean that = (AsnPackageBean) o;
        return Objects.equals(packageNo, that.packageNo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(packageNo);
    }


    public String getAsnTypeName(int type) {

        return null;
    }

}
