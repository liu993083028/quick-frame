package com.chcit.mobile.mvp.inventory.adapter;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;

import com.chad.library.adapter.base.BaseViewHolder;
import com.chcit.mobile.R;
import com.chcit.mobile.helper.recycler.BaseSelectQuickAdapter;
import com.chcit.mobile.mvp.inventory.entity.PackageHistoryBean;

import java.util.List;

public class PackageHistoryAdapter extends BaseSelectQuickAdapter<PackageHistoryBean,BaseViewHolder> {

    //先声明一个int成员变量
    private int thisPosition = -1;

    public PackageHistoryAdapter(@Nullable List<PackageHistoryBean> data) {
        super(R.layout.item_package_history_list,data);
    }

    @Override
    protected void convert(final BaseViewHolder helper, final PackageHistoryBean item) {

        int i = helper.getAdapterPosition();
        if(thisPosition == i){
            helper.getView(R.id.ll_package_history_item).setBackgroundColor(mContext.getResources().getColor(R.color.Moccasin));
        }else if((i+2)%2 == 0){
            helper.getView(R.id.ll_package_history_item).setBackgroundColor(mContext.getResources().getColor(R.color.gray_f2));
        }else{
            helper.getView(R.id.ll_package_history_item).setBackgroundColor(mContext.getResources().getColor(R.color.white));
        }

        helper.setText(R.id.tv_package_history_no,String.valueOf(i+1));
        helper.setText(R.id.tv_package_history_work_type,item.getWorkTypeName().toString());
        helper.setText(R.id.tv_package_history_work_user,item.getWorkUserName().toString());
        helper.setText(R.id.tv_package_history_work_date,item.getWorkDate().toString());
    }
}
