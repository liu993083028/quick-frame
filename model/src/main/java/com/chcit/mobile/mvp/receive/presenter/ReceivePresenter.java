package com.chcit.mobile.mvp.receive.presenter;

import android.app.Application;

import com.alibaba.fastjson.JSONObject;
import com.chcit.mobile.R;
import com.chcit.mobile.app.exception.BaseException;
import com.chcit.mobile.common.SPDErrorHandle;
import com.chcit.mobile.mvp.common.contract.CommonContract;
import com.chcit.mobile.mvp.common.model.CommonModel;
import com.chcit.mobile.mvp.entity.BaseResponse;
import com.chcit.mobile.mvp.entity.BpartnerBean;
import com.chcit.mobile.mvp.entity.PageBean;
import com.chcit.mobile.mvp.entity.ReceiveBean;
import com.chcit.mobile.mvp.entity.ResultBean;
import com.chcit.mobile.mvp.receive.Entity.AsnResultBean;
import com.chcit.mobile.mvp.receive.Entity.ImageUrl;
import com.chcit.mobile.mvp.receive.api.QueryListInput;
import com.jess.arms.integration.AppManager;
import com.jess.arms.di.scope.FragmentScope;
import com.jess.arms.mvp.BasePresenter;
import com.jess.arms.http.imageloader.ImageLoader;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import me.jessyan.rxerrorhandler.core.RxErrorHandler;
import me.jessyan.rxerrorhandler.handler.ErrorHandleSubscriber;
import javax.inject.Inject;
import javax.inject.Named;

import com.chcit.mobile.mvp.receive.contract.ReceiveContract;
import com.jess.arms.utils.RxLifecycleUtils;
import com.luck.picture.lib.entity.LocalMedia;
import com.xuexiang.xui.widget.toast.XToast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@FragmentScope
public class ReceivePresenter extends BasePresenter<ReceiveContract.Model, ReceiveContract.View> {
    @Inject
    RxErrorHandler mErrorHandler;
    @Inject
    Application mApplication;
    @Inject
    ImageLoader mImageLoader;
    @Inject
    AppManager mAppManager;

   @Inject
    CommonContract.Model commModel;
    @Inject
    public ReceivePresenter(ReceiveContract.Model model, ReceiveContract.View rootView) {
        super(model, rootView);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mErrorHandler = null;
        this.mAppManager = null;
        this.mImageLoader = null;
        this.mApplication = null;

    }

    public void getData(String method,JSONObject json) {
        mModel.getReceives(method,json)
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                })
                .subscribe(new SPDErrorHandle<List<ReceiveBean>>(mErrorHandler) {
                    @Override
                    public void onNext(List<ReceiveBean> lists) {

                        mRootView.updateContentList(new ArrayList<>(lists));
                    }
                });
    }

    //盲扫获取单号信息
    public void getListData(PageBean pageBean, QueryListInput input) {
        mModel.queryDataList(pageBean,input)
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                })
                .subscribe(new SPDErrorHandle<List<AsnResultBean>>(mErrorHandler) {
                    @Override
                    public void onNext(List<AsnResultBean> lists) {
                        mRootView.updateContentList(new ArrayList<>(lists));
                    }
                });
    }
    /**
     * 获取供应商信息
     * @param consumer
     */
    public void getBpartners( Consumer<List<BpartnerBean>> consumer){
        commModel.getBparenerBeans()
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .subscribe(new SPDErrorHandle<List<BpartnerBean>>(mErrorHandler){

                    @Override
                    public void onNext(List<BpartnerBean> bpartnerBeans) {
                        try {
                            consumer.accept(bpartnerBeans);
                        } catch (Exception e) {
                            onError(e);
                        }
                    }
                });
    }
    /**
     * 获取供应商信息
     * @param consumer
     */
    public void getBpartners(String wareHoseId, Consumer<List<BpartnerBean>> consumer){
        commModel.getBparenerBeans(wareHoseId)
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .subscribe(new SPDErrorHandle<List<BpartnerBean>>(mErrorHandler){

                    @Override
                    public void onNext(List<BpartnerBean> bpartnerBeans) {
                        try {
                            consumer.accept(bpartnerBeans);
                        } catch (Exception e) {
                            onError(e);
                        }
                    }
                });
    }
    public void doBatchConfirm(JSONObject json, Action action) {
        mModel.doBatchConfirm(json)
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                }).subscribe(new SPDErrorHandle<ResultBean>(mErrorHandler) {
            @Override
            public void onNext(ResultBean resultBean) {
                try {
                    action.run();
                } catch (Exception e) {
                    onError(e);
                }
            }
        });
    }

    public void uploadImage(String asnId, List<String> filePaths,Consumer<ImageUrl> action){
        mModel.uploadImage(asnId,filePaths)
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                }).subscribe(new SPDErrorHandle<ImageUrl>(mErrorHandler) {
            @Override
            public void onNext(ImageUrl resultBean) {
                try {

                    action.accept(resultBean);
                } catch (Exception e) {
                    onError(e);
                }
            }
        });

//        mModel.uploadImage(asnId,Arrays.asList(localMedia.getPath()));
//
//        Observable<LocalMedia> listObservable = Observable.fromIterable(filePaths);
//
//        //使用zip操作符合并两个Observable
//        Observable.zip(listObservable, timerObservable, new BiFunction<String, Long, String>() {
//            @Override
//            public String apply(String s, Long aLong) throws Exception {
//                return s;
//            }
//        }).doOnNext(new Consumer<String>() {
//            @Override
//            public void accept(String s) throws Exception {
//                //此处接收
//            }
//        }).subscribe();
    }

    public void queryImageUrls(String asnId,Consumer<List<ImageUrl>> consumer){
        mModel.queryImageIds(asnId)
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                }).subscribe(new SPDErrorHandle<List<ImageUrl>>(mErrorHandler) {
            @Override
            public void onNext(List<ImageUrl> list) {
                try {
                    consumer.accept(list);
                } catch (Throwable e) {
                    onError(e);
                }
            }
        });

//        mModel.uploadImage(asnId,Arrays.asList(localMedia.getPath()));
//
//        Observable<LocalMedia> listObservable = Observable.fromIterable(filePaths);
//
//        //使用zip操作符合并两个Observable
//        Observable.zip(listObservable, timerObservable, new BiFunction<String, Long, String>() {
//            @Override
//            public String apply(String s, Long aLong) throws Exception {
//                return s;
//            }
//        }).doOnNext(new Consumer<String>() {
//            @Override
//            public void accept(String s) throws Exception {
//                //此处接收
//            }
//        }).subscribe();
    }

    public void deleteImage(String imageId,Action action){
        mModel.deleteImage(imageId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SPDErrorHandle<BaseResponse>(mErrorHandler){

                    @Override
                    public void onNext(BaseResponse baseResponse) {
                        try {
                            action.run();
                        } catch (Exception e) {
                            e.printStackTrace();
                            onError(e);
                        }
                    }
                });

    }
}
