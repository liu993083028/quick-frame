package com.chcit.mobile.mvp.common.model;

import android.app.Application;

import com.alibaba.fastjson.JSONObject;
import com.chcit.mobile.common.HttpMethodContains;
import com.chcit.mobile.mvp.common.api.service.UserService;
import com.chcit.mobile.mvp.entity.ResultBean;
import com.chcit.mobile.mvp.entity.WareHose;
import com.google.gson.Gson;
import com.jess.arms.integration.IRepositoryManager;
import com.jess.arms.mvp.BaseModel;

import com.jess.arms.di.scope.ActivityScope;

import javax.inject.Inject;
import com.chcit.mobile.mvp.common.contract.LoginContract;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.MediaType;
import okhttp3.RequestBody;


@ActivityScope
public class LoginModel extends BaseModel implements LoginContract.Model {
    @Inject
    Gson mGson;
    @Inject
    Application mApplication;

    @Inject
    public LoginModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mGson = null;
        this.mApplication = null;
    }

    @Override
    public Observable<JSONObject> login(String username, String password) {
        JSONObject requestData = new JSONObject();
        requestData.put("userName",username);
        requestData.put("password",password);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"),requestData.toString());
        return  /*mRepositoryManager
                .obtainRetrofitService(UserService.class)
                .login(username,password);*/
                mRepositoryManager
                        .obtainRetrofitService(UserService.class)
                        .login(username,password)
              ;
             /*   .flatMap(new Function<Observable<JSONObject>, ObservableSource<JSONObject>>() {
                    @Override
                    public ObservableSource<JSONObject> apply(Observable<JSONObject> jsonObjectObservable) throws Exception {
                        return mRepositoryManager.obtainCacheService(UserService.class)
                                .login(username,password);
                    }
                });*/
              /*  .flatMap(new Function<Observable<JSONObject>, ObservableSource<JSONObject>>() {

                    @Override
                    public ObservableSource<JSONObject> apply(@NonNull Observable<JSONObject> jsonObjectObservable) throws Exception {
                        return mRepositoryManager.obtainCacheService(CommonCache.class)
                                .getUsers(jsonObjectObservable
                                        , new DynamicKey("")
                                        , new EvictDynamicKey(false))
                                .map(listReply -> listReply.getData());
                    }
                });*/
    }

    @Override
    public Observable<List<WareHose>> getWares() {
        return mRepositoryManager .obtainRetrofitService(UserService.class)
                .request(HttpMethodContains.WAREHOUSE_QUERY)
                .map(jsonObject -> {
                   Gson gson = new Gson();
                  return gson.fromJson(jsonObject.toJSONString(),ResultBean.class).getRows(WareHose.class);

                });
//                .flatMap(new Function<JSONObject, ObservableSource<List<WareHose>>>() {
//                    @Override
//                    public ObservableSource<List<WareHose>> apply(JSONObject jsonObject) throws Exception {
//
//                        return null;
//                    }
//                });
    }
}