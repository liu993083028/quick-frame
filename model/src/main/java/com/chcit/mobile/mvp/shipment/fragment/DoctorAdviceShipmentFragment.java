package com.chcit.mobile.mvp.shipment.fragment;

import com.chcit.mobile.mvp.common.model.MenuTypeEnum;
import com.chcit.mobile.mvp.entity.RecipeBean;

import java.util.List;

/**
 * 医嘱拣货页面
 */
public class DoctorAdviceShipmentFragment extends DispensingFragment {

    public static DoctorAdviceShipmentFragment newInstance(String preNo, List<RecipeBean> list) {
        DoctorAdviceShipmentFragment fragment = new DoctorAdviceShipmentFragment();
        fragment.preNo = preNo;
        fragment.unRecipeBeans = list;
        return fragment;
    }
    public DoctorAdviceShipmentFragment(){
        this.type = MenuTypeEnum.DOCUTOR_ADVICE_SHIPMNET;
    }
}
