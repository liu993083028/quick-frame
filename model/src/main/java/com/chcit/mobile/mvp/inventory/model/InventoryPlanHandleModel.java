package com.chcit.mobile.mvp.inventory.model;

import android.app.Application;

import com.alibaba.fastjson.JSONObject;
import com.chcit.mobile.mvp.entity.ResultBean;
import com.chcit.mobile.mvp.inventory.model.api.InventoryService;
import com.google.gson.Gson;
import com.jess.arms.integration.IRepositoryManager;
import com.jess.arms.mvp.BaseModel;

import com.jess.arms.di.scope.FragmentScope;

import javax.inject.Inject;

import com.chcit.mobile.mvp.inventory.contract.InventoryPlanHandleContract;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

/**
 * 盘点
 */
@FragmentScope
public class InventoryPlanHandleModel extends BaseModel implements InventoryPlanHandleContract.Model {
    @Inject
    Gson mGson;
    @Inject
    Application mApplication;

    @Inject
    public InventoryPlanHandleModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mGson = null;
        this.mApplication = null;
    }

    @Override
    public Observable<ResultBean> doConfirm(JSONObject json) {
        return mRepositoryManager
                .obtainRetrofitService(InventoryService.class)
                .doConfirm(json).subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<ResultBean> doConfirmPackage(JSONObject json) {
        return mRepositoryManager
                .obtainRetrofitService(InventoryService.class)
                .doConfirmPackage(json).subscribeOn(Schedulers.io());
    }
}