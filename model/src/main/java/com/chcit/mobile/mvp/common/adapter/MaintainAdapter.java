package com.chcit.mobile.mvp.common.adapter;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chcit.mobile.R;
import com.chcit.mobile.mvp.entity.MaintainBean;


import java.util.List;

public class MaintainAdapter extends BaseQuickAdapter<MaintainBean,BaseViewHolder> {

    public MaintainAdapter(@Nullable List<MaintainBean> data) {
        super(R.layout.item_maintain_data,data);
    }

    @Override
    protected void convert(BaseViewHolder helper, MaintainBean item) {
        helper.setText(R.id.tv_maintain_pname,item.getProductName());
        helper.setText(R.id.tv_maintain_Lot,item.getLot());
        helper.setText(R.id.tv_maintain_ConservationMeasureName,item.getConservationMeasureName());
        helper.setText(R.id.tv_maintain_ConservationQty,item.getConservationQty()+"");
        helper.setText(R.id.tv_maintain_StorageStatusName,item.getStorageQty()+"");
        helper.setText(R.id.tv_maintain_QtyOnHand,item.getStorageQty()+"");
        helper.setText(R.id.tv_maintain_productSpec,item.getProductSpec());
        helper.setText(R.id.tv_maintain_locator,item.getLocatorName());
        helper.setText(R.id.tv_maintain_VendorName,item.getOrgName());
    }
}
