package com.chcit.mobile.mvp.inventory.fragment.move;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.alibaba.fastjson.JSONObject;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chcit.custom.view.edit.SuperInputEditText;
import com.chcit.custom.view.loader.QuickLoader;
import com.chcit.mobile.R;
import com.chcit.mobile.app.base.ScanFragment;
import com.chcit.mobile.helper.HttpClientHelper;
import com.chcit.mobile.helper.recycler.RecycleViewDivider;
import com.chcit.mobile.mvp.entity.MovementPlanBean;
import com.chcit.mobile.mvp.inventory.adapter.MovementPlanAdapter;
import com.chcit.mobile.mvp.inventory.contract.MovementPlanContract;
import com.chcit.mobile.mvp.inventory.di.component.DaggerMovementPlanComponent;
import com.chcit.mobile.mvp.inventory.presenter.MovementPlanPresenter;
import com.jess.arms.di.component.AppComponent;
import com.jess.arms.utils.ArmsUtils;
import com.xuexiang.xui.widget.toast.XToast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

import static com.jess.arms.utils.Preconditions.checkNotNull;

public class MovementPlanFragment extends ScanFragment<MovementPlanPresenter> implements MovementPlanContract.View {

    private static int REQUEST_CODE = -2;

    @BindView(R.id.et_movement_plan_location_no)
    SuperInputEditText etLocatorValue;
    @BindView(R.id.bt_movement_plan_query)
    Button btQuery;
    @BindView(R.id.lv_movement_plan_list)
    RecyclerView mRecyclerView;

    RecyclerView.LayoutManager mLayoutManager ;
    private int itemPosition = -1;

    private MovementPlanBean movementPlanBean;
    private List<MovementPlanBean> movementPlanBeans = new ArrayList<>();
    protected MovementPlanAdapter mAdapter = new MovementPlanAdapter(movementPlanBeans);

    public static MovementPlanFragment newInstance() {
        MovementPlanFragment fragment = new MovementPlanFragment();
        return fragment;
    }

    @Override
    public void setupFragmentComponent(@NonNull AppComponent appComponent) {
        DaggerMovementPlanComponent //如找不到该类,请编译一下项目
                .builder()
                .appComponent(appComponent)
                .view(this)
                .build()
                .inject(this);
    }

    @Override
    public View initView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_movement_plan, container, false);
    }

    @Override
    public void initData(@Nullable Bundle savedInstanceState) {

    }

    @Override
    public void setData(@Nullable Object data) {

    }

    @Override
    public void showLoading() {
        QuickLoader.showLoading(getContext());
    }

    @Override
    public void hideLoading() {
        QuickLoader.stopLoading();
    }

    @Override
    public void showMessage(@NonNull String message) {
        checkNotNull(message);
        XToast.info(mContext,message).show();
    }

    @Override
    public void launchActivity(@NonNull Intent intent) {
        checkNotNull(intent);
        ArmsUtils.startActivity(intent);
    }

    @Override
    public void killMyself() {
        pop();
        getActivity().finish();
    }

    @Override
    protected void onBarcodeEventSuccess(String barcodeData) {
        etLocatorValue.setText(barcodeData);
        btQuery.performClick();
    }

    @Override
    public void onEnterAnimationEnd(Bundle savedInstanceState) {
        super.onEnterAnimationEnd(savedInstanceState);
        initRecyclerView();
    }

    @OnClick({R.id.bt_movement_plan_query})
    public void onViewClicked(View view) {
        //查询移库指示任务
        mAdapter.getData().clear();
        mAdapter.notifyDataSetChanged();

        JSONObject data = new JSONObject();

        data.put("warehouseId", HttpClientHelper.getSelectWareHouse().getId());
        data.put("locatorValue", etLocatorValue.getText().toString());
        data.put("page", "move");

        mPresenter.getData(data);
    }

    @Override
    protected void initToolBar() {
        super.initToolBar();
        mCommonToolbar.getLeftMenuView(0).setOnClickListener(l -> {
            pop();
            l.setOnClickListener(null);
            if (getActivity() != null) {
                _mActivity.finish();
            }
        });
    }

    private void initRecyclerView() {
        mLayoutManager = new LinearLayoutManager(mContext);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addItemDecoration(new RecycleViewDivider(mContext, LinearLayoutManager.HORIZONTAL, R.drawable.item_drivide));
        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                itemPosition = position;
                if(position>=0&& adapter.getData().size()>position){
                    toDeal(position);
                }
            }
        });
        mRecyclerView.setAdapter(mAdapter);
    }

    private void toDeal(int position) {
        movementPlanBean = mAdapter.getData().get(position);
        //不同的移库场合
        if (movementPlanBean.getIsStoragePackage().toString().equals("Y")) {
            //包装移库
            startForResult(MovementPlanScanPackageFragment.newInstance(movementPlanBean), REQUEST_CODE);
        }
        else {
            //非包装移库
            startForResult(MovementPlanDetailFragment.newInstance(movementPlanBean), REQUEST_CODE);
        }
    }

    @Override
    public void onFragmentResult(int requestCode, int resultCode, Bundle data) {
        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {
            mAdapter.getData().clear();
            mAdapter.notifyDataSetChanged();

            btQuery.performClick();
        }
    }

    @Override
    public void updateContentList(List<MovementPlanBean> movementPlanBeans) {
        mAdapter.addData(movementPlanBeans);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public <T extends View> T getViewByTag(int i) {
        switch (i) {
            case 1:
                return (T) btQuery;
            case 2:
                break;
        }
        return null;
    }
}
