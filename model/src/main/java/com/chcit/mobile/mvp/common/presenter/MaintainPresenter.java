package com.chcit.mobile.mvp.common.presenter;

import android.app.Application;

import com.alibaba.fastjson.JSONObject;
import com.chcit.mobile.common.SPDErrorHandle;
import com.chcit.mobile.mvp.common.contract.CommonContract;
import com.chcit.mobile.mvp.common.model.CommonModel;
import com.chcit.mobile.mvp.entity.BpartnerBean;
import com.chcit.mobile.mvp.entity.MaintainBean;
import com.chcit.mobile.mvp.entity.Statu;
import com.jess.arms.integration.AppManager;
import com.jess.arms.di.scope.FragmentScope;
import com.jess.arms.mvp.BasePresenter;
import com.jess.arms.http.imageloader.ImageLoader;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import me.jessyan.rxerrorhandler.core.RxErrorHandler;


import javax.inject.Inject;
import javax.inject.Named;

import com.chcit.mobile.mvp.common.contract.MaintainContract;
import com.jess.arms.utils.RxLifecycleUtils;

import java.util.List;


@FragmentScope
public class MaintainPresenter extends BasePresenter<MaintainContract.Model, MaintainContract.View> {
    @Inject
    RxErrorHandler mErrorHandler;
    @Inject
    Application mApplication;
    @Inject
    ImageLoader mImageLoader;
    @Inject
    AppManager mAppManager;
    @Inject
    CommonContract.Model commModel;

    @Inject
    public MaintainPresenter(MaintainContract.Model model, MaintainContract.View rootView) {
        super(model, rootView);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mErrorHandler = null;
        this.mAppManager = null;
        this.mImageLoader = null;
        this.mApplication = null;
    }
   /* id:1000387*/
   public void getBpartners( Consumer<List<BpartnerBean>> consumer){
       commModel.getBparenerBeans()
               .observeOn(AndroidSchedulers.mainThread())
               .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
               .subscribe(new SPDErrorHandle<List<BpartnerBean>>(mErrorHandler){

                   @Override
                   public void onNext(List<BpartnerBean> bpartnerBeans) {
                       try {
                           consumer.accept(bpartnerBeans);
                       } catch (Exception e) {
                           onError(e);
                       }
                   }
               });
   }
    public void getLevels( Consumer<List<Statu>> consumer){
        commModel.refList(1000387,false)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .subscribe(new SPDErrorHandle<List<Statu>>(mErrorHandler){

                    @Override
                    public void onNext(List<Statu> bpartnerBeans) {
                        try {
                            consumer.accept(bpartnerBeans);
                        } catch (Exception e) {
                            onError(e);
                        }
                    }
                });
    }

    public void getData(JSONObject json, Consumer<List<MaintainBean>> onNext){
        mModel.getDatas(json)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                })
                .subscribe(new SPDErrorHandle<List<MaintainBean>>(mErrorHandler){

                    @Override
                    public void onNext(List<MaintainBean> list) {
                        try {
                            onNext.accept(list);
                        } catch (Exception e) {
                            onError(e);
                        }
                    }
                });
    }

}
