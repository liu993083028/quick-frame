package com.chcit.mobile.mvp.inventory.api;

import com.alibaba.fastjson.JSONObject;
import com.chcit.mobile.common.HttpMethodContains;
import com.chcit.mobile.mvp.entity.BaseData;
import com.chcit.mobile.mvp.entity.MovementPlanBean;
import com.chcit.mobile.mvp.entity.PackageUnitBean;
import com.chcit.mobile.mvp.entity.ResultBean;
import com.chcit.mobile.mvp.entity.ShipmentBean;

import io.reactivex.Observable;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

import static com.chcit.mobile.common.HttpMethodContains.MOVEMENT_PLAN_QUERY_DETAIL;

public interface InventoryApi {

    @GET(HttpMethodContains.Inventory.QUERY_URL)
    Observable<BaseData<PackageUnitBean>> getDataList(@QueryMap JSONObject query);

    @FormUrlEncoded
    @POST(MOVEMENT_PLAN_QUERY_DETAIL)
    Observable<BaseData<MovementPlanBean>> queryMovementPlanDetail(@FieldMap JSONObject json);

    @GET(HttpMethodContains.MOVEMENT_PLAN_CONFIRM)
    Observable<ResultBean> movementPlanSubmit(@QueryMap JSONObject json);

    @GET(HttpMethodContains.MOVEMENT_PLAN_PACKAGE_CONFIRM)
    Observable<ResultBean> movementPackageConfirm(@QueryMap JSONObject json);
}
