package com.chcit.mobile.mvp.di.component;

import dagger.BindsInstance;
import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.chcit.mobile.mvp.di.module.DirectShipmentModule;
import com.chcit.mobile.mvp.shipment.contract.DirectShipmentContract;

import com.jess.arms.di.scope.FragmentScope;
import com.chcit.mobile.mvp.shipment.fragment.DirectShipmentFragment;

@FragmentScope
@Component(modules = DirectShipmentModule.class, dependencies = AppComponent.class)
public interface DirectShipmentComponent {
    void inject(DirectShipmentFragment fragment);

    @Component.Builder
    interface Builder {
        @BindsInstance
        DirectShipmentComponent.Builder view(DirectShipmentContract.View view);

        DirectShipmentComponent.Builder appComponent(AppComponent appComponent);

        DirectShipmentComponent build();
    }
}