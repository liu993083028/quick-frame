package com.chcit.mobile.mvp.hight.di.module;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import com.chcit.mobile.mvp.hight.contract.HightCostBindContract;
import com.chcit.mobile.mvp.hight.contract.HightCostContract;
import com.chcit.mobile.mvp.hight.model.HightCostBindModel;
import com.chcit.mobile.mvp.hight.model.HightCostModel;
import com.jess.arms.di.scope.FragmentScope;


@Module
public abstract class HightCostBindModule {


    @Binds
    abstract HightCostBindContract.Model bindHightCostBindModel(HightCostBindModel model);
}