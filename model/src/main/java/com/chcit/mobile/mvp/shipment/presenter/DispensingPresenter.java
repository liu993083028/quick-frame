package com.chcit.mobile.mvp.shipment.presenter;

import android.app.Application;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.chcit.mobile.common.SPDErrorHandle;
import com.chcit.mobile.mvp.common.model.CommonModel;
import com.chcit.mobile.mvp.entity.RecipeBean;
import com.chcit.mobile.mvp.entity.ResultBean;
import com.chcit.mobile.mvp.shipment.contract.DispensingContract;
import com.jess.arms.di.scope.FragmentScope;
import com.jess.arms.http.imageloader.ImageLoader;
import com.jess.arms.integration.AppManager;
import com.jess.arms.mvp.BasePresenter;
import com.jess.arms.utils.RxLifecycleUtils;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import me.jessyan.rxerrorhandler.core.RxErrorHandler;


@FragmentScope
public class DispensingPresenter extends BasePresenter<DispensingContract.Model, DispensingContract.View> {
    @Inject
    RxErrorHandler mErrorHandler;
    @Inject
    Application mApplication;
    @Inject
    ImageLoader mImageLoader;
    @Inject
    AppManager mAppManager;
    @Inject
    CommonModel commonModel;
    @Inject
    public DispensingPresenter(DispensingContract.Model model, DispensingContract.View rootView) {
        super(model, rootView);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mErrorHandler = null;
        this.mAppManager = null;
        this.mImageLoader = null;
        this.mApplication = null;
    }

    public void getData(String method,JSONObject json, Consumer<List<RecipeBean>> onNext){
        mModel.getData(method,json)
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                })
                .subscribe(new SPDErrorHandle<List<RecipeBean>>(mErrorHandler){

                    @Override
                    public void onNext(List<RecipeBean> list) {
                        try {
                            onNext.accept(list);
                        } catch (Exception e) {
                            onError(e);
                        }
                    }
                });
    }

    public void doConfirm(String method,JSONObject json, Action onFinally) {
        mModel.doConfirm( method,json)
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .doOnDispose(() -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                }).subscribe(new SPDErrorHandle<ResultBean>(mErrorHandler) {

            @Override
            public void onNext(ResultBean resultBean) {
                try {
                    onFinally.run();
                } catch (Exception e) {
                    onError(e);
                }
            }
        });

    }



    public void checkUserCode(JSONObject json, Consumer<JSONArray> onFinally) {
        commonModel.checkUserCode(json)
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .doOnDispose(() -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                }).subscribe(new SPDErrorHandle<JSONArray>(mErrorHandler) {

            @Override
            public void onNext(JSONArray resultBean) {
                try {
                    onFinally.accept(resultBean);
                } catch (Exception e) {
                    onError(e);
                }
            }
        });

    }

}
