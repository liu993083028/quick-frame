package com.chcit.mobile.mvp.common.adapter;

import android.content.Context;
import androidx.appcompat.widget.AppCompatCheckedTextView;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.chcit.mobile.R;

import java.util.List;


public class LogLevelAdapter<T> extends BaseAdapter {
    private List<T> mData;
    private Context mContext;
    private int selected;

    public LogLevelAdapter(List<T> datas, Context context){
        mData = datas;
        mContext = context;
    }
    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Object getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = View.inflate(mContext, R.layout.dialog_menu_item,
                    null);
        }

        AppCompatCheckedTextView checkedTextView = convertView.findViewById(R.id.text1);
        checkedTextView.setGravity(Gravity.CENTER);
        checkedTextView.setText((CharSequence) getItem(position));
        if (selected == position) {
            //说明该位置需要checked
            checkedTextView.setChecked(true);
        } else {
            checkedTextView.setChecked(false);
        }

        return convertView;
    }

    public void setSelected(int position) {
        selected = position;
    }
}
