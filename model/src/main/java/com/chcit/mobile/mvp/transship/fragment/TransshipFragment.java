package com.chcit.mobile.mvp.transship.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.alibaba.fastjson.JSONObject;
import com.chcit.custom.view.edit.SuperInputEditText;
import com.chcit.custom.view.loader.QuickLoader;
import com.chcit.custom.view.util.DialogUtil;
import com.chcit.mobile.app.base.ScanFragment;
import com.chcit.mobile.helper.recycler.RecycleViewDivider;
import com.chcit.mobile.mvp.common.model.MenuTypeEnum;
import com.chcit.mobile.mvp.entity.TransshipBean;
import com.chcit.mobile.mvp.transship.adapter.TransshipAdapter;
import com.jess.arms.di.component.AppComponent;
import com.jess.arms.utils.ArmsUtils;

import com.chcit.mobile.mvp.transship.di.component.DaggerTransshipComponent;
import com.chcit.mobile.mvp.transship.contract.TransshipContract;
import com.chcit.mobile.mvp.transship.presenter.TransshipPresenter;

import com.chcit.mobile.R;
import com.xuexiang.xui.widget.toast.XToast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

import static com.jess.arms.utils.Preconditions.checkNotNull;

public class TransshipFragment extends ScanFragment<TransshipPresenter> implements TransshipContract.View {


    @BindView(R.id.et_transship_document_no)
    SuperInputEditText etDocumentNo;
    @BindView(R.id.et_transship_job_no)
    SuperInputEditText etJobNo;
    @BindView(R.id.lv_transship_list)
    RecyclerView mRecyclerView;
    @BindView(R.id.bt_transship_query)
    Button btQuery;
    @BindView(R.id.bt_transship_ok)
    Button btOk;
    RecyclerView.LayoutManager mLayoutManager ;

    private TransshipBean transshipBean;
    private List<TransshipBean> transshipBeans = new ArrayList<>();
    protected TransshipAdapter mAdapter = new TransshipAdapter(transshipBeans);

    protected MenuTypeEnum type;

    public static TransshipFragment newInstance() {
        TransshipFragment fragment = new TransshipFragment();
        return fragment;
    }

    @Override
    public void setupFragmentComponent(@NonNull AppComponent appComponent) {
        DaggerTransshipComponent //如找不到该类,请编译一下项目
                .builder()
                .appComponent(appComponent)
                .view(this)
                .build()
                .inject(this);
    }

    @Override
    public View initView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_transship, container, false);
    }

    @Override
    public void initData(@Nullable Bundle savedInstanceState) {
        btOk.setEnabled(false);
    }

    @Override
    public void setData(@Nullable Object data) {

    }

    @Override
    public void showLoading() {
        QuickLoader.showLoading(getContext());
    }

    @Override
    public void hideLoading() {
        QuickLoader.stopLoading();
    }

    @Override
    public void showMessage(@NonNull String message) {
        checkNotNull(message);
        XToast.info(mContext,message).show();
    }

    @Override
    public void launchActivity(@NonNull Intent intent) {
        checkNotNull(intent);
        ArmsUtils.startActivity(intent);
    }

    @Override
    public void killMyself() {
        pop();
        getActivity().finish();
    }

    @Override
    public void onEnterAnimationEnd(Bundle savedInstanceState) {
        super.onEnterAnimationEnd(savedInstanceState);
        initRecyclerView();
    }

    @Override
    public void updateContentList(List<TransshipBean> transshipBeans) {
        mAdapter.addData(transshipBeans);
        mAdapter.notifyDataSetChanged();
        if (transshipBeans.size() > 0) {
            transshipBean = transshipBeans.get(0);
        }

        if (transshipBean != null) {
            btOk.setEnabled(true);
        } else {
            btOk.setEnabled(false);
        }
    }

    @Override
    public <T extends View> T getViewByTag(int i) {
        switch (i) {
            case 1:
                return (T) btQuery;
            case 2:
                break;
        }
        return null;
    }

    @Override
    protected void onBarcodeEventSuccess(String barcodeData) {
        if(etJobNo.isFocused()){
            etJobNo.setText(barcodeData);
        }else{
            etDocumentNo.setText(barcodeData);
            btQuery.performClick();
        }
    }

    @OnClick({R.id.bt_transship_query, R.id.bt_transship_ok, R.id.bt_transship_cancel})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.bt_transship_query:
                String asnNo = etDocumentNo.getText().toString();

                JSONObject data = new JSONObject();
                if (!asnNo.isEmpty()) {
                    mAdapter.getData().clear();
                    mAdapter.notifyDataSetChanged();

                    data.put("asnNo", asnNo);
                    mPresenter.getData(data);
                }
                break;
            case R.id.bt_transship_ok:
                String jobNo = etJobNo.getText().toString();
                if (jobNo.isEmpty()) {
                    DialogUtil.showErrorDialog(getContext(), "请输入工号！");
                    break;
                }
                doConfirm();
                break;
            case R.id.bt_transship_cancel:
                doCcncel();
                break;
        }
    }

    @Override
    protected void initToolBar() {
        super.initToolBar();
        mCommonToolbar.getLeftMenuView(0).setOnClickListener(l -> {
            pop();
            l.setOnClickListener(null);
            if (getActivity() != null) {
                _mActivity.finish();
            }
        });
    }

    private void initRecyclerView() {
        mLayoutManager = new LinearLayoutManager(mContext);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addItemDecoration(new RecycleViewDivider(mContext, LinearLayoutManager.HORIZONTAL, R.drawable.item_drivide));
        mRecyclerView.setAdapter(mAdapter);
    }

    private void doConfirm() {
        int asnId = transshipBean.getAsnId();
        String jobNo = etJobNo.getText().toString();

        JSONObject data = new JSONObject();
        data.put("asnId", asnId);//
        data.put("WorkerNo", jobNo);
        String page = "IN";
        switch (type){
            case TRANSSHIP_IN:
                page = "IN";
                break;
            case TRANSSHIP_OUT:
                page = "OUT";
                break;
        }
        data.put("ASNRegType", page);

        assert mPresenter != null;
        mPresenter.doConfirm(data, () -> {
            showMessage("处理成功");
            doCcncel();
        });
    }

    private void doCcncel() {
        etDocumentNo.setText("");
        etJobNo.setText("");

        mAdapter.getData().clear();
        mAdapter.notifyDataSetChanged();
    }
}
