package com.chcit.mobile.mvp.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.math.BigDecimal;

public class PackageUnitBean implements Serializable {

    /**
     * warehouseId : 1000214
     * warehouseName : 西药库
     * productId : 1037383
     * productName : 丙泊酚注射液
     * productSpec : 20ml:0.2g/支
     * manufacturer : CordenPharmaS.P.A
     * uomName : 支
     * productCode : 2030
     * packageCount : 1
     * qty : 500
     * qtyText : 500支
     * unitPackQty : 500
     * unitPackQtyText : 500支
     * vendorId : 1004822
     * vendorName : 南京医药药业有限公司
     * lot : 1212
     * guaranteeDate : 2021-09-21
     * locatorId : 1010510
     * locatorName : 存储货位
     */

    @SerializedName("warehouseId")
    private int warehouseId;
    @SerializedName("warehouseName")
    private String warehouseName;
    @SerializedName("productId")
    private int productId;
    @SerializedName("productName")
    private String productName;
    @SerializedName("productSpec")
    private String productSpec;
    @SerializedName("manufacturer")
    private String manufacturer;
    @SerializedName("uomName")
    private String uomName;
    @SerializedName("productCode")
    private String productCode;
    @SerializedName("packageCount")
    private BigDecimal packageCount = BigDecimal.ZERO;
    @SerializedName("qty")
    private BigDecimal qty = BigDecimal.ZERO;
    @SerializedName("qtyText")
    private String qtyText;
    @SerializedName("unitPackQty")
    private BigDecimal unitPackQty = BigDecimal.ZERO;
    @SerializedName("unitPackQtyText")
    private String unitPackQtyText;
    @SerializedName("vendorId")
    private int vendorId;
    @SerializedName("vendorName")
    private String vendorName;
    @SerializedName("lot")
    private String lot;
    @SerializedName("guaranteeDate")
    private String guaranteeDate;
    @SerializedName("locatorId")
    private String locatorId;
    @SerializedName("locatorName")
    private String locatorName;

    public int getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(int warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductSpec() {
        return productSpec;
    }

    public void setProductSpec(String productSpec) {
        this.productSpec = productSpec;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getUomName() {
        return uomName;
    }

    public void setUomName(String uomName) {
        this.uomName = uomName;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public BigDecimal getPackageCount() {
        return packageCount;
    }

    public void setPackageCount(BigDecimal packageCount) {
        this.packageCount = packageCount;
    }

    public BigDecimal getQty() {
        return qty;
    }

    public void setQty(BigDecimal qty) {
        this.qty = qty;
    }

    public String getQtyText() {
        return qtyText;
    }

    public void setQtyText(String qtyText) {
        this.qtyText = qtyText;
    }

    public BigDecimal getUnitPackQty() {
        return unitPackQty;
    }

    public void setUnitPackQty(BigDecimal unitPackQty) {
        this.unitPackQty = unitPackQty;
    }

    public String getUnitPackQtyText() {
        return unitPackQtyText;
    }

    public void setUnitPackQtyText(String unitPackQtyText) {
        this.unitPackQtyText = unitPackQtyText;
    }

    public int getVendorId() {
        return vendorId;
    }

    public void setVendorId(int vendorId) {
        this.vendorId = vendorId;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getLot() {
        return lot;
    }

    public void setLot(String lot) {
        this.lot = lot;
    }

    public String getGuaranteeDate() {
        return guaranteeDate;
    }

    public void setGuaranteeDate(String guaranteeDate) {
        this.guaranteeDate = guaranteeDate;
    }

    public String getLocatorId() {
        return locatorId;
    }

    public void setLocatorId(String locatorId) {
        this.locatorId = locatorId;
    }

    public String getLocatorName() {
        return locatorName;
    }

    public void setLocatorName(String locatorName) {
        this.locatorName = locatorName;
    }
}
