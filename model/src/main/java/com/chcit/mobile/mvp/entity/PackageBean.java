package com.chcit.mobile.mvp.entity;

import com.alibaba.fastjson.annotation.JSONField;

import java.math.BigDecimal;
import java.util.Objects;

public class PackageBean {

    /**
     * packageNo : (00)090000004000000966
     * packageId : 1001911
     * asnlineId : 1050804
     * unitPackQty : 50
     * asnId : 1013797
     */

    private String packageNo;
    private String packageId;
    private String asnlineId;
    private BigDecimal unitPackQty;
    private String asnId;
    private BigDecimal qty;
    private BigDecimal storagePackageCount;
    /**
     * warehouseId : 1000220
     * warehouseName : 药剂科
     * productId : 1037385
     * productName : 丙泊酚注射液
     * productSpec : 50ml:0.5g(乳剂)/瓶
     * manufacturer : 中德北京费森尤斯
     * uomName : 瓶
     * productCode : 2032
     * packageStatus : S
     * packageStatusName : 在库
     * qty : 10
     * vendorId : 1004822
     * vendorName : 南京医药药业有限公司
     * lot : fdfk54
     * guaranteeDate : 2019-01-25
     * locatorName : 药剂科存储
     * receiveTime : 2018-12-20 14:17:28
     */

    private int warehouseId;
    private String warehouseName;
    private int productId;
    private String productName;
    private String productSpec;
    private String manufacturer;
    private String uomName;
    private String productCode;
    private String packageStatus;
    private String packageStatusName;
    private int vendorId;
    private String vendorName;
    private String lot;
    private String guaranteeDate;
    private String locatorName;
    private String receiveTime;

    public PackageBean(){}

    public PackageBean(String packageNo){
        this.packageNo=packageNo;
    }

    public String getPackageNo() {
        return packageNo;
    }

    public void setPackageNo(String packageNo) {
        this.packageNo = packageNo;
    }

    public String getPackageId() {
        return packageId;
    }

    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }

    public String getAsnlineId() {
        return asnlineId;
    }

    public void setAsnlineId(String asnlineId) {
        this.asnlineId = asnlineId;
    }

    public void setUnitPackQty(BigDecimal unitPackQty) {
        this.unitPackQty = unitPackQty;
    }

    public BigDecimal getUnitPackQty() {
        if(unitPackQty == null){
            return qty;
        }
        return unitPackQty;
    }

    public String getAsnId() {
        return asnId;
    }

    public void setAsnId(String asnId) {
        this.asnId = asnId;
    }

    public BigDecimal getQty() {
        return qty;
    }

    public void setQty(BigDecimal qty) {
        this.qty = qty;

    }

    public BigDecimal getStoragePackageCount() {
        return storagePackageCount;
    }

    public void setStoragePackageCount(BigDecimal storagePackageCount) {
        this.storagePackageCount = storagePackageCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PackageBean that = (PackageBean) o;
        return Objects.equals(packageNo, that.packageNo);
    }

    @Override
    public int hashCode() {

        return Objects.hash(packageNo);
    }

    public int getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(int warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductSpec() {
        return productSpec;
    }

    public void setProductSpec(String productSpec) {
        this.productSpec = productSpec;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getUomName() {
        return uomName;
    }

    public void setUomName(String uomName) {
        this.uomName = uomName;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getPackageStatus() {
        return packageStatus;
    }

    public void setPackageStatus(String packageStatus) {
        this.packageStatus = packageStatus;
    }

    public String getPackageStatusName() {
        return packageStatusName;
    }

    public void setPackageStatusName(String packageStatusName) {
        this.packageStatusName = packageStatusName;
    }


    public int getVendorId() {
        return vendorId;
    }

    public void setVendorId(int vendorId) {
        this.vendorId = vendorId;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getLot() {
        return lot;
    }

    public void setLot(String lot) {
        this.lot = lot;
    }

    public String getGuaranteeDate() {
        return guaranteeDate;
    }

    public void setGuaranteeDate(String guaranteeDate) {
        this.guaranteeDate = guaranteeDate;
    }

    public String getLocatorName() {
        return locatorName;
    }

    public void setLocatorName(String locatorName) {
        this.locatorName = locatorName;
    }

    public String getReceiveTime() {
        return receiveTime;
    }

    public void setReceiveTime(String receiveTime) {
        this.receiveTime = receiveTime;
    }
}
