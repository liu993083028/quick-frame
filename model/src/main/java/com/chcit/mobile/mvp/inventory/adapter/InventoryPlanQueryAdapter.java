package com.chcit.mobile.mvp.inventory.adapter;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseViewHolder;
import com.chcit.mobile.R;
import com.chcit.mobile.helper.recycler.BaseItemChangeQuickAdapter;
import com.chcit.mobile.mvp.inventory.entity.InventoryBean;

import java.util.List;

public class InventoryPlanQueryAdapter extends BaseItemChangeQuickAdapter<InventoryBean,BaseViewHolder> {


    public InventoryPlanQueryAdapter(@Nullable List<InventoryBean> data) {
        super(R.layout.item_plan_query_data,data);
    }

    @Override
    protected void convert(BaseViewHolder helper, InventoryBean item) {
        helper.setText(R.id.tv_inventory_inventoryPlanNo,item.getInventoryPlanNo());
        helper.setText(R.id.tv_inventory_createdByName,item.getCreatedByName());
        helper.setText(R.id.tv_inventory_docDate,item.getDocDate());
        int position = helper.getAdapterPosition();
        if(thisPosition == position){
            helper.setBackgroundColor(R.id.ll_first, mContext.getResources().getColor(R.color.MistyRose));
        }else if((position+1)%2 ==0){
            helper.setBackgroundColor(R.id.ll_first, mContext.getResources().getColor(R.color.bg_gray));
        }else{
            helper.setBackgroundColor(R.id.ll_first, mContext.getResources().getColor(R.color.White));
        }


    }
}
