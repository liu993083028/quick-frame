package com.chcit.mobile.mvp.entity;

import java.io.Serializable;
import java.util.Objects;

public class WareHose implements Serializable {

    /**
     * id : 1000214
     * name : 西药库
     * parentId : 0
     */
    private int id;
    private String name;
    private int parentId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    @Override
    public String toString() {
        return name;
    }


}
