package com.chcit.mobile.mvp.entity;

import java.math.BigDecimal;

public class TransshipBean {


    /**
     * asnId : 1013729
     * asnNo : 1013729
     * bpartnerName : 草药库
     * warehouseName : 中草药房
     * dateArrived : 2019-12-21
     */

    private int asnId;
    private String asnNo;
    private String warehouseName;
    private String bpartnerName;
    private String dateArrived;

    public int getAsnId() {
        return asnId;
    }

    public void setAsnId(int asnId) {
        this.asnId = asnId;
    }

    public String getAsnNo() {
        return asnNo;
    }

    public void setAsnNo(String asnNo) {
        this.asnNo = asnNo;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public String getBpartnerName() {
        return bpartnerName;
    }

    public void setBpartnerName(String bpartnerName) {
        this.bpartnerName = bpartnerName;
    }

    public String getDateArrived() {
        return dateArrived;
    }

    public void setDateArrived(String dateArrived) {
        this.dateArrived = dateArrived;
    }
}
