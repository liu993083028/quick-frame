package com.chcit.mobile.mvp.entity;

import android.os.Parcel;
import android.os.Parcelable;

import java.math.BigDecimal;

public class InventoryBean implements Parcelable {

    /**
     * warehouseId : 1000214
     * warehouseName : 西药库
     * productId : 1037355
     * productName : 阿司匹林肠溶片
     * productSpec : 0.1g×30片(肠溶片)/盒
     * manufacturer : 拜耳医药
     * uomName : 盒
     * productCode : 2002
     * qtyOnHand : 48.333333333333336
     * qtyMoving : 0
     * qtyAllocated : 0
     * storageStatus : S
     * storageStatusName : 正常
     * vendorId : 1004822
     * vendorName : 南京医药药业有限公司
     * lot : wgj01
     * guaranteeDate : 2018-12-31
     * leaveDays : 136
     * locatorId : 1013769
     * locatorName : 默认储存货位
     * pricePo : 7
     * qtyFin : 0
     * qtyCanMove : 48.333333333333333
     * receiptTypeName : 采购
     * attributeSetInstanceId : 1047198
     */

    private int warehouseId;
    private String warehouseName;
    private int productId;
    private String productName;
    private String productSpec;
    private String manufacturer;
    private String uomName;
    private String productCode;
    private BigDecimal qtyOnHand;
    private int qtyMoving;
    private int qtyAllocated;
    private String storageStatus;
    private String storageStatusName;
    private int vendorId;
    private String vendorName;
    private String lot;
    private String guaranteeDate;
    private String leaveDays;
    private String locatorId;
    private String locatorName;
    private String pricePo;
    private String qtyFin;
    private BigDecimal qtyCanMove;
    private String receiptTypeName;
    private String attributeSetInstanceId;

    public int getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(int warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductSpec() {
        return productSpec;
    }

    public void setProductSpec(String productSpec) {
        this.productSpec = productSpec;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getUomName() {
        return uomName;
    }

    public void setUomName(String uomName) {
        this.uomName = uomName;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public BigDecimal getQtyOnHand() {
        return qtyOnHand;
    }

    public void setQtyOnHand(BigDecimal qtyOnHand) {
        this.qtyOnHand = qtyOnHand;
    }

    public int getQtyMoving() {
        return qtyMoving;
    }

    public void setQtyMoving(int qtyMoving) {
        this.qtyMoving = qtyMoving;
    }

    public int getQtyAllocated() {
        return qtyAllocated;
    }

    public void setQtyAllocated(int qtyAllocated) {
        this.qtyAllocated = qtyAllocated;
    }

    public String getStorageStatus() {
        return storageStatus;
    }

    public void setStorageStatus(String storageStatus) {
        this.storageStatus = storageStatus;
    }

    public String getStorageStatusName() {
        return storageStatusName;
    }

    public void setStorageStatusName(String storageStatusName) {
        this.storageStatusName = storageStatusName;
    }

    public int getVendorId() {
        return vendorId;
    }

    public void setVendorId(int vendorId) {
        this.vendorId = vendorId;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getLot() {
        return lot;
    }

    public void setLot(String lot) {
        this.lot = lot;
    }

    public String getGuaranteeDate() {
        return guaranteeDate;
    }

    public void setGuaranteeDate(String guaranteeDate) {
        this.guaranteeDate = guaranteeDate;
    }

    public String getLeaveDays() {
        return leaveDays;
    }

    public void setLeaveDays(String leaveDays) {
        this.leaveDays = leaveDays;
    }

    public String getLocatorId() {
        return locatorId;
    }

    public void setLocatorId(String locatorId) {
        this.locatorId = locatorId;
    }

    public String getLocatorName() {
        return locatorName;
    }

    public void setLocatorName(String locatorName) {
        this.locatorName = locatorName;
    }

    public String getPricePo() {
        return pricePo;
    }

    public void setPricePo(String pricePo) {
        this.pricePo = pricePo;
    }

    public String getQtyFin() {
        return qtyFin;
    }

    public void setQtyFin(String qtyFin) {
        this.qtyFin = qtyFin;
    }

    public BigDecimal getQtyCanMove() {
        return qtyCanMove;
    }

    public void setQtyCanMove(BigDecimal qtyCanMove) {
        this.qtyCanMove = qtyCanMove;
    }

    public String getReceiptTypeName() {
        return receiptTypeName;
    }

    public void setReceiptTypeName(String receiptTypeName) {
        this.receiptTypeName = receiptTypeName;
    }

    public String getAttributeSetInstanceId() {
        return attributeSetInstanceId;
    }

    public void setAttributeSetInstanceId(String attributeSetInstanceId) {
        this.attributeSetInstanceId = attributeSetInstanceId;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.warehouseId);
        dest.writeString(this.warehouseName);
        dest.writeInt(this.productId);
        dest.writeString(this.productName);
        dest.writeString(this.productSpec);
        dest.writeString(this.manufacturer);
        dest.writeString(this.uomName);
        dest.writeString(this.productCode);
        dest.writeSerializable(this.qtyOnHand);
        dest.writeInt(this.qtyMoving);
        dest.writeInt(this.qtyAllocated);
        dest.writeString(this.storageStatus);
        dest.writeString(this.storageStatusName);
        dest.writeInt(this.vendorId);
        dest.writeString(this.vendorName);
        dest.writeString(this.lot);
        dest.writeString(this.guaranteeDate);
        dest.writeString(this.leaveDays);
        dest.writeString(this.locatorId);
        dest.writeString(this.locatorName);
        dest.writeString(this.pricePo);
        dest.writeString(this.qtyFin);
        dest.writeSerializable(this.qtyCanMove);
        dest.writeString(this.receiptTypeName);
        dest.writeString(this.attributeSetInstanceId);
    }

    public InventoryBean() {
    }

    protected InventoryBean(Parcel in) {
        this.warehouseId = in.readInt();
        this.warehouseName = in.readString();
        this.productId = in.readInt();
        this.productName = in.readString();
        this.productSpec = in.readString();
        this.manufacturer = in.readString();
        this.uomName = in.readString();
        this.productCode = in.readString();
        this.qtyOnHand = (BigDecimal) in.readSerializable();
        this.qtyMoving = in.readInt();
        this.qtyAllocated = in.readInt();
        this.storageStatus = in.readString();
        this.storageStatusName = in.readString();
        this.vendorId = in.readInt();
        this.vendorName = in.readString();
        this.lot = in.readString();
        this.guaranteeDate = in.readString();
        this.leaveDays = in.readString();
        this.locatorId = in.readString();
        this.locatorName = in.readString();
        this.pricePo = in.readString();
        this.qtyFin = in.readString();
        this.qtyCanMove = (BigDecimal) in.readSerializable();
        this.receiptTypeName = in.readString();
        this.attributeSetInstanceId = in.readString();
    }

    public static final Creator<InventoryBean> CREATOR = new Creator<InventoryBean>() {
        @Override
        public InventoryBean createFromParcel(Parcel source) {
            return new InventoryBean(source);
        }

        @Override
        public InventoryBean[] newArray(int size) {
            return new InventoryBean[size];
        }
    };
}
