package com.chcit.mobile.mvp.common.model;

import android.app.Application;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.chcit.mobile.app.exception.BaseException;
import com.chcit.mobile.common.HttpMethodContains;
import com.chcit.mobile.mvp.common.api.service.CommonService;
import com.chcit.mobile.mvp.common.api.service.UserService;
import com.chcit.mobile.mvp.entity.BaseResponse;
import com.chcit.mobile.mvp.entity.ResultBean;
import com.chcit.mobile.mvp.receive.Entity.AsnLineBean;
import com.chcit.mobile.mvp.receive.Entity.AsnPackageBean;
import com.chcit.mobile.mvp.receive.Entity.AsnResultBean;
import com.chcit.mobile.mvp.receive.api.QueryListInput;
import com.chcit.mobile.mvp.receive.api.ReceiveApi;
import com.google.gson.Gson;
import com.jess.arms.integration.IRepositoryManager;
import com.jess.arms.mvp.BaseModel;

import com.jess.arms.di.scope.FragmentScope;

import javax.inject.Inject;

import com.chcit.mobile.mvp.common.contract.ScanPackageContract;
import com.jess.arms.utils.ArmsUtils;
import com.xuexiang.xui.widget.toast.XToast;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;


@FragmentScope
public class ScanPackageModel extends BaseModel implements ScanPackageContract.Model {
    @Inject
    Gson mGson;
    @Inject
    Application mApplication;

    @Inject
    public ScanPackageModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mGson = null;
        this.mApplication = null;
    }

    @Override
    public Observable<String> queryAsnPackageDetail(JSONObject json) {
        return mRepositoryManager.obtainRetrofitService(UserService.class)
                    .requestOnString(HttpMethodContains.RECEIVE_PACKAGE_QUERY,json).subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<JSONObject> request(String method, JSONObject json) {
       return mRepositoryManager.obtainRetrofitService(UserService.class)
                .request(method,json).subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<ResultBean> requestBySubmit(String method, JSONObject json) {
        return mRepositoryManager.obtainRetrofitService(UserService.class)
                .requestBySubmit(method,json)
                .map(result -> result.toJavaObject(ResultBean.class)).subscribeOn(Schedulers.io());
    }
    //
    @Override
    public Observable<String> requestOnString(String method, JSONObject json) {
        return mRepositoryManager.obtainRetrofitService(UserService.class)
                .requestOnString(method,json).subscribeOn(Schedulers.io());
    }
    //按包装验视
    @Override
    public Observable<ResultBean> inspectePackageSubmit(JSONObject json) {
        return mRepositoryManager.obtainRetrofitService(CommonService.class)
                .inspectePackage(json).subscribeOn(Schedulers.io());
    }
    //按包装验收
    @Override
    public Observable<ResultBean> packageReceiveSubmit(JSONObject json) {
        return mRepositoryManager.obtainRetrofitService(CommonService.class)
                .checkPackageConfirm(json).subscribeOn(Schedulers.io());
    }
    //按包装上架
    @Override
    public Observable<ResultBean> pickupPackageSubmit(JSONObject json) {
        return mRepositoryManager.obtainRetrofitService(CommonService.class)
                .pickupPackageConfirm(json).subscribeOn(Schedulers.io());
    }
    //按包裝拒收
    @Override
    public Observable<ResultBean> requestRejectBySubmit(JSONObject json) {
        return mRepositoryManager.obtainRetrofitService(CommonService.class)
                .rejectPackageConfirm(json)
                .subscribeOn(Schedulers.io());
    }
    //按包裝退回
    @Override
    public Observable<ResultBean> pickbackPackageSubmit(JSONObject json) {
        return mRepositoryManager.obtainRetrofitService(CommonService.class)
                .pickbackPackageConfirm(json).subscribeOn(Schedulers.io());
    }
    //按包裝拣货确认
    @Override
    public Observable<ResultBean> packagePickComfirm(JSONObject json) {
        return mRepositoryManager.obtainRetrofitService(CommonService.class)
                .packagePickComfirm(json).subscribeOn(Schedulers.io());
    }
    //按包裝拒收入库确认
    @Override
    public Observable<ResultBean> packageRejctComfirm(JSONObject json) {
        return mRepositoryManager.obtainRetrofitService(ReceiveApi.class)
                .rejectReceivePackage(json).subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<List<AsnLineBean>> queryAsnLineTask(QueryListInput queryListInput) {

        return mRepositoryManager.obtainRetrofitService(ReceiveApi.class)
                .queryAsnTask(JSONObject.parseObject(JSONObject.toJSONString(queryListInput)))
                .map(resultString -> {
                    //ResponseBean<ReceiveBean> data =mGson.<ResponseBean<ReceiveBean>>fromJson(result,ResponseBean.class);
                    BaseResponse resultJson = JSONObject.parseObject(resultString).toJavaObject(BaseResponse.class);
                    if(resultJson.isSuccess()){

                        if(resultJson.getTotal()==0){
                            XToast.warning(mApplication,"没有查询到数据！").show();

                        }
                        return resultJson.getRows().toJavaList(AsnLineBean.class);

                    }else{
                        throw new Exception("请求失败: "+(resultJson.getMsg().isEmpty()?"未知错误":resultJson.getMsg()));
                    }

                }).subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<List<AsnPackageBean>> queryAsnPackageList(QueryListInput queryListInput) {
        return mRepositoryManager.obtainRetrofitService(ReceiveApi.class)
                .queryAsnPackageList(JSONObject.parseObject(JSONObject.toJSONString(queryListInput)))
                .map(resultString -> {
                    BaseResponse resultJson = JSONObject.parseObject(resultString).toJavaObject(BaseResponse.class);
                    if(resultJson.isSuccess()){

                        if(resultJson.getTotal()==0){
                            throw new BaseException("没有查询到数据！");
                        }
                        return resultJson.getRows(AsnPackageBean.class);

                    }else{
                        throw new Exception("请求失败: "+(resultJson.getMsg().isEmpty()?"未知错误":resultJson.getMsg()));
                    }

                }).subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<Boolean> reCheckTaskLine(List<String> packagNos, String asnId) {
        return mRepositoryManager.obtainRetrofitService(ReceiveApi.class)
                .reCheckPackage(JSONArray.toJSONString(packagNos),asnId)
                .map(resultJson -> {
                    if(resultJson.isSuccess()){

                        return true;

                    }else{
                        throw new Exception("请求失败: "+(resultJson.getMsg().isEmpty()?"未知错误":resultJson.getMsg()));
                    }

                }).subscribeOn(Schedulers.io());
    }
}