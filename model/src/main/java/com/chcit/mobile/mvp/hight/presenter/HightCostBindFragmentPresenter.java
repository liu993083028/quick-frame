package com.chcit.mobile.mvp.hight.presenter;

import android.app.Application;
import android.app.Dialog;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.chcit.custom.view.util.DialogUtil;
import com.chcit.mobile.app.exception.BaseException;
import com.chcit.mobile.common.SPDErrorHandle;
import com.chcit.mobile.mvp.entity.BaseResponse;
import com.chcit.mobile.mvp.entity.HightCostBindBean;
import com.jess.arms.integration.AppManager;
import com.jess.arms.di.scope.FragmentScope;
import com.jess.arms.mvp.BasePresenter;
import com.jess.arms.http.imageloader.ImageLoader;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import me.jessyan.rxerrorhandler.core.RxErrorHandler;

import javax.inject.Inject;

import com.chcit.mobile.mvp.hight.contract.HightCostBindContract;
import com.jess.arms.utils.RxLifecycleUtils;

import java.util.List;


/**
 * ================================================
 * Description:
 * <p>
 * Created by MVPArmsTemplate on 06/17/2019 08:45
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms">Star me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms/wiki">See me</a>
 * <a href="https://github.com/JessYanCoding/MVPArmsTemplate">模版请保持更新</a>
 * ================================================
 */
@FragmentScope
public class HightCostBindFragmentPresenter extends BasePresenter<HightCostBindContract.Model, HightCostBindContract.View> {
    @Inject
    RxErrorHandler mErrorHandler;
    @Inject
    Application mApplication;
    @Inject
    ImageLoader mImageLoader;
    @Inject
    AppManager mAppManager;

    @Inject
    public HightCostBindFragmentPresenter(HightCostBindContract.Model model, HightCostBindContract.View rootView) {
        super(model, rootView);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mErrorHandler = null;
        this.mAppManager = null;
        this.mImageLoader = null;
        this.mApplication = null;
    }
    //    pageNum:1
    //    pageSize:25
    //    limit:25
    //    start:0
    //    value:1232 -- rfid或者包装号
    //    productName:1232
    public void queryBindPakcage(JSONObject json, Consumer<List<HightCostBindBean>> onNext) {

        mModel.queryBind(json)
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                }).subscribe(new SPDErrorHandle<List<HightCostBindBean>>(mErrorHandler) {

                @Override
                public void onNext(List<HightCostBindBean> list) {
                    try {
                        onNext.accept(list);
                    } catch (Exception e) {
                        onError(e);
                    }
                }
        });
    }

    public void bindPackage(String rfVal, String packageNo, Action action) {

        mModel.bindPackage(rfVal,packageNo)
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                }).subscribe(new SPDErrorHandle<BaseResponse>(mErrorHandler) {

            @Override
            public void onNext(BaseResponse result) {
                try {
                    if(result.isSuccess()){
                        action.run();
                    }else{
                        mRootView.showDialogError(result.getMsg());
                    }
                } catch (Exception e) {
                    onError(e);
                }
            }
        });
    }

    public void unBindPackage(String rfId, Action action) {

        mModel.unBindPackage(rfId)
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                }).subscribe(new SPDErrorHandle<Boolean>(mErrorHandler) {

            @Override
            public void onNext(Boolean result) {
                try {
                    action.run();
                } catch (Exception e) {
                    onError(e);
                }
            }
        });
    }
}
