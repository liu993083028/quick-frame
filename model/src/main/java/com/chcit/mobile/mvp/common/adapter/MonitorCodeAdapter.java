package com.chcit.mobile.mvp.common.adapter;

import androidx.annotation.Nullable;
import android.widget.ImageView;

import com.chad.library.adapter.base.BaseItemDraggableAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chcit.mobile.R;
import com.chcit.mobile.mvp.entity.MonitorCode;


import java.util.List;

public class MonitorCodeAdapter extends BaseItemDraggableAdapter<MonitorCode,BaseViewHolder> {

    public MonitorCodeAdapter(@Nullable List<MonitorCode> data) {
        super(R.layout.item_shipment_scanview,data);
    }

    @Override
    protected void convert(BaseViewHolder helper, MonitorCode item) {

        helper.setText(R.id.tv_shipment_scan_no,helper.getAdapterPosition()+1+"");
        helper.setText(R.id.tv_shipment_scan_code,item.getCode());
        ImageView imageView = helper.getView(R.id.iv_shipment_scan_delete);
        imageView.setOnClickListener(view ->{
            int position = helper.getAdapterPosition();
            mOnItemSwipeListener.onItemSwiped(null,position);
            remove(position);
        });
    }
}
