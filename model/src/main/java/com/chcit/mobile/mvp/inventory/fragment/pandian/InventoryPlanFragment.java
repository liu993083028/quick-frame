package com.chcit.mobile.mvp.inventory.fragment.pandian;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;

import com.alibaba.fastjson.JSONObject;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chcit.custom.view.loader.LoaderStyle;
import com.chcit.custom.view.loader.QuickLoader;
import com.chcit.custom.view.edit.SuperInputEditText;
import com.chcit.mobile.R;
import com.chcit.mobile.app.base.ScanFragment;
import com.chcit.mobile.di.component.DaggerInventoryPlanComponent;
import com.chcit.mobile.di.module.InventoryPlanModule;
import com.chcit.mobile.helper.recycler.RecycleViewDivider;
import com.chcit.mobile.mvp.inventory.contract.InventoryPlanContract;
import com.chcit.mobile.mvp.entity.InventoryPlanBean;
import com.chcit.mobile.mvp.entity.Statu;
import com.chcit.mobile.mvp.entity.StorageBean;
import com.chcit.mobile.mvp.inventory.entity.InventoryBean;
import com.chcit.mobile.mvp.inventory.presenter.InventoryPlanPresenter;
import com.chcit.mobile.mvp.inventory.adapter.InventoryPlanAdapter;
import com.jess.arms.di.component.AppComponent;
import com.jess.arms.utils.ArmsUtils;
import com.xuexiang.xui.widget.toast.XToast;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import fr.ganfra.materialspinner.MaterialSpinner;

import static com.jess.arms.utils.Preconditions.checkNotNull;


public class InventoryPlanFragment extends ScanFragment<InventoryPlanPresenter> implements InventoryPlanContract.View {

    private InventoryBean mInventoryBean;
    @BindView(R.id.et_inventory_plan_productName)
    SuperInputEditText etProductName;
    @BindView(R.id.bt_inventory_plan_query)
    Button btQuery;
    @BindView(R.id.spinner_inventory_plan_statu)
    MaterialSpinner spinner;
    @BindView(R.id.lv_inventory_plan_list)
    RecyclerView mRecyclerView;
    protected String type;
    private List<StorageBean> storageBeans;
    private InventoryPlanAdapter mAdapter;
    private StorageBean storageBean;
    private int REQUEST_CODE = this.getClass().hashCode();
    private int itemPosition;
    private InventoryPlanBean mInventoryPlanBean;

    public static InventoryPlanFragment newInstance(InventoryPlanBean inventoryPlanBean) {
        InventoryPlanFragment fragment = new InventoryPlanFragment();
        fragment.mInventoryPlanBean = inventoryPlanBean;
        return fragment;
    }

    @Override
    public void setupFragmentComponent(@NonNull AppComponent appComponent) {
        DaggerInventoryPlanComponent //如找不到该类,请编译一下项目
                .builder()
                .appComponent(appComponent)
                .inventoryPlanModule(new InventoryPlanModule(this))
                .build()
                .inject(this);
    }

    @Override
    public View initView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
      return inflater.inflate(R.layout.fragment_inventory_plan, container, false);

    }
    private int checkItemPosition = -2;
    private Statu statu = null;
    @Override
    public void initData(@Nullable Bundle savedInstanceState) {
        mCommonToolbar.setTitleText("盘点一览");
        mCommonToolbar.getLeftMenuView(0).setOnClickListener(l -> {
            pop();
        });
        List<Statu> status = Arrays.asList(new Statu(" o.lineprocessed = 'N'","未修改"),new Statu(" o.lineprocessed = 'Y'","已修改"));
        ArrayAdapter<Statu> adapter = new ArrayAdapter<Statu>(getContext(), R.layout.spinner_display_style,R.id.text,status){

            @NonNull
            @Override
            public View getDropDownView(int position,  @Nullable View convertView,  @NonNull ViewGroup parent) {
                View  view =super.getDropDownView(position, convertView, parent);
                AppCompatImageView imageView = view.findViewById(R.id.spinner_image);
                if (checkItemPosition == position) {
                    imageView.setVisibility(View.VISIBLE);
                } else {
                    imageView.setVisibility(View.INVISIBLE);
                }
                return  view;
            }
        };
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_stytle);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                checkItemPosition = position;
                if(position>=0){
                    statu = status.get(position);
                }else{
                    statu = null;
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        initRecyclerView();

    }


    @Override
    public void setData(@Nullable Object data) {

    }

    @Override
    public void showLoading() {
        QuickLoader.showLoading(mContext,LoaderStyle.BallBeatIndicator);
    }

    @Override
    public void hideLoading() {
        QuickLoader.stopLoading();
    }

    @Override
    public void showMessage(@NonNull String message) {
        checkNotNull(message);
        XToast.info(mContext,message).show();
    }

    @Override
    public void launchActivity(@NonNull Intent intent) {
        checkNotNull(intent);
        ArmsUtils.startActivity(intent);
    }

    @Override
    public void killMyself() {
        pop();
    }

    @Override
    protected void onBarcodeEventSuccess(String barcodeData) {
        etProductName.setText(barcodeData);
        mPresenter.setStart(0);
        getData();
    }

    @Override
    public void onEnterAnimationEnd(Bundle savedInstanceState) {
        super.onEnterAnimationEnd(savedInstanceState);

    }

    private void initRecyclerView() {
        mAdapter = new InventoryPlanAdapter(storageBeans);
        //ArmsUtils.configRecyclerView(mRecyclerView, mLayoutManager);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.addItemDecoration(new RecycleViewDivider(getContext(), LinearLayoutManager.HORIZONTAL, R.drawable.item_drivide));
        mAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                mAdapter.loadMoreComplete();
                getData();
            }
        }, mRecyclerView);
        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {

            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                itemPosition = position;
                storageBean = (StorageBean) adapter.getData().get(position);
               // startForResult(InventoryPlanHandleFragment.newInstance(storageBean), REQUEST_CODE);

            }
        });
        mRecyclerView.setAdapter(mAdapter);


    }

    @OnClick({R.id.bt_inventory_plan_query})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.bt_inventory_plan_query:
                mAdapter.getData().clear();
                mPresenter.setStart(0);
                getData();
                break;
        }
    }
    @Override
    public void onFragmentResult(int requestCode, int resultCode, Bundle data) {
        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {
            mAdapter.remove(itemPosition);
        }
    }
    private void getData() {
        JSONObject data = new JSONObject();
               /* start:0
                limit:25
                headerID:1000050
                inventoryPlanType:1
                appSessionID:1542677744838.12*/

        data.put("headerID", mInventoryPlanBean.getM_InventoryPlan_ID());
        data.put("inventoryPlanType",mInventoryPlanBean.getInventoryPlanType());
        if (!etProductName.getText().toString().isEmpty()) {
            data.put("productName", etProductName.getText().toString());
        }
        if(statu != null){
            data.put("validation",statu.getId());
        }
             /*   if (!tietProductName.getText().toString().isEmpty()) {
                    data.put("productName", tietProductName.getText().toString());
                }
                if (!tietLocator.getText().toString().isEmpty()) {
                    data.put("manufacturer", tietLocator.getText().toString());
                }
                if (mStatu != null) {
                    data.put("storageStatus", mStatu.getId());
                }
                if (bpartnerBean != null) {
                    data.put("orgID", bpartnerBean.getId());
                }*/
        mPresenter.getData(data, list -> {

            mAdapter.addData(list);
        });

    }

    @Override
    public void showNoMoreData() {
        mAdapter.setEnableLoadMore(true);
    }

}
