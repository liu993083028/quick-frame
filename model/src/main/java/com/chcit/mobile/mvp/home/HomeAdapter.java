package com.chcit.mobile.mvp.home;

import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat;
import androidx.appcompat.widget.AppCompatImageView;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chcit.mobile.R;
import com.chcit.mobile.mvp.entity.HomeItem;
import com.xuexiang.xui.widget.textview.badge.Badge;
import com.xuexiang.xui.widget.textview.badge.BadgeView;

import java.util.List;


/**
 * https://github.com/CymChad/BaseRecyclerViewAdapterHelper
 */
public class HomeAdapter extends BaseQuickAdapter<HomeItem, BaseViewHolder> {



    public HomeAdapter(int layoutResId, List data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, HomeItem item) {
        helper.setText(R.id.text, item.getTitle());
        helper.setImageResource(R.id.icon, item.getImageResource());
          /*
        RequestOptions options = new RequestOptions()
                .placeholder(item.getImageResource());

      GlideArms.with(Quick.getApplicationContext())
                .load(item.getUrl())
                .apply(options)
                .into(helper.<AppCompatImageView>getView(R.id.icon));*/
       // AppCompatResources.getDrawable(mContext,item.getImageResource())
        //setImageResource(item.getImageResource());
        LinearLayout llBadge = helper.getView(R.id.item_home_show);
        BadgeView badge = (BadgeView) llBadge.getTag();
        if(badge == null){
             badge  = new BadgeView(mContext);
             badge.bindTarget(llBadge);
             llBadge.setTag(badge);
            VectorDrawableCompat vectorDrawableCompat = VectorDrawableCompat.create(mContext.getResources(),item.getImageResource(),mContext.getTheme());
            //你需要改变的颜色
            //vectorDrawableCompat.setTint();
            helper.<AppCompatImageView>getView(R.id.icon).setImageDrawable(vectorDrawableCompat);
            badge.setBadgeTextSize(12, true);
        }

        badge.setBadgeNumber(item.getTaskNumber());
        //badges.put(String.valueOf(item.getTaskNumber()),badge);

    }

}
