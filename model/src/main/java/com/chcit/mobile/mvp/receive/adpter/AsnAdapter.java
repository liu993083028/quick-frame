package com.chcit.mobile.mvp.receive.adpter;

import androidx.annotation.Nullable;
import android.view.View;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chcit.mobile.R;
import com.chcit.mobile.helper.recycler.BaseItemChangeQuickAdapter;
import com.chcit.mobile.mvp.common.model.MenuTypeEnum;
import com.chcit.mobile.mvp.receive.Entity.AsnResultBean;

import java.util.List;

public class AsnAdapter extends BaseItemChangeQuickAdapter<AsnResultBean,BaseViewHolder> {

    private View.OnClickListener mOnClickListener;
    private MenuTypeEnum type;
    public AsnAdapter(@Nullable List<AsnResultBean> data) {
        super(R.layout.item_asn_data,data);

    }

    public void setOnClickListener(View.OnClickListener onClickListener){
        mOnClickListener = onClickListener;
    }
    @Override
    protected void convert(final BaseViewHolder helper, final AsnResultBean item) {
        int i = helper.getAdapterPosition();
        if(thisPosition == i){
            helper.getView(R.id.ll_asn_item).setBackgroundColor(mContext.getResources().getColor(R.color.LavenderBlush));
        }else if((i+2)%2 == 0){
            helper.getView(R.id.ll_asn_item).setBackgroundColor(mContext.getResources().getColor(R.color.gray_f2));
        }else{
            helper.getView(R.id.ll_asn_item).setBackgroundColor(mContext.getResources().getColor(R.color.white));
        }
        helper.setText(R.id.tv_asn_no,item.getAsnNo());
        helper.setText(R.id.tv_asn_bpartnerName,item.getBpartnerName());
        helper.setText(R.id.tv_asn_dateArrived,item.getDateArrived());
        helper.setText(R.id.tv_asn_asnTypeName,item.getAsnTypeName());

    }


    public MenuTypeEnum getType() {
        return type;
    }

    public void setType(MenuTypeEnum type) {
        this.type = type;
    }
}
