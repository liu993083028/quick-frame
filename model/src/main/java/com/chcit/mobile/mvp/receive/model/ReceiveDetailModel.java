package com.chcit.mobile.mvp.receive.model;

import android.app.Application;

import com.alibaba.fastjson.JSONObject;
import com.chcit.mobile.common.HttpMethodContains;
import com.chcit.mobile.mvp.common.api.service.CommonService;
import com.chcit.mobile.mvp.common.api.service.UserService;
import com.chcit.mobile.mvp.entity.Locator;
import com.chcit.mobile.mvp.entity.Reason;
import com.chcit.mobile.mvp.entity.ResultBean;
import com.chcit.mobile.mvp.receive.api.ReceiveApi;
import com.chcit.mobile.mvp.receive.api.ReceiveInput;
import com.google.gson.Gson;
import com.jess.arms.integration.IRepositoryManager;
import com.jess.arms.mvp.BaseModel;

import com.jess.arms.di.scope.FragmentScope;

import javax.inject.Inject;

import com.chcit.mobile.mvp.receive.contract.ReceiveDetailContract;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;


@FragmentScope
public class ReceiveDetailModel extends BaseModel implements ReceiveDetailContract.Model {
    @Inject
    Gson mGson;
    @Inject
    Application mApplication;

    @Inject
    public ReceiveDetailModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mGson = null;
        this.mApplication = null;
    }

    @Override
    public Observable<Locator> getLocatorId(JSONObject json) {
        return mRepositoryManager.obtainRetrofitService(UserService.class)
                .request(HttpMethodContains.LOCATORS,json)
                .map(jsonObject -> {
                    ResultBean resultBean = mGson.fromJson(jsonObject.toJSONString(),ResultBean.class);
                    if(resultBean.isSuccess()){
                        return resultBean.getRows(Locator.class).get(0);
                    }else{
                        throw new Exception("请求失败: "+resultBean.getMsg());
                    }


                });
    }

    @Override
    public Observable<ResultBean> doConfirm(String method,JSONObject json) {
        return mRepositoryManager.obtainRetrofitService(UserService.class)
                .request(method,json)
                .map(jsonObject -> {
                    ResultBean resultBean = mGson.fromJson(jsonObject.toJSONString(),ResultBean.class);
                    if(resultBean.isSuccess()){
                        return resultBean;
                    }else{
                        throw new Exception("请求失败: "+resultBean.getMsg());
                    }

                });

    }

    @Override
    public Observable<List<Locator>> getLocators(JSONObject json) {
        return mRepositoryManager.obtainRetrofitService(UserService.class)
                .request(HttpMethodContains.LOCATORS,json)
                .map(jsonObject -> {
                    ResultBean resultBean = mGson.fromJson(jsonObject.toJSONString(),ResultBean.class);
                    if(resultBean.isSuccess()){
                        return resultBean.getRows(Locator.class);
                    }else{
                        throw new Exception("请求失败: "+resultBean.getMsg());
                    }

                });
    }

    @Override
    public Observable<List<Reason>> getReans(JSONObject json) {
       return mRepositoryManager.obtainRetrofitService(UserService.class)
                .request(HttpMethodContains.REJECT_REASON,json)
                .map(jsonObject -> {
                    ResultBean resultBean = mGson.fromJson(jsonObject.toJSONString(),ResultBean.class);
                    if(resultBean.isSuccess()){
                        return resultBean.getRows(Reason.class);
                    }else{
                        throw new Exception("请求失败: "+resultBean.getMsg());
                    }

                });
    }

    //非包装验收确认
    @Override
    public Observable<ResultBean> checkAsnSubmit(JSONObject json) {
        return mRepositoryManager.obtainRetrofitService(CommonService.class)
                .checkAsnSubmit(json)
                .subscribeOn(Schedulers.io());
    }

    //非包装上架确认
    @Override
    public Observable<ResultBean> pickupAsnSubmit(JSONObject json) {
        return mRepositoryManager.obtainRetrofitService(CommonService.class)
                .pickupAsnSubmit(json)
                .subscribeOn(Schedulers.io());
    }

    //按包裝拣货确认
    @Override
    public Observable<ResultBean> preCheckRecieve(JSONObject json) {
        return mRepositoryManager.obtainRetrofitService(CommonService.class)
                .preCheckRecieve(json);
    }

    // 拣货确认
    public Observable<ResultBean> checkAsnLine(ReceiveInput receiveInput) {

        return mRepositoryManager.obtainRetrofitService(ReceiveApi.class)
                .checkAsnLine(receiveInput);
    }
}