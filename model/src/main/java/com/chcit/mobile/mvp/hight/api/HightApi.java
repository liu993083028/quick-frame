package com.chcit.mobile.mvp.hight.api;

import com.alibaba.fastjson.JSONObject;
import com.chcit.mobile.mvp.entity.BaseResponse;
import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

import static com.chcit.mobile.common.HttpMethodContains.Hight.HIGHT_BIND_OK;
import static com.chcit.mobile.common.HttpMethodContains.Hight.HIGHT_COST_CREATE;
import static com.chcit.mobile.common.HttpMethodContains.Hight.HIGHT_COST_QUEERY;
import static com.chcit.mobile.common.HttpMethodContains.Hight.HIGHT_COST_RECEIVE;
import static com.chcit.mobile.common.HttpMethodContains.Hight.HIGHT_COST_REJECT;
import static com.chcit.mobile.common.HttpMethodContains.Hight.HIGHT_QUERY_BIND;
import static com.chcit.mobile.common.HttpMethodContains.Hight.HIGHT_UN_BIND;

public interface HightApi {
    @FormUrlEncoded
    @POST(HIGHT_COST_CREATE)
    Observable<BaseResponse> createBorrowOrderByPackage(@FieldMap JSONObject json);

    @FormUrlEncoded
    @POST(HIGHT_COST_REJECT)
    Observable<BaseResponse> borrowBackByPackage(@FieldMap JSONObject json);

    @FormUrlEncoded
    @POST(HIGHT_COST_RECEIVE)
    Observable<BaseResponse> brrowUseByPackage(@FieldMap JSONObject json);

    @FormUrlEncoded
    @POST(HIGHT_QUERY_BIND)
    Observable<BaseResponse> queryBind(@FieldMap JSONObject json);

    @FormUrlEncoded
    @POST(HIGHT_BIND_OK)
    Observable<BaseResponse> bindPackage(@Field("rfVal")String rfVal, @Field("packageNo")String packageNo);

    @FormUrlEncoded
    @POST(HIGHT_UN_BIND)
    Observable<BaseResponse> unBindPackage(@Field("id")String packageId);

    @FormUrlEncoded
    @POST(HIGHT_COST_QUEERY)
    Observable<BaseResponse> queryHightCost(@FieldMap JSONObject json);

}
