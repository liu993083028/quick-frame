package com.chcit.mobile.mvp.entity;

import java.math.BigDecimal;

public class MovementPlanBean{

    /**
     * movementPlanId :
     * movementPlanNo :
     * movementPlanLineId :
     * productId :
     * productCode :
     * productName : 阿司匹林肠溶片
     * productSpec : 0.1g×30片(肠溶片)/盒
     * manufacturer : 拜耳医药
     * uomId :
     * uomName : 盒
     * lot :
     * guaranteeDate :
     * qtyPlaned :
     * qtyConfirmed :
     * qtyCancelled :
     * qtyLeft :
     * datePlaned" :
     * warehouseId :
     * warehouseName :
     * description :
     * vendorName :
     * storageStatus :
     * storageStatusName :
     * toStorageStatus :
     * toStorageStatusName :
     * zoneId :
     * zoneName :
     * locatorId :
     * locatorValue :
     * locatorName :
     * locatorToId :
     * toLocatorValue :
     * toLocatorName :
     * isStoragePackage : Y 包装 N 非包装
     * lineStatus :
     * lineStatusName :
     * unitPackQty :
     */

    private int movementPlanId;
    private String movementPlanNo;
    private int movementPlanLineId;
    private int productId;
    private String productCode;
    private String productName;
    private String productSpec;
    private String manufacturer;
    private int uomId;
    private String uomName;
    private String lot;
    private String guaranteeDate;
    private BigDecimal qtyPlaned;
    private BigDecimal qtyConfirmed;
    private BigDecimal qtyCancelled;
    private BigDecimal qtyLeft;
    private String datePlaned;
    private int warehouseId;
    private String warehouseName;
    private String description;
    private String vendorName;
    private String storageStatus;
    private String storageStatusName;
    private String toStorageStatus;
    private String toStorageStatusName;
    private int zoneId;
    private String zoneName;
    private int locatorId;
    private String locatorValue;
    private String locatorName;
    private int locatorToId;
    private String toLocatorValue;
    private String toLocatorName;
    private String isStoragePackage;
    private String lineStatus;
    private String lineStatusName;
    private String  unitPackQty;

    public int getMovementPlanId() {
        return movementPlanId;
    }

    public void setMovementPlanId(int movementPlanId) {
        this.movementPlanId = movementPlanId;
    }

    public String getMovementPlanNo() {
        return movementPlanNo;
    }

    public void setMovementPlanNo(String movementPlanNo) {
        this.movementPlanNo = movementPlanNo;
    }

    public int getMovementPlanLineId() {
        return movementPlanLineId;
    }

    public void setMovementPlanLineId(int movementPlanLineId) {
        this.movementPlanLineId = movementPlanLineId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductSpec() {
        return productSpec;
    }

    public void setProductSpec(String productSpec) {
        this.productSpec = productSpec;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public int getUomId() {
        return uomId;
    }

    public void setUomId(int uomId) {
        this.uomId = uomId;
    }

    public String getUomName() {
        return uomName;
    }

    public void setUomName(String uomName) {
        this.uomName = uomName;
    }

    public String getLot() {
        return lot;
    }

    public void setLot(String lot) {
        this.lot = lot;
    }

    public String getGuaranteeDate() {
        return guaranteeDate;
    }

    public void setGuaranteeDate(String guaranteeDate) {
        this.guaranteeDate = guaranteeDate;
    }

    public BigDecimal getQtyPlaned() {
        return qtyPlaned;
    }

    public void setQtyPlaned(BigDecimal qtyPlaned) {
        this.qtyPlaned = qtyPlaned;
    }

    public BigDecimal getQtyConfirmed() {
        return qtyConfirmed;
    }

    public void setQtyConfirmed(BigDecimal qtyConfirmed) {
        this.qtyConfirmed = qtyConfirmed;
    }

    public BigDecimal getQtyCancelled() {
        return qtyCancelled;
    }

    public void setQtyCancelled(BigDecimal qtyCancelled) {
        this.qtyCancelled = qtyCancelled;
    }

    public BigDecimal getQtyLeft() {
        return qtyLeft;
    }

    public void setQtyLeft(BigDecimal qtyLeft) {
        this.qtyLeft = qtyLeft;
    }

    public String getDatePlaned() {
        return datePlaned;
    }

    public void setDatePlaned(String datePlaned) {
        this.datePlaned = datePlaned;
    }

    public int getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(int warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getStorageStatus() {
        return storageStatus;
    }

    public void setStorageStatus(String storageStatus) {
        this.storageStatus = storageStatus;
    }

    public String getStorageStatusName() {
        return storageStatusName;
    }

    public void setStorageStatusName(String storageStatusName) {
        this.storageStatusName = storageStatusName;
    }

    public String getToStorageStatus() {
        return toStorageStatus;
    }

    public void setToStorageStatus(String toStorageStatus) {
        this.toStorageStatus = toStorageStatus;
    }

    public String getToStorageStatusName() {
        return toStorageStatusName;
    }

    public void setToStorageStatusName(String toStorageStatusName) {
        this.toStorageStatusName = toStorageStatusName;
    }

    public int getZoneId() {
        return zoneId;
    }

    public void setZoneId(int zoneId) {
        this.zoneId = zoneId;
    }

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String zoneName) {
        this.zoneName = zoneName;
    }

    public int getLocatorId() {
        return locatorId;
    }

    public void setLocatorId(int locatorId) {
        this.locatorId = locatorId;
    }

    public String getLocatorValue() {
        return locatorValue;
    }

    public void setLocatorValue(String locatorValue) {
        this.locatorValue = locatorValue;
    }

    public String getLocatorName() {
        return locatorName;
    }

    public void setLocatorName(String locatorName) {
        this.locatorName = locatorName;
    }

    public int getLocatorToId() {
        return locatorToId;
    }

    public void setLocatorToId(int locatorToId) {
        this.locatorToId = locatorToId;
    }

    public String getToLocatorValue() {
        return toLocatorValue;
    }

    public void setToLocatorValue(String toLocatorValue) {
        this.toLocatorValue = toLocatorValue;
    }

    public String getToLocatorName() {
        return toLocatorName;
    }

    public void setToLocatorName(String toLocatorName) {
        this.toLocatorName = toLocatorName;
    }

    public String getIsStoragePackage() {
        return isStoragePackage;
    }

    public void setIsStoragePackage(String isStoragePackage) {
        this.isStoragePackage = isStoragePackage;
    }

    public String getLineStatus() {
        return lineStatus;
    }

    public void setLineStatus(String lineStatus) {
        this.lineStatus = lineStatus;
    }

    public String getLineStatusName() {
        return lineStatusName;
    }

    public void setLineStatusName(String lineStatusName) {
        this.lineStatusName = lineStatusName;
    }

    public String getUnitPackQty() {
        return unitPackQty;
    }

    public void setUnitPackQty(String unitPackQty) {
        this.unitPackQty = unitPackQty;
    }

    public MovementPlanBean() {
    }
}
