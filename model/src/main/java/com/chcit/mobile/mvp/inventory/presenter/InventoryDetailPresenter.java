package com.chcit.mobile.mvp.inventory.presenter;

import android.app.Application;
import com.alibaba.fastjson.JSONObject;
import com.chcit.custom.view.util.DialogUtil;
import com.chcit.mobile.common.HttpMethodContains;
import com.chcit.mobile.common.SPDErrorHandle;
import com.chcit.mobile.mvp.common.contract.CommonContract;
import com.chcit.mobile.mvp.entity.Locator;
import com.chcit.mobile.mvp.entity.ResultBean;
import com.jess.arms.integration.AppManager;
import com.jess.arms.di.scope.FragmentScope;
import com.jess.arms.mvp.BasePresenter;
import com.jess.arms.http.imageloader.ImageLoader;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import me.jessyan.rxerrorhandler.core.RxErrorHandler;
import javax.inject.Inject;
import com.chcit.mobile.mvp.inventory.contract.InventoryDetailContract;
import com.jess.arms.utils.RxLifecycleUtils;


import java.util.List;


@FragmentScope
public class InventoryDetailPresenter extends BasePresenter<CommonContract.Model, InventoryDetailContract.View> {
    @Inject
    RxErrorHandler mErrorHandler;
    @Inject
    Application mApplication;
    @Inject
    ImageLoader mImageLoader;
    @Inject
    AppManager mAppManager;

    @Inject
    public InventoryDetailPresenter(CommonContract.Model model, InventoryDetailContract.View rootView) {
        super(model, rootView);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mErrorHandler = null;
        this.mAppManager = null;
        this.mImageLoader = null;
        this.mApplication = null;
    }

    public void getLocators( JSONObject json,Consumer<List<Locator>> consumer){
        mModel.requestOnString(HttpMethodContains.LOCATORS,json)
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                })
                .subscribe(new SPDErrorHandle<JSONObject>(mErrorHandler) {

                    @Override
                    public void onNext(JSONObject data) {
                        if(data.getInteger("total")>0){
                            try {
                                consumer.accept(data.getJSONArray("rows").toJavaList(Locator.class));
                            } catch (Exception e) {
                                onError(e);
                            }
                        }else{
                            mRootView.showMessage("没有查询到货位信息");
                        }

                    }
                });

    }

    public void moven(JSONObject json, Action action){
        mModel.requestBySubmit(HttpMethodContains.MOVE_OK,json)
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                })
                .subscribe(new SPDErrorHandle<JSONObject>(mErrorHandler) {

                    @Override
                    public void onNext(JSONObject data) {
                        if(data.getBoolean("success")){
                            try {
                                action.run();
                            } catch (Exception e) {
                                onError(e);
                            }
                        }else{
                            onError(new Exception(data.getString("msg")));
                        }
                    }
                });
    }
}
