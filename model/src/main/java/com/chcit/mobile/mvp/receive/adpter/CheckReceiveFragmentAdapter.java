package com.chcit.mobile.mvp.receive.adpter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.PagerAdapter;

import com.chcit.mobile.mvp.common.model.MenuTypeEnum;
import com.chcit.mobile.mvp.receive.fragment.pack.child.FirstPagerFragment;
import com.chcit.mobile.mvp.receive.fragment.pack.child.SecondPagerFragment;

public class CheckReceiveFragmentAdapter extends FragmentStatePagerAdapter {
    private String[] mTitles;
    private MenuTypeEnum type;
    public CheckReceiveFragmentAdapter(FragmentManager fm, MenuTypeEnum type, String... titles) {
        super(fm);
        mTitles = titles;
        this.type = type;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return FirstPagerFragment.newInstance(type);
        } else {
            return SecondPagerFragment.newInstance(type);
        }
    }

    @Override
    public int getCount() {
        return mTitles.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTitles[position];
    }

    /**
     * 使用这个方式，让页面不缓存，能够在清除fragment的时候对其做了删除
     *
     * @param object
     * @return
     */
    @Override
    public int getItemPosition(Object object) {
        return PagerAdapter.POSITION_NONE;
    }


}