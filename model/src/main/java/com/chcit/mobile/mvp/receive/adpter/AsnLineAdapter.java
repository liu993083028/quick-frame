package com.chcit.mobile.mvp.receive.adpter;

import androidx.annotation.Nullable;
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat;
import androidx.appcompat.widget.AppCompatImageView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chcit.custom.view.notify.Badge;
import com.chcit.custom.view.notify.QBadgeView;
import com.chcit.mobile.R;
import com.chcit.mobile.mvp.receive.Entity.AsnLineBean;

import java.util.List;

public class AsnLineAdapter extends BaseQuickAdapter<AsnLineBean,BaseViewHolder> {

    public AsnLineAdapter(@Nullable List<AsnLineBean> data) {
        super(R.layout.item_asn_line_task,data);
    }

    @Override
    protected void convert(BaseViewHolder helper, AsnLineBean item) {

        helper.setText(R.id.tv_asn_line_productName,item.getProductName()+"-"+item.getProductSpec()+"-配送单号"+item.getDeliveryNo());
        helper.setText(R.id.tv_asn_line_qtyArrived,item.getPackageCountArrived()+"包/"+item.getQtyArrived().toString()+item.getUomName());
        helper.setText(R.id.tv_asn_line_qtyChecked,item.getPackageCountChecked()+"包/"+item.getQtyChecked().toString()+item.getUomName());
        helper.setText(R.id.tv_asn_line_qtyCheckLeft,item.getPackageCountCheckLeft()+"包/"+item.getQtyCheckLeft().toString()+item.getUomName());
        helper.setText(R.id.tv_asn_line_rejectLabel,item.getPackageCountRejected()+"包/"+item.getQtyRejected().toString()+item.getUomName());
        Badge badge = new QBadgeView(mContext).bindTarget(helper.getView(R.id.ll_asn_line_task));
        badge.setBadgeTextSize(16, false);
        //badge.setBadgeGravity(Gravity.CENTER);
        if(item.getQtyCheckLeft().intValue() ==0){
            badge.setBadgeText("已完成");
        }else{
            badge.setBadgeBackgroundColor(R.color.xui_config_color_white);
            badge.setBadgeTextColor(R.color.xui_config_color_red);
            badge.setBadgeText("");
        }
//        helper.setText(R.id.tv_asn_line_productCode,item.getProductCode());
//        helper.setText(R.id.tv_asn_line_productSpec,item.getProductSpec());
    }
}
