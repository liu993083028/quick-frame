package com.chcit.mobile.mvp.inventory.di.module;

import com.jess.arms.di.scope.FragmentScope;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

import com.chcit.mobile.mvp.inventory.contract.PackageInfoQueryContract;
import com.chcit.mobile.mvp.inventory.model.PackageInfoQueryModel;


/**
 * ================================================
 * Description:
 * <p>
 * Created by MVPArmsTemplate on 09/18/2019 16:50
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms">Star me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms/wiki">See me</a>
 * <a href="https://github.com/JessYanCoding/MVPArmsTemplate">模版请保持更新</a>
 * ================================================
 */
@Module
public abstract class PackageInfoQueryModule {

    @Binds
    abstract PackageInfoQueryContract.Model bindPackageInfoQueryModel(PackageInfoQueryModel model);
}