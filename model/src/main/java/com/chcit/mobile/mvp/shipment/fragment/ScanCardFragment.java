package com.chcit.mobile.mvp.shipment.fragment;


import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alibaba.fastjson.JSONObject;
import com.chcit.custom.view.edit.SuperInputEditText;
import com.chcit.custom.view.imagetext.SuperTextView;
import com.chcit.custom.view.loader.QuickLoader;
import com.chcit.custom.view.widget.MarqueTextView;
import com.chcit.mobile.R;
import com.chcit.mobile.app.base.ScanFragment;
import com.chcit.mobile.app.base.ScanToolFragment;
import com.chcit.mobile.common.DataKeys;
import com.chcit.mobile.common.HttpMethodContains;
import com.chcit.mobile.di.component.DaggerDispensingComponent;
import com.chcit.mobile.di.module.DispensingModule;
import com.chcit.mobile.mvp.common.model.MenuTypeEnum;
import com.chcit.mobile.mvp.entity.HomeItem;
import com.chcit.mobile.mvp.shipment.contract.DispensingContract;
import com.chcit.mobile.mvp.shipment.presenter.DispensingPresenter;
import com.jakewharton.rxbinding3.widget.RxTextView;
import com.jess.arms.di.component.AppComponent;
import com.xuexiang.xui.utils.SnackbarUtils;
import com.xuexiang.xui.widget.edittext.ValidatorEditText;
import com.xuexiang.xui.widget.toast.XToast;

import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

import butterknife.BindView;
import io.reactivex.disposables.Disposable;


import static com.jess.arms.utils.Preconditions.checkNotNull;

public class ScanCardFragment extends ScanToolFragment<DispensingPresenter> implements DispensingContract.View {


    @BindView(R.id.stv_card_image)
    SuperTextView stvImage;
    @BindView(R.id.tv_card_dec)
    MarqueTextView tvDec;
    @BindView(R.id.et_card_no)
    ValidatorEditText etNo;
    ScanFragment fragment = null;
    MenuTypeEnum type;
    HomeItem item;
    @Override
    protected void onBarcodeEventSuccess(String barcodeData) {
        etNo.setText(barcodeData);
        if(type.equals(MenuTypeEnum.SC)){
            fragment = DirectShipmentFragment.newInstance();
            JSONObject requestParams = new JSONObject();
            requestParams.put("userCode",barcodeData);
            mPresenter.checkUserCode(requestParams, (list) -> {
                //页面传参
                Bundle args = new Bundle();
                args.putString("userCode", barcodeData);
                args.putInt("userID", list.getJSONObject(0).getIntValue("AD_User_ID"));
                fragment.setArguments(args);
                start(fragment);
            });
        }else{
            getDataList(barcodeData);
        }

    }

    @Override
    public void setupFragmentComponent(@NonNull AppComponent appComponent) {
        DaggerDispensingComponent //如找不到该类,请编译一下项目
                .builder()
                .appComponent(appComponent)
                .dispensingModule(new DispensingModule(this))
                .build()
                .inject(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_scan_card;
    }

    Disposable subscribe;
    @Override
    public void initData(@Nullable Bundle bundle) {
        stvImage.setOnClickListener(v->{
            initZxing();
        });
        item = _mActivity.getIntent().getParcelableExtra(DataKeys.HOME_ITEM.name());
        switch (item.getId()) {
            case "spd.mobile.pms.PrescriptionOut":
                tvDec.setText("请扫码处方拣货单号");
                type = MenuTypeEnum.PRESCRIPTION_SHIPMENT;
                break;
            case "spd.mobile.pms.WardApplyOut":
                tvDec.setText("请扫码医嘱拣货单号");
                type = MenuTypeEnum.DOCUTOR_ADVICE_SHIPMNET;
                break;
             default:
                 type= MenuTypeEnum.SC;
        }

        subscribe = RxTextView.editorActionEvents(etNo)
                .subscribe(textViewEditorActionEvent -> {
                    String text = Objects.requireNonNull(etNo.getText()).toString();
                    if (textViewEditorActionEvent.getActionId() == KeyEvent.KEYCODE_ENDCALL&&!text.isEmpty()) {
                        onBarcodeEventSuccess(text);
                    }
                });

    }

    @Override
    public void setData(@Nullable Object o) {

    }

    @Override
    public void showLoading() {
        QuickLoader.showLoading(mContext);
    }

    @Override
    public void hideLoading() {
        QuickLoader.stopLoading();
    }

    @Override
    public void showMessage(@NonNull String message) {
        checkNotNull(message);
        XToast.info(mContext,message).show();
    }
    private void getDataList(String preNo) {

        JSONObject data = new JSONObject();
               /* start:0
                limit:25
                bpartnerID:1019706
                showLot:true
                appSessionID:1541585528858.6*/
        String method = null;
        if (StringUtils.isNotEmpty(preNo)) {
            switch (type){
                case PRESCRIPTION_SHIPMENT:
                    data.put("presNo", preNo);
                    method = HttpMethodContains.DISPENSING_QUERY;
                    mPresenter.getData(method, data, list -> {
                        if(list.size()>0) {
                            start(PrescriptionShipmentFragment.newInstance(preNo, list));
                        }else{
                            SnackbarUtils.Short(getView(), "没有查询到拣货任务！")
                                    .info()
                                    .show();
                        }
                    });
                    break;
                case DOCUTOR_ADVICE_SHIPMNET:
                    data.put("documentNo", preNo);
                    method = HttpMethodContains.DOCUTOR_DISPENSING_QUERY;
                    mPresenter.getData(method, data, list -> {
                        if(list.size()>0) {
                            start(DoctorAdviceShipmentFragment.newInstance(preNo, list));
                        }else{
                            SnackbarUtils.Short(getView(), "没有查询到拣货任务！")
                                    .info()
                                    .show();
                        }
                    });
                    break;
            }



        }
    }

    @Override
    public void killMyself() {
        pop();
        if(subscribe!=null){
            subscribe.dispose();
        }

    }


}
