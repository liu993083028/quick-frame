package com.chcit.mobile.mvp.inventory.adapter;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseViewHolder;
import com.chcit.mobile.R;
import com.chcit.mobile.helper.recycler.BaseItemChangeQuickAdapter;
import com.chcit.mobile.mvp.entity.PackageUnitBean;

import java.util.List;

public class PackageAdapter extends BaseItemChangeQuickAdapter<PackageUnitBean,BaseViewHolder> {

   

    public PackageAdapter(@Nullable List<PackageUnitBean> data) {
        super(R.layout.item_inventory_package_data,data);
    }
    

    @Override
    protected void convert(BaseViewHolder helper, PackageUnitBean item) {
        helper.setText(R.id.tv_package_pname,item.getProductName());
        helper.setText(R.id.tv_package_Lot,item.getLot());
        helper.setText(R.id.tv_package_Manufacturer,item.getManufacturer());
        helper.setText(R.id.tv_package_GuaranteeDate,item.getGuaranteeDate());
        helper.setText(R.id.tv_package_count,item.getPackageCount().toString());
        helper.setText(R.id.tv_package_Qty,item.getUnitPackQty()+"");
        helper.setText(R.id.tv_package_productSpec,item.getProductSpec());
        helper.setText(R.id.tv_package_locator,item.getLocatorName());
        helper.setText(R.id.tv_package_code,item.getProductCode());
        helper.setText(R.id.tv_package_VendorName,item.getVendorName());
        helper.setText(R.id.tv_package_unitPackQtyText,item.getUnitPackQtyText());
        int position =  helper.getAdapterPosition();
        if(thisPosition==position){
            helper.setBackgroundColor(R.id.ll_list_item, mContext.getResources().getColor(R.color.MistyRose));
        }else if((position+1)%2 ==0){
            helper.setBackgroundColor(R.id.ll_list_item, mContext.getResources().getColor(R.color.bg_gray));
        }else{
            helper.setBackgroundColor(R.id.ll_list_item, mContext.getResources().getColor(R.color.White));
        }
    }
}
