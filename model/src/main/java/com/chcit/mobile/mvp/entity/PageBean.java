package com.chcit.mobile.mvp.entity;

import com.alibaba.fastjson.annotation.JSONField;

public class PageBean {

    /**
     * pageNum : 1
     * pageSize : 25
     * limit : 25
     * start : 0
     */


    private Integer pageNum ;

    private Integer pageSize = 25;

    private Integer limit = 50;

    private Integer start;
    public PageBean(){

    }
    private PageBean(Builder builder) {
        setPageNum(builder.pageNum);
        setPageSize(builder.pageSize);
        setLimit(builder.limit);
        setStart(builder.start);
    }

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public static final class Builder {
        private Integer pageNum;
        private Integer pageSize;
        private Integer limit;
        private Integer start;

        public Builder() {
        }

        public Builder pageNum(Integer val) {
            pageNum = val;
            return this;
        }

        public Builder pageSize(Integer val) {
            pageSize = val;
            return this;
        }

        public Builder limit(Integer val) {
            limit = val;
            return this;
        }

        public Builder start(Integer val) {
            start = val;
            return this;
        }

        public PageBean build() {
            return new PageBean(this);
        }
    }
}
