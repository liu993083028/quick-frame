package com.chcit.mobile.mvp.entity;

import java.util.Objects;

public class SupervisionCode {
    private int serNo;
    private String code;

    public SupervisionCode(int serNo, String code) {
        this.serNo = serNo;
        this.code = code;
    }
    public SupervisionCode( String code) {
        this.code = code;
    }
    public SupervisionCode( ) {

    }
    public int getSerNo() {
        return serNo;
    }

    public void setSerNo(int serNo) {
        this.serNo = serNo;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SupervisionCode that = (SupervisionCode) o;
        return Objects.equals(code, that.code);
    }

    @Override
    public int hashCode() {

        return Objects.hash(code);
    }
}
