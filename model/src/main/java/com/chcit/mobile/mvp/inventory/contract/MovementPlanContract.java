package com.chcit.mobile.mvp.inventory.contract;

import android.content.Context;

import com.alibaba.fastjson.JSONObject;
import com.chcit.mobile.mvp.entity.MovementPlanBean;
import com.jess.arms.mvp.IView;
import com.jess.arms.mvp.IModel;

import java.util.List;

import io.reactivex.Observable;

public interface MovementPlanContract {
    //对于经常使用的关于UI的方法可以定义到IView中,如显示隐藏进度条,和显示文字消息
    interface View extends IView {
        /**
         * 更新查询到的数据
         * @param movementPlanBeans
         */
        void updateContentList(List<MovementPlanBean> movementPlanBeans);

        /**
         * 获取页面的控件
         * @param i
         * @return
         */
        <T extends android.view.View> T getViewByTag(int i);

        Context getActivity();
    }

    //Model层定义接口,外部只需关心Model返回的数据,无需关心内部细节,即是否使用缓存
    interface Model extends IModel {
        Observable<List<MovementPlanBean>> getMovementPlans(JSONObject json);
    }
}
