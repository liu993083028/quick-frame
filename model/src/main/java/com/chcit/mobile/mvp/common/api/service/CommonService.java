/*
 * Copyright 2017 JessYan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.chcit.mobile.mvp.common.api.service;

import com.alibaba.fastjson.JSONObject;
import com.chcit.mobile.common.HttpMethodContains;
import com.chcit.mobile.mvp.entity.BaseData;
import com.chcit.mobile.mvp.entity.BaseResponseBean;
import com.chcit.mobile.mvp.entity.BpartnerBean;
import com.chcit.mobile.mvp.entity.GoodsAllocation;
import com.chcit.mobile.mvp.entity.Locator;
import com.chcit.mobile.mvp.entity.MonitorCode;
import com.chcit.mobile.mvp.entity.ResultBean;
import com.chcit.mobile.mvp.entity.ShipmentBean;
import com.chcit.mobile.mvp.entity.Statu;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

import static com.chcit.mobile.common.HttpMethodContains.REASON_URL;
import static com.chcit.mobile.common.HttpMethodContains.REQUEST_QUERY_URL;
import static com.chcit.mobile.common.HttpMethodContains.STATUS_URL;

/**
 * ================================================
 * 存放通用的一些 API
 * <p>
 * Created by JessYan on 08/05/2016 12:05
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * ================================================
 */
public interface CommonService {
    @GET(STATUS_URL)
    Observable<BaseData<Statu>> refList(@Query("id") int id);

    @GET(STATUS_URL)
    Observable<BaseData<GoodsAllocation>> getLocators(@Query("id") int id);

    @GET(REQUEST_QUERY_URL)
    Observable<BaseResponseBean<MonitorCode>> getMonitorCodes(@Query("method")String method, @Query("data")JSONObject json);
    //获取拒收原因
    @GET(REASON_URL)
    Observable<BaseData<Statu>> queryResons();

    @GET(HttpMethodContains.PACKAGE_UNIT)
    Observable<String> queryPackageUnit(@QueryMap JSONObject param);

    @GET(HttpMethodContains.PACKAGE_INFO)
    Observable<String> queryPackageInfo(@QueryMap JSONObject param);
    /*checkUser2:0
    password:123*/
    @GET(HttpMethodContains.USER_URL)
    Observable<BaseData<Statu>> queryUsers(@QueryMap JSONObject param);

    @POST(HttpMethodContains.USER_CHECK)
    @FormUrlEncoded
    Observable<String> checkUser(@Field("checkUser2") String checkUser, @Field("password") String password);

    @GET(HttpMethodContains.SHIPMENT_PACKAGE_URL)
    Observable<String> shipmentPackageConfirm(@QueryMap JSONObject param);

    @GET(HttpMethodContains.PACKAGE_MERGE)
    Observable<String> packageMergeConfirm(@QueryMap JSONObject param);

    @GET(HttpMethodContains.PACKAGE_SPLIT)
    Observable<String> packageSplitConfirm(@QueryMap JSONObject param);

    @GET(HttpMethodContains.PACKAGE_SUB)
    Observable<String> packageSubConfirm(@QueryMap JSONObject json);

    @GET(HttpMethodContains.CHECK_PACKAGE)
    Observable<ResultBean> checkPackageConfirm(@QueryMap JSONObject json);

    @GET(HttpMethodContains.REJECT_PACKAGE)
    Observable<ResultBean> rejectPackageConfirm(@QueryMap JSONObject json);

    @GET(HttpMethodContains.INSPECTE_PACKAGE)
    Observable<ResultBean> inspectePackage(@QueryMap JSONObject json);

    @GET(HttpMethodContains.PICKUP_PACKAGELINE)
    Observable<ResultBean> pickupPackageConfirm(@QueryMap JSONObject json);

    @GET(HttpMethodContains.PICKBACK_PACKAGE)
    Observable<ResultBean> pickbackPackageConfirm(@QueryMap JSONObject json);

    @GET(HttpMethodContains.Shipment.CONFIRM_LIST)
    Observable<ResultBean> pickComfirm(@QueryMap JSONObject json);

    @GET(HttpMethodContains.PACKAGEPICKCOMFIRM_SHIPMENT)
    Observable<ResultBean> packagePickComfirm(@QueryMap JSONObject json);

    @GET(HttpMethodContains.PRECHECK_PACKAGE)
    Observable<ResultBean> preCheckRecieve(@QueryMap JSONObject json);

    @GET(HttpMethodContains.CHECK_ASN)
    Observable<ResultBean> checkAsnSubmit(@QueryMap JSONObject json);

    @GET(HttpMethodContains.PICKUP_ASN)
    Observable<ResultBean> pickupAsnSubmit(@QueryMap JSONObject json);

    @GET(HttpMethodContains.Common.VENDOR_URL)
    Observable<BaseData<BpartnerBean>> getVendor(@Query("warehouse_id") String wareHoseId);

    @GET(HttpMethodContains.Inventory.PACKAGE_HISTORY_INFO)
    Observable<String> queryPackageHistoryInfo(@QueryMap JSONObject param);
}
