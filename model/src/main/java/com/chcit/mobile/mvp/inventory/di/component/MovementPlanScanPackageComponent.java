package com.chcit.mobile.mvp.inventory.di.component;

import dagger.BindsInstance;
import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.chcit.mobile.mvp.inventory.di.module.MovementPlanScanPackageModule;
import com.chcit.mobile.mvp.inventory.contract.MovementPlanScanPackageContract;

import com.jess.arms.di.scope.FragmentScope;
import com.chcit.mobile.mvp.inventory.fragment.move.MovementPlanScanPackageFragment;


/**
 * ================================================
 * Description:
 * <p>
 * Created by MVPArmsTemplate on 10/14/2019 09:53
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms">Star me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms/wiki">See me</a>
 * <a href="https://github.com/JessYanCoding/MVPArmsTemplate">模版请保持更新</a>
 * ================================================
 */
@FragmentScope
@Component(modules = MovementPlanScanPackageModule.class, dependencies = AppComponent.class)
public interface MovementPlanScanPackageComponent {
    void inject(MovementPlanScanPackageFragment fragment);

    @Component.Builder
    interface Builder {
        @BindsInstance
        MovementPlanScanPackageComponent.Builder view(MovementPlanScanPackageContract.View view);

        MovementPlanScanPackageComponent.Builder appComponent(AppComponent appComponent);

        MovementPlanScanPackageComponent build();
    }
}