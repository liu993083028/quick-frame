package com.chcit.mobile.mvp.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.google.gson.annotations.SerializedName;

public class TaskMenuBean {

   private int count;

   @JSONField(name="menu")
   // @SerializedName("name")
   private ChildrenBean menu;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public ChildrenBean getMenu() {
        return menu;
    }

    public void setMenu(ChildrenBean menu) {
        this.menu = menu;
    }
}
