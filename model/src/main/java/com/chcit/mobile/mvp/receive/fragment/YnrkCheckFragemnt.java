package com.chcit.mobile.mvp.receive.fragment;

import com.chcit.mobile.mvp.common.model.MenuTypeEnum;

/**
 * 院内入库
 */
public class YnrkCheckFragemnt extends OldReceiveFragment {

    public YnrkCheckFragemnt() {
        this.type = MenuTypeEnum.YN_CHECK;
    }
}
