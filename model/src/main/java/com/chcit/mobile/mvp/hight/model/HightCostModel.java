package com.chcit.mobile.mvp.hight.model;

import android.app.Application;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.chcit.mobile.app.exception.BaseException;
import com.chcit.mobile.app.exception.CodeMsg;
import com.chcit.mobile.common.HttpMethodContains;
import com.chcit.mobile.helper.HttpClientHelper;
import com.chcit.mobile.mvp.common.api.service.UserService;
import com.chcit.mobile.mvp.entity.BaseResponse;
import com.chcit.mobile.mvp.entity.PageBean;
import com.chcit.mobile.mvp.entity.ResponseBean;
import com.chcit.mobile.mvp.entity.ResultBean;
import com.chcit.mobile.mvp.hight.api.HightApi;
import com.google.gson.Gson;
import com.jess.arms.integration.IRepositoryManager;
import com.jess.arms.mvp.BaseModel;

import com.jess.arms.di.scope.FragmentScope;

import javax.inject.Inject;

import com.chcit.mobile.mvp.hight.contract.HightCostContract;

import org.apache.commons.lang3.StringUtils;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

@FragmentScope
public class HightCostModel extends BaseModel implements HightCostContract.Model {
    @Inject
    Gson mGson;
    @Inject
    Application mApplication;

    @Inject
    public HightCostModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mGson = null;
        this.mApplication = null;
    }

    @Override
    public Observable<JSONArray> createBorrowOrderByPackage(JSONObject json) {

        return mRepositoryManager.obtainRetrofitService(HightApi.class)
                .createBorrowOrderByPackage(json)
                .map(result -> {
                    if(result.isSuccess()){
                        return  result.getRows();
                    }else{
                        throw new BaseException(result.getMsg());
                    }
                }).subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<JSONArray> borrowBackByPackage(JSONObject json) {
        return mRepositoryManager.obtainRetrofitService(HightApi.class)
                .borrowBackByPackage(json)
                .map(result -> {
                    if(result.isSuccess()){
                        return  result.getRows();
                    }else{
                        throw new BaseException(result.getMsg());
                    }
                }).subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<JSONArray> borrowUserByPackage(JSONObject json) {
        return mRepositoryManager.obtainRetrofitService(HightApi.class)
                .brrowUseByPackage(json)
                .map(result -> {
                    if(result.isSuccess()){
                        return  result.getRows();
                    }else{
                        throw new BaseException(result.getMsg());
                    }
                }).subscribeOn(Schedulers.io());
    }
//    pageNum: 1
//    pageSize: 25
//    limit: 25
//    start: 0
//    menuId: borrow/order_query.xml
//    dateFrom: 2019-10-11
//    dateTo: 2019-10-18
    @Override
    public Observable<JSONArray> queryHightCost(PageBean pageBean,String dateForm,String dateTo) {
        JSONObject param =   JSONObject.parseObject(JSONObject.toJSONString(pageBean));
        param.put("menuId","borrow/order_query.xml");
        if( StringUtils.isNotEmpty(dateForm)){
            param.put("dateFrom",dateForm);
        }
        if(StringUtils.isNotEmpty(dateTo)){
            param.put("dateTo",dateTo);
        }
        param.put("warehouseId", HttpClientHelper.getSelectWareHouse().getId());
        return mRepositoryManager.obtainRetrofitService(HightApi.class)
                .queryHightCost(param)
                .map(result -> {
                    if(result.isSuccess()){
                        return  result.getRows();
                    }else{
                        throw new BaseException(result.getMsg());
                    }
                }).subscribeOn(Schedulers.io());
    }
}