package com.chcit.mobile.mvp.inventory.di.module;

import com.jess.arms.di.scope.FragmentScope;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

import com.chcit.mobile.mvp.inventory.contract.MovementPlanScanPackageContract;
import com.chcit.mobile.mvp.inventory.model.MovementPlanScanPackageModel;


/**
 * ================================================
 * Description:
 * <p>
 * Created by MVPArmsTemplate on 10/14/2019 09:53
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms">Star me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms/wiki">See me</a>
 * <a href="https://github.com/JessYanCoding/MVPArmsTemplate">模版请保持更新</a>
 * ================================================
 */
@Module
public abstract class MovementPlanScanPackageModule {

    @Binds
    abstract MovementPlanScanPackageContract.Model bindMovementPlanScanPackageModel(MovementPlanScanPackageModel model);
}