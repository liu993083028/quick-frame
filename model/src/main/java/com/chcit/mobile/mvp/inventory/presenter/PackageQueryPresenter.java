package com.chcit.mobile.mvp.inventory.presenter;

import android.app.Application;
import android.graphics.pdf.PdfDocument;

import com.alibaba.fastjson.JSONObject;
import com.chcit.mobile.common.HttpMethodContains;
import com.chcit.mobile.common.SPDErrorHandle;
import com.chcit.mobile.mvp.common.model.CommonModel;
import com.chcit.mobile.mvp.entity.BaseData;
import com.chcit.mobile.mvp.entity.BpartnerBean;
import com.chcit.mobile.mvp.entity.InventoryBean;
import com.chcit.mobile.mvp.entity.PackageBean;
import com.chcit.mobile.mvp.entity.PackageUnitBean;
import com.chcit.mobile.mvp.entity.PageBean;
import com.chcit.mobile.mvp.entity.Statu;
import com.chcit.mobile.mvp.inventory.model.api.PackageDetailInput;
import com.chcit.mobile.mvp.inventory.model.api.PackageQueyInput;
import com.jess.arms.integration.AppManager;
import com.jess.arms.di.scope.FragmentScope;
import com.jess.arms.mvp.BasePresenter;
import com.jess.arms.http.imageloader.ImageLoader;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import me.jessyan.rxerrorhandler.core.RxErrorHandler;

import javax.inject.Inject;

import com.chcit.mobile.mvp.inventory.contract.PackageQueryContract;
import com.jess.arms.utils.RxLifecycleUtils;

import java.util.List;

@FragmentScope
public class PackageQueryPresenter extends BasePresenter<PackageQueryContract.Model, PackageQueryContract.View> {
    @Inject
    RxErrorHandler mErrorHandler;
    @Inject
    Application mApplication;
    @Inject
    ImageLoader mImageLoader;
    @Inject
    AppManager mAppManager;
    @Inject
    CommonModel commModel;
    private boolean isLoading;
    @Inject
    PageBean pageBean;

    @Inject
    public PackageQueryPresenter(PackageQueryContract.Model model, PackageQueryContract.View rootView) {
        super(model, rootView);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mErrorHandler = null;
        this.mAppManager = null;
        this.mImageLoader = null;
        this.mApplication = null;
    }

    public void getBpartners( Consumer<List<BpartnerBean>> consumer){
        commModel.getBparenerBeans()
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SPDErrorHandle<List<BpartnerBean>>(mErrorHandler){
                    @Override
                    public void onNext(List<BpartnerBean> bpartnerBeans) {
                        try {
                            consumer.accept(bpartnerBeans);
                        } catch (Exception e) {
                            onError(e);
                        }
                    }
                });
    }

    public void getStatus(int id, Consumer<List<Statu>> consumer,boolean update){
        commModel.refList(id,update)
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SPDErrorHandle<List<Statu>>(mErrorHandler){

                    @Override
                    public void onNext(List<Statu> status) {
                        try {
                            consumer.accept(status);
                        } catch (Exception e) {
                            onError(e);
                        }
                    }
                });
    }

    public void getData(PackageQueyInput packageQueyInput, Consumer<List<PackageUnitBean>> onNext){
        if (!isLoading) {
            isLoading = true;
            mModel.getDataList(pageBean,packageQueyInput)
                    .subscribeOn(Schedulers.io())
                    .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                    .observeOn(AndroidSchedulers.mainThread())
                    .doFinally(()->mRootView.hideLoading())
                    .subscribe(new SPDErrorHandle<BaseData<PackageUnitBean>>(mErrorHandler){

                        @Override
                        public void onNext(BaseData<PackageUnitBean> baseData) {
                            isLoading = false;
                            try {
                                int total = baseData.getTotal();
                                mRootView.showTotal("共"+total+"条结果");
                                if(total == 0){
                                    mRootView.showMessage("没有查询到库存数据！");
                                }else if(total> pageBean.getStart()){
                                    pageBean.setStart(pageBean.getPageSize()+pageBean.getStart());
                                    onNext.accept(baseData.getRows());
                                    if(pageBean.getLimit()>= total){
                                        mRootView.setEnableLoadMore(false);
                                    }
                                }else{
                                    mRootView.setEnableLoadMore(false);
                                    // mAdapter.setEnableLoadMore(false);
                                }

                            } catch (Exception e) {
                                onError(e);
                            }
                        }

                        @Override
                        public void onError(Throwable t) {
                            isLoading = false;
                            super.onError(t);
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        }

    }

    public void getPakcageInfo(PageBean page, PackageDetailInput packageDetailInput, Consumer<List<PackageBean>> consumer){
        String pageString = JSONObject.toJSONString(page);
        pageString = pageString.substring(0,pageString.length()-1);
        String packageQueryString = JSONObject.toJSONString(packageDetailInput);
        packageQueryString = packageQueryString.substring(1,packageQueryString.length());
        if(packageQueryString.length()>1){
            pageString+=",";
        }
        JSONObject queryJSON = JSONObject.parseObject(pageString + packageQueryString);
        commModel.queryPackageInfo(queryJSON)
                .map(result -> result.toJavaList(PackageBean.class))
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                })
                .subscribe(new SPDErrorHandle<List<PackageBean>>(mErrorHandler) {
                    @Override
                    public void onNext(List<PackageBean> lists) {
                        try {
                            consumer.accept(lists);
                            mRootView.showTotal("共查询到"+lists.size()+"个包装");
                        } catch (Exception e) {
                            onError(e);
                        }
                    }
                });
    }
}
