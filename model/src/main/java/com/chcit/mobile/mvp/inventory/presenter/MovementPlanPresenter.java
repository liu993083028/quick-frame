package com.chcit.mobile.mvp.inventory.presenter;

import android.app.Application;

import com.alibaba.fastjson.JSONObject;
import com.chcit.mobile.common.SPDErrorHandle;
import com.chcit.mobile.mvp.entity.MovementPlanBean;
import com.chcit.mobile.mvp.inventory.model.MovementPlanModel;
import com.jess.arms.integration.AppManager;
import com.jess.arms.di.scope.FragmentScope;
import com.jess.arms.mvp.BasePresenter;
import com.jess.arms.http.imageloader.ImageLoader;

import io.reactivex.android.schedulers.AndroidSchedulers;
import me.jessyan.rxerrorhandler.core.RxErrorHandler;

import javax.inject.Inject;

import com.chcit.mobile.mvp.inventory.contract.MovementPlanContract;
import com.jess.arms.utils.RxLifecycleUtils;

import java.util.ArrayList;
import java.util.List;

@FragmentScope
public class MovementPlanPresenter extends BasePresenter<MovementPlanContract.Model, MovementPlanContract.View> {
    @Inject
    RxErrorHandler mErrorHandler;
    @Inject
    Application mApplication;
    @Inject
    ImageLoader mImageLoader;
    @Inject
    AppManager mAppManager;
    @Inject
    MovementPlanModel cModel;

    @Inject
    public MovementPlanPresenter(MovementPlanContract.Model model, MovementPlanContract.View rootView) {
        super(model, rootView);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mErrorHandler = null;
        this.mAppManager = null;
        this.mImageLoader = null;
        this.mApplication = null;
    }

    public void getData(JSONObject json) {
        cModel.getMovementPlans(json)
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> {
                    mRootView.showLoading();
                })
                .doFinally(() -> {
                    mRootView.hideLoading();
                })
                .subscribe(new SPDErrorHandle<List<MovementPlanBean>>(mErrorHandler) {
                    @Override
                    public void onNext(List<MovementPlanBean> lists) {
                        mRootView.updateContentList(new ArrayList<>(lists));
                    }
                });
    }
}
