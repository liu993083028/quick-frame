package com.chcit.mobile.mvp.inventory.adapter;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseViewHolder;
import com.chcit.mobile.R;
import com.chcit.mobile.helper.recycler.BaseSelectQuickAdapter;
import com.chcit.mobile.mvp.entity.MovementPlanBean;

import java.util.List;

public class MovementPlanAdapter extends BaseSelectQuickAdapter<MovementPlanBean,BaseViewHolder> {

    public MovementPlanAdapter(@Nullable List<MovementPlanBean> data) {
        super(R.layout.item_inventory_data,data);
    }

    @Override
    protected void convert(final BaseViewHolder helper, final MovementPlanBean item) {

        helper.setText(R.id.tv_inventory_pname,item.getProductName());
        helper.setText(R.id.tv_inventory_productSpec,item.getProductSpec());
        helper.setText(R.id.tv_inventory_Manufacturer,item.getManufacturer());
        helper.setText(R.id.tv_inventory_VendorName,item.getVendorName());
        helper.setText(R.id.tv_inventory_GuaranteeDate,item.getGuaranteeDate());
        helper.setText(R.id.tv_inventory_Lot,item.getLot());
        helper.setText(R.id.tv_inventory_locator,item.getLocatorValue());
        helper.setText(R.id.tv_inventory_StorageStatusName,item.getStorageStatusName());
        helper.setText(R.id.tv_inventory_QtyOnHand,item.getQtyPlaned()+"" + item.getUomName());

//        helper.setText(R.id.lv_inventory_QtyOnHand,"移库数量:");
        helper.setText(R.id.tv_inventory_deal,"去移库");
//        helper.getView(R.id.tv_inventory_deal).setVisibility(View.GONE);
    }
}
