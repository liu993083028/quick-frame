package com.chcit.mobile.mvp.transship.api;

import com.alibaba.fastjson.JSONObject;
import com.chcit.mobile.common.HttpMethodContains;
import com.chcit.mobile.mvp.entity.BaseData;
import com.chcit.mobile.mvp.entity.ResultBean;
import com.chcit.mobile.mvp.entity.TransshipBean;

import io.reactivex.Observable;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

public interface TransshipApi {

    @FormUrlEncoded
    @POST(HttpMethodContains.Receive.ASN_QUERY)
    Observable<BaseData<TransshipBean>> queryReceives(@FieldMap JSONObject json);

    @GET(HttpMethodContains.Transship.TRANSSHIP_CONFORM)
    Observable<ResultBean> transshipConfirm(@QueryMap JSONObject json);

}
