package com.chcit.mobile.mvp.inventory.entity;

import com.alibaba.fastjson.annotation.JSONField;

import java.math.BigDecimal;

public class PackageHistoryBean {


    /**
     * packageId :
     * warehouseId :
     * workWarehouseName :
     * productId :
     * productName :
     * productSpec :
     * manufacturer :
     * uomName :
     * productCode :
     * packageType :
     * packageTypeName :
     * packageStatus :
     * packageStatusName :
     * packageNo :
     * soucePackageNo :
     * qty :
     * qtyText :
     * createdByName :
     * created :
     * storageStatus :
     * storageStatusName :
     * vendorId :
     * vendorName :
     * lot :
     * guaranteeDate :
     * pricePo :
     * locatorName :
     * serNo :
     * receiveTime :
     * isControlledProduct :
     * workTypeName :
     * workUserName :
     * workUser2Name :
     * workDate :
     */

    @JSONField(name = "packageId")
    private int packageId;
    @JSONField(name = "warehouseId")
    private int warehouseId;
    @JSONField(name = "workWarehouseName")
    private String workWarehouseName;
    @JSONField(name = "productId")
    private String productId;
    @JSONField(name = "productName")
    private String productName;
    @JSONField(name = "productSpec")
    private String productSpec;
    @JSONField(name = "manufacturer")
    private String manufacturer;
    @JSONField(name = "uomName")
    private String uomName;
    @JSONField(name = "productCode")
    private String productCode;
    @JSONField(name = "packageType")
    private String packageType;
    @JSONField(name = "packageTypeName")
    private String packageTypeName;
    @JSONField(name = "packageStatus")
    private String packageStatus;
    @JSONField(name = "packageStatusName")
    private String packageStatusName;
    @JSONField(name = "packageNo")
    private String packageNo;
    @JSONField(name = "soucePackageNo")
    private String soucePackageNo;
    @JSONField(name = "qty")
    private String qty;
    @JSONField(name = "qtyText")
    private String qtyText;
    @JSONField(name = "createdByName")
    private String createdByName;
    @JSONField(name = "created")
    private String created;
    @JSONField(name = "storageStatus")
    private String storageStatus;
    @JSONField(name = "storageStatusName")
    private String storageStatusName;
    @JSONField(name = "vendorId")
    private String vendorId;
    @JSONField(name = "vendorName")
    private String vendorName;
    @JSONField(name = "lot")
    private String lot;
    @JSONField(name = "guaranteeDate")
    private String guaranteeDate;
    @JSONField(name = "pricePo")
    private String pricePo;
    @JSONField(name = "locatorName")
    private String locatorName;
    @JSONField(name = "serNo")
    private String serNo;
    @JSONField(name = "receiveTime")
    private String receiveTime;
    @JSONField(name = "isControlledProduct")
    private String isControlledProduct;
    @JSONField(name = "workTypeName")
    private String workTypeName;
    @JSONField(name = "workUserName")
    private String workUserName;
    @JSONField(name = "workUser2Name")
    private String workUser2Name;
    @JSONField(name = "workDate")
    private String workDate;

    public int getPackageId() {
        return packageId;
    }

    public void setPackageId(int packageId) {
        this.packageId = packageId;
    }

    public int getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(int warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getWorkWarehouseName() {
        return workWarehouseName;
    }

    public void setWorkWarehouseName(String workWarehouseName) {
        this.workWarehouseName = workWarehouseName;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductSpec() {
        return productSpec;
    }

    public void setProductSpec(String productSpec) {
        this.productSpec = productSpec;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getUomName() {
        return uomName;
    }

    public void setUomName(String uomName) {
        this.uomName = uomName;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getPackageType() {
        return packageType;
    }

    public void setPackageType(String packageType) {
        this.packageType = packageType;
    }

    public String getPackageTypeName() {
        return packageTypeName;
    }

    public void setPackageTypeName(String packageTypeName) {
        this.packageTypeName = packageTypeName;
    }

    public String getPackageStatus() {
        return packageStatus;
    }

    public void setPackageStatus(String packageStatus) {
        this.packageStatus = packageStatus;
    }

    public String getPackageStatusName() {
        return packageStatusName;
    }

    public void setPackageStatusName(String packageStatusName) {
        this.packageStatusName = packageStatusName;
    }

    public String getPackageNo() {
        return packageNo;
    }

    public void setPackageNo(String packageNo) {
        this.packageNo = packageNo;
    }

    public String getSoucePackageNo() {
        return soucePackageNo;
    }

    public void setSoucePackageNo(String soucePackageNo) {
        this.soucePackageNo = soucePackageNo;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getQtyText() {
        return qtyText;
    }

    public void setQtyText(String qtyText) {
        this.qtyText = qtyText;
    }

    public String getCreatedByName() {
        return createdByName;
    }

    public void setCreatedByName(String createdByName) {
        this.createdByName = createdByName;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getStorageStatus() {
        return storageStatus;
    }

    public void setStorageStatus(String storageStatus) {
        this.storageStatus = storageStatus;
    }

    public String getStorageStatusName() {
        return storageStatusName;
    }

    public void setStorageStatusName(String storageStatusName) {
        this.storageStatusName = storageStatusName;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getLot() {
        return lot;
    }

    public void setLot(String lot) {
        this.lot = lot;
    }

    public String getGuaranteeDate() {
        return guaranteeDate;
    }

    public void setGuaranteeDate(String guaranteeDate) {
        this.guaranteeDate = guaranteeDate;
    }

    public String getPricePo() {
        return pricePo;
    }

    public void setPricePo(String pricePo) {
        this.pricePo = pricePo;
    }

    public String getLocatorName() {
        return locatorName;
    }

    public void setLocatorName(String locatorName) {
        this.locatorName = locatorName;
    }

    public String getSerNo() {
        return serNo;
    }

    public void setSerNo(String serNo) {
        this.serNo = serNo;
    }

    public String getReceiveTime() {
        return receiveTime;
    }

    public void setReceiveTime(String receiveTime) {
        this.receiveTime = receiveTime;
    }

    public String getIsControlledProduct() {
        return isControlledProduct;
    }

    public void setIsControlledProduct(String isControlledProduct) {
        this.isControlledProduct = isControlledProduct;
    }

    public String getWorkTypeName() {
        return workTypeName;
    }

    public void setWorkTypeName(String workTypeName) {
        this.workTypeName = workTypeName;
    }

    public String getWorkUserName() {
        return workUserName;
    }

    public void setWorkUserName(String workUserName) {
        this.workUserName = workUserName;
    }

    public String getWorkUser2Name() {
        return workUser2Name;
    }

    public void setWorkUser2Name(String workUser2Name) {
        this.workUser2Name = workUser2Name;
    }

    public String getWorkDate() {
        return workDate;
    }

    public void setWorkDate(String workDate) {
        this.workDate = workDate;
    }
}
