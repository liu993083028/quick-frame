package com.chcit.mobile.mvp.entity;

import java.util.Objects;

public class MonitorCode {

    /**
     * id : 1212
     * monitoringCode : 1111
     */

    private int id;
    private String Code;

    public MonitorCode(int id, String monitoringCode) {
        this.id = id;
        this.Code = monitoringCode;

    }

    public MonitorCode() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MonitorCode that = (MonitorCode) o;
        return Code.equals(that.Code);
    }



    @Override
    public int hashCode() {

        return Objects.hash(id, Code);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String monitoringCode) {
        this.Code = monitoringCode;
    }
}
