package com.chcit.mobile.mvp.hight.fragment;


import com.chcit.mobile.mvp.common.model.MenuTypeEnum;

/**
 * 高值耗材退回
 */
public class HightCostRejectFragment extends HightCostFragment {

    public HightCostRejectFragment() {
        this.type = MenuTypeEnum.HIGHT_REJECT;
    }
}
