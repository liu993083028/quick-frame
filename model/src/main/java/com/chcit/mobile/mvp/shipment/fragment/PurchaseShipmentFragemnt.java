package com.chcit.mobile.mvp.shipment.fragment;

import com.chcit.mobile.mvp.common.model.MenuTypeEnum;

/**
 * 采购出库页面
 */
public class PurchaseShipmentFragemnt extends ShipmentFragment{

    public PurchaseShipmentFragemnt(){
        this.type = MenuTypeEnum.PR;
    }
}
