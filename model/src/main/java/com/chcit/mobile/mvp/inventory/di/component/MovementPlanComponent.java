package com.chcit.mobile.mvp.inventory.di.component;

import dagger.BindsInstance;
import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.chcit.mobile.mvp.inventory.di.module.MovementPlanModule;
import com.chcit.mobile.mvp.inventory.contract.MovementPlanContract;

import com.jess.arms.di.scope.FragmentScope;
import com.chcit.mobile.mvp.inventory.fragment.move.MovementPlanFragment;


/**
 * ================================================
 * Description:
 * <p>
 * Created by MVPArmsTemplate on 09/27/2019 08:45
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms">Star me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms/wiki">See me</a>
 * <a href="https://github.com/JessYanCoding/MVPArmsTemplate">模版请保持更新</a>
 * ================================================
 */
@FragmentScope
@Component(modules = MovementPlanModule.class, dependencies = AppComponent.class)
public interface MovementPlanComponent {
    void inject(MovementPlanFragment fragment);

    @Component.Builder
    interface Builder {
        @BindsInstance
        MovementPlanComponent.Builder view(MovementPlanContract.View view);

        MovementPlanComponent.Builder appComponent(AppComponent appComponent);

        MovementPlanComponent build();



    }
}