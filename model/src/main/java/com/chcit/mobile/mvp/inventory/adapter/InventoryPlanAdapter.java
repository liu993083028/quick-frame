package com.chcit.mobile.mvp.inventory.adapter;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chcit.mobile.R;
import com.chcit.mobile.mvp.entity.StorageBean;

import java.util.List;

public class InventoryPlanAdapter extends BaseQuickAdapter<StorageBean,BaseViewHolder> {

    public InventoryPlanAdapter(@Nullable List<StorageBean> data) {
        super(R.layout.item_inventory_plan_data,data);
    }

    @Override
    protected void convert(BaseViewHolder helper, StorageBean item) {
        helper.setText(R.id.tv_inventory_pname,item.getProductName());
        helper.setText(R.id.tv_inventory_Lot,item.getLot());
        helper.setText(R.id.tv_inventory_GuaranteeDate,item.getGuaranteeDate());
        helper.setText(R.id.tv_inventory_QtyCount,item.getQtyCount()+"");
        helper.setText(R.id.tv_inventory_StorageStatusName,item.getStorageStatusName());
        helper.setText(R.id.tv_inventory_QtyOnHand,item.getQtyBook()+"");
        helper.setText(R.id.tv_inventory_productSpec,item.getProductSpec());
        helper.setText(R.id.tv_inventory_locator,item.getLocatorValue());
        helper.setText(R.id.tv_inventory_VendorName,item.getOrgName());
    }
}
