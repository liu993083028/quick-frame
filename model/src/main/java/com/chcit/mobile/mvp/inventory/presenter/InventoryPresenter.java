package com.chcit.mobile.mvp.inventory.presenter;

import android.app.Application;

import com.alibaba.fastjson.JSONObject;
import com.chcit.mobile.common.HttpMethodContains;
import com.chcit.mobile.common.SPDErrorHandle;
import com.chcit.mobile.mvp.common.contract.CommonContract;
import com.chcit.mobile.mvp.entity.BpartnerBean;
import com.chcit.mobile.mvp.entity.InventoryBean;
import com.chcit.mobile.mvp.entity.Statu;
import com.chcit.mobile.mvp.inventory.adapter.InventoryAdapter;
import com.jess.arms.integration.AppManager;
import com.jess.arms.di.scope.FragmentScope;
import com.jess.arms.mvp.BasePresenter;
import com.jess.arms.http.imageloader.ImageLoader;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import me.jessyan.rxerrorhandler.core.RxErrorHandler;

import javax.inject.Inject;

import com.chcit.mobile.mvp.inventory.contract.InventoryContract;
import com.jess.arms.utils.RxLifecycleUtils;


import java.util.List;


@FragmentScope
public class InventoryPresenter extends BasePresenter<InventoryContract.Model, InventoryContract.View> {
    @Inject
    RxErrorHandler mErrorHandler;
    @Inject
    Application mApplication;
    @Inject
    ImageLoader mImageLoader;
    @Inject
    AppManager mAppManager;
    @Inject
    InventoryAdapter mAdapter;
    @Inject
    CommonContract.Model commModel;
    private boolean isLoading;
    private int mStart;
    private int mCount = 30;

    private boolean update = false;
    @Inject
    public InventoryPresenter(InventoryContract.Model model, InventoryContract.View rootView) {
        super(model, rootView);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mErrorHandler = null;
        this.mAppManager = null;
        this.mImageLoader = null;
        this.mApplication = null;
    }

    public void getData(JSONObject json, Consumer<List<InventoryBean>> onNext){
        if (!isLoading) {
            isLoading = true;
            json.put("start", mStart);
            json.put("limit", mCount);
            commModel.requestOnString(HttpMethodContains.MOVE_STORAGE_QUERY,json)
                    .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                    .observeOn(AndroidSchedulers.mainThread())
                    .doFinally(()->mRootView.hideLoading())
                    .subscribe(new SPDErrorHandle<JSONObject>(mErrorHandler){

                        @Override
                        public void onNext(JSONObject response) {
                            isLoading = false;
                            try {
                                int total = response.getInteger("total");
                                mRootView.showDataTotal(String.valueOf(total));
                                if(total == 0){
                                    mRootView.showMessage("没有查询到库存数据！");
                                }else if(total>mStart){
                                    mStart += mCount;
                                    onNext.accept(response.getJSONArray("rows").toJavaList(InventoryBean.class));
                                }else{

                                    mAdapter.setEnableLoadMore(false);
                                }

                            } catch (Exception e) {
                                onError(e);
                            }
                        }

                        @Override
                        public void onError(Throwable t) {
                            isLoading = false;
                            super.onError(t);
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        }

    }
    public void getBpartners( Consumer<List<BpartnerBean>> consumer){
        commModel.getBparenerBeans()
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SPDErrorHandle<List<BpartnerBean>>(mErrorHandler){
                    @Override
                    public void onNext(List<BpartnerBean> bpartnerBeans) {
                        try {
                            consumer.accept(bpartnerBeans);
                        } catch (Exception e) {
                            onError(e);
                        }
                    }
                });
    }

    public void getStatus( Consumer<List<Statu>> consumer){
        commModel.refList(1000346,update)
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SPDErrorHandle<List<Statu>>(mErrorHandler){

                    @Override
                    public void onNext(List<Statu> status) {
                        try {
                            consumer.accept(status);
                        } catch (Exception e) {
                            onError(e);
                        }
                    }
                });
    }

    public void setmStart(int mStart) {
        this.mStart = mStart;
    }
}
