package com.chcit.mobile.mvp.entity;

public class Reason {

    /**
     * value : 02
     * name : 破损
     */

    private String value;
    private String name;
    public Reason(){

    }
    public Reason(String name){
        this.name = name;
    }
    public Reason(String name, String value){
        this.name = name;
        this.value = value;
    }
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

}
