package com.chcit.mobile.mvp.entity;

import java.util.Objects;

public class Statu {

    /**
     * bpartnerName : 南京医药药业有限公司
     * bpartnerId : 1004822
     */

    private String id;
    private String name;

    public Statu() {
    }

    public Statu(String s1, String s2) {
        this.id = s1;
        this.name = s2;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Statu statu = (Statu) o;
        return id.equals(statu.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return name ;
    }
}
