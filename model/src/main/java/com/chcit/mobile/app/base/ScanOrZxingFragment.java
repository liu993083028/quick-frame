package com.chcit.mobile.app.base;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.chcit.custom.view.toolbar.CommonToolbar;
import com.chcit.custom.view.toolbar.TextViewOptions;
import com.chcit.mobile.R;
import com.chcit.mobile.helper.HttpClientHelper;
import com.jess.arms.base.BaseFragment;
import com.jess.arms.mvp.IPresenter;
import com.yzq.zxinglibrary.android.CaptureActivity;
import com.yzq.zxinglibrary.bean.ZxingConfig;
import com.yzq.zxinglibrary.common.Constant;

public abstract class ScanOrZxingFragment<P extends IPresenter>  extends BaseSupoortFragment<P> {
    private static final int REQUEST_CODE_SCAN = 222;
    protected Intent captureActivityIntent = null;
    public static final String CODE_DATA = "SCAN_DATA";
    private static final String INTENT_ACTION_SCAN_RESULT = "com.honeywell.spdbroadcast";
    protected LocalBroadcastManager broadcastManager;
    protected IntentFilter intentFilter;
    protected BroadcastReceiver mReceiver;
    private boolean onBroadFlag = true;//广播开关
    protected boolean isShown = true;
    /**
     * 网络异常View
     */
    protected View errorView;
    protected TextView tvShowMsg;
    protected TextView tvTitle;


    protected abstract void onBarcodeEventSuccess(String barcodeData);

    protected void initBarcod() {
        broadcastManager = LocalBroadcastManager.getInstance(_mActivity);
        intentFilter = new IntentFilter();
        intentFilter.addAction(INTENT_ACTION_SCAN_RESULT);
        mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (INTENT_ACTION_SCAN_RESULT.equals(intent.getAction()) && onBroadFlag) {
                    //获取扫描数据，并将扫描数据存放在barcodeData中
                    if (isVisibleOnScreen()) {
                        final String barcodeData = intent.getStringExtra(CODE_DATA);
                        onBarcodeEventSuccess(barcodeData);
                    }
                }
            }
        };
        broadcastManager.registerReceiver(mReceiver, intentFilter);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        initBarcod();
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onEnterAnimationEnd(Bundle savedInstanceState) {
        super.onEnterAnimationEnd(savedInstanceState);
        initEmityView();
    }


    public View getEmityViewParent() {
        return null;
    }


    private void initEmityView() {

        if (getEmityViewParent() != null) {
            errorView = getEmityViewParent().inflate(getContext(), R.layout.view_network_error, null);
            tvShowMsg = errorView.findViewById(R.id.tv_show_msg);
            tvTitle = errorView.findViewById(R.id.tv_show_title);
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        onBroadFlag = false;
    }

    @Override
    public void onResume() {
        super.onResume();
        onBroadFlag = true;
        isShown = true;


    }

    @Override
    public void onStop() {
        super.onStop();
        isShown = false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (broadcastManager != null) {
            broadcastManager.unregisterReceiver(mReceiver);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isShown = isVisibleToUser;

    }

    // 判断可见性，对手动显示与PagerAdapter方式均有效，且跟随父fragment可见性状态
    public boolean isVisibleOnScreen() {
        if (isShown && getUserVisibleHint() && isVisible()) {
            if (getParentFragment() == null) {
                return true;
            }
            if (getParentFragment() instanceof BaseFragment) {
                return ((ScanFragment) getParentFragment()).isVisibleOnScreen();
            } else {
                return getParentFragment().isVisible();
            }
        }
        return false;
    }

    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        isShown = !hidden;
    }




    protected void initZxing(){

        /*ZxingConfig是配置类
         *可以设置是否显示底部布局，闪光灯，相册，
         * 是否播放提示音  震动
         * 设置扫描框颜色等
         * 也可以不传这个参数
         * */
        if(captureActivityIntent==null){
            captureActivityIntent = new Intent(mContext, CaptureActivity.class);
            ZxingConfig config = new ZxingConfig();
            config.setPlayBeep(true);//是否播放扫描声音 默认为true
            config.setShake(true);//是否震动  默认为true
            config.setDecodeBarCode(true);//是否扫描条形码 默认为true
            config.setReactColor(R.color.colorAccent);//设置扫描框四个角的颜色 默认为白色
            config.setFrameLineColor(R.color.colorAccent);//设置扫描框边框颜色 默认无色
            config.setScanLineColor(R.color.colorAccent);//设置扫描线的颜色 默认白色
            config.setFullScreenScan(false);//是否全屏扫描  默认为true  设为false则只会在扫描框中扫描
            captureActivityIntent.putExtra(Constant.INTENT_ZXING_CONFIG, config);
        }
        startActivityForResult(captureActivityIntent, REQUEST_CODE_SCAN);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // 扫描二维码/条码回传
        if (requestCode == REQUEST_CODE_SCAN && resultCode == RESULT_OK) {
            if (data != null) {
                String content = data.getStringExtra(Constant.CODED_CONTENT);
                onBarcodeEventSuccess(content);
            }
        }
    }
}
