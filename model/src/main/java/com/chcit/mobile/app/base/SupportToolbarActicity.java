package com.chcit.mobile.app.base;

import android.util.TypedValue;

import androidx.fragment.app.FragmentManager;

import com.chcit.custom.view.toolbar.CommonToolbar;
import com.chcit.mobile.R;
import com.jess.arms.mvp.IPresenter;

import butterknife.BindView;

public abstract class SupportToolbarActicity<P extends IPresenter> extends BaseSupportActivity<P> {

    @BindView(R.id.common_toolbar)
    protected CommonToolbar mCommonToolbar ;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.mCommonToolbar = null;
    }



    /**
     * 是否使用 EventBus
     * Arms 核心库现在并不会依赖某个 EventBus, 要想使用 EventBus, 还请在项目中自行依赖对应的 EventBus
     * 现在支持两种 EventBus, greenrobot 的 EventBus 和畅销书 《Android源码设计模式解析与实战》的作者 何红辉 所作的 AndroidEventBus
     * 确保依赖后, 将此方法返回 true, Arms 会自动检测您依赖的 EventBus, 并自动注册
     * 这种做法可以让使用者有自行选择三方库的权利, 并且还可以减轻 Arms 的体积
     *
     * @return 返回 {@code true} (默认为使用 {@code true}), Arms 会自动注册 EventBus
     */
    @Override
    public boolean useEventBus() {
        return true;
    }

    /**
     * 这个Activity是否会使用Fragment,框架会根据这个属性判断是否注册{@link FragmentManager.FragmentLifecycleCallbacks}
     * 如果返回false,那意味着这个Activity不需要绑定Fragment,那你再在这个Activity中绑定继承于 {@link com.jess.arms.base.BaseFragment} 的Fragment将不起任何作用
     *
     * @return
     */
    @Override
    public boolean useFragment() {
        return true;
    }


    /**
     * 1 - 最左边的view
     * 2 - 左边的第二个view
     * 依次类推
     * @return
     */
    @Override
    protected  void initToolBar(){
        TypedValue typedValue = new TypedValue();
        getTheme().resolveAttribute(R.attr.colorPrimaryDark, typedValue, true);
        mCommonToolbar.setBackgroundColor(typedValue.data);
        mCommonToolbar.setTitleText(getTitle());

    };



}
