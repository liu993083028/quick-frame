package com.chcit.mobile.app.base;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chcit.custom.view.toolbar.CommonToolbar;
import com.chcit.mobile.R;
import com.chcit.mobile.common.DataKeys;
import com.chcit.mobile.mvp.entity.HomeItem;
import com.jess.arms.mvp.IPresenter;

import org.apache.commons.lang3.StringUtils;


public abstract class SupportToolbarFragement<P extends IPresenter> extends BaseSupoortFragment<P> {


    protected CommonToolbar mCommonToolbar ;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        mCommonToolbar = view.findViewById(R.id.common_toolbar);
        if(mCommonToolbar!=null){
            initToolBar();
        }
        return view;
    }

    public  SupportToolbarActicity getProActivity() {
        return (SupportToolbarActicity)SupportToolbarFragement.this.getActivity();
    }



    protected  void initToolBar(){
        TypedValue typedValue = new TypedValue();
        getActivity().getTheme().resolveAttribute(R.attr.colorPrimary, typedValue, true);
        mCommonToolbar.setBackgroundColor(typedValue.data);
        HomeItem item = _mActivity.getIntent().getParcelableExtra(DataKeys.HOME_ITEM.name());
        if(item != null && StringUtils.isNotBlank(item.getTitle())){
            mCommonToolbar.setTitleText(item.getTitle());
        }

    };



}
