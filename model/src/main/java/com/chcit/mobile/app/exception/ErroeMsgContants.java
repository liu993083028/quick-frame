package com.chcit.mobile.app.exception;
/**
 * 错误信息定义类
 * @author Administrator
 *
 */
public interface ErroeMsgContants {
	
	public static String CONNECT_FAIL = "连接失败！";
	public static String CONNECT_SERVER_FAIL = "服务器异常";
	public static String CONNECT_SUCCESS = "连接成功！";
	public static String LOGIN_FAIL = "登录失败！";
	public static String LOGIN_NULL = "用户名或者密码为空！";
	public static String CONNECT_OVER_TIME = "网络异常";
	
	

}
