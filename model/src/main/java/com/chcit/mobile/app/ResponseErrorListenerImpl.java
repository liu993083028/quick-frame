/*
 * Copyright 2017 JessYan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.chcit.mobile.app;

import android.app.Activity;
import android.content.Context;
import android.net.ParseException;
import android.view.View;

import com.chcit.custom.view.util.DialogUtil;
import com.chcit.mobile.R;
import com.chcit.mobile.app.exception.BaseException;
import com.chcit.mobile.app.exception.CodeMsg;
import com.chcit.mobile.app.utils.ToastUtils;
import com.elvishew.xlog.XLog;
import com.google.gson.JsonIOException;
import com.google.gson.JsonParseException;
import com.jess.arms.base.Platform;
import com.jess.arms.integration.AppManager;
import com.jess.arms.utils.ArmsUtils;
import com.xuexiang.xui.widget.popupwindow.bar.CookieBar;
import com.xuexiang.xui.widget.toast.XToast;

import org.json.JSONException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.exceptions.CompositeException;
import me.jessyan.rxerrorhandler.handler.listener.ResponseErrorListener;
import retrofit2.HttpException;


/**
 * ================================================
 * 展示 {@link ResponseErrorListener} 的用法
 * <p>
 * Created by JessYan on 04/09/2017 17:18
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * ================================================
 */
public class ResponseErrorListenerImpl implements ResponseErrorListener {

    @Override
    public void handleResponseError(Context context, Throwable t) {
        XLog.tag("Catch-Error").w(t.getMessage());
        //这里不光只能打印错误, 还可以根据不同的错误做出不同的逻辑处理
        //这里只是对几个常用错误进行简单的处理, 展示这个类的用法, 在实际开发中请您自行对更多错误进行更严谨的处理
        String msg = "未知错误";
        if (t instanceof UnknownHostException) {
            msg = "网络不可用";
        } else if (t instanceof SocketTimeoutException) {
            msg = "请求网络超时";
        } else if (t instanceof HttpException) {
            HttpException httpException = (HttpException) t;
            msg = convertStatusCode(httpException);
        } else if (t instanceof BaseException) {
            BaseException baseException = (BaseException) t;
            if(baseException.getcodeMsg()!=null){
                msg = baseException.getcodeMsg().getMsg();
                if(baseException.getcodeMsg().getCode()== CodeMsg.NO_DATE.getCode()&&context!=null){
                    XToast.warning(context,msg).show();
                    return;
                }

            }else{
                msg = baseException.getMessage();
            }

            if(msg == null){
                 msg = "未知错误";
            }
        }else if (t instanceof CompositeException){
            CompositeException compositeException = (CompositeException) t;
             t =  compositeException.getExceptions().get(0);
             if(t instanceof ConnectException){
                 msg = "网络连接异常！";
             }else if (t instanceof HttpException){
                 msg = convertStatusCode((HttpException)t);
             }else{
                 msg = t.getMessage();
             }
        }else if (t instanceof JsonParseException || t instanceof ParseException || t instanceof JSONException || t instanceof JsonIOException) {
            msg = "数据解析错误";
        }else if(t.getMessage() != null){
            msg = t.getMessage();
            if(msg.contains("Failed to connect to /")){
                msg = "网络连接异常！";
            }
        }
        final String message = msg;
        Completable.fromAction(()->{
            if (Platform.DEPENDENCY_SUPPORT_DESIGN) {
                Activity activity = AppManager.getAppManager().getCurrentActivity() == null ? AppManager.getAppManager().getTopActivity() : AppManager.getAppManager().getCurrentActivity();
                if(activity != null){
                    CookieBar.builder(activity)
                            .setMessage(message)
                            .setDuration(-1)
                            .setTitleColor(android.R.color.white)
                            .setBackgroundColor(android.R.color.holo_red_light)
//                            .setMessageColor(android.R.color.white)
                            .setActionColor(android.R.color.white)
                            .setActionWithIcon(R.drawable.png_close, view -> {

                            })
                            .show();
                }else{
                    XToast.error(context,message).show();
                }
            } else {
                XToast.error(context,message).show();
            }
        }).subscribeOn(AndroidSchedulers.mainThread()).subscribe();

    }

    private String convertStatusCode(HttpException httpException) {
        String msg;
        if (httpException.code() == 500) {
            msg = "服务器发生错误";
        } else if (httpException.code() == 404) {
            msg = "请求地址不存在";
        } else if (httpException.code() == 403) {
            msg = "请求被服务器拒绝";
        } else if (httpException.code() == 307) {
            msg = "请求被重定向到其他页面";
        } else {
            msg = httpException.message();
        }
        return msg;
    }
}
