package com.chcit.mobile.app.interceptor;

import androidx.annotation.NonNull;
import androidx.annotation.RawRes;

import com.chcit.mobile.R;
import com.chcit.mobile.common.HttpMethodContains;
import com.chcit.mobile.app.utils.FileUtil;

import java.io.IOException;

import me.jessyan.retrofiturlmanager.RetrofitUrlManager;
import okhttp3.MediaType;
import okhttp3.Protocol;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class SPDInteceptor extends BaseInterceptor {

    private final String DEBUG_URL;
    private final int RAW_ID;

    public SPDInteceptor(String debugUrl,@RawRes int rawId) {
        this.DEBUG_URL = debugUrl;
        this.RAW_ID = rawId;

    }

    private Response getResponse(Chain chain, String json) {
        return new Response.Builder()
                .code(200)
                .addHeader("Content-Type", "application/json")
                .body(ResponseBody.create(MediaType.parse("application/json"), json))
                .message("OK")
                .request(chain.request())
                .protocol(Protocol.HTTP_1_1)
                .build();
    }

    private Response debugResponse(Chain chain, @RawRes int rawId) {
        final String json = FileUtil.getRawFile(rawId);
        return getResponse(chain, json);
    }

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
         String url = chain.request().url().toString();
        if (url.contains(DEBUG_URL)) {
            String requestData ="";
          /*  FormBody formBody = (FormBody) Objects.requireNonNull(chain.request().body());
            for(int i=0;i<formBody.size();i++){
                if("method".equals(formBody.name(i))){
                    requestData = formBody.value(i);
                    break;
                }
            }*/
           requestData = chain.request().url().queryParameter("method");
            if(HttpMethodContains.REJECT_REASON.equals(requestData)){
                return debugResponse(chain, R.raw.reason);
            }else if(HttpMethodContains.WAREHOUSE_QUERY.equals(requestData)) {
                return debugResponse(chain, R.raw.warehouse);
            }else if(HttpMethodContains.PURCHASE_RECEVIE_QUERY_DETAIL.equals(requestData)){
                return debugResponse(chain, R.raw.test);
            }else if(HttpMethodContains.SHIPMENT_QUERY_DETAIL.equals(requestData)){
                return debugResponse(chain, R.raw.prshipment);
            }else if(HttpMethodContains.LOCATORS.equals(requestData)) {
                return debugResponse(chain, R.raw.locator);
            }else if(HttpMethodContains.SHIPMENT_QUERY.equals(requestData)) {
                return debugResponse(chain, R.raw.prshipment);
            }else if(HttpMethodContains.MOVE_STORAGE_QUERY.equals(requestData)) {
                return debugResponse(chain, R.raw.storage);
            }else if(HttpMethodContains.Home.MENU_QUERY.equals(requestData)) {
                return debugResponse(chain, R.raw.menu);
            }else if(HttpMethodContains.DISPENSING_QUERY.equals(requestData)) {
                if(chain.request().url().queryParameterValue(1).contains("\"status\":\"N\"")){
                    return debugResponse(chain, R.raw.hospital);
                }
                return debugResponse(chain, R.raw.undispensing);
            }else{
                return debugResponse(chain, R.raw.success);
            }
        }
        url =  chain.request().url().encodedPath();
        String baseUrl = RetrofitUrlManager.getInstance().getBaseUrl().encodedPath();
        switch (url.substring(baseUrl.length())){
            case HttpMethodContains.Receive.ASN_PACKAGE_LIST:
                return debugResponse(chain,R.raw.query_package_detail);
            case HttpMethodContains.CHECK_PACKAGE:
                return debugResponse(chain,R.raw.check_package);
            case HttpMethodContains.Receive.QUERY_IMAGE_ID:
                return debugResponse(chain,R.raw.img_url);
            case HttpMethodContains.Home.Task_URL:
                return debugResponse(chain,R.raw.task);
            case HttpMethodContains.REJECT_PACKAGE:
                return debugResponse(chain,R.raw.reject_success);
            case HttpMethodContains.INSPECTE_PACKAGE:
                return debugResponse(chain,R.raw.success);
            case HttpMethodContains.PICKUP_PACKAGELINE:
                return debugResponse(chain,R.raw.success);
            case HttpMethodContains.REASON_URL:
                return debugResponse(chain,R.raw.reject_reson);
        }
        return chain.proceed(chain.request());
    }
}
