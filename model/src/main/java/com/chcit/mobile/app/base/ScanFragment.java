package com.chcit.mobile.app.base;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chcit.custom.view.toolbar.TextViewOptions;
import com.chcit.mobile.R;
import com.chcit.mobile.helper.HttpClientHelper;
import com.jess.arms.base.BaseFragment;
import com.jess.arms.mvp.IPresenter;

public abstract class ScanFragment<P extends IPresenter> extends SupportToolbarFragement<P> {
    public static final String CODE_DATA = "SCAN_DATA";
    private static final String INTENT_ACTION_SCAN_RESULT = "com.honeywell.spdbroadcast";
    protected LocalBroadcastManager broadcastManager;
    protected IntentFilter intentFilter;
    protected BroadcastReceiver mReceiver;
    private boolean onBroadFlag = true;//广播开关
    protected boolean isShown = true;
    /**
     * 网络异常View
     */
    protected View errorView;
    protected TextView tvShowMsg;
    protected TextView tvTitle;


    protected abstract void onBarcodeEventSuccess(String barcodeData);

    protected void initBarcod() {
        broadcastManager = LocalBroadcastManager.getInstance(_mActivity);
        intentFilter = new IntentFilter();
        intentFilter.addAction(INTENT_ACTION_SCAN_RESULT);
        mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (INTENT_ACTION_SCAN_RESULT.equals(intent.getAction()) && onBroadFlag) {
                    //获取扫描数据，并将扫描数据存放在barcodeData中
                    if (isVisibleOnScreen()) {
                        final String barcodeData = intent.getStringExtra(CODE_DATA);
                        onBarcodeEventSuccess(barcodeData);
                    }
                }
            }
        };
        broadcastManager.registerReceiver(mReceiver, intentFilter);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        initBarcod();
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onEnterAnimationEnd(Bundle savedInstanceState) {
        super.onEnterAnimationEnd(savedInstanceState);
        initEmityView();
    }


    public View getEmityViewParent() {
        return null;
    }

    private void initEmityView() {

        if (getEmityViewParent() != null) {
            errorView = getEmityViewParent().inflate(getContext(), R.layout.view_network_error, null);
            tvShowMsg = errorView.findViewById(R.id.tv_show_msg);
            tvTitle = errorView.findViewById(R.id.tv_show_title);
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        onBroadFlag = false;
    }

    @Override
    public void onResume() {
        super.onResume();
        onBroadFlag = true;
        isShown = true;


    }

    @Override
    public void onStop() {
        super.onStop();
        isShown = false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (broadcastManager != null) {
            broadcastManager.unregisterReceiver(mReceiver);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isShown = isVisibleToUser;

    }

    // 判断可见性，对手动显示与PagerAdapter方式均有效，且跟随父fragment可见性状态
    public boolean isVisibleOnScreen() {
        if (isShown && getUserVisibleHint() && isVisible()) {
            if (getParentFragment() == null) {
                return true;
            }
            if (getParentFragment() instanceof BaseFragment) {
                return ((ScanFragment) getParentFragment()).isVisibleOnScreen();
            } else {
                return getParentFragment().isVisible();
            }
        }
        return false;
    }

    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        isShown = !hidden;
    }

    @Override
    protected void initToolBar() {
        super.initToolBar();
        mCommonToolbar.addRightMenuText(TextViewOptions.Builder()
                .setText(HttpClientHelper.getSelectWareHouse().getName())
                .setLines(2)
                .setListener(v -> {
                    HttpClientHelper.showWareSelect(mContext, (dialog, itemView, which, text) -> {
                        ((TextView) v).setText(text);
                        return false;
                    });
                })
                .build());
    }
}
