package com.chcit.mobile.app.exception;

public class BaseException extends Exception {

	private static final long serialVersionUID = 1L;
	

	private CodeMsg codeMsg;
	
	public BaseException(CodeMsg cm) {
		super(cm.toString());
		this.codeMsg = cm;
	}
	public BaseException(String msg) {
		super(msg);
		if("没有查询到数据！".equals(msg)){
			this.codeMsg = CodeMsg.NO_DATE;
		}
	}
	public CodeMsg getcodeMsg() {
		return codeMsg;
	}
}
