package com.chcit.mobile.app.exception;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.xuexiang.xui.widget.dialog.materialdialog.MaterialDialog;
import com.chcit.mobile.app.utils.FileUtil;
import com.chcit.mobile.mvp.LoginActivity;
import com.jess.arms.utils.ArmsUtils;


public class ErrorShowActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT));
        setContentView(linearLayout);
        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title("温馨提示")
                .content("由于发生了一个未知错误，应用出现异常，我们对此引起的不便表示抱歉！错误信息在手机内存对应的"+FileUtil.LOG_DIR +"目录中，请发送给我们帮助我们快速解决问题，谢谢！")
                .positiveText("退出")
                .negativeText("重启")
                .onPositive((dialog1, which) -> ArmsUtils.exitApp()
                ).onNegative( (dialog1, which) -> {

                    android.os.Process.killProcess(android.os.Process.myPid());
                    Intent intent = new Intent(this, LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }).build();
    /*    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Objects.requireNonNull(dialog.getWindow()).setType(WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY);
        } else {
            //Android6.0以下，不用动态声明权限
            Objects.requireNonNull(dialog.getWindow()).setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        }*/
        dialog.setCanceledOnTouchOutside(false);// 设置点击屏幕其他地方，dialog不消失
        dialog.setCancelable(false);// 设置点击返回键和HOme键，dialog不消失
        dialog.show();
    }
}
