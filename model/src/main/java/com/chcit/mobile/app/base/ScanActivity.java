package com.chcit.mobile.app.base;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import com.chcit.mobile.app.utils.BarcodeUtil;
import com.jess.arms.mvp.IPresenter;



public abstract class ScanActivity<P extends IPresenter> extends SupportToolbarActicity<P>{

    private static final String INTENT_ACTION_SCAN_RESULT = "com.honeywell.spdbroadcast";
    private static String BAR_READ_ACTION = "SYSTEM_BAR_READ";
    private static final String ACTION_CEUISE1_HC = "com.android.server.scannerservice.broadcast";
    private static String CLEAR = "SYSTEM_BARrfid_READ_CLEAR";
    private LocalBroadcastManager broadcastManager;
    private BroadcastReceiver systemButtonReciever;
    private String action;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String model=android.os.Build.MODEL;
        if(model.contains("ScanPal")){
            BarcodeUtil.getInstance().start();
        }else if(model.contains("BN-HH-G02")){
            this.systemButtonReciever = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    action = intent.getAction();
                    if (action.equals(BAR_READ_ACTION))
                    {
                        sendBarcode(intent.getStringExtra("BAR_VALUE"));
                    }
                }
            };
            IntentFilter localIntentFilter = new IntentFilter();
            localIntentFilter.addAction(BAR_READ_ACTION);
            //localIntentFilter.addAction(RFID_READ_ACTION);
            localIntentFilter.addAction(CLEAR);
            registerReceiver(this.systemButtonReciever, localIntentFilter);
        }else if(model.contains("CRUISE")){
            this.systemButtonReciever = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    action = intent.getAction();
                    if (action.equals(ACTION_CEUISE1_HC))
                    {

                        sendBarcode(intent.getStringExtra("scannerdata"));
                    }
                }
            };
            IntentFilter localIntentFilter = new IntentFilter();
            //localIntentFilter.addAction(BAR_READ_ACTION);
            localIntentFilter.setPriority(Integer.MAX_VALUE);
            //localIntentFilter.addAction(RFID_READ_ACTION);
            localIntentFilter.addAction(ACTION_CEUISE1_HC);
            registerReceiver(this.systemButtonReciever, localIntentFilter);
        }

        initBroadCastManange();
    }

    private void initBroadCastManange() {
        broadcastManager = LocalBroadcastManager.getInstance(this);

    }
    private void sendBarcode(String barcodeString) {
        Intent intent = new Intent(INTENT_ACTION_SCAN_RESULT);
        intent.putExtra(ScanFragment.CODE_DATA, barcodeString);
        broadcastManager.sendBroadcast(intent);
    }


    @Override
    protected void onResume() {
        BarcodeUtil.getInstance().start();
        super.onResume();

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        BarcodeUtil.getInstance().stop();
        broadcastManager = null;
        destroyAidcBroad();

    }
    public void destroyAidcBroad() {

        if(systemButtonReciever!=null){
            unregisterReceiver(this.systemButtonReciever);
        }

    }

    @Override
    protected void onPause() {
        BarcodeUtil.getInstance().stop();
        super.onPause();
    }

}
