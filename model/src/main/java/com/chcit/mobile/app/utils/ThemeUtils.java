package com.chcit.mobile.app.utils;

import android.content.Context;
import android.content.res.TypedArray;

import com.chcit.custom.view.widget.theme.Theme;
import com.chcit.mobile.R;
import com.jess.arms.base.app.Quick;
import com.jess.arms.utils.ArmsUtils;

/**
 * Created by dongjunkun on 2016/2/6.
 */
public class ThemeUtils {
    public static int getThemeColor2Array(Context context, int attrRes) {
        TypedArray typedArray = context.obtainStyledAttributes(new int[]{attrRes});
        int color = typedArray.getColor(0, 0xffffff);
        typedArray.recycle();
        return color;
    }

    /**
     * 获取主题颜色（color）
     *
     * @return
     */
    public static int getThemeColor() {
        Theme theme = SharedPreUtils.getInstance().getCurrentTheme();
        switch (theme) {
            case Blue:
                return ArmsUtils.getColor(Quick.getApplicationContext(), R.color.colorBluePrimary);
            case Red:
                return ArmsUtils.getColor(Quick.getApplicationContext(), R.color.colorRedPrimary);
            case Brown:
                return ArmsUtils.getColor(Quick.getApplicationContext(), R.color.colorBrownPrimary);
            case Green:
                return ArmsUtils.getColor(Quick.getApplicationContext(), R.color.colorGreenPrimary);
            case Purple:
                return ArmsUtils.getColor(Quick.getApplicationContext(), R.color.colorPurplePrimary);
            case Teal:
                return ArmsUtils.getColor(Quick.getApplicationContext(), R.color.colorTealPrimary);
            case Pink:
                return ArmsUtils.getColor(Quick.getApplicationContext(), R.color.colorPinkPrimary);
            case DeepPurple:
                return ArmsUtils.getColor(Quick.getApplicationContext(), R.color.colorDeepPurplePrimary);
            case Orange:
                return ArmsUtils.getColor(Quick.getApplicationContext(), R.color.colorOrangePrimary);
            case Indigo:
                return ArmsUtils.getColor(Quick.getApplicationContext(), R.color.colorIndigoPrimary);
            case LightGreen:
                return ArmsUtils.getColor(Quick.getApplicationContext(), R.color.colorLightGreenPrimary);
            case Lime:
                return ArmsUtils.getColor(Quick.getApplicationContext(), R.color.colorLimePrimary);
            case DeepOrange:
                return ArmsUtils.getColor(Quick.getApplicationContext(), R.color.colorDeepOrangePrimary);
            case Cyan:
                return ArmsUtils.getColor(Quick.getApplicationContext(), R.color.colorCyanPrimary);
            case BlueGrey:
                return ArmsUtils.getColor(Quick.getApplicationContext(), R.color.colorBlueGreyPrimary);

        }
        return ArmsUtils.getColor(Quick.getApplicationContext(), R.color.colorCyanPrimary);
    }

    /**
     * 获取主题颜色（color）
     *
     * @return
     */
    public static int getThemeColorId() {
        Theme theme = SharedPreUtils.getInstance().getCurrentTheme();
        switch (theme) {
            case Blue:
                return R.color.colorBluePrimary;
            case Red:
                return R.color.colorRedPrimary;
            case Brown:
                return R.color.colorBrownPrimary;
            case Green:
                return R.color.colorGreenPrimary;
            case Purple:
                return R.color.colorPurplePrimary;
            case Teal:
                return R.color.colorTealPrimary;
            case Pink:
                return R.color.colorPinkPrimary;
            case DeepPurple:
                return R.color.colorDeepPurplePrimary;
            case Orange:
                return R.color.colorOrangePrimary;
            case Indigo:
                return R.color.colorIndigoPrimary;
            case LightGreen:
                return R.color.colorLightGreenPrimary;
            case Lime:
                return R.color.colorLimePrimary;
            case DeepOrange:
                return R.color.colorDeepOrangePrimary;
            case Cyan:
                return R.color.colorCyanPrimary;
            case BlueGrey:
                return R.color.colorBlueGreyPrimary;

        }
        return R.color.colorCyanPrimary;
    }
}
