package com.chcit.mobile.app.base;

import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.chcit.custom.view.toolbar.CommonToolbar;
import com.chcit.custom.view.toolbar.TextViewOptions;
import com.chcit.mobile.R;
import com.chcit.mobile.common.DataKeys;
import com.chcit.mobile.helper.HttpClientHelper;
import com.chcit.mobile.mvp.entity.HomeItem;
import com.jess.arms.mvp.IPresenter;

import org.apache.commons.lang3.StringUtils;

public abstract class ScanToolFragment<P extends IPresenter> extends ScanOrZxingFragment<P>{

    protected CommonToolbar mCommonToolbar ;
    protected void initToolBar(View rootView) {

        mCommonToolbar = rootView.findViewById(R.id.common_toolbar);
        TypedValue typedValue = new TypedValue();
        getActivity().getTheme().resolveAttribute(R.attr.colorPrimary, typedValue, true);
        mCommonToolbar.setBackgroundColor(typedValue.data);
        HomeItem item = _mActivity.getIntent().getParcelableExtra(DataKeys.HOME_ITEM.name());
        if(item != null && StringUtils.isNotBlank(item.getTitle())){
            mCommonToolbar.setTitleText(item.getTitle());
        }
        mCommonToolbar.addRightMenuText(TextViewOptions.Builder()
                .setText(HttpClientHelper.getSelectWareHouse().getName())
                .setLines(2)
                .setListener(v -> {
                    HttpClientHelper.showWareSelect(mContext, (dialog, itemView, which, text) -> {
                        ((TextView) v).setText(text);
                        return false;
                    });
                })
                .build());

    }

    @Override
    public View initView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view =inflater.inflate(R.layout.layout_scan_tool,container,false);
        initToolBar(view);
        LinearLayout linearLayout = view.findViewById(R.id.ll_root);
        return inflater.inflate(getLayoutId(), linearLayout, true);
    }

    protected abstract int getLayoutId();
}
