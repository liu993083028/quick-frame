package com.chcit.mobile.app.utils;

import android.content.Context;
import android.content.Intent;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.chcit.mobile.app.base.ScanFragment;
import com.elvishew.xlog.XLog;
import com.honeywell.aidc.AidcManager;
import com.honeywell.aidc.BarcodeFailureEvent;
import com.honeywell.aidc.BarcodeReadEvent;
import com.honeywell.aidc.BarcodeReader;
import com.honeywell.aidc.TriggerStateChangeEvent;
import com.jess.arms.base.app.Quick;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class BarcodeUtil implements BarcodeReader.BarcodeListener, BarcodeReader.TriggerListener{
    private static final BarcodeUtil ourInstance = new BarcodeUtil();
    protected AidcManager manager;
    private boolean isInit;
    protected BarcodeReader barcodeReader;
    public static BarcodeUtil getInstance() {
        return ourInstance;
    }
    private Disposable disposable;
    private BarcodeUtil() {
        initAidcBroad(Quick.getApplicationContext());
    }

    public void destroyAidBroad(){
        if (barcodeReader != null) {
            barcodeReader.removeTriggerListener(this);
            barcodeReader.removeBarcodeListener(this);
            barcodeReader.close();
            manager.close();
        }
        if(disposable != null){
            disposable.dispose();
        }
    }
    private void initAidcBroad(Context context) {
        Observable.just(1)
                .observeOn(Schedulers.newThread())
                .subscribe(new Observer<Integer>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;
                    }
                    @Override
                    public void onNext(Integer integer) {
                        AidcManager.create(context, new AidcManager.CreatedCallback() {
                            @Override
                            public void onCreated(AidcManager aidcManager) {
                                //创建AidcManager和BarcodeReader对象
                                manager = aidcManager;
                                barcodeReader = manager.createBarcodeReader();
                                //设置扫描属性，打开扫描功能
                                try {
                                    barcodeReader.setProperty(BarcodeReader.PROPERTY_CODE_128_ENABLED, true);
                                    barcodeReader.setProperty(BarcodeReader.PROPERTY_QR_CODE_ENABLED, true);
                                    barcodeReader.setProperty(BarcodeReader.PROPERTY_EAN_13_ENABLED, true);	//关闭EAN13码制
                                    barcodeReader.setProperty(BarcodeReader.PROPERTY_DATA_PROCESSOR_LAUNCH_BROWSER,false );
                                    barcodeReader.setProperty(BarcodeReader.PROPERTY_TRIGGER_CONTROL_MODE,
                                            BarcodeReader.TRIGGER_CONTROL_MODE_CLIENT_CONTROL);					//设置手动触发模式

                                    //注册Trigger监听器和Barcode监听器
                                    barcodeReader.addTriggerListener( BarcodeUtil.this);
                                    barcodeReader.addBarcodeListener(BarcodeUtil.this);
                                    barcodeReader.claim();
                                } catch (Exception e) {
                                    XLog.e("修改属性失败");
                                }

                            }
                        });
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                        isInit = true;
                    }
                });

    }

    private void sendBarcode(String barcodeString) {
        Intent intent = new Intent("com.honeywell.spdbroadcast");
        intent.putExtra(ScanFragment.CODE_DATA, barcodeString);
        LocalBroadcastManager.getInstance(Quick.getApplicationContext()).sendBroadcast(intent);
    }

    @Override
    public void onBarcodeEvent(BarcodeReadEvent barcodeReadEvent) {
        String barcodeString = barcodeReadEvent.getBarcodeData();
        sendBarcode(barcodeString);
    }

    @Override
    public void onFailureEvent(BarcodeFailureEvent barcodeFailureEvent) {

    }

    @Override
    public void onTriggerEvent(TriggerStateChangeEvent triggerStateChangeEvent) {
        if(triggerStateChangeEvent.getState()){
            try {
                barcodeReader.light(true);		//开启补光
                barcodeReader.aim(true);		//开启瞄准线
                barcodeReader.decode(true);		//开启解码
            }
            catch (Exception e){
                XLog.e("开启扫描失败");
            }
        }else{
            try{
                barcodeReader.light(false);		//关闭补光
                barcodeReader.aim(false);		//关闭瞄准线
                barcodeReader.decode(false);	//关闭解码
            }
            catch(Exception e){
                XLog.e("关闭扫描失败");
            }
        }
    }
    public void stop() {
        if (barcodeReader != null) {
            barcodeReader.release();
        }
    }

    public void start() {
        try {

            if (barcodeReader != null) {
                barcodeReader.claim();
            }
        } catch (Exception e) {
            XLog.e(e);
        }
    }
}
