package com.chcit.mobile.app.cookie;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.HttpUrl;

public class MyCookiesManager implements CookieJar {
    //缓存在内存中
    private final HashMap<String, List<Cookie>> cookieStore = new HashMap<>();
    private final String JESSIONID ="LOGIN";

    @Override
    public void saveFromResponse(HttpUrl url, List<Cookie> cookies) {
         cookieStore.put(JESSIONID, cookies); //将url地址上的cookies保存到内存中
    }

    @Override
    public List<Cookie> loadForRequest(HttpUrl url) {
        if(url.uri().toString().contains("login.do")){
            return  new ArrayList<Cookie>();
        }else{
            List<Cookie> cookies = cookieStore.get(JESSIONID); //根据url获取其地址上的所有cookies
            return cookies != null ? cookies : new ArrayList<Cookie>();
        }
    }
}