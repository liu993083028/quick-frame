package com.chcit.mobile.app.interceptor;

import androidx.annotation.NonNull;
import androidx.annotation.RawRes;

import com.chcit.mobile.R;
import com.chcit.mobile.app.utils.FileUtil;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.Protocol;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class LoginInteceptor extends BaseInterceptor {

    private final String DEBUG_URL;


    public LoginInteceptor(String debugUrl) {
        this.DEBUG_URL = debugUrl;

    }

    private Response getResponse(Chain chain, String json) {
        return new Response.Builder()
                .code(200)
                .addHeader("Content-Type", "application/json")
                .body(ResponseBody.create(MediaType.parse("application/json"), json))
                .message("OK")
                .request(chain.request())
                .protocol(Protocol.HTTP_1_1)
                .build();
    }

    private Response debugResponse(Chain chain, @RawRes int rawId) {
        final String json = FileUtil.getRawFile(rawId);
        return getResponse(chain, json);
    }

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        final String url = chain.request().url().toString();
       // String requestData = ((FormBody) Objects.requireNonNull(chain.request().body())).value(0);//.value(0);//JSONObject.parseObject(chain.request().body().toString());
        if (url.contains(DEBUG_URL)) {
                return debugResponse(chain, R.raw.success);

        }
        return chain.proceed(chain.request());
    }
}
