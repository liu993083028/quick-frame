package com.chcit.mobile.app.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.chcit.custom.view.widget.theme.Theme;
import com.chcit.mobile.BuildConfig;
import com.jess.arms.base.app.ConfigKeys;
import com.jess.arms.base.app.Quick;

/**
 * Created by newbiechen on 17-4-16.
 */

public class SharedPreUtils {
    private static final String SHARED_NAME = "SPD_YP";
    private static SharedPreUtils sInstance;
    private static SharedPreferences sharedReadable;
    private static SharedPreferences.Editor sharedWritable;
    private final String IS_USE_CUSTOM_THEME_KEY = "is_use_custom_theme_key";
    private SharedPreUtils() {
        sharedReadable = Quick.getApplicationContext()
                .getSharedPreferences(SHARED_NAME, Context.MODE_MULTI_PROCESS);
        sharedWritable = sharedReadable.edit();
    }

    public static SharedPreUtils getInstance() {
        if (sInstance == null) {
            synchronized (SharedPreUtils.class) {
                if (sInstance == null) {
                    sInstance = new SharedPreUtils();
                }
            }
        }
        return sInstance;
    }

    /**
     * 清除本地数据
     */
    public void sharedPreClear() {
        sharedWritable.clear().apply();
    }

    /**
     * 清除本地数据指定key
     */
    public void sharedPreRemove(String key) {
        sharedWritable.remove(key).apply();
    }

    public String getString(String key, String defValue) {
        return sharedReadable.getString(key, defValue);
    }
    public String getBaseUrl (){
      return   getString(ConfigKeys.API_HOST.name(),"http://192.168.20.206:8080/yp-web/");
    }
    public void putString(String key, String value) {
        sharedWritable.putString(key, value);
        sharedWritable.apply();
    }

    public void putInt(String key, int value) {
        sharedWritable.putInt(key, value);
        sharedWritable.apply();
    }

    public void putBoolean(String key, boolean value) {
        sharedWritable.putBoolean(key, value);
        sharedWritable.apply();
    }

    public int getInt(String key, int def) {
        return sharedReadable.getInt(key, def);
    }

    public boolean getBoolean(String key, boolean def) {
        return sharedReadable.getBoolean(key, def);
    }


    public Theme getCurrentTheme() {
        return Theme.valueOf(getString("app_theme", BuildConfig.THEME));
    }

    public void setCurrentTheme(Theme currentTheme) {
        putString("app_theme", currentTheme.name());
    }

    public boolean isUseCustomTheme() {
        return getBoolean(IS_USE_CUSTOM_THEME_KEY, false);
    }

    public void setIsUseCustomTheme(boolean isUseCustomTheme) {
        putBoolean(IS_USE_CUSTOM_THEME_KEY, isUseCustomTheme);
    }

}
