package com.chcit.mobile.app.utils;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.DecimalFormat;
import java.text.Format;
import java.util.Calendar;
import java.util.Date;

public class TimeDialogUtil {
    public interface OnShowListerner{
        void onShowListerner(String s);
    }

    /**
     *
     * @param themeResId 日期样式
     * @param showListerner 回调函数
     * @param i 前几天（例如当 i=1 时表示当前日期的前一天）
     */
    public static void showDatePickerDialog(Activity activity, int themeResId, OnShowListerner showListerner, int i, Calendar calendar) {
        // 直接创建一个DatePickerDialog对话框实例，并将它显示出来

        new DatePickerDialog(activity
                ,  themeResId
                // 绑定监听器(How the parent is notified that the date is set.)
                , (view, year, monthOfYear, dayOfMonth) -> {
                    // 此处得到选择的时间，可以进行你想要的操作
                    Format f = new DecimalFormat("00");
                    showListerner.onShowListerner( (year + "-" + f.format(monthOfYear+1) + "-" + f.format(dayOfMonth) ));
                    calendar.set(Calendar.YEAR,year);
                    calendar.set(Calendar.MONTH,monthOfYear);
                    calendar.set(Calendar.DAY_OF_MONTH,dayOfMonth);
                }
                // 设置初始日期
                , calendar.get(Calendar.YEAR)
                ,calendar.get(Calendar.MONTH)
                ,calendar.get(Calendar.DAY_OF_MONTH)-i).show();
    }
    public static void showDatePickerDialog(Activity activity, int themeResId, OnShowListerner showListerner, int i) {
        // 直接创建一个DatePickerDialog对话框实例，并将它显示出来
        showDatePickerDialog(activity,themeResId,showListerner,i,Calendar.getInstance());
    }
    public static void showTimePickerDialog(AppCompatActivity activity, final TextView tv, Calendar calendar) {
// Calendar c = Calendar.getInstance();
        // 创建一个TimePickerDialog实例，并把它显示出来
        // 解释一哈，Activity是context的子类
        new TimePickerDialog( activity,
                // 绑定监听器
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view,
                                          int hourOfDay, int minute) {
                        tv.setText((hourOfDay + "-" + minute));
                    }
                }
                // 设置初始时间
                , calendar.get(Calendar.HOUR_OF_DAY)
                , calendar.get(Calendar.MINUTE)
                // true表示采用24小时制
                ,true).show();
    }

    /**
     * 根据提供的年月日获取该月份的第一天
     * @Since: 2017-1-9下午2:26:57
     * @param year
     * @param monthOfYear
     * @return
     */
    public static String getSupportBeginDayofMonth(int year, int monthOfYear) {
       return getSupportBeginDayofMonth(year,monthOfYear,1);
    }
    public static String getSupportBeginDayofMonth(int year, int monthOfYear, int i) {
        Calendar cal = Calendar.getInstance();
        // 不加下面2行，就是取当前时间前一个月的第一天及最后一天
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, monthOfYear);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);

        cal.add(Calendar.DAY_OF_MONTH, i*-1);
        Date lastDate = cal.getTime();

        cal.set(Calendar.DAY_OF_MONTH, 1);
        // Date firstDate = cal.getTime();
        Format f = new DecimalFormat("00");

        return (year + "-" + f.format(monthOfYear+1)
                + "-" + 1 );
    }
    /**
     * 获取i月前的日期
     * @param i
     * @return
     */
    public static String getBeforeMonth(int i) {
        Calendar cal = Calendar.getInstance();
        // 不加下面2行，就是取当前时间前一个月的第一天及最后一天
        cal.add(Calendar.MONTH, i*-1);
        Format f = new DecimalFormat("00");
        return (cal.get(Calendar.YEAR) + "-" + f.format(cal.get(Calendar.MONTH)+1)
                + "-" + f.format(cal.get(Calendar.DAY_OF_MONTH)));
    }

    /**
     * 获取i周前的日期
     * @param i
     * @return
     */
    public static String getBeforeDay(int i) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, i*-7);
        Format f = new DecimalFormat("00");
        return (cal.get(Calendar.YEAR) + "-" + f.format(cal.get(Calendar.MONTH)+1)
                + "-" + f.format(cal.get(Calendar.DAY_OF_MONTH)));
    }
    public static String formatDate(){
        Calendar cal = Calendar.getInstance();
        Format f = new DecimalFormat("00");
        return (cal.get(Calendar.YEAR) + "-" + f.format(cal.get(Calendar.MONTH)+1)
                + "-" + f.format(cal.get(Calendar.DAY_OF_MONTH)));
    }
    /**
     * @Description: 根据提供的年月获取该月份的最后一天
     * @Author: gyz
     * @Since: 2017-1-9下午2:29:38
     * @param year
     * @param monthOfYear
     * @return
     */
    public static String getSupportEndDayofMonth(int year, int monthOfYear) {
        return getEndMonth(year,monthOfYear,1);
    }

    public static String getSupportEndBeforeMonth(int year, int monthOfYear) {

        return getEndMonth(year,monthOfYear,2);

    }

    public  static String getEndMonth(int year, int monthOfYear, int i){
        Calendar cal = Calendar.getInstance();
        // 不加下面2行，就是取当前时间前一个月的第一天及最后一天
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, monthOfYear);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.add(Calendar.MONTH, -1*i);
        cal.add(Calendar.DAY_OF_MONTH, -1);
        // Date lastDate = cal.getTime();

        cal.set(Calendar.DAY_OF_MONTH, 1);
        // Date firstDate = cal.getTime();
        Format f = new DecimalFormat("00");
        return   (year + "-" + f.format(monthOfYear+1)
                + "-" + f.format(cal.get(Calendar.DAY_OF_MONTH)) );

    }
}
