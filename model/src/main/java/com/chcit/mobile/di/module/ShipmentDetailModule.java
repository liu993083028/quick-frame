package com.chcit.mobile.di.module;

import com.chcit.mobile.mvp.common.contract.CommonContract;
import com.chcit.mobile.mvp.common.model.CommonModel;
import com.jess.arms.di.scope.FragmentScope;

import dagger.Module;
import dagger.Provides;

import com.chcit.mobile.mvp.shipment.contract.ShipmentDetailContract;
import com.chcit.mobile.mvp.shipment.model.ShipmentDetailModel;

import javax.inject.Named;


@Module
public class ShipmentDetailModule {
    private ShipmentDetailContract.View view;

    /**
     * 构建ShipmentDetailModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public ShipmentDetailModule(ShipmentDetailContract.View view) {
        this.view = view;
    }

    @FragmentScope
    @Provides
    ShipmentDetailContract.View provideShipmentDetailView() {
        return this.view;
    }

    @FragmentScope
    @Provides
    ShipmentDetailContract.Model provideShipmentDetailModel(ShipmentDetailModel model) {
        return model;
    }


}