package com.chcit.mobile.di.module;

import com.chcit.mobile.mvp.common.contract.CommonContract;
import com.chcit.mobile.mvp.common.contract.TaskContract;
import com.chcit.mobile.mvp.common.model.CommonModel;
import com.jess.arms.di.scope.ActivityScope;
import com.jess.arms.di.scope.FragmentScope;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;


@Module
public class CommonModule {


    /**
     * 构建TaskModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     */
    public CommonModule() {

    }
    @FragmentScope
    @Provides
    CommonContract.Model provideCommonModel(CommonModel model) {
        return model;
    }
}