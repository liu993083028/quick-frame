package com.chcit.mobile.di.module;

import com.jess.arms.di.scope.ActivityScope;

import dagger.Module;
import dagger.Provides;

import com.chcit.mobile.mvp.common.contract.LoginContract;
import com.chcit.mobile.mvp.common.model.LoginModel;


@Module
public class LoginModule {
    private LoginContract.View view;
    private LoginContract.FragmentView fragmentView;
    /**
     * 构建LoginModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public LoginModule(LoginContract.View view) {
        this.view = view;
    }
    public LoginModule(LoginContract.FragmentView view) {
        this.fragmentView = view;
    }


    @ActivityScope
    @Provides
    LoginContract.View provideLoginView() {
        return this.view;
    }

    @ActivityScope
    @Provides
    LoginContract.FragmentView provideFragmentView() {
        return this.fragmentView;
    }
    @ActivityScope
    @Provides
    LoginContract.Model provideLoginModel(LoginModel model) {
        return model;
    }
}