package com.chcit.mobile.di.component;

import dagger.Component;

import com.chcit.mobile.mvp.shipment.fragment.DispensingFragment;
import com.chcit.mobile.mvp.shipment.fragment.ScanCardFragment;
import com.jess.arms.di.component.AppComponent;

import com.chcit.mobile.di.module.DispensingModule;

import com.jess.arms.di.scope.FragmentScope;

@FragmentScope
@Component(modules = DispensingModule.class, dependencies = AppComponent.class)
public interface DispensingComponent {
    void inject(DispensingFragment fragment);

    void inject(ScanCardFragment fragment);
}