package com.chcit.mobile.di.component;

import dagger.Component;

import com.chcit.mobile.di.module.CommonModule;
import com.chcit.mobile.mvp.common.ui.fragment.LoginFragment;
import com.jess.arms.di.component.AppComponent;

import com.chcit.mobile.di.module.LoginModule;

import com.jess.arms.di.scope.ActivityScope;

import com.chcit.mobile.mvp.LoginActivity;

@ActivityScope
@Component(modules = {LoginModule.class}, dependencies = AppComponent.class)
public interface LoginComponent {

    void inject(LoginFragment fragment);

    void inject(LoginActivity activity);

}