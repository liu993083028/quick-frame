package com.chcit.mobile.di.module;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chcit.mobile.mvp.entity.ShipmentBean;
import com.chcit.mobile.mvp.shipment.adapter.ShipmentAdapter;
import com.jess.arms.di.scope.FragmentScope;

import dagger.Module;
import dagger.Provides;

import com.chcit.mobile.mvp.shipment.contract.ShipmentContract;
import com.chcit.mobile.mvp.shipment.model.ShipmentModel;

import java.util.ArrayList;
import java.util.List;


@Module
public class ShipmentModule {
    private ShipmentContract.View view;

    /**
     * 构建ShipmentModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public ShipmentModule(ShipmentContract.View view) {
        this.view = view;
    }

    @FragmentScope
    @Provides
    ShipmentContract.View provideShipmentView() {
        return this.view;
    }

    @FragmentScope
    @Provides
    ShipmentContract.Model provideShipmentModel(ShipmentModel model) {
        return model;
    }

    @FragmentScope
    @Provides
    RecyclerView.LayoutManager provideLayoutManager() {
        return new LinearLayoutManager(view.getContext());
    }

    @FragmentScope
    @Provides
    List<ShipmentBean> provideShipmentList(){
        return new ArrayList<>();
    }

    @FragmentScope
    @Provides
    ShipmentAdapter provideShipmentAdapter(List<ShipmentBean> shipmentBeans){
        return new ShipmentAdapter(shipmentBeans);
    }



}