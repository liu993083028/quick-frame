package com.chcit.mobile.di.component;

import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.chcit.mobile.di.module.ServerModule;

import com.jess.arms.di.scope.FragmentScope;
import com.chcit.mobile.mvp.common.ui.fragment.ServerFragment;

@FragmentScope
@Component(modules = ServerModule.class, dependencies = AppComponent.class)
public interface ServerComponent {
    void inject(ServerFragment fragment);
}