package com.chcit.mobile.di.component;

import dagger.Component;

import com.chcit.mobile.di.module.CommonModule;
import com.jess.arms.di.component.AppComponent;

import com.chcit.mobile.di.module.InventoryModule;

import com.jess.arms.di.scope.FragmentScope;
import com.chcit.mobile.mvp.inventory.fragment.move.InventoryFragment;

@FragmentScope
@Component(modules = {InventoryModule.class,CommonModule.class}, dependencies = AppComponent.class)
public interface InventoryComponent {
    void inject(InventoryFragment fragment);
}