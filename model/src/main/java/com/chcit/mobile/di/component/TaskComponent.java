package com.chcit.mobile.di.component;

import dagger.BindsInstance;
import dagger.Component;

import com.chcit.mobile.app.base.ScanFragment;
import com.chcit.mobile.di.module.CommonModule;
import com.chcit.mobile.di.module.DispensingModule;
import com.chcit.mobile.mvp.common.contract.TaskContract;
import com.chcit.mobile.mvp.hight.contract.HightCostBindContract;
import com.chcit.mobile.mvp.hight.di.component.HightCostBindComponent;
import com.chcit.mobile.mvp.shipment.fragment.ScanCardFragment;
import com.chcit.mobile.mvp.shipment.model.DispensingModel;
import com.jess.arms.di.component.AppComponent;

import com.chcit.mobile.di.module.TaskModule;

import com.jess.arms.di.scope.ActivityScope;
import com.chcit.mobile.mvp.TaskActivity;
import com.jess.arms.di.scope.FragmentScope;

@ActivityScope
@Component(modules ={ TaskModule.class}, dependencies = AppComponent.class)
public interface TaskComponent {
    void inject(TaskActivity activity);

    @Component.Builder
    interface Builder {
        @BindsInstance
        TaskComponent.Builder view(TaskContract.View view);

        TaskComponent.Builder appComponent(AppComponent appComponent);

        TaskComponent build();
    }
}