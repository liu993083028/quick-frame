package com.chcit.mobile.di.module;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chcit.mobile.mvp.common.contract.CommonContract;
import com.chcit.mobile.mvp.common.model.CommonModel;
import com.chcit.mobile.mvp.entity.ReceiveBean;
import com.chcit.mobile.mvp.receive.adpter.ReceiveAdapter;
import com.jess.arms.di.scope.FragmentScope;
import dagger.Module;
import dagger.Provides;
import com.chcit.mobile.mvp.receive.contract.ReceiveContract;
import com.chcit.mobile.mvp.receive.model.ReceiveModel;
import java.util.ArrayList;
import java.util.List;


@Module
public class ReceiveModule {
    private ReceiveContract.View view;

    /**
     * 构建ReceiveModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public ReceiveModule(ReceiveContract.View view) {
        this.view = view;
    }

    @FragmentScope
    @Provides
    ReceiveContract.View provideReceiveView() {
        return this.view;
    }

    @FragmentScope
    @Provides
    ReceiveContract.Model provideReceiveModel(ReceiveModel model) {
        return model;
    }


    @FragmentScope
    @Provides
    RecyclerView.LayoutManager provideLayoutManager() {
        return new LinearLayoutManager(view.getActivity());
    }
    @FragmentScope
    @Provides
    List<ReceiveBean> provideReceiveList() {
        return new ArrayList<>();
    }

    @FragmentScope
    @Provides
    ReceiveAdapter provideReceiveAdapter(List<ReceiveBean> list){
        return new ReceiveAdapter(list);
    }

    @FragmentScope
    @Provides
    CommonContract.Model provideCommonModel(CommonModel model) {
        return model;
    }



}