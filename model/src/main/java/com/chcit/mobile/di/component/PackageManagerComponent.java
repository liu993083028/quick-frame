package com.chcit.mobile.di.component;

import dagger.Component;

import com.chcit.mobile.di.module.CommonModule;
import com.jess.arms.di.component.AppComponent;

import com.chcit.mobile.di.module.PackageManagerModule;

import com.jess.arms.di.scope.FragmentScope;
import com.chcit.mobile.mvp.packmanager.fragment.PackageManagerFragment;

@FragmentScope
@Component(modules = {PackageManagerModule.class,CommonModule.class}, dependencies = AppComponent.class)
public interface PackageManagerComponent {
    void inject(PackageManagerFragment fragment);
}