package com.chcit.mobile.di.module;

import com.chcit.mobile.mvp.common.contract.CommonContract;
import com.chcit.mobile.mvp.common.model.CommonModel;
import com.jess.arms.di.scope.FragmentScope;

import dagger.Module;
import dagger.Provides;

import com.chcit.mobile.mvp.inventory.contract.InventoryPlanHandleContract;
import com.chcit.mobile.mvp.inventory.model.InventoryPlanHandleModel;


@Module
public class InventoryPlanHandleModule {
    private InventoryPlanHandleContract.View view;

    /**
     * 构建InventoryPlanHandleModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public InventoryPlanHandleModule(InventoryPlanHandleContract.View view) {
        this.view = view;
    }

    @FragmentScope
    @Provides
    InventoryPlanHandleContract.View provideInventoryPlanHandleView() {
        return this.view;
    }

    @FragmentScope
    @Provides
    InventoryPlanHandleContract.Model provideInventoryPlanHandleModel(InventoryPlanHandleModel model) {
        return model;
    }


}