package com.chcit.mobile.di.component;

import dagger.Component;

import com.chcit.mobile.di.module.CommonModule;
import com.jess.arms.di.component.AppComponent;

import com.chcit.mobile.di.module.MaintainModule;

import com.jess.arms.di.scope.FragmentScope;
import com.chcit.mobile.mvp.common.ui.fragment.MaintainFragment;

@FragmentScope
@Component(modules = {MaintainModule.class,CommonModule.class}, dependencies = AppComponent.class)
public interface MaintainComponent {
    void inject(MaintainFragment fragment);
}