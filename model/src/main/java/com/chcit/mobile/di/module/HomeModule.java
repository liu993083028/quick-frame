package com.chcit.mobile.di.module;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.chcit.mobile.R;
import com.chcit.mobile.mvp.common.contract.HomeContract;
import com.chcit.mobile.mvp.entity.HomeItem;
import com.chcit.mobile.mvp.home.HomeAdapter;
import com.jess.arms.di.scope.ActivityScope;
import dagger.Module;
import dagger.Provides;
import com.chcit.mobile.mvp.common.model.HomeModel;

import java.util.ArrayList;
import java.util.List;


@Module
public class HomeModule {
    private HomeContract.View view;

    /**
     * 构建HomeActivityModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public HomeModule(HomeContract.View view) {
        this.view = view;
    }

    @ActivityScope
    @Provides
    HomeContract.View provideHomeView() {
        return this.view;
    }

    @ActivityScope
    @Provides
    HomeContract.Model provideHomeModel(HomeModel model) {
        return model;
    }
    @ActivityScope
    @Provides
    RecyclerView.LayoutManager provideLayoutManager() {
        return new GridLayoutManager(view.getActivity(), 3);
    }
    @ActivityScope
    @Provides
    List<HomeItem> provideUserList() {
        return new ArrayList<>();
    }

    @ActivityScope
    @Provides
    HomeAdapter provideUserAdapter(List<HomeItem> list){
        return new HomeAdapter(R.layout.home_item_view,list);
    }
}