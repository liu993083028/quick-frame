package com.chcit.mobile.di.module;

import com.chcit.mobile.mvp.common.contract.CommonContract;
import com.chcit.mobile.mvp.common.model.CommonModel;
import com.chcit.mobile.mvp.common.model.ScanPackageModel;
import com.chcit.mobile.mvp.hight.contract.HightCostContract;
import com.chcit.mobile.mvp.hight.model.HightCostModel;
import com.jess.arms.di.scope.FragmentScope;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

import com.chcit.mobile.mvp.common.contract.ScanPackageContract;

import javax.inject.Named;

//@Module
//public abstract class HightCostModule {
//
//    @Binds
//    abstract HightCostContract.Model bindHightCostModel(HightCostModel model);
//}
@Module
public abstract class ScanPackageModule {


    @Binds
    abstract ScanPackageContract.Model provideReceiveScanPackageModel(ScanPackageModel model);


}