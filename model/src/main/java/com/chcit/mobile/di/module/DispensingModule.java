package com.chcit.mobile.di.module;

import com.jess.arms.di.scope.FragmentScope;

import dagger.Module;
import dagger.Provides;

import com.chcit.mobile.mvp.shipment.contract.DispensingContract;
import com.chcit.mobile.mvp.shipment.model.DispensingModel;


@Module
public class DispensingModule {
    private DispensingContract.View view;

    /**
     * 构建DispensingModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public DispensingModule(DispensingContract.View view) {
        this.view = view;
    }

    @FragmentScope
    @Provides
    DispensingContract.View provideDispensingView() {
        return this.view;
    }

    @FragmentScope
    @Provides
    DispensingContract.Model provideDispensingModel(DispensingModel model) {
        return model;
    }
}