package com.chcit.mobile.di.module;

import com.jess.arms.di.scope.FragmentScope;

import dagger.Module;
import dagger.Provides;

import com.chcit.mobile.mvp.inventory.contract.InventoryPlanContract;
import com.chcit.mobile.mvp.inventory.model.InventoryPlanModel;


@Module
public class InventoryPlanModule {
    private InventoryPlanContract.View view;

    /**
     * 构建InventoryPlanModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public InventoryPlanModule(InventoryPlanContract.View view) {
        this.view = view;
    }

    @FragmentScope
    @Provides
    InventoryPlanContract.View provideInventoryPlanView() {
        return this.view;
    }

    @FragmentScope
    @Provides
    InventoryPlanContract.Model provideInventoryPlanModel(InventoryPlanModel model) {
        return model;
    }
}