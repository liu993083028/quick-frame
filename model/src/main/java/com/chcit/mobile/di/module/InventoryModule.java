package com.chcit.mobile.di.module;

import com.chcit.mobile.mvp.inventory.adapter.InventoryAdapter;
import com.jess.arms.di.scope.FragmentScope;

import dagger.Module;
import dagger.Provides;

import com.chcit.mobile.mvp.inventory.contract.InventoryContract;
import com.chcit.mobile.mvp.inventory.model.InventoryModel;


@Module
public class InventoryModule {
    private InventoryContract.View view;
    private InventoryAdapter adapter;

    /**
     * 构建InventoryModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public InventoryModule(InventoryContract.View view, InventoryAdapter adapter) {
        this.view = view;
        this.adapter = adapter;
    }

    @FragmentScope
    @Provides
    InventoryContract.View provideInventoryView() {
        return this.view;
    }

    @FragmentScope
    @Provides
    InventoryContract.Model provideInventoryModel(InventoryModel model) {
        return model;
    }



    @FragmentScope
    @Provides
    InventoryAdapter provideInventoryAdapter(){
        return adapter;
    }
}