package com.chcit.mobile.di.module;

import com.chcit.mobile.mvp.common.contract.CommonContract;
import com.chcit.mobile.mvp.common.model.CommonModel;
import com.jess.arms.di.scope.FragmentScope;

import dagger.Module;
import dagger.Provides;

import com.chcit.mobile.mvp.common.contract.MaintainContract;
import com.chcit.mobile.mvp.common.model.MaintainModel;

import javax.inject.Named;


@Module
public class MaintainModule {
    private MaintainContract.View view;

    /**
     * 构建MaintainModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public MaintainModule(MaintainContract.View view) {
        this.view = view;
    }

    @FragmentScope
    @Provides
    MaintainContract.View provideMaintainView() {
        return this.view;
    }

    @FragmentScope
    @Provides
    MaintainContract.Model provideMaintainModel(MaintainModel model) {
        return model;
    }



}