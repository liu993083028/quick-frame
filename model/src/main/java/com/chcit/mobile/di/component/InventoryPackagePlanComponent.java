package com.chcit.mobile.di.component;

import com.chcit.mobile.di.module.CommonModule;
import com.chcit.mobile.di.module.InventoryPackagePlanModule;
import com.chcit.mobile.mvp.inventory.fragment.pandian.InventorySecondFragment;
import com.jess.arms.di.component.AppComponent;
import com.jess.arms.di.scope.FragmentScope;

import dagger.Component;

@FragmentScope
@Component(modules = {InventoryPackagePlanModule.class,CommonModule.class}, dependencies = AppComponent.class)
public interface InventoryPackagePlanComponent {
    void inject(InventorySecondFragment fragment);
}