package com.chcit.mobile.di.component;

import dagger.Component;

import com.chcit.mobile.di.module.CommonModule;
import com.jess.arms.di.component.AppComponent;

import com.chcit.mobile.di.module.InventoryPlanQueryModule;

import com.jess.arms.di.scope.FragmentScope;
import com.chcit.mobile.mvp.inventory.fragment.pandian.InventoryFirstFragment;

@FragmentScope
@Component(modules = {InventoryPlanQueryModule.class,CommonModule.class}, dependencies = AppComponent.class)
public interface InventoryPlanQueryComponent {
    void inject(InventoryFirstFragment fragment);
}