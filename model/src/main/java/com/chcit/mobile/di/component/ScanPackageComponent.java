package com.chcit.mobile.di.component;

import dagger.BindsInstance;
import dagger.Component;

import com.chcit.mobile.di.module.CommonModule;
import com.chcit.mobile.mvp.common.contract.ScanPackageContract;
import com.chcit.mobile.mvp.receive.fragment.pack.PackageReceiveFragment;
import com.chcit.mobile.mvp.receive.fragment.ReceiveBindSearchDetailFragment;
import com.chcit.mobile.mvp.receive.fragment.pack.child.FirstPagerFragment;
import com.chcit.mobile.mvp.receive.fragment.pack.child.SecondPagerFragment;
import com.chcit.mobile.mvp.shipment.fragment.ShipmentScanPackageFragment;
import com.jess.arms.di.component.AppComponent;

import com.chcit.mobile.di.module.ScanPackageModule;

import com.jess.arms.di.scope.FragmentScope;
import com.chcit.mobile.mvp.receive.fragment.ReceiveScanPackageFragment;

@FragmentScope
@Component(modules = {ScanPackageModule.class,CommonModule.class}, dependencies = AppComponent.class)
public interface ScanPackageComponent {
    void inject(ReceiveScanPackageFragment fragment);

    void inject(ShipmentScanPackageFragment fragment);

    void inject(ReceiveBindSearchDetailFragment fragment);

    void inject(PackageReceiveFragment fragment);

    void inject(FirstPagerFragment fragment);

    void inject(SecondPagerFragment fragment);

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder view(ScanPackageContract.View view);

        Builder appComponent(AppComponent appComponent);

        ScanPackageComponent build();
    }
}