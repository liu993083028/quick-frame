package com.chcit.mobile.di.component;

import dagger.Component;

import com.chcit.mobile.di.module.CommonModule;
import com.chcit.mobile.mvp.inventory.fragment.pandian.InventoryPackagePlanHandleFragment;
import com.jess.arms.di.component.AppComponent;

import com.chcit.mobile.di.module.InventoryPlanHandleModule;

import com.jess.arms.di.scope.FragmentScope;
import com.chcit.mobile.mvp.inventory.fragment.pandian.InventoryPlanHandleFragment;

@FragmentScope
@Component(modules = {InventoryPlanHandleModule.class,CommonModule.class}, dependencies = AppComponent.class)
public interface InventoryPlanHandleComponent {
    void inject(InventoryPlanHandleFragment fragment);

    void inject(InventoryPackagePlanHandleFragment fragment);
}