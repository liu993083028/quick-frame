package com.chcit.mobile.di.component;

import dagger.Component;

import com.chcit.mobile.di.module.CommonModule;
import com.jess.arms.di.component.AppComponent;

import com.chcit.mobile.di.module.InventoryDetailModule;

import com.jess.arms.di.scope.FragmentScope;
import com.chcit.mobile.mvp.inventory.fragment.move.InventoryDetailFragment;

@FragmentScope
@Component(modules = {InventoryDetailModule.class,CommonModule.class}, dependencies = AppComponent.class)
public interface InventoryDetailComponent {
    void inject(InventoryDetailFragment fragment);
}