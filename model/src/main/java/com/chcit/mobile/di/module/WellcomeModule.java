package com.chcit.mobile.di.module;

import com.jess.arms.di.scope.ActivityScope;

import dagger.Module;
import dagger.Provides;

import com.chcit.mobile.mvp.common.contract.WellcomeContract;
import com.chcit.mobile.mvp.common.model.WellcomeModel;


@Module
public class WellcomeModule {
    private WellcomeContract.View view;

    /**
     * 构建WellcomeModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public WellcomeModule(WellcomeContract.View view) {
        this.view = view;
    }

    @ActivityScope
    @Provides
    WellcomeContract.View provideWellcomeView() {
        return this.view;
    }

    @ActivityScope
    @Provides
    WellcomeContract.Model provideWellcomeModel(WellcomeModel model) {
        return model;
    }
}