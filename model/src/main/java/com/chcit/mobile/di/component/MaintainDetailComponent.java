package com.chcit.mobile.di.component;

import dagger.Component;

import com.chcit.mobile.di.module.CommonModule;
import com.jess.arms.di.component.AppComponent;

import com.chcit.mobile.di.module.MaintainDetailModule;

import com.jess.arms.di.scope.FragmentScope;
import com.chcit.mobile.mvp.common.ui.fragment.MaintainDetailFragment;

@FragmentScope
@Component(modules = {MaintainDetailModule.class,CommonModule.class}, dependencies = AppComponent.class)
public interface MaintainDetailComponent {
    void inject(MaintainDetailFragment fragment);
}