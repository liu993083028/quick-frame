package com.chcit.mobile.di.module;

import com.jess.arms.di.scope.FragmentScope;

import dagger.Module;
import dagger.Provides;

import com.chcit.mobile.mvp.common.contract.ServerContract;
import com.chcit.mobile.mvp.common.model.ServerModel;


@Module
public class ServerModule {
    private ServerContract.View view;

    /**
     * 构建ServerModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public ServerModule(ServerContract.View view) {
        this.view = view;
    }

    @FragmentScope
    @Provides
    ServerContract.View provideServerView() {
        return this.view;
    }

    @FragmentScope
    @Provides
    ServerContract.Model provideServerModel(ServerModel model) {
        return model;
    }
}