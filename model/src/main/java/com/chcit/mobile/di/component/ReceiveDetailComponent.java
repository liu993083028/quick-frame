package com.chcit.mobile.di.component;

import dagger.Component;

import com.chcit.mobile.di.module.CommonModule;
import com.chcit.mobile.mvp.receive.fragment.ReceiveDetailFragment;
import com.chcit.mobile.mvp.receive.fragment.ReceiveOldDetailFragment;
import com.jess.arms.di.component.AppComponent;

import com.chcit.mobile.di.module.ReceiveDetailModule;

import com.jess.arms.di.scope.FragmentScope;


@FragmentScope
@Component(modules = {ReceiveDetailModule.class,CommonModule.class}, dependencies = AppComponent.class)
public interface
ReceiveDetailComponent {
    void inject(ReceiveDetailFragment fragment);
    void inject(ReceiveOldDetailFragment fragment);
}