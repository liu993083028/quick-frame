package com.chcit.mobile.di.component;

import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.chcit.mobile.di.module.ShipmentModule;

import com.jess.arms.di.scope.FragmentScope;
import com.chcit.mobile.mvp.shipment.fragment.ShipmentFragment;

@FragmentScope
@Component(modules = ShipmentModule.class, dependencies = AppComponent.class)
public interface ShipmentComponent {
    void inject(ShipmentFragment fragment);
}