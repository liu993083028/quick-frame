package com.chcit.mobile.di.module;

import com.chcit.mobile.mvp.common.contract.CommonContract;
import com.chcit.mobile.mvp.common.model.CommonModel;
import com.chcit.mobile.mvp.inventory.adapter.InventoryPackagePlanAdapter;
import com.chcit.mobile.mvp.inventory.contract.InventoryPlanContract;
import com.chcit.mobile.mvp.inventory.entity.InventoryPackageBean;
import com.chcit.mobile.mvp.inventory.model.InventoryPlanModel;
import com.jess.arms.di.scope.FragmentScope;
import java.util.ArrayList;
import java.util.List;
import dagger.Module;
import dagger.Provides;
@Module
public class InventoryPackagePlanModule {
    private InventoryPlanContract.View view;

    /**
     * 构建InventoryPlanModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public InventoryPackagePlanModule(InventoryPlanContract.View view) {
        this.view = view;
    }

    @FragmentScope
    @Provides
    InventoryPlanContract.View provideInventoryPackagePlanView() {
        return this.view;
    }

    @FragmentScope
    @Provides
    InventoryPlanContract.Model provideInventoryPlanModel(InventoryPlanModel model) {
        return model;
    }


    @FragmentScope
    @Provides
    List<InventoryPackageBean> provideArrayList(){
        return new ArrayList<>();
    }

    @FragmentScope
    @Provides
    InventoryPackagePlanAdapter provideInventoryPackagePlanAdapter(List<InventoryPackageBean> list){
        return new InventoryPackagePlanAdapter(list);
    }



}