package com.chcit.mobile.di.module;

import com.chcit.mobile.mvp.common.contract.CommonContract;
import com.chcit.mobile.mvp.common.model.CommonModel;
import com.jess.arms.di.scope.FragmentScope;

import dagger.Module;
import dagger.Provides;

import com.chcit.mobile.mvp.packmanager.contract.PackageManagerContract;
import com.chcit.mobile.mvp.packmanager.model.PackageManagerModel;


@Module
public class PackageManagerModule {
    private PackageManagerContract.View view;

    /**
     * 构建PackageManagerModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public PackageManagerModule(PackageManagerContract.View view) {
        this.view = view;
    }

    @FragmentScope
    @Provides
    PackageManagerContract.View providePackageManagerView() {
        return this.view;
    }

    @FragmentScope
    @Provides
    PackageManagerContract.Model providePackageManagerModel(PackageManagerModel model) {
        return model;
    }


}