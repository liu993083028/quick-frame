package com.chcit.mobile.di.component;

import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.chcit.mobile.di.module.HomeModule;

import com.jess.arms.di.scope.ActivityScope;
import com.chcit.mobile.mvp.home.HomeActivity;

@ActivityScope
@Component(modules = HomeModule.class, dependencies = AppComponent.class)
public interface HomeComponent {
    void inject(HomeActivity activity);
}