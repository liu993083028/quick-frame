package com.chcit.mobile.di.component;

import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.chcit.mobile.di.module.WellcomeModule;

import com.jess.arms.di.scope.ActivityScope;
import com.chcit.mobile.mvp.WellcomeActivity;

@ActivityScope
@Component(modules = WellcomeModule.class, dependencies = AppComponent.class)
public interface WellcomeComponent {
    void inject(WellcomeActivity activity);
}