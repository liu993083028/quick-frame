package com.chcit.mobile.di.module;

import com.chcit.mobile.mvp.common.contract.CommonContract;
import com.chcit.mobile.mvp.common.model.CommonModel;
import com.jess.arms.di.scope.FragmentScope;

import dagger.Module;
import dagger.Provides;

import com.chcit.mobile.mvp.inventory.contract.InventoryDetailContract;
import com.chcit.mobile.mvp.inventory.model.InventoryDetailModel;


@Module
public class InventoryDetailModule {
    private InventoryDetailContract.View view;

    /**
     * 构建InventoryDetailModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public InventoryDetailModule(InventoryDetailContract.View view) {
        this.view = view;
    }

    @FragmentScope
    @Provides
    InventoryDetailContract.View provideInventoryDetailView() {
        return this.view;
    }






}