package com.chcit.mobile.di.component;

import dagger.Component;

import com.chcit.mobile.di.module.CommonModule;
import com.chcit.mobile.mvp.shipment.fragment.ShipmentOldDetailFragment;
import com.jess.arms.di.component.AppComponent;

import com.chcit.mobile.di.module.ShipmentDetailModule;

import com.jess.arms.di.scope.FragmentScope;
import com.chcit.mobile.mvp.shipment.fragment.ShipmentDetailFragment;

@FragmentScope
@Component(modules = {ShipmentDetailModule.class,CommonModule.class}, dependencies = AppComponent.class)
public interface ShipmentDetailComponent {
    void inject(ShipmentDetailFragment fragment);

    void inject(ShipmentOldDetailFragment fragment);
}