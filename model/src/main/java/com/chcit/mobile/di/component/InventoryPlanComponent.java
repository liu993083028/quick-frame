package com.chcit.mobile.di.component;

import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.chcit.mobile.di.module.InventoryPlanModule;

import com.jess.arms.di.scope.FragmentScope;
import com.chcit.mobile.mvp.inventory.fragment.pandian.InventoryPlanFragment;

@FragmentScope
@Component(modules = InventoryPlanModule.class, dependencies = AppComponent.class)
public interface InventoryPlanComponent {
    void inject(InventoryPlanFragment fragment);
}