package com.chcit.mobile.di.component;

import dagger.Component;

import com.chcit.mobile.mvp.receive.fragment.OldReceiveFragment;
import com.chcit.mobile.mvp.receive.fragment.ReceiveBindSearchFragment;
import com.chcit.mobile.mvp.receive.fragment.pack.child.ImagePickFragment;
import com.jess.arms.di.component.AppComponent;

import com.chcit.mobile.di.module.ReceiveModule;

import com.jess.arms.di.scope.FragmentScope;

@FragmentScope
@Component(modules = {ReceiveModule.class}, dependencies = AppComponent.class)
public interface ReceiveComponent {
    void inject(OldReceiveFragment fragment);

    void inject(ReceiveBindSearchFragment fragment);

    void inject(ImagePickFragment fragment);
}