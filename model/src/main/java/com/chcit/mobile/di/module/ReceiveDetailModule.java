package com.chcit.mobile.di.module;

import com.chcit.mobile.mvp.common.contract.CommonContract;
import com.chcit.mobile.mvp.common.model.CommonModel;
import com.chcit.mobile.mvp.entity.Locator;
import com.chcit.mobile.mvp.entity.Reason;
import com.jess.arms.di.scope.FragmentScope;

import dagger.Module;
import dagger.Provides;

import com.chcit.mobile.mvp.receive.contract.ReceiveDetailContract;
import com.chcit.mobile.mvp.receive.model.ReceiveDetailModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;


@Module
public class ReceiveDetailModule {
    private ReceiveDetailContract.View view;

    /**
     * 构建ReceiveDetailModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public ReceiveDetailModule(ReceiveDetailContract.View view) {
        this.view = view;
    }

    @FragmentScope
    @Provides
    ReceiveDetailContract.View provideReceiveDetailView() {
        return this.view;
    }

    @FragmentScope
    @Provides
    ReceiveDetailContract.Model provideReceiveDetailModel(ReceiveDetailModel model) {
        return model;
    }
    @FragmentScope
    @Provides
    List<Locator> provideLocators(){
        return  new ArrayList<>();
    }

    @FragmentScope
    @Provides
    List<Reason> provideReasons(){
        List<Reason> reasons = new ArrayList<>();
        reasons.add(new Reason("无"));
        return  reasons;
    }



}