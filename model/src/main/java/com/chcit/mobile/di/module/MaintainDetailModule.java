package com.chcit.mobile.di.module;

import com.chcit.mobile.mvp.common.contract.CommonContract;
import com.chcit.mobile.mvp.common.model.CommonModel;
import com.jess.arms.di.scope.FragmentScope;

import dagger.Module;
import dagger.Provides;

import com.chcit.mobile.mvp.common.contract.MaintainDetailContract;
import com.chcit.mobile.mvp.common.model.MaintainDetailModel;

import javax.inject.Named;


@Module
public class MaintainDetailModule {
    private MaintainDetailContract.View view;

    /**
     * 构建MaintainDetailModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public MaintainDetailModule(MaintainDetailContract.View view) {
        this.view = view;
    }

    @FragmentScope
    @Provides
    MaintainDetailContract.View provideMaintainDetailView() {
        return this.view;
    }

    @FragmentScope
    @Provides
    MaintainDetailContract.Model provideMaintainDetailModel(MaintainDetailModel model) {
        return model;
    }



}