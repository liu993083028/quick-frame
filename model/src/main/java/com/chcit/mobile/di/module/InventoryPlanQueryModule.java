package com.chcit.mobile.di.module;


import com.chcit.mobile.mvp.inventory.adapter.InventoryPlanQueryAdapter;
import com.chcit.mobile.mvp.inventory.entity.InventoryBean;
import com.jess.arms.di.scope.FragmentScope;
import dagger.Module;
import dagger.Provides;
import com.chcit.mobile.mvp.inventory.contract.InventoryPlanQueryContract;
import com.chcit.mobile.mvp.inventory.model.InventoryPlanQueryModel;
import java.util.ArrayList;
import java.util.List;


@Module
public class InventoryPlanQueryModule {
    private InventoryPlanQueryContract.View view;

    /**
     * 构建InventoryPlanQueryModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public InventoryPlanQueryModule(InventoryPlanQueryContract.View view) {
        this.view = view;
    }

    @FragmentScope
    @Provides
    InventoryPlanQueryContract.View provideInventoryPlanQueryView() {
        return this.view;
    }

    @FragmentScope
    @Provides
    InventoryPlanQueryContract.Model provideInventoryPlanQueryModel(InventoryPlanQueryModel model) {
        return model;
    }



    @FragmentScope
    @Provides
    List<InventoryBean> provideArrayList(){
        return new ArrayList<>();
    }

    @FragmentScope
    @Provides
    InventoryPlanQueryAdapter provideInventoryPlanQueryAdapter(List<InventoryBean> list){
        return new InventoryPlanQueryAdapter(list);
    }

}