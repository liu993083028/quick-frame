package com.chcit.mobile.di.module;



import dagger.Binds;
import dagger.Module;
import com.chcit.mobile.mvp.common.contract.TaskContract;
import com.chcit.mobile.mvp.common.model.TaskModel;


@Module
public abstract class TaskModule {
    @Binds
    abstract TaskContract.Model bindTaskModel(TaskModel model);

}