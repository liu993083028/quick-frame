package com.chcit.spd;

import android.util.SparseArray;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.chcit.mobile.app.utils.FTPUtils;
import com.chcit.mobile.mvp.inventory.model.api.PackageQueyInput;
import com.chcit.mobile.mvp.receive.Entity.AsnStatuEnum;
import com.chcit.mobile.mvp.shipment.api.ShipmentInput;
import com.google.gson.Gson;

import org.apache.commons.net.ftp.FTPClient;
import org.junit.Test;

import java.io.OutputStream;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        /**
         * 动态代理
         * 代理类在程序运行时创建的代理方式
         * 1.jdk动态代理
         * 2.CGLIB代理
         */
//        Man man = new Man();
//        Proxy proxy = new Proxy(man);
//        //
//        Subject subject = (Subject)java.lang.reflect.Proxy
//                .newProxyInstance(man.getClass().getClassLoader(),man.getClass().getInterfaces(),proxy);
//        subject.shopping();
//        System.out.println();subject.getClass().getName();
//        JSONObject jsonoBject = JSONObject.parseObject("<h1>dfkdkfj</h1>");
   /*     List<WareHose> list =new ArrayList<>();
        WareHose wareHose = new WareHose();
        wareHose.setName("西药库");
        list.add(wareHose);
        Object o = list.get(0);
        System.out.print(o.toString());*/
      //  Calendar cal = Calendar.getInstance();
        // 不加下面2行，就是取当前时间前一个月的第一天及最后一天

      /*  cal.add(Calendar.MONTH, 0);
        Date lastDate = cal.getTime();

       // cal.set(Calendar.DAY_OF_MONTH, 1);
        // Date firstDate = cal.getTime();
        Format f = new DecimalFormat("00");

        System.out.print (cal.get(Calendar.YEAR) + "-" + f.format(cal.get(Calendar.MONTH)+1)
                + "-" + cal.get(Calendar.DAY_OF_MONTH) );*/
       // assertEquals(4, 2 + 2);
//        SparseArray<Object> stringSparseArray = new SparseArray<>();
//        stringSparseArray.put(1,"dsfjkdfjs");
//        System.out.print (stringSparseArray.get(0));
        ShipmentInput shipmentInput = new ShipmentInput();
        PackageQueyInput packageQueyInput = new PackageQueyInput();
        shipmentInput.setAsnType("2312313");
        Gson gson = new Gson();
        System.out.println(JSONObject.toJSONString(shipmentInput));
//        packageQueyInput.setLot("23213213123211");
//        packageQueyInput.setDir("sdfewrwer");
//        // System.out.println(gson.toJson(shipmentInput));
//        System.out.println(packageQueyInput.toString());
    }

    @Test
    public void testMap(){
       List<String> list =Arrays.asList("(11)1039012930193");

       System.out.println(JSONArray.toJSONString(list));
       JSONArray jsonArray = new JSONArray();
       jsonArray.add("(87)23312312213");
        System.out.println(jsonArray.toJSONString());
    }

    @Test
    public void testDate(){
        String knights = "KK32137237";
        String s2 = "Ksfjskfjksj";
        String regex = "^[K][K].*";
        //通过pattern类的静态函数matchs去判定字符串是否满足这样一个条件
        System.out.println(Pattern.matches(regex, knights));
        System.out.println(Pattern.matches(regex, s2));
    }

    @Test
    public void testFtpUrl(){
        String regex = "^(\\S{4})(\\d{14})";
        Pattern p=Pattern.compile(regex);
        Matcher m=p.matcher("(11)1039012930192132133");
        if(m.find()){
            System.out.println(m.group(0));

        }
//        String ftpUrl = "ftp://administrator:1@192.168.20.211:21/GLYY/AD_Attachment/1000038.jpeg";
//        FTPUtils ftpUtils = FTPUtils.getInstance();
//        ftpUtils.initFTP(ftpUrl);
//        OutputStream os = ftpUtils.getFileInputStream(ftpUrl);
//        System.out.println(os.toString());
//        ftpUtils.closeFTPConnect();

//        String pattent = "/(https?:\\/\\/|ftps?:\\/\\/)?((\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3})(:[0-9]+)?|(localhost)(:[0-9]+)?|([\\w]+\\.)(\\S+)(\\w{2,4})(:[0-9]+)?)(\\/?([\\w#!:.?+=&%@!\\-\\/]+))?/ig;";
//        String pat1 = "ftp://([-A-Za-z0-9+&@#/%?=~_|!:,.;])+[-A-Za-z0-9+&@#/%=~_|]";
//        String pat2 = "^ftp:\\/\\/(([1-9]|([1-9]\\d)|(1\\d\\d)|(2([0-4]\\d|5[0-5])))\\.)((d|([1-9]\\d)|(1\\d\\d)|(2([0-4]\\d|5[0-5])))\\.){2}([1-9]|([1-9]\\d)|(1\\d\\d)|(2([0-4]\\d|5[0-5]))$)|^ftp:\\/\\/([a-zA-Z0-9_-])+:([a-zA-Z0-9_-])+@(([1-9]|([1-9]\\d)|(1\\d\\d)|(2([0-4]\\d|5[0-5])))\\.)((d|([1-9]\\d)|(1\\d\\d)|(2([0-4]\\d|5[0-5])))\\.){2}([1-9]|([1-9]\\d)|(1\\d\\d)|(2([0-4]\\d|5[0-5]))$)";
//       String pat3 ="^ftp://(\\S+)([@])(\\S+)[:](\\d+)([/]\\S+)$";
//        String pat4 = "^ftp://(\\S+)([@])(\\S+)[:](\\d+)[/](\\S+)[/](\\S+)[.]";
//        Pattern p=Pattern.compile(pat3);
//        Matcher m=p.matcher(ftpUrl);
//        while(m.find()){
//
//            String addr = m.group(3);
//            int port =   Integer.valueOf(m.group(4));
//            String[] userNameAndPasswords =  m.group(1).split(":");
//            System.out.println(m.group(5));
//
//        }
//
//        System.out.println("-----------------结束-----------------------");
    }

    @Test
    public void testEnum(){
//        SparseArray<String> stringSparseArray = new SparseArray<>();
//        stringSparseArray.put(0,"31221");
//        AsnStatuEnum asnReject = AsnStatuEnum.REJECT;
//        System.out.println(asnReject.name());
        String str ="01,,32312,2231,,,,";

        int count = 0;
        String content = str;
        int startIndex = 0;
        int endIndex = 0;
        for(int i = 0; i < str.length(); i++){
            char c = str.charAt(i);
            if(c == ','){
                ++count;
                if(count == 3){
                    startIndex = i+1;
                }else if(count == 4){
                    endIndex =i;
                }
            }

        }
        System.out.println(str.substring(startIndex,endIndex));
        System.out.println(Arrays.toString(str.split(",")));


    }
}