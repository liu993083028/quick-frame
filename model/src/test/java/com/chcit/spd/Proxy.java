package com.chcit.spd;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * 代理类
 * jdk动态代理
 */
public class Proxy implements InvocationHandler{

    private Object target;//要代理的真实对象
    public Proxy(Subject target){
        this.target = target;
    }

    /**
     *
     * @param proxy 真实对象
     * @param method 需要代理的方法
     * @param args 真实对象的参数
     * @return 返回参数
     * @throws Throwable
     */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("proxy:"+proxy.getClass().getName());
        System.out.println("before...");
        method.invoke(target,args);
        System.out.println("after....");
        return null;
    }
}
