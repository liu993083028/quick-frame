package com.chcit.spd;

/**
 * 共同的接口
 */
public interface Subject {
    void shopping();
}
