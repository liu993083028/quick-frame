package com.chcit.spd;

import com.chcit.mobile.mvp.entity.BaseResponse;
import com.chcit.mobile.mvp.receive.api.ReceiveApi;

import org.junit.Test;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class UploadTest {
    @Test
    public void testUpload(){

//        repositoryManager.obtainRetrofitService(ReceiveApi.class)
//                .uploadImg("", Arrays.asList("/uplate"))
//                .subscribeOn(Schedulers.io());
        List<String> media = Arrays.asList("C:\\Users\\Administrator\\Pictures\\Saved Pictures\\OATH2认证流程.png");

        // 参数初始化....
        Map<String, RequestBody> mapParams = new HashMap<>();
        RequestBody traceBody = RequestBody.create(MediaType.parse("multipart/form-data"), "123123123123");
        mapParams.put("asnId", traceBody);
        MultipartBody.Part[] parts = new MultipartBody.Part[1];
        int cnt = 0;
        for (String m : media) {
            File file = new File(m);
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            MultipartBody.Part filePart = MultipartBody.Part.createFormData("images[]", file.getName(), requestFile);
            parts[cnt] = filePart;
            cnt++;
        }

//        provideRetrofit().create(ReceiveApi.class)
//                .uploadImg(mapParams,parts)
//                .subscribeOn(Schedulers.io())
//                .subscribe(new Observer<BaseResponse>(){
//                    @Override
//                    public void onSubscribe(Disposable d) {
//
//                    }
//
//                    @Override
//                    public void onNext(BaseResponse baseResponse) {
//                        System.out.println(baseResponse.getMsg());
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        System.out.println(e.getMessage());
//                    }
//
//                    @Override
//                    public void onComplete() {
//
//                    }
//                });
    }



    public static Retrofit provideRetrofit() {
        OkHttpClient okhttpClient = new OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .client(okhttpClient)
                .baseUrl("http://localhost:8888/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();


        return retrofit;
    }
}
